===================
Subarray subpackage
===================

.. automodule:: ska_low_mccs.subarray


.. toctree::

  Subarray component manager<subarray_component_manager>
  Subarray device<subarray_device>
  Subarray health model<subarray_health_model>
  Subarray health rules<subarray_health_rules>
