
===================
Station Beam Device
===================

.. uml:: station_beam_device_class_diagram.uml

.. automodule:: ska_low_mccs.station_beam.station_beam_device
   :members:
