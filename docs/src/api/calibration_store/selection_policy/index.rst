================
Selection Policy
================

The SelectionManager is the owner of SelectionPolicies and will use 
them to execute sql queries to the database. 

The SelectionManager has a 1-1 relationship with the CalibrationStore, 
new configurable policies can be injected into the manager from
the tango interface using :doc:`UpdateSelectionPolicy <../calibration_store_device>` 


.. currentmodule:: ska_low_mccs.calibration_store.selection_policy

.. autosummary::
  .. toctree::
    :caption: API
    :maxdepth: 1

    SelectionManager
    SelectPreferred
    SelectClosestInRange

.. automodule:: ska_low_mccs.calibration_store.selection_policy


