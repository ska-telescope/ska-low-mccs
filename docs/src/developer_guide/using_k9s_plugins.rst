#########################
Using k9s Plugins
#########################

A set of plugins to interact with Tango devices from k9s are provided in `ska-low-mccs/scripts/k9s/plugin.yml`.
There is an instructions document there on using them.
In short, the `plugin.yml` file should be copied to `$HOME/.config/k9s/` to use the plugins.