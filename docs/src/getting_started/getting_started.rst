===============
Getting started
===============

This page contains instructions for software developers who want to get
started with using and developing on the SKA Low MCCS subsystem.

Background
----------
A great deal of information on how the SKA Software development
community works is available at the `SKA software developer portal`_.
There you will find guidelines, policies, standards and a range of other
documentation.

Set up your development environment
-----------------------------------
To set up a basic development environment, follow the instructions at the
page below. This will let you download
and edit the code, and run unit tests and lightweight integration tests.
Two setup options are presented; for MCCS development, you are highly
recommended to take the Docker option.

* :doc:`setup_development_environment`

Set up Visual Studio code
-------------------------
It is always a good idea to develop in an IDE. If you followed the
Docker option in setting up your development environment, consider
setting up Visual Studio code with remote container integration.
Instructions are at the following page.

* :doc:`setup_vscode`

Set up a deployment environment
-------------------------------
Want to deploy a real MCCS device cluster? Follow the instructions in

* :doc:`setup_deployment_environment`


Using a deploy MCCS cluster
---------------------------
Now that you have an MCCS device cluster, what kinds of things can you
do with it?

* :doc:`use_mccs`


.. _SKA software developer portal: https://developer.skatelescope.org/

