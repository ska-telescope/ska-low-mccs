================================
MCCS LMC Prototype documentation
================================

This project is developing the Local Monitoring and Control (LMC)
prototype for the `Square Kilometre Array`_.

.. _Square Kilometre Array: https://skatelescope.org/

.. toctree::
   :maxdepth: 1
   :caption: Getting started

   getting_started/getting_started
   getting_started/setup_development_environment
   getting_started/setup_vscode
   getting_started/setup_deployment_environment
   getting_started/use_mccs

.. toctree::
   :maxdepth: 1
   :caption: Developer guide

   developer_guide/device_model
   developer_guide/using_telmodel
   developer_guide/using_k9s_plugins
   developer_guide/code_quality

.. toctree::
   :maxdepth: 1
   :caption: Reference material

   reference/class_diagrams.rst
   reference/pointing.rst
   reference/configuration.rst
   reference/calibration/index.rst
   reference/power/index.rst

.. toctree::
   :maxdepth: 1
   :caption: API

   api/index

.. toctree::
   :maxdepth: 1
   :caption: Schemas

   schemas/index

.. toctree::
   :maxdepth: 1
   :caption: Devices

   devices/index
   
Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

