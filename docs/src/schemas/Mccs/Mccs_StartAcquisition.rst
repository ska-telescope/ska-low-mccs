=======================
StartAcquisition schema
=======================

Schema for StartAcquisition command

**********
Properties
**********

* **start_time** (string, format: time): .

* **delay** (integer): An acquisition start delay.

