============
Mccs Schemas
============

These schemas are for use with Mccs commands

.. toctree::
  :caption: Mccs Schemas
  :maxdepth: 2

  Mccs_Scan_3_0
  Mccs_StartAcquisition
