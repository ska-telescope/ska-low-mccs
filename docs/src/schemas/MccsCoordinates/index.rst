=======================
MccsCoordinates Schemas
=======================

These schemas are for use with MccsCoordinates commands

.. toctree::
  :caption: MccsCoordinates Schemas
  :maxdepth: 2

  MccsCoordinates_3_0
