================================
Station GetPointingDelays schema
================================

Schema for Station's GetPointingDelays command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **pointing_type** (string): The type of pointing requested. Must be one of: ["alt_az", "ra_dec"].

* **pointing_time** (string, format: date-time): Time of observation (in format astropy time).

* **scan_time** (number): the total scan time in seconds.

* **values** (object): The values provided for either the right ascension and declination or the alitude and azimuth.

  **One of**
    * object

      * **altitude** (number, **required**): the altitude of the target.

      * **azimuth** (number, **required**): the azimuth of the target.

      * **altitude_rate** (number): the rate of altitude of the target.

      * **azimuth_rate** (number): the rate of  azimuth of the target.

    * object

      * **right_ascension** (number, **required**): the right_ascension of the target.

      * **declination** (number, **required**): the declination of the target.

      * **right_ascension_rate** (number): the rate of right_ascension of the target.

      * **declination_rate** (number): the rate of declination of the target.

