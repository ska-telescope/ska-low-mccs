=====================================
Station SetPointingCoordinates schema
=====================================

Schema for MccsStation's SetPointingCoordinates command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **subarray_id** (integer): The ID of the subarray which commands pointing. Minimum: 1. Maximum: 16.

* **beam_id** (integer): The ID of the hardware beam to point. Minimum: 0. Maximum: 47.

* **update_rate** (number): Minimum: 0.0.

* **sky_coordinates** (object)

  * **timestamp** (string): UTC time for begin of drift.

  * **reference_frame** (string, **required**): Must be one of: ["AltAz", "topocentric", "ICRS", "Galactic"].

  * **target_name** (string): The name of the target.

  * **c1** (number, **required**): first coordinate, in degrees. Minimum: 0.0. Maximum: 360.0.

  * **c1_rate** (number): Drift rate for first coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

  * **c2** (number, **required**): second coordinate, in degrees. Minimum: -90.0. Maximum: 90.0.

  * **c2_rate** (number): Drift rate for second coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

* **field** (object)

  * **timestamp** (string): UTC time for begin of drift.

  * **target_name** (string, **required**): The name of the target.

  * **reference_frame** (string, **required**): Must be one of: ["AltAz", "topocentric", "ICRS", "Galactic"].

  * **attrs** (object, **required**): Coordinates and rates of scan.

    * **c1** (number, **required**): first coordinate, in degrees. Minimum: 0.0. Maximum: 360.0.

    * **c1_rate** (number): Drift rate for first coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

    * **c2** (number, **required**): second coordinate, in degrees. Minimum: -90.0. Maximum: 90.0.

    * **c2_rate** (number): Drift rate for second coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

