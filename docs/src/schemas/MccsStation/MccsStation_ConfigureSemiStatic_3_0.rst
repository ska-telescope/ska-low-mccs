=============================================
MccsStation Configure Semi static data schema
=============================================

Used to configure the semi static data of the station i.e. (x, y offset)

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **station_config** (object): Station semi static data.

  * **refLongitude** (number)

  * **refLatitude** (number)

  * **refHeight** (number)

  * **StationId** (number)

* **antenna_config** (object): Antenna semi static data.

* **field_station_config** (object): Field station semi static data.

