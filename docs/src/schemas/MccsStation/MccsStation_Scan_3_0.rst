=======================
MccsStation Scan schema
=======================

Schema for MccsSubarray's Scan command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **subarray_id** (integer): Subarray ID for the scan. Minimum: 0. Maximum: 16.

* **scan_id** (integer): Unique ID for the scan.

* **start_time** (string): UTC time for begin of drift.

* **duration** (number): The scan duration in seconds. 0.0 or omitted means forever. Minimum: 0.0. Maximum: 86400.0.

