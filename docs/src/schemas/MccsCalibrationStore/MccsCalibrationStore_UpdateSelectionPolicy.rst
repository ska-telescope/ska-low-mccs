=================================================
MccsCalibrationStore UpdateSelectionPolicy schema
=================================================

Schema for MccsCalibrationStore's UpdateSelectionPolicy command

**********
Properties
**********

* **policy_name** (string): The name of the policy. Please see details in ReadTheDocs https://developer.skao.int/projects/ska-low-mccs/en/latest/api/calibration_store/selection_policy/index.html. Must be one of: ["preferred", "closest_in_range"].

* **frequency_tolerance** (integer): The absolute tolerance on the frequency.

* **temperature_tolerance** (number): The absolute tolerance on the temperature.

