====================
MccsSubarray Schemas
====================

These schemas are for use with MccsSubarray commands

.. toctree::
  :caption: MccsSubarray Schemas
  :maxdepth: 2

  MccsSubarray_AssignResources_3_0
  MccsSubarray_Configure_3_0
