=======
Schemas
=======

These schemas are auto-generated as part of our release process. See ska_low_mccs/src/schemas for source code.

.. toctree::
  :caption: Device Schemas
  :maxdepth: 2

  MCCS<Mccs/index>
  MccsCoordinates<MccsCoordinates/index>
  Antenna<MccsAntenna/index>
  Controller<MccsController/index>
  Station<MccsStation/index>
  Station beam<MccsStationBeam/index>
  Subarray<MccsSubarray/index>
  Subarray beam<MccsSubarrayBeam/index>
  Calibration store<MccsCalibrationStore/index>
  Station Calibrator<MccsStationCalibrator/index>    