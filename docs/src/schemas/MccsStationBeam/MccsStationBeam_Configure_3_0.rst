============================
StationBeam Configure schema
============================

Schema for StationBeam's Configure command

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **update_rate** (number): Minimum: 0.0.

* **logical_bands** (array): Length must be at most 48.

  * **Items** (object)

    * **start_channel** (integer, **required**): Minimum: 2. Maximum: 504. Must be a multiple of: 2.

    * **number_of_channels** (integer, **required**): Minimum: 8. Maximum: 384. Must be a multiple of: 8.

* **weighting_key_ref** (string): descriptive ID for the aperture weights in the aperture database.

* **sky_coordinates** (object)

  * **timestamp** (string): UTC time for begin of drift.

  * **reference_frame** (string, **required**): Must be one of: ["AltAz", "topocentric", "ICRS", "Galactic"].

  * **target_name** (string): The name of the target.

  * **c1** (number, **required**): first coordinate, in degrees. Minimum: 0.0. Maximum: 360.0.

  * **c1_rate** (number): Drift rate for first coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

  * **c2** (number, **required**): second coordinate, in degrees. Minimum: -90.0. Maximum: 90.0.

  * **c2_rate** (number): Drift rate for second coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

* **field** (object)

  * **timestamp** (string): UTC time for begin of drift.

  * **target_name** (string, **required**): The name of the target.

  * **reference_frame** (string, **required**): Must be one of: ["AltAz", "topocentric", "ICRS", "Galactic"].

  * **attrs** (object, **required**): Coordinates and rates of scan.

    * **c1** (number, **required**): first coordinate, in degrees. Minimum: 0.0. Maximum: 360.0.

    * **c1_rate** (number): Drift rate for first coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

    * **c2** (number, **required**): second coordinate, in degrees. Minimum: -90.0. Maximum: 90.0.

    * **c2_rate** (number): Drift rate for second coordinate, in degrees/second. Minimum: -0.016. Maximum: 0.016.

