=====================================
MccsController strict Allocate schema
=====================================

**********
Properties
**********

* **interface** (string): The schema version which you expect to run against. Must match pattern ``/^https?://.+/.+/[0-9]+.[0-9]+$/``.

* **subarray_id** (integer): Minimum: 16. Maximum: 16.

* **stations** (array): Length must be equal to 512.

  * **Items** (string)

