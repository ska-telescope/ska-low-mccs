#############################
MCCS Calibration Architecture
#############################

The diagram below attempts to describes the calibration architecture.


.. figure:: ../images/station-calibration-architecture-v0-17-0.svg
  :alt: Calibration architecture

  Calibration architecture as currently implemented 0.17.0

* **observational_context**: Currently consists of station_id and channel
* **solutions**: Currently gain solutions.
* **observational_name**: Currently the path of the hdf5 file dumped
* **field_conditions**: Currently the outsideTemperature as reported by ``FieldStation``



************************
Application of Solutions
************************

The calibration solutions are applied to each allocated station during the `Subarray`` ``Configure`` command.
If no solution is found a unit calibration will be applied as a default.

Below is the current sequence diagram:

.. uml:: ../uml/ApplyCalibration.uml


********************
Storing of Solutions
********************

The ``MccsStationCalibrator`` is responsible for orchestrating the retrieval and storage of calibration
solutions.

See sequence diagram for an overview of the internals:

.. uml:: ../uml/StoreCalibration.uml


