MCCS Calibration (Internals)
============================

This document provides internal details for MCCS subsystem operators and complements the API documentation.

For high-level information, refer to the `Architecture <../architecture/index.html>`_.

SelectionPolicy
---------------
The ``MccsCalibrationStore`` uses a ``SelectionPolicy`` to define the SQL queries for retrieving data from the database.
this is configurable (see https://developer.skao.int/projects/ska-low-mccs/en/0.22.0/api/calibration_store/selection_policy/index.html)

Manual Storage
--------------
The ``MccsCalibrationStore`` device interfaces with the stored calibration products. Use the ``StoreSolution`` method to store a calibration solution.

Manual Acquisition
------------------
Calibration data can be acquired manually by interfacing with the Tile and DAQ. Alternatively, the ``AcquireDataForCalibration`` method in ``SpsStation`` automates DAQ and Tile configuration for data collection.

Applying a solution
-------------------

As of version 0.22.0, it is believed that the calibration solutions do not vary as much
as originally anticipated in the architecture. As a result, a feature flag, `loadSolutionInConfigure`,
has been introduced.

Setting this property in the deployment configuration will modify its default behavior.
If the attribute is defined after deployment, it will override the default setting. By default,
this property is set to `True`, meaning the calibration solutions will be loaded and applied
during the configure command (as it was before). 
However, it is believed that setting this flag to `False` may
be an appropriate option given the consistency of solutions.

When the flag is set to `False`, the system will no longer attempt to load or apply the
calibration solutions during configuration. Instead, the `MccsStation On` command will trigger
the loading and application of the calibration solutions (after synchronisation), 
ensuring that this is only done once.
note, added to station is `LoadCalibrationCoefficients` and `ApplyCalibration` allowing for 
modifications during runtime (please note, the CalibrationStore SelectionPolicy is still in
charge of the solution loaded and applied). Work in THORN-29 is in discusstion about how a notion
of `calibration_id` can be communicated with TMC.

Currently (0.22.0), MCCS does not internally have an effective way to measure the quality 
of the calibration solutions. Therefore, it is up to operations to determine whether the 
applied solutions are suitable.

Calibration Loop
----------------
The ``MccsStationCalibrator`` manages the retrieval and storage of calibration solutions.

For an overview of the internals, see the sequence diagram below:

.. uml:: ../uml/StoreCalibration.uml