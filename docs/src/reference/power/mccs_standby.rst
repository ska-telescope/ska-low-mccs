MCCS in the STANDBY State
=========================

The diagram below indicates an MCCS which is in STANDBY.

Note: This is planned, but not currently implemented.

In the FieldStation:
  - **MccsSmartboxes** are STANDBY.
  - **MccsFNDH** is always ON.
  - **FieldStation** is reporting STANDBY.

In the SpsStation:
  - **MccsTiles** are OFF.
  - **MccsSubracks** are ON.
  - **SpsStation** is reporting STANDBY.


.. uml:: mccs_standby.puml