Off Command
===========

Overview
--------

This document details the Off() command within the MCCS, 
covering the three primary repositories in MCCS:
SPSHW, PaSD, and the Main repository.

**Note**: This command is current not implemented in **SpsStation**
as we do not have control of the upstream power of the subracks. As such
**MccsStation** currently calls **SpsStation.Standby()**.

SPSHW Repository
----------------

Devices
~~~~~~~

- **MccsTile**: Controls the TPM (Tile Processing Module). For more
  detailed information, see `MccsTile Overview
  <https://developer.skao.int/projects/ska-low-mccs-spshw/en/latest/
  reference/tile_brief_overview.html>`_.

- **MccsSubrack**: Manages the subrack containing TPMs. Note: Power control
  for the subrack is currently unimplemented due to the absence of PDU
  devices, which are planned for future development.

- **SpsStation**: The central controller for the ``MccsTile`` and
  ``MccsSubrack`` devices.

Power Management Workflow
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **SpsStation.Off() Command**:

   - The ``SpsStation`` sends an `Off()` command to the ``MccsTile``
     devices.
   - The ``MccsTile`` devices then turn off.
   - The ``SpsStation`` sends an `Off()` command to the ``MccsSubrack`` devices.
   - The ``MccsSubrack`` devices then turn off.

2. **MccsTile.Off() Command**:

   - The ``MccsTile`` sends a request to the ``MccsSubrack`` to turn off
     power upon receiving the `Off()` command.

3. **MccsSubrack.Off() Command**:

   - The ``MccsSubrack`` currently lacks power control due to the absence
     of PDU devices.

Future Development
~~~~~~~~~~~~~~~~~~

- **PDU Integration**: The ``MccsSubrack`` will be updated to include power
  control once the PDU devices are developed and integrated.

PaSD Repository
---------------

Devices
~~~~~~~

- **MccsPasdBus**: A Modbus device that communicates with PaSD hardware,
  including FNDH (Field Node Distribution Hub) and Smartboxes.

- **MccsFNDH**: Manages the FNDH, which provides power to connected
  Smartboxes.

- **MccsSmartbox**: Controls a Smartbox, which distributes power to
  connected antennas.

- **FieldStation**: The main controller for the ``MccsFNDH`` and
  ``MccsSmartbox`` devices.

Power Management Workflow
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **FieldStation.Off() Command**:

   - The ``FieldStation`` sends a `PowerOffPorts()` command to the
     ``MccsFNDH``.
   - The ``MccsFNDH`` then powers on the relevant set of ports (if not
     already powered off), thereby powering off the Smartboxes.

2. **MccsSmartbox.Off() Command**:

   - The ``MccsSmartbox`` commands the ``MccsPasdBus`` to power off the FNDH port it is
     attached to, if not already powered off.

3. **MccsFNDH.Off() Command**:

   - The ``MccsFNDH`` is considered to be always ON as we do not have control of upstream power supply.

Main Repository
---------------

Devices
~~~~~~~

- **MccsStation**: The top-level controller for both ``SpsStation`` and
  ``FieldStation``.

- **MccsController**: Manages multiple ``MccsStation`` devices, each
  responsible for a different station.

Power Management Workflow
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **MccsStation.Off() Command**:

   - The ``MccsStation`` sends the `Off()` command to the ``SpsStation``
     and ``FieldStation``.
   - It waits for the completion of these commands to ensure that all
     associated hardware components are powered off correctly.

2. **MccsController.Off() Command**:

   - The ``MccsController`` initiates the power-off process by sending the
     `Off()` command to each ``MccsStation``.

Command Flow
------------

The workflow above in diagrammatic form:

.. uml:: off_command.puml
