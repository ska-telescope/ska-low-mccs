MCCS in the ON state
====================

The diagram below indicates an MCCS which is ON.

Every device is ON, therefore MCCS is ON.

.. uml:: mccs_on.puml