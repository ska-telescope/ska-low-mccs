MCCS in the OFF State
=====================

The diagram below indicates an MCCS which is OFF.

In the FieldStation:
  - **MccsSmartboxes** are OFF.
  - **MccsFNDH** is always ON.
  - **FieldStation** is reporting OFF.

In the SpsStation:
  - **MccsTiles** are OFF.
  - **MccsSubracks** are ON.
  - **SpsStation** is reporting STANDBY.

As we don't have control over the PDUs, STANDBY is the lowest state 
the SpsStation can reach. Consequently, MccsStation treats this 
state as OFF for now.

.. uml:: mccs_off.puml
