Standby Command
===============

Overview
--------

This document details the Standby() command within the MCCS, 
covering the three primary repositories in MCCS:
SPSHW, PaSD, and the Main repository. This command transitions hardware
to a low-power state, where TPMs are OFF, subracks are ON, FNDH ports
are ON, but Smartbox ports are OFF. The STANDBY state must be
supported from both ON and OFF states. 

**Note**: Currently, this mode is
implemented in SPSHW but not in PaSD.

SPSHW Repository
----------------

Devices
~~~~~~~

- **MccsTile**: Controls the TPM (Tile Processing Module). For more
  detailed information, see `MccsTile Overview
  <https://developer.skao.int/projects/ska-low-mccs-spshw/en/latest/
  reference/tile_brief_overview.html>`_.

- **MccsSubrack**: Manages the subrack containing TPMs. Note: Power
  control for the subrack is currently unimplemented due to the absence
  of PDU devices, which are planned for future development.

- **SpsStation**: The central controller for the ``MccsTile`` and
  ``MccsSubrack`` devices.

Power Management Workflow
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **SpsStation.Standby() Command**:

   - The ``SpsStation`` sends an `On()` command to the ``MccsSubrack``
     devices.
   - (Once implemented) The ``MccsSubrack`` devices then turn on, if
     not already on.
   - The ``SpsStation`` sends an `Off()` command to the ``MccsTile``
     devices.
   - The ``MccsTile`` devices then turn off if not already off, which is
     a straightforward process without complex sequencing.

2. **MccsTile.Standby() Command**:

   - The ``MccsTile`` does not support a `Standby()` command; its state
     is managed by the ``SpsStation``.

3. **MccsSubrack.Standby() Command**:

   - The ``MccsSubrack`` does not support a `Standby()` command; its
     state is managed by the ``SpsStation``.

Future Development
~~~~~~~~~~~~~~~~~~

- **PDU Integration**: The ``MccsSubrack`` will be updated to include
  power control once the PDU devices are developed and integrated.

PaSD Repository
---------------

Devices
~~~~~~~

- **MccsPasdBus**: A Modbus device that communicates with PaSD hardware,
  including FNDH (Field Node Distribution Hub) and Smartboxes.

- **MccsFNDH**: Manages the FNDH, which distributes power to connected
  Smartboxes.

- **MccsSmartbox**: Controls a Smartbox, which distributes power to
  connected antennas.

- **FieldStation**: The main controller for the ``MccsFNDH`` and
  ``MccsSmartbox`` devices.

Power Management Workflow
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **FieldStation.Standby() Command**:

   - The ``FieldStation`` sends a `PowerOnPorts()` command to the
     ``MccsFNDH``.
   - The ``MccsFNDH`` then powers on the relevant set of ports (if not
     already powered on), thereby powering on the Smartboxes.
   - The ``FieldStation`` sends a `PowerOffPorts()` command to each
     ``MccsSmartbox``.
   - The ``MccsSmartbox`` then powers off the relevant set of ports (if
     not already powered off), thereby powering off the antennas.

2. **MccsSmartbox.Standby() Command**:

   - The ``MccsSmartbox`` commands the ``MccsPasdBus`` to power on the
     FNDH port it is attached to, then commands the ``MccsPasdBus`` to power off
     its own ports.

3. **MccsFNDH.Standby() Command**:

   - The ``MccsFNDH`` is considered to be always ON as we do
     not have control of upstream power supply.

Main Repository
---------------

Devices
~~~~~~~

- **MccsStation**: The top-level controller for both ``SpsStation`` and
  ``FieldStation``.

- **MccsController**: Manages multiple ``MccsStation`` devices, each
  responsible for a different station.

Power Management Workflow
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **MccsStation.Standby() Command**:

   - The ``MccsStation`` sends the `Standby()` command to the ``SpsStation``
     and ``FieldStation``.
   - It waits for the completion of these commands to ensure that all
     associated hardware components are powered to standby correctly.

2. **MccsController.Standby() Command**:

   - The ``MccsController`` initiates the standby process by sending the
     `Standby()` command to each ``MccsStation``.

Command Flow
------------

The workflow above in diagrammatic form:

.. uml:: standby_command.puml
