Example Configuration of PaSD devices
=====================================
.. toctree::
  :caption: MCCS device configuration
  :maxdepth: 1

  configServers
  fieldstations
  fndhs
  pasdconfiguration
  simulators
  smartboxes