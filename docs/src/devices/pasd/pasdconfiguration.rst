Pasdconfiguration
=================

Below is an example pasdconfiguration configuration.

.. code-block:: yaml

  ci-1:
    antennas:
        sb01-01:
            masked: false
            smartbox: sb01
            smartbox_port: 1
    pasd:
        smartboxes:
            sb01:
                fndh_port: 1
