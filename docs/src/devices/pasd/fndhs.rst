Fndhs
=====

Below is an example fndhs configuration.

.. code-block:: yaml

  ci-1:
    low-mccs/fndh/ci-1:
        logging_level_default: 5
        pasdbus_name: low-mccs/pasdbus/ci-1
