Solvers
=======

Below is an example solvers configuration.

.. code-block:: yaml

  ci-1:
    low-mccs/solver/ci-1:
        eep_root_path: /app/tests/unit/calibration_solver/testing_data
        logging_level_default: 5
        root_path: /app/tests/unit/calibration_solver/testing_data
