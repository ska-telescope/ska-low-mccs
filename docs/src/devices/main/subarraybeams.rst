Subarraybeams
=============

Below is an example subarraybeams configuration.

.. code-block:: yaml

  subarraybeam-01:
    low-mccs/subarraybeam/01:
        logging_level_default: 5
