Stationcalibrators
==================

Below is an example stationcalibrators configuration.

.. code-block:: yaml

  ci-1:
    low-mccs/stationcalibrator/ci-1:
        calibration_solver_trl: low-mccs/solver/solver-ci-1
        calibration_store_trl: low-mccs/calibrationstore/ci-1
        logging_level_default: 5
        station_trl: low-mccs/station/ci-1
