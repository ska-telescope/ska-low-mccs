Controllers
===========

Below is an example controllers configuration.

.. code-block:: yaml

  controller:
    low-mccs/control/control:
        stationbeams:
        - low-mccs/beam/ci-1-01
        - low-mccs/beam/ci-1-02
        stations:
        - low-mccs/station/ci-1
        - low-mccs/station/ci-2
        subarraybeams:
        - low-mccs/subarraybeam/01
        - low-mccs/subarraybeam/02
        subarrays:
        - low-mccs/subarray/01
