Stationbeams
============

Below is an example stationbeams configuration.

.. code-block:: yaml

  stationbeams-ci-1:
    low-mccs/beam/ci-1-01:
        beam_id: 1
