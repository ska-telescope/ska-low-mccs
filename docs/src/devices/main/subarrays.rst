Subarrays
=========

Below is an example subarrays configuration.

.. code-block:: yaml

  subarray-01:
    low-mccs/subarray/01:
        logging_level_default: 5
        skuid_url: http://ska-ser-skuid-ska-ser-skuid-svc:9870/
        subarray_id: 1
