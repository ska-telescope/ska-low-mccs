Example Configuration of SPSHW devices
======================================
.. toctree::
  :caption: MCCS device configuration
  :maxdepth: 1

  daqs
  simulators
  spsstations
  subracks
  tpms