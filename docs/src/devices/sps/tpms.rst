Tpms
====

Below is an example tpms configuration.

.. code-block:: yaml

  ci-1-tpm01:
    low-mccs/tile/ci-1-tpm01:
        host: 10.0.10.201
        logging_level_default: 5
        port: 10000
        simulation_config: 1
        station_id: 1
        subrack: ci-1-sr1
        subrack_slot: 1
        test_config: 1
        tile_id: 0
        version: tpm_v1_6
