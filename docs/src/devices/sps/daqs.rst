Daqs
====

Below is an example daqs configuration.

.. code-block:: yaml

  bandpass-daq-ci-1:
    low-mccs/bandpass-daq/ci-1:
        host: daqrx-ci-1-bandpass
        id: 1
        logging_level_default: 5
        port: 50051
        skuid_url: http://ska-ser-skuid-ska-ser-skuid-svc:9870/
