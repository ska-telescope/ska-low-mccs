Subracks
========

Below is an example subracks configuration.

.. code-block:: yaml

  ci-1-sr1:
    low-mccs/subrack/ci-1-sr1:
        logging_level_default: 5
        srmb_host: subrack-simulator-ci-1-sr1
        srmb_port: 8081
