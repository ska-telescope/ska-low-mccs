# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This script generates documentation for our devices."""

import os
from pathlib import Path
from typing import Any

import yaml


def fetch_key_from_nested_dict(
    data: dict[str, Any], target_key: str
) -> dict[str, Any] | None:
    """
    Recursively search for a key in a nested dictionary.

    :param data: The dictionary to search within.
    :param target_key: The key to search for.
    :returns: The value associated with the target_key, or None if not found.
    """
    if not isinstance(data, dict):
        return None

    if target_key in data:
        return data[target_key]

    for value in data.values():
        if isinstance(value, dict):
            result = fetch_key_from_nested_dict(value, target_key)
            if result is not None:
                return result

    return None


def dict_to_rst(device: str, device_config: dict) -> str:
    """
    Convert a dictionary entry into a human-readable reStructuredText (reST) format.

    :param device: The name of the device.
    :param device_config: The config of the device.
    :returns: The formatted string in reStructuredText format.
    """
    rst_lines = []

    # Add a title
    rst_lines.append(f"{device.capitalize()}")
    rst_lines.append("=" * len(device))
    rst_lines.append("")
    rst_lines.append(f"Below is an example {device} configuration.\n")
    rst_lines.append(".. code-block:: yaml\n")
    rst_lines.append("  " + yaml.dump(device_config, indent=4))
    return "\n".join(rst_lines)


def generate_index(devices: list[str]) -> str:
    """
    Generate the index for the devices.

    :param devices: A list of device names to include in the index.
    :returns: The formatted index as a string in reStructuredText format.
    """
    index_lines = []

    title = "Example Configuration of MCCS devices"
    index_lines.append(title)
    index_lines.append("=" * len(title))

    index_lines.append(
        "\nThis is an auto-generated document. "
        "It provides an example configuration for deploying each MCCS device, "
        "and an example full deployment of MCCS.\n"
    )

    index_lines.append(".. toctree::")
    index_lines.append("  :caption: MCCS device configuration")
    index_lines.append("  :maxdepth: 1\n")
    index_lines.append("  full_example")
    index_lines.append("  main/index")
    index_lines.append("  sps/index")
    index_lines.append("  pasd/index")
    return "\n".join(index_lines)


def generate_sub_index(name: str, devices: list[str]) -> str:
    """
    Generate the index for the devices.

    :param name: the name of the device subgroup.
    :param devices: A list of device names to include in the index.
    :returns: The formatted index as a string in reStructuredText format.
    """
    index_lines = []

    title = f"Example Configuration of {name} devices"
    index_lines.append(title)
    index_lines.append("=" * len(title))

    index_lines.append(".. toctree::")
    index_lines.append("  :caption: MCCS device configuration")
    index_lines.append("  :maxdepth: 1\n")
    for device in devices:
        index_lines.append(f"  {device}")
    return "\n".join(index_lines)


def generate_full_example(all_devices: list[dict[str, Any]]) -> str:
    """
    Generate the full example YAML configuration.

    :param all_devices: A dictionary containing all devices and their configurations.
    :returns: The formatted full example as a string in reStructuredText format.
    """
    example_lines = []
    title = "Example full deployment YAML of MCCS devices"
    example_lines.append(title)
    example_lines.append("=" * len(title))
    example_lines.append(
        "\nThis config is for reference only. "
        "\nIt is auto-generated so may not provide a useful deployment.\n"
    )
    example_lines.append(".. code-block:: yaml\n")
    # Generate the YAML output with yaml.dump
    yaml_content = yaml.dump(all_devices, indent=2)

    # Indent each line of the YAML content
    indented_yaml_content = "\n".join("  " + line for line in yaml_content.splitlines())

    # Append the indented YAML content to the example lines
    example_lines.append(indented_yaml_content)
    return "\n".join(example_lines)


def filter_first_entry_in_list(data: dict | Any) -> dict | Any:
    """
    Recursively filter the data to remove duplicate low level entries.

    :param data: data in which to remove duplicates.

    :return: reduced data
    """
    if isinstance(data, dict):
        for key, value in data.items():
            if isinstance(value, dict):
                for next_key, next_values in value.items():
                    if not isinstance(next_values, dict):
                        return {key: value}
            data[key] = filter_first_entry_in_list(value)
    return data


def reduce_list_length(data: Any, max_length: int = 2) -> dict | list:
    """
    Recursively reduce the length of any list properties.

    :param data: data in which to reduce the max list length.
    :param max_length: max length of a list attribute.

    :return: reduced data
    """
    if isinstance(data, dict):
        for key, value in data.items():
            data[key] = reduce_list_length(value, max_length)
    elif isinstance(data, list):
        if len(data) > max_length:
            return data[:max_length]
        return [reduce_list_length(item, max_length) for item in data]
    return data


def main() -> None:
    """
    Generate the auto-docs.

    This function loads the YAML file, processes each device type, and creates
    an RST file for each. It also generates an index and a full example file.
    """
    # Load the YAML file
    yaml_file = "helmfile.d/build/ska-low-mccs.yaml"
    with open(yaml_file, encoding="utf-8") as file:
        data = yaml.safe_load(file)

    output_dir = os.path.join(os.getcwd(), "docs/src/devices")

    # Ensure the output directory exists
    Path(output_dir).resolve().mkdir(exist_ok=True)
    Path(f"{output_dir}/main").resolve().mkdir(exist_ok=True)
    Path(f"{output_dir}/sps").resolve().mkdir(exist_ok=True)
    Path(f"{output_dir}/pasd").resolve().mkdir(exist_ok=True)

    # Transform the structure. Only take one copy of each device, then trim
    # any list properties to length 2 at max.
    full_sanitised_data_yaml = ""
    for key, value in data.items():
        transformed_data = reduce_list_length(filter_first_entry_in_list(value))
        full_sanitised_data_yaml += yaml.dump(({key: transformed_data}))

    full_sanitised_data = yaml.safe_load(full_sanitised_data_yaml)

    # Process each device type and create an RST file for each
    main_devices = sorted(
        [
            "antennas",
            "controllers",
            "calibrationstores",
            "solvers",
            "stationbeams",
            "stationcalibrators",
            "stations",
            "subarraybeams",
            "subarrays",
        ]
    )
    sps_devices = sorted(["spsstations", "daqs", "tpms", "subracks", "simulators"])
    pasd_devices = sorted(
        [
            "fieldstations",
            "smartboxes",
            "fndhs",
            "pasdconfiguration",
            "simulators",
            "configServers",
        ]
    )
    devices = main_devices + sps_devices + pasd_devices

    with open(os.path.join(output_dir, "index.rst"), "w", encoding="utf-8") as file:
        file.write(generate_index(devices))

    subgroups = [
        ("MCCS", main_devices, "main"),
        ("SPSHW", sps_devices, "sps"),
        ("PaSD", pasd_devices, "pasd"),
    ]

    for name, devices, subdir in subgroups:
        # Generate and write the index file for each subgroup
        index_content = generate_sub_index(name, devices)
        with open(
            os.path.join(output_dir, f"{subdir}/index.rst"), "w", encoding="utf-8"
        ) as file:
            file.write(index_content)

        # Generate and write the RST file for each device in the subgroup
        for device in devices:
            device_data = full_sanitised_data
            if subdir != "main":
                device_data = full_sanitised_data[f"ska-low-mccs-{name.lower()}"]

            device_config = fetch_key_from_nested_dict(device_data, device)
            if isinstance(device_config, dict):
                single_example = dict([next(iter(device_config.items()))])
                rst_content = dict_to_rst(device, single_example)
                with open(
                    os.path.join(output_dir, f"{subdir}/{device}.rst"),
                    "w",
                    encoding="utf-8",
                ) as file:
                    file.write(rst_content)

        with open(
            os.path.join(output_dir, "full_example.rst"), "w", encoding="utf-8"
        ) as file:
            file.write(generate_full_example(full_sanitised_data))


if __name__ == "__main__":
    main()
