# flake8: noqa
# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""THIS FILE IS FOR PI21 DEMO!! Likely has errors."""
from __future__ import annotations

from abc import abstractmethod
from typing import Any

import h5py
import numpy as np

# THIS FILE IS FOR PI21 DEMO!! It is NOT production code.
# These parsers have not been tested and may have issues.


class AntCount:
    """Specialised descriptor for antenna count."""

    def __get__(self: AntCount, obj: Any, objtype: Any = None) -> int:
        """
        Get the antenna count from spead hdf5 file.

        :param obj: the object instance
        :param objtype: the object type

        :return: the antenna count.
        """
        return obj.main_dset.attrs["n_antennas"]


class PolCount:
    """Specialised descriptor for polorisation count."""

    def __get__(self: PolCount, obj: Any, objtype: Any = None) -> int:
        """
        Get the polarisation count from spead hdf5 file.

        :param obj: the object instance
        :param objtype: the object type

        :return: the polarisation count.
        """
        return obj.main_dset.attrs["n_pols"]


class SampleCount:
    """Specialised descriptor for sample count."""

    def __get__(self: SampleCount, obj: Any, objtype: Any = None) -> int:
        """
        Return the sample count from spead hdf5 file.

        :param obj: the object instance
        :param objtype: the object type

        :return: the sample count.
        """
        return obj.main_dset.attrs["n_samples"]


class ChannelCount:
    """Specialised descriptor for channel count."""

    def __get__(self: ChannelCount, obj: Any, objtype: Any = None) -> int:
        """
        Return the channel count from spead hdf5 file.

        :param obj: the object instance
        :param objtype: the object type

        :return: the channel count.
        """
        return obj.main_dset.attrs["n_chans"]


class BeamCount:
    """Specialised descriptor for beam count."""

    def __get__(self: BeamCount, obj: Any, objtype: Any = None) -> int:
        """
        Return the beam count from spead hdf5 file.

        :param obj: the object instance
        :param objtype: the object type

        :return: the beam count.
        """
        return obj.main_dset.attrs["n_beams"]


class SampleTimestamps:
    """Specialised descriptor for sample timestamps."""

    def __get__(self: SampleTimestamps, obj: Any, objtype: Any = None) -> int:
        """
        Return the sample timestamps from spead hdf5 file.

        :param obj: the object instance
        :param objtype: the object type

        :return: the sample timestamps.
        """
        return obj.file_obj["sample_timestamps"]["data"]


class DaqData:
    """Specialised descriptor for data."""

    def __init__(self: DaqData, group_name: str):
        """
        Specialised descriptor for data.

        :param group_name: the group name to extract data from.
        """
        self.group_name = group_name

    def __get__(self: DaqData, obj: Any, objtype: Any = None) -> Any:
        """
        Return the sample data from spead hdf5 file.

        :param obj: the object instance
        :param objtype: the object types

        :return: the data.
        """
        return obj.file_obj[self.group_name]["data"]


class DaqDataFile:
    """An abstract class capturing commonality between readers."""

    @abstractmethod
    def form_data_buffer(self) -> None:
        """
        Abstract method to form data.

        :raises NotImplementedError: this is a abstract method,
            must be overriden.
        """
        raise NotImplementedError("This is a abstract method")

    @abstractmethod
    def form_timestamp_buffer(self) -> None:
        """
        Abstract method to form data timestamps.

        :raises NotImplementedError: this is a abstract method,
            must be overriden.
        """
        raise NotImplementedError("This is a abstract method")


class RawDataFile(DaqDataFile):
    """A class to read raw data hdf5 files."""

    # list the raw data descriptors used in RAW.
    nof_antennas = AntCount()
    nof_pols = PolCount()
    nof_samples = SampleCount()
    #
    sample_timestamps = SampleTimestamps()
    data = DaqData("/raw_")

    def __init__(self: RawDataFile, file: str) -> None:
        """
        Initialise a instance for reading raw spead hdf5 files.

        Note: this is a temporary testing tool until mccs-1785 is done.

        :param file: the absolute path.
        """
        self.file_obj = h5py.File(file, "r")
        self.main_dset = self.file_obj["root"]

    def form_data_buffer(self: RawDataFile) -> Any:
        """
        Form data from raw data hdf5.

        Data output looks like:
        [antenna, pols, samples]

        :return: data formatted like `[antenna, pols, samples]`
        """
        nof_items = self.data[0].size
        output_buffer = np.zeros(
            [self.nof_antennas, self.nof_pols, self.nof_samples], dtype="complex"
        )
        for current_antenna in range(self.nof_antennas):
            for pol in range(self.nof_pols):
                output_buffer[current_antenna, pol, 0:nof_items] = self.data[
                    (current_antenna * self.nof_pols) + pol, 0:nof_items
                ]
        return output_buffer

    def form_timestamp_buffer(self: RawDataFile) -> Any:
        """
        Form timestamp data for raw data.

        :return: the timestamps.
        """
        return self.sample_timestamps

    def close(self: RawDataFile) -> None:
        """Close the file."""
        self.file_obj.close()


class BeamDataFile(DaqDataFile):
    """A class to read beam data hdf5 files."""

    # list the raw data descriptors used in RAW.
    nof_antennas = AntCount()
    nof_pols = PolCount()
    nof_samples = SampleCount()
    nof_channels = ChannelCount()
    nof_beams = BeamCount()

    sample_timestamps = SampleTimestamps()
    data_0 = DaqData("polarization_0")
    data_1 = DaqData("polarization_1")

    def __init__(self: BeamDataFile, file: str) -> None:
        """
        Initialise a instance for reading beam spead hdf5 files.

        Note: this is a temporary testing tool until mccs-1785 is done.

        :param file: the absolute path.
        """
        self.file_obj = h5py.File(file, "r")
        self.main_dset = self.file_obj["root"]

    def form_data_buffer(self: BeamDataFile) -> Any:
        """
        Form data from beam data hdf5.

        Data output looks like:
        [pols, channels, samples, beams]

        :return: data formatted like `[pols, channels, samples, beams]`
        """
        complex_16t = np.dtype([("real", np.int16), ("imag", np.int16)])
        output_buffer = np.zeros(
            [self.nof_pols, self.nof_channels, self.nof_samples, self.nof_beams],
            dtype=complex_16t,
        )
        for polarization_idx in range(0, self.nof_pols):
            dset = getattr(self, f"data_{polarization_idx}")
            for channel_idx in range(self.nof_channels):
                output_buffer[polarization_idx, channel_idx, :, :] = dset[
                    0 : self.nof_samples, channel_idx, [0]
                ]
        return output_buffer

    def form_timestamp_buffer(self: BeamDataFile) -> Any:
        """
        Form timestamp data for beam data.

        :return: the timestamps.
        """
        return self.sample_timestamps

    def close(self: BeamDataFile) -> None:
        """Close the file."""
        self.file_obj.close()


class StationBeamDataFile(DaqDataFile):
    """A class to read beam data hdf5 files."""

    # list the raw data descriptors used in RAW.
    nof_antennas = AntCount()
    nof_pols = PolCount()
    nof_samples = SampleCount()
    nof_channels = ChannelCount()
    nof_beams = BeamCount()

    sample_timestamps = SampleTimestamps()
    data_0 = DaqData("polarization_0")
    data_1 = DaqData("polarization_1")

    def __init__(self: StationBeamDataFile, file: str) -> None:
        """
        Initialise a instance for reading beam spead hdf5 files.

        Note: this is a temporary testing tool until mccs-1785 is done.

        :param file: the absolute path.
        """
        self.file_obj = h5py.File(file, "r")
        self.main_dset = self.file_obj["root"]

    def form_data_buffer(self: StationBeamDataFile) -> Any:
        """
        Form data from beam data hdf5.

        Data output looks like:
        [pols, channels, samples, beams]

        :return: data formatted like `[pols, channels, samples, beams]`
        """
        complex_16t = np.dtype([("real", np.int16), ("imag", np.int16)])
        output_buffer = np.zeros(
            [self.nof_pols, self.nof_samples, self.nof_channels],
            dtype=complex_16t,
        )
        for polarization_idx in range(0, self.nof_pols):
            dset = getattr(self, f"data_{polarization_idx}")
            for channel_idx in range(self.nof_channels):
                output_buffer[polarization_idx, :, channel_idx] = dset[
                    0 : self.nof_samples, channel_idx
                ]
        return output_buffer

    def form_timestamp_buffer(self: StationBeamDataFile) -> Any:
        """
        Form timestamp data for beam data.

        :return: the timestamps.
        """
        return self.sample_timestamps

    def close(self: StationBeamDataFile) -> None:
        """Close the file."""
        self.file_obj.close()
