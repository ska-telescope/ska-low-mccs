# Version History

## Unreleased

* [THORN-85] Update devices to serialise their events through the EventSerialiser. This should have no operational
changes, but an attribute EventHistory is now available on all devices to debug which events this device received,
where they came from, the order they came in, and what callbacks were executed with those events. It is a json-ified list of lists.
* [THORN-70] Update ApplyConfiguration to fail early.
* [THORN-70] Remove LoadSolutionInConfigure feature flag.

## 1.0.0

* [REL-1956] 1.0.0 release - all MCCS repos
* [SKB-765] Updates to the calibration loop to account for changes in SpsStation to how AcquireDataForCalibration works. Previously SpsStation.AcquireDataForCalibration would reconfigure the DAQ each time it was ran. Now a command SpsStation.ConfigureStationForCalibration() has been added which is called once at the start of the loop, which configures the DAQ. AcquireDataForCalibration no longer configures the DAQ, it starts the daq, sends data, waits for the data, then stops the daq.
* [THORN-34] Update station calibrator to store solution in tpm_idx.
* [SKB-761] Apply calibration solution to all tile channels in deployed configuration.
* [THORN-33] Add fields to calibration database (acquisition_time, solution, corrcoeff, residual_max, residual_std, xy_phase, n_masked_initial, n_masked_final, lst, galactic_centre_elevation, sun_elevation, sun_adjustment_factor, masked_antennas)
* [THORN-33] StoreSolution 'solution_path' renamed to 'calibration_path'
* [THORN-33] Added mandatory key 'solution' to StoreSolution.
* [THORN-33] StoreSolution extra optional keys (acquisition_time, solution, corrcoeff, residual_max, residual_std, xy_phase, n_masked_initial, n_masked_final, lst, galactic_centre_elevation, sun_elevation, sun_adjustment_factor, masked_antennas)
* [THORN-33] CalibrationLoop no longer attempts to store solution (Known bug. It stores in eep order not tpm_idx order.)
* [SKB-789] More fixes to ADR-63 backwards compatibility. Now when converting `sky_coordinates` to `field`, we omit keys that are missing rather than supplying None.

## 0.24.1

* [THORN-9] Fix MccsSubarray.Configure() ADR-63 backward compatibility. Until this schema update is supported by TMC, MCCS will support both versions.
* [SKB-773] MccsStation will now continue to attempt to update pointing delays when a DevFailed exception is encountered in SpsStation. Previously, any DevFailed exception would cause the tracking thread to exit, halting tracking for the scan.

## 0.24.0

* [THORN-79] Fix MccsController.RestartSubarray() handling of empty devices.
* [SKB-762] Fix the schema validation.
* [THORN-34] Update default selection policy. Make configurable from deployment.

## 0.23.1

* [SKB-721] Changed PowerState roll up behavior in controller

## 0.23.0

* [ADR-63] Updated the configure commands to be ADR-63 compliant.
* [THORN-25] MCCS - link existing tests to requirements
* [THORN-77] Update ska-low-mccs-calibration to 0.4.0
* Update helm package ska-low-mccs-pasd 0.16.0 -> 0.16.1 (<https://gitlab.com/ska-telescope/mccs/ska-low-mccs-pasd/-/releases>)
* [THORN-33] Add additional .h5 product to the solver solve command.
* [THORN-34] Update SelectionPolicy security.
* [THORN-34] Add feature flag to application of calibration solutions.

## 0.22.0

* [MCCS-2256] Removed cabinet bank device.
* [THORN-34] Add preferred key to calibration products.
* [THORN-67] Add arguments to calibration loop method.
* Update helm package ska-low-mccs-spshw 0.21.2 -> 0.23.0 (<https://gitlab.com/ska-telescope/mccs/ska-low-mccs-spshw/-/releases>)

## 0.21.0

* [MCCS-2330] Update pytango to 10.0.0
* [SKB-719] Parallelise commands in Configure() to speed it up.
* [THORN-28] Patch through calibration commands to the Tango interface.
* Update helm package ska-low-mccs-pasd 0.15.2 -> 0.16.0 (<https://gitlab.com/ska-telescope/mccs/ska-low-mccs-pasd/-/releases>)
* Update helm package ska-low-mccs-spshw 0.21.1 -> 0.21.2 (<https://gitlab.com/ska-telescope/mccs/ska-low-mccs-spshw/-/releases>)

## 0.20.0

### Helm Chart

* Update helm package ska-low-mccs-pasd 0.15.1 -> 0.15.2 (<https://gitlab.com/ska-telescope/mccs/ska-low-mccs-pasd/-/releases>)
* Update helm package ska-low-mccs-spshw 0.20.1 -> 0.21.1 (<https://gitlab.com/ska-telescope/mccs/ska-low-mccs-spshw/-/releases>)
* [THORN-48] Update deploy submodule.

### OCI

* [THORN-63] Update dependency ska-tango-base to ^1.2.3
* [THORN-28] Patch through calibration arguments to TANGO interface.
* [THORN-53] Use new base images

## 0.19.0

* [THORN-37] Update DeviceComponentManager to use tango.DevSource.DEV
* [THORN-7] Add calibration documentation
* [THORN-1] Multi-class device server endpoint

## 0.18.0

* [LOW-1146] Change selection policy to prefer recent solutions when multiple are available.
* [LOW-1131] Expose MCCS Station as LoadBalancer

## 0.17.0

* [SKB-635] Final manual check in obs commands in case of missed change events
* [SKB-635] Fix garbage collect in DeviceComponentManager
* [MCCS-2312] Add station_id to calibration products.
* [MCCS-2277] Allow MccsController to allocate unhealthy resources to MccsSubarray
* [SKB-610] Update downstream refs to include StationID in hdf5 metadata.

## 0.16.2

* [MCCS-2260] Pull from artefact.skao.int
* [MCCS-2248] Update functional tests to be location agnostic.

## 0.16.1

* [LOW-987] Allow disabling spshw and pasd components. (This allows us to independently bump or pin the versions of these components' charts in production.)
* [SKB-506] wrong pointing delay sign
* [MCCS-2228] Correct MccsController handling of StationIDs.

## 0.16.0

* [MCCS-2141] Update ska-tango-base to 1.0.0
* [MCCS-2225] Add auto-docs for deviceservers yaml
* [MCCS-2224] Cache data during the image build process
* [MCCS-2132] Update power documentation

## 0.15.0

* [SKB-486] Reorder antenna positions to EEP order.
* [MCCS-2213] Update to latest upstream templates. Remove local copies of templates.
* [MCCS-2211] Update `.deploy` submodule.
* [MCCS-2213] Configure MCCS via TelModel.
* [SKB-458] [SKB-459] Fix `GetPointingDelays` and `LoadPointingDelays`. Add `MccsStation.lastPointingDelays` attribute.
* [MCCS-2211] Add Solver device to MCCS deployment.
* [MCCS-2206] Fix `Subarray.Configure` reporting command results incorrectly.
* [LOW-938] Add ska-low-deployment as a submodule and remove local templates.
* [MCCS-2203] Increase helmfile templating to make them more DRY.
* [MCCS-2201] Remove unused property: `AntennaTpmIds`.
* [MCCS-2189] Update to latest version of ska-low-mccs-spshw.
* [MCCS-2064] Add PI23.2 demo notebook to demo folder.
* [MCCS-2198] Improve calibration loop responsiveness/response time to stop commands.
* [MCCS-2064] Apply a calibration solution from the CalibrationStore to MCCS.
* [LOW-916] Improve `point_array` docstring for clarity.
* [MCCS-2187] Fix a flaky `HealthState` test.
* [MCCS-2119] Add PI19 notebook to demo folder.
* [MCCS-2064] Add a basic `SelectionPolicy` to the CalibrationStore.
* [MCCS-2187] Add a set of tests for `HealthState` aggregation in the main repo.
* [MCCS-2186] Update Obs devices to use the new `BaseHealthModel`.
* [MCCS-2047] Add `StartCalibrationLoop` and `StopCalibrationLoop` to MccsStationCalibrator to acquire data, solve and store solution for each channel.
* [MCCS-2052] Modify CalibrationStore to retrieve calibration solutions from the artefact repository.

## 0.14.0

* [MCCS-2177] - Update RAL platform spec to 0.5.0
* [MCCS-2179] - Fix variety of HealthState issues, upgrade HealthModel base class
* [MCCS-2038] - Add CabinetBank device.
* [MCCS-1975] helmfile update
* [MCCS-1981] Pull in reference location data to MccsStation and MccsAntenna
* [MCCS-1995] chart values schema
* [MCCS-1995] -dirty fix
* [MCCS-2066] station address handling
* [MCCS-1821] Fix failed Allocate staying in queue forever
* [MCCS-2067] Allow MCCS station device to track multiple objects
* [MCCS-2056] Add stfc-ci yaml to TelModel
* [MCCS-2057] Refactor functional tests
* [MCCS-2040] Refactor allocate/configure functional test
* [MCCS-2070] String keys
* [MCCS-2060] Add eda-config.yaml with all attributes.
* [MCCS-2085] Abort() in the ObsDevices
* [MCCS-2085] Add AbortDevice()
* [MCCS-2097] itango support
* [MCCS-2089] Add PyPi as a source for Poetry to pyproject.toml
* [MCCS-2103] Gateway support
* [MCCS-2054] [MCCS-2035] Add calibration devices, add artefact repo to calibration devices
* [MCCS-2107] Remove deprecated environments
* [MCCS-2106] EDA configmap
* [MCCS-2108] Fix docs
* [MCCS-2104] Add temporary fix for On command.
* [MCCS-2032] Implement MccsController.RestartSubarray()
* [MCCS-2030] Pull antenna position data from telmodel into core MCCS on init
* [MCCS-2033] Functional tests for Abort/Restart
* [MCCS-2104] Fix intermittent tests.
* [MCCS-2109] Correct Off command to account for no subrack PDU control.
* [MCCS-2112] bump spshw to a version that doesn't deploy postgres
* [MCCS-2118] Allow MccsStation to be deployed without a FieldStation
* [MCCS-2109] Correct test bdd.
* [MCCS-2017] Update schemas on python-format
* [MCCS-2120] Support DAQ data sync
* [SKB-375] [MCCS-2122] Allow no start time for Scan()
* [MCCS-2112] expose controller and subarrays
* [MCCS-2153] Add MCCS schemas to TelModel
* [MCCS-2180] integrate spshw
* [LOW-890] Add antenna locations to station devices
* [MCCS-2044] Add AcquireDataForCalibration method to MccsStation.
* [LOW-907] Update specs

## 0.13.0

* MCCS-1948 - Exercise StartAcquisition in MccsController.On()
* MCCS-1904 - Functional tests for MccsController.On()/Off()
* MCCS-1957 - Support namespace override
* LOW-707 - Replace FQDN with TRL
* MCCS-1764 - Implement regular pointing delays updates to the station
* MCCS-1966 - Use infra-issued credentials for AAVS3
* MCCS-1967 - Add ResultCode to our LRC end
* MCCS-1974 - Update schemas to include 'interface' key

## 0.12.0

* MCCS-1859 - Helm values
* MCCS-1840 - Deploy stations
* MCCS-1866 - Fix antenna template bug
* MCCS-1865 - Adjust test so I can pass when PaSD subdevices deployed
* MCCS-1023 - Connecting MccsStation and SpsStation
* MCCS-1860 - Continue test harness work
* MCCS-1920 - Notebook to turn on AdminMode of all devices
* MCCS-1805 - Implement test xtp-27894
* MCCS-1878 - Finish test harness refactor
* MCCS-1939 - aavs3 staging
* MCCS-1919 - Chart and notebook for TMC integration
* MCCS-1944 - Check in Chart.lock
* MCCS-1947 - Support platform spec without pasd
* MCCS-1945 - Updated the gmrt.yaml for deployment
* MCCS-1946 - Correct tests to pass when Smartboxes and Antennas deployed
* MCCS-1908 - Extend BDD tests to exercise Scan/EndScan
* MCCS-1913 - Extend BDD tests down to MccsTile
* MCCS-1949 - flexible helm chart
* MCCS-1951 - Update docs to latest deployment
* MCCS-1872 - MccsController powerstate incorrect when deployed alone
* MCCS-1874 - Allocate cannot be called multiple times in a row
* MCCS-1953 - Add schemas to docs
* MCCS-1923 - Refactor allocation to use pools of station beams
* MCCS-1955 - Station beams still incrementing after failed allocate
* MCCS-1956 - Allocate generating station beam TRL from HW beam ID and station ID
* MCCS-1950 - Use PaSD 0.6.0, SPSHW 0.13.0

## 0.11.0

* MCCS-1192 - k8s-test bug fix
* MCCS-970  - Initial version of refactor MccsTile for subarray support
* MCCS-1038 - Finalise linting
* MCCS-1205 - Merge files required by MCCS-1181 to main
* MCCS-1210 - linting & static type checking
* MCCS-1278 - remove tile switching
* MCCS-957  - implement db interface, pull in data, apply to attributes
* MCCS-1172 - Pushing tileProgrammingState attribute change events.
* MCCS-1283 - Minor refactor of TPMDriver for ease of testing
* MCCS-1287 - Fix spelling errors in station.yaml
* MCCS-1095 - Add functional tests for the thin slice
* MCCS-1296 - functional test fixes
* MCCS-1264 - Export antenna and station geojson data to telmodel repo
* MCCS-1254 - Remove tile and subrack code from ska-low-mccs repository
* MCCS-1187
* MCCS-1138 - Fix for ReleaseResources command
* MCCS-1322 - Add schema checking to the confguration commands
* MCCS-1216 - Update jupyterhub to use ingress
* MCCS-1185 - Add jupyTango to the thin slice
* MCCS-1303 - Add folder value_files for storing umbrella value files
* MCCS-1193 - fix docs
* MCCS-1224,MCCS-1232 - Dashboards for tile and station for running thickened slice with
* MCCS-1214 - Reduce the complexity of subarray_device component_state_changed_callback
* MCCS-1058 - Added new schemas for Allocate/Release/Confgure in MccsController, Subarray, SubarrayBeam, StationBeam
* MCCS-1425 - Add plugin file for k9s
* MCCS-1362 - Add functionality to controller device to read health of subdevices
* MCCS-1434 - Add xray integration to mccs repo
* Mccs 1434 - Add xray integration to mccs repo
* MCCS-1467 - implement minimum json (allocate & configure) updates following ADR-51
* MCCS-1504 - Update to ska-tango-base 0.18.1
* MCCS-1392 - Add rules for station health
* MCCS-1446 - Implement health in StationBeam
* MCCS-1447 - Implement health in Subarray
* MCCS-1500 - Correct typo.
* MCCS-1391 - Added rules for controller
* MCCS-764  - remove apiu
* MCCS-1492 - Add rules for Antenna health
* MCCS-1521 - Add rules for Subarray Beam health
* MCCS-1458 - Write bdd tests for the controller test plan
* MCCS-764  - Update station pasd interface
* MCCS-1458 - Write bdd tests for the controller test plan
* MCCS-764  - update pasd chart
* MCCS-1531 - Fix broken docs
* MCCS-1353 - Update state changed callback to use kwargs and update to tango-testing
* MCCS-1612 - Add MccsSubarrayBeam.StationBeamFQDNs and MccsSubarray.SubarrayId to helm chart
* MCCS-1613 - Add logging statements
* MCCS-1575 - Implement MccsStation.Configure to apply a calibration solution
* MCCS-1633 - Update /docs/src/_static/img/ in MCCS repos
* MCCS-1636 - Use ska-ser-sphinx-theme
* MCCS-1486 - Refactor subarray beam.
* MCCS-639  - Tie generated delays (+ delay rates) to Tile device(s)
* MCCS-372  - Add health roll-up capability of the StationBeam into SubarrayBeam
* MCCS-1643 - Updated the values_thinslice_gmrt
* MCCS-1590 - Add SKUID
* MCCS-1653 - EMPTY devices report HealthStatus.OK
* MCCS-1655 - Configure ska-low-mccs to pull in other charts
* MCCS-1639 - Implement command to generate delays and rates for celestial pointing
* MCCS-1649 - fix @check_on decorator
* MCCS-1655 - Enable ska-tango-base in default values file.
* MCCS-1667 - Allocate schema not compliant with ADR-62
* MCCS-372  - Created integration test that verify health roll-up of the StationBeam into SubarrayBeam
* MCCS-1654 - Added "command"_invoked and "command"_completed events in subarray...
* MCCS-1677 - Define tests for beam behaviour
* MCCS-1692 - Remove subrack from controller
* MCCS-1459 - Raw data received test
* MCCS-1773 - Platform dependent subchart config
* MCCS-1774 - update to pytango 9.4.2
* MCCS-1782 - station health bug
* MCCS-1703 - Define updated TM->MCCS interface defintion (schemas)
* MCCS-1571 - Implement Scan and EndScan commands
* MCCS-1459 - Implement XTP-21404 test.
* MCCS-1749 - PaSD end-to-end tests outline
* MCCS-1724 - Add TileSimulator DAQ test.
* MCCS-319  - Allocate/Configure Tests
* MCCS-1700 - Remove MccsTile from MccsStation
* MCCS-1833 - Deploy subarrays
* MCCS-1768 - Validate all commands to MCCS devices with schemas
* MCCS-1826 - Update geojson files with AAVS3 locations
* MCCS-1836 - Deploy station beam devices
* MCCS-1838 - Deploy subarray beams
* MCCS-1745 - Update smartbox and fndh references to FieldStation
* MCCS-1842 - Update antenna config
* MCCS-1841 - deploy antennas
* MCCS-1843 - Validate Station.ConfigureSemiStatic command with schemas
* MCCS-1850 - deploy controller
* MCCS-1645 - Update Subarray Configure schema to allow a transaction id to be passed
* MCCS-1830 - Remove xfail from test.
* MCCS-1763 - Notebook to turn MCCS on/off
* MCCS-1824 - Station GetPointingDelays - time parameter and delay rates
* MCCS-1853 - update functional test harness
* MCCS-1741 - Created a dashboard for monitoring and controlling subarrays
* MCCS-1738 - Dashboard for MccsController for use in AAVS3
* MCCS-1852 - switch to new chart

## 0.10.2

* MCCS-1192 - k8s-test bug fix

## 0.10.1

## 0.10.0

* MCCS-1154 - Add make k8s test to the ska-low-mccs pipeline
* MCCS-1190 - Support zero instances
* MCCS-1096 - Investigate MCCS code coverage threshold
* MCCS-1136 - Create script for setting up dev environment
* MCCS-1176 - archive cluster manager
* MCCS-1136 - Create script for setting up dev environment
* MCCS-1174 - remove pasd code from ska-low-mccs repository
* MCCS-1145 - Impose max-line-length = 88
* MCCS-1157 - Dependency update
* MCCS-1143 - Create Sequence Diagram to capture the PI16 'Thin Slice'
* MCCS-969  - Changed pyfabil and pyaavs whl to revision mccs-969
* MCCS-1147 - Identify where the callbacks where strings are missmanaged
* MCCS-1078 - Remove DAQ code from main MCCS repo
* MCCS-1029 - Integrate DAQ library with MCCS
* LOW-365   - dependency update
* MCCS-1134 - Remove common code from ska-low-mccs repository
* MCCS-1067 - Update MCCS BDD Tests for Controller-Only Deployment
* MCCS-1062 - debug integration test pipeline failures
* MCCS-1132 - fix Tile Off command failure
* MCCS-1135 - Updated Dockerfile."
* LOW-304   - Update development environment
* MCCS-1065 - Investigate MCCSController with no subservient devices
* MCCS-1030 - DAQ Documentation
* MCCS-976  - Create and test chart with AAVS multiplicity
* LOW-280   - Test harness race condition
* MCCS-1014 - Change base classes to V0.13.2
* MCCS-1011 - Updated charts for Taranta Dashboard
* MCCS-1014 - Base Classes 0.13 update feature branch
* MCCS-1054 - Typos corrected
* MCCS-1054 - Updated deployment documentation"
* MCCS-1011 - Raj
* MCCS-997  - Config fixes
* MCCS-1008 - Update test harness
* MCCS-1005 - Raj
* MCCS-1001 - Docs style update
* MCCS-992  - only publish ska-low-mccs chart

## 0.9.1

* MCCS-915 - Adopt poetry and update CI Pipeline
* MCCS-493 - Poetry
* MCCS-874 - Monitor hardware connection status for Tile
* MCCS-701 - add adminMode to config file
* MCCS-736 - Test hardware tile interaction using 0.12 base classes

## 0.9.0

* MCCS-622 - Correcting physical device drivers to work with 0.11 base classes.
* MCCS-842 - Add assigned resources to subarray
* MCCS-837 - Long Running Commands - ska-tango-base usage
* MCCS-908 - add make target to wait for Taranta pod CPU utilisation to reduce
* MCCS-903 - Add command to upgrade pip at the start of the Docker build process
* MCCS-476 - licensing cleanup
* MCCS-851 - Synchronous disable
* MCCS-844 - Dependency update
* MCCS-771 - Changed Taranta to 1.1.5 in Chart.yaml and created a Taranta Dashboard for...
* MCCS-847 - State space reduction
* MCCS-735 - Refactor pyaavs package to be used directly from MCCS
* MCCS-845 - Subrack refactor
* MCCS-838 - Add tile orchestrator
* MCCS-708 - Remove allocated_resources attribute from controller
* MCCS-767 - PaSD bus device
* MCCS-405 - Resource management of Channels within Station
* MCCS-835 - Fix test harness bug
* MCCS-816 - Unit test stability improvements
* MCCS-403 - Station  beam resource management
* MCCS-821 - Refactor complex test
* MCCS-817 - Refactor complex method and fix silent failure
* MCCS-810 - Make docstrings comply with PEP-257
* MCCS-572 - Add typehints to tile
* MCCS-747 - Make subrack component manager report progress and status of commands
* MCCS-749 - Add Message queue size callback
* MCCS-806 - Update dependencies
* MCCS-571 - Add typehints to subrack
* MCCS-726 - Add message queue to subrack driver
* MCCS-726 - Allow message queue sharing
* MCCS-712 - Add unit tests for subrack driver
* MCCS-568 - Add typehints to cluster manager
* MCCS-573 - Add typehints to station, station beam and point station
* MCCS-716 - Update to Sphinx 4
* MCCS-715 - Sundry instability fixes
* MCCS-574 - Add typehints to subarray and subarray beam
* MCCS-575 - Add typehints to transient buffer and telstate
* MCCS-681 - Add Taranta dashboard for allocate and release testing
* MCCS-702 - Omit Webjive charts by default
* MCCS-567 - Add typehints to APIU
* MCCS-668 - Start on developer guideline
* MCCS-559 - Add typehints to antenna
* MCCS-579 - Test harness typehints
* MCCS-699 - Stability and thread safety
* MCCS-569 - Add typehints to controller
* MCCS-697 - Move test harness to main source tree
* MCCS-579 - Add typehints to hardware client
* MCCS-480 - Relax pod initialisation constraint
* MCCS-400 - Update to ska_tango_base 0.11, and use concurrency throughout

## 0.8.6

* MCCS-642 - Update to latest CI templates

## 0.8.5

* MCCS-643 - Update from Nexus to new SKAO central artefact repository
* MCCS-635 - Refactor complex TPM methods to reduce complexity
* MCCS-693 - Update publish stage to point to CAR

## 0.8.4

* MCCS-682 - Port AssignResources mods from MCCS-560
* MCCS-592 - Refactor message queue complex routine
* MCCS-404 - Move subarraybeam resource manager from subarray to controller
* MCCS-560 - Add MCCS Subarray "AssignResources" command to use message queue
* MCCS-677 - Test stability improvements
* MCCS-678 - Changed ska-tango-base-itango-console
* MCCS-402 - Move StationsResourceManager from Subarray to Controller
* MCCS-665 - Add subarray ID to parameters passed to the beamformer channels

## 0.8.3

* MCCS-667 - modify gitlab ci pipeline
* MCCS-654 - modify url for JSON
* MCCS-539 - initial msgq to subarray
* MCCS-425 - Write documentation for new features
* MCCS-576 - Add typehints to code in non-device modules
* MCCS-662 - Move event handling to MccsDeviceProxy and DevicePool
* MCCS-484 - Support for hardware testing
* MCCS-521 - Support mocking of all devices
* MCCS-594 - Remove ERROR in test log
* MCCS-536 - Convert Controller Allocate command to use message queue
* MCCS-590 - PEP257 docstring linting
* MCCS-597 - Add static type checking to toolchain
* MCCS-589 - Add cov-report term to setup.cfg
* MCCS-550 - make tests and make lint
* MCCS-565 - Remove tabs from charts/ska-low-mccs/data/configuration.json
* MCCS-588 - Update charts to fix local deployment failure
* MCCS-583 - Add cyclomatic complexity to linting
* MCCS-160 - Add zDisplacement. Geo posn --> Station
* MCCS-423 - Add 40G interface programming: destination IP, status
* MCCS-530 - Add MCCS Controller "Off" command to use message queue

## 0.8.2

* MCCS-581 - Fix assignedResources in Restart command
* MCCS-564 - Fix assignedResources json string
* MCCS-566 - Pin ska_tango_base and pause intersphinx cross-link checks
             (Temporary workaround pending update to ska_tango_base
             version 0.11.0)

## 0.8.1

* MCCS-561 - Fix location of assignedResources attribute init

## 0.8.0

* MCCS-442 - Restart command implementation
* MCCS-459 - Integration of new json strings
* MCCS-554 - Update container images
* MCCS-452 - Import/adapt AAVS pointing→delay routines
* MCCS-271 - add ApplyPointing to StationBeam (and Station)
* MCCS-547 - RTD bugfix again
* MCCS-549 - yaml bugfix
* MCCS-510 - Message queue integration for "On" command
* MCCS-420 - Imported and edited Tile object for tpm 1.2 and 1.6
* MCCS-422 - Add DAQ packet generation functionalities into Tango device
* MCCS-511 - message queue with unit tests
* MCCS-421 - Added support for TPM test signal generator to Tango tile
* MCCS-523 - skip configure as well
* MCCS-410 - Implement interface of Tango device with hardware Subrack
* MCCS-517 - Fix to integration tests
* MCCS-502 - convert apiu to svg & add subarray structure class diagram
* MCCS-441 - enable abort and obsreset
* MCCS-477 - package rename: ska.low.mccs -> ska_low_mccs
* MCCS-386 - mock builders
* MCCS-475 - machine requirements docs
* MCCS-473 - tests and harness in a single package
* MCCS-458 - MccsDeviceProxy and unify backoff_connect
* MCCS-392 - Add missing docs (including MCCS-490 race condition workaround)
* MCCS-357 - Add autogenerated class diagrams
* MCCS-391 - Remove DeviceProxy.get_fqdn() calls
* MCCS-388 - Memorized simulationMode
* MCCS-383 - Update to ska-tango-base 0.9.1
* MCCS-257 - Upgrade of TPI Tango device to the ICD/ECD
* MCCS-335 - Added typehints to MCCS controller_device
* MCCS-384 - docstring coverage to 100%
* MCCS-378 - Support for simulators with dynamic values
* MCCS-116 - Add station beam health monitoring to MCCS Station

## 0.7.0

* MCCS-381 - Move "Configure" command implementation to station beams resource manager
* MCCS-368 - Update resource usage
* MCCS-258 - Update of TPI Tango device to obDevice of lmcBaseClasses
* MCCS-359 - Add scan, abort and obsreset unit tests
* MCCS-379 - Diagnose and fix/workaround PSI subrack/APIU bug
* MCCS-239 - Subarray scan implementation
* MCCS-334 - Fix unsafe dictionary look-ups
* MCCS-252 - New and modified Tile commands for beam configuration and calibration
* MCCS-374 - Correct initialisation values for controller commandResult
* MCCS-309 - Integration of devices into end-to-end power flow
* MCCS-307 - Add PSI chart configuration

## 0.6.0

* MCCS-366 - Updates to APIU and Antenna
* MCCS-253 - Add simulation of ARP resolution to TPM simulator
* MCCS-371 - Replace plantuml markup with static svgs
* MCCS-302 - Updates for PI#9 json strings according to:
             <https://confluence.skatelescope.org/pages/viewpage.action?pageId=110799390>
* MCCS-370 - Async mechanism for subarray ObsReset command
* MCCS-180 - updated power management, new subrack device, MccsController.Startup()
* MCCS-317 - Basic Abort and ObsReset implementation
* MCCS-361 - robust device connection
* MCCS-320 - deployment bugfixes and move to hyphenated make targets
* MCCS-350 - Set commandResult to UNKNOWN before MccsController commands are executed
* MCCS-345 - Basic connect to "turn on" TPM1.2 in Simulator
* MCCS-272 - SubArrayBeam device - implement stub device
* MCCS-251 - Software control of physical Tile Processing Board

## 0.5.0

* MCCS-169 - Add configure to subarray
* MCCS-313 - MCCS Tile WebJive pages
* MCCS-325 - minikube and WebJive setup
* MCCS-162 - SubArray device: add resource management
* MCCS-322 - Improve xray_report job stability
* MCCS-306 - Document flow and control of power
* MCCS-331 - update dependencies for new ska-docker images
* MCCS-242 - Test harness refactor
* MCCS-331 - Don't assume pytango is numpy-enabled

## 0.4.0

* MCCS-208 - re-enable webjive chart
* MCCS-237 - device subpackages
* MCCS-240 - allow deviceserver overrides
* MCCS-225 - further work on hardware model, especially antenna
* MCCS-162 - Subarray resource management model

## 0.3.0

* MCCS-156 - controller resource management model
* MCCS-225 - hardware model
* MCCS-207 - refactor helm charts to use tango-util

## 0.2.1

* MCCS-227 - address segfaults

## 0.2.0

test release - to verify MCCS-209

## 0.1.2

Update to lmc-base-classes version 0.6.1

## 0.1.1

Patch release to create versioned docker image

## 0.1.0

Initial release

* control model hierachy of MCCS LMC Tango device classes
* cli tools for _master_ and _tile_
* tox environment
* k8s helm chart for tango device containers
