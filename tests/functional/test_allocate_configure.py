# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains BDD tests for a "Controller Only" deployment of MCCS."""
from __future__ import annotations

import itertools
import json
from typing import Callable, Generator

import numpy as np
import tango
from pytest_bdd import parsers, scenarios, then, when
from ska_control_model import ObsState

from tests.functional.helpers.obs_helpers import (
    _flatten,
    configure_resources,
    deconfigure_resources,
    end_scan,
    scan,
)
from tests.harness import get_station_beam_trl, get_subarray_beam_trl

RFC_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"

scenarios("features/allocate_and_configure.feature")

# HELPER METHODS #


def _next_station_beam_trl(station_name: str) -> Generator:
    station_beam_id = 1
    while True:
        yield get_station_beam_trl(station_name, station_beam_id)
        station_beam_id += 1


# WHEN \ THEN #


@when(
    parsers.parse(
        "MccsController allocates them to "
        "MccsSubarray {subarray_id} with invalid schema"
    ),
    converters={
        "subarray_id": int,
    },
    target_fixture="allocate_state",
)
def allocate_with_incorrect_schema(
    controller: tango.DeviceProxy, subarray_id: int
) -> str:
    """
    Attempt to allocate with incorrect schema, if failed return "Allocate failed".

    :param controller: MccsController device proxy for use in the test.
    :param subarray_id: The id of the subarray to use in this test.

    :returns: "Allocate Failed" if allocation failed.
    """
    resource_spec = {
        "subarray_id": subarray_id,
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                # Incorrect aperture_id
                "apertures": [{"station_id": 1, "aperture_id": "AP01.00"}],
                "number_of_channels": 8,
            }
        ],
    }
    try:
        controller.Allocate(json.dumps(resource_spec))
    except tango.DevFailed:
        return "Allocate Failed"
    return ""


@when(
    parsers.parse(
        "MccsController allocates them to MccsSubarray {subarray_id} "
        "with too many channels"
    ),
    converters={"subarray_id": int},
    target_fixture="allocate_state",
)
def over_allocate_resources(
    controller: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    subarray_id: int,
) -> str:
    """
    Over-allocate with too many channels.

    :param controller: MccsController device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param subarray_id: The id of the subarray to use in this test.

    :returns: "Allocate Failed" if allocation failed.
    """
    blocks = 25
    resource_spec = {
        "subarray_id": subarray_id,
        # Total number_of_channels / 8 > 48 so allocation fails.
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "apertures": [{"station_id": 1, "aperture_id": "AP001.01"}],
                "number_of_channels": 8 * blocks,
            },
            {
                "subarray_beam_id": 2,
                "apertures": [{"station_id": 1, "aperture_id": "AP002.02"}],
                "number_of_channels": 8 * blocks,
            },
        ],
    }
    result = controller.Allocate(json.dumps(resource_spec))
    wait_for_lrcs_to_finish()
    result = controller.longrunningcommandresult
    if (
        f"Cannot allocate resources: Station {get_subarray_beam_trl(2)}"
        f" has no {blocks} channel blocks to allocate."
    ) in result[1]:
        return "Allocate Failed"
    return ""


@when(
    parsers.parse("MccsController releases resources from MccsSubarray {subarray_id}"),
    converters={
        "subarray_id": int,
    },
)
def release_resources_step(
    controller: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> None:
    """
    Release resources from MccsController.

    :param controller: MccsController device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    """
    subarray = subarrays[int(subarray_id) - 1]
    release_args = json.dumps({"subarray_id": subarray_id})
    controller.Release(release_args)
    wait_for_lrcs_to_finish()
    wait_for_obsstate(ObsState.EMPTY, subarray)


@when(
    parsers.parse(
        "MccsSubarray {subarray_id} configures the "
        "{subarray_beam_count} MccsSubarrayBeams"
    ),
    converters={"subarray_id": int, "subarray_beam_count": int},
    target_fixture="resources_configured",
)
@when(
    parsers.parse(
        "MccsSubarray {subarray_id} reconfigures the "
        "{subarray_beam_count} MccsSubarrayBeams"
    ),
    converters={"subarray_id": int, "subarray_beam_count": int},
    target_fixture="resources_configured",
)
def configure_resources_step(
    subarray_beam_count: int,
    resources_allocated: dict,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> list:
    """
    Configure resources according to data within bounds specified by Configure() schema.

    :param subarray_beam_count: amonut of MccsSubarrayBeam used in this test.
    :param resources_allocated: schema compliant fixture holding resources
        allocated earlier.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.

    :returns: list of configuration parameters
    """
    subarray = subarrays[int(subarray_id) - 1]
    return configure_resources(
        subarray,
        subarray_beam_count,
        resources_allocated,
        wait_for_lrcs_to_finish,
        wait_for_obsstate,
    )


@when(
    parsers.parse("MccsSubarray {subarray_id} deconfigure its resources"),
    converters={
        "subarray_id": int,
    },
)
def deconfigure_resources_step(
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> None:
    """
    Deconfigure resources from the MccsSubarray.

    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    """
    subarray = subarrays[int(subarray_id) - 1]
    deconfigure_resources(subarray, wait_for_lrcs_to_finish, wait_for_obsstate)


@then("Allocation fails")
def allocation_fails(allocate_state: str) -> None:
    """
    Check that our Allocate_state fixture has been updated with "Allocate Failed".

    Weak step, just here for readability of Gherkin.

    :param allocate_state: fixture holding state of allocation command.
    """
    assert allocate_state == "Allocate Failed"


@then(
    parsers.parse("MccsController reports correct allocation"),
    converters={"subarray_beam_count": int},
)
def check_controller_allocation(
    controller: tango.DeviceProxy,
    resources_to_allocate: dict,
    wait_for_lrcs_to_finish: Callable,
    station_names: list[str],
) -> None:
    """
    Check MccsController allocated resources correctly according to fixture.

    :param controller: MccsController device proxy for use in the test.
    :param resources_to_allocate: Non-compliant resources to allocate ditctionary.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param station_names: stations used in the test.
    """
    subarray_beams = [
        f"low-mccs/subarraybeam/{subarray_beam_id:02}"
        for subarray_beam_id in list(resources_to_allocate.keys())
    ]

    # This works only if we are assigning resources to a single subarray, starting
    # with an empty system. Each station has a set of 48 available hardware beams
    # to allocate. We can therefore derive each station beam TRL from the station ID
    # and number of beams we expect to be allocated from that station.
    station_beams: list[str] = []
    trl_generator_dict: dict[str, Generator] = {}
    for station_spec in resources_to_allocate.values():
        for station_id, substation_list in station_spec.items():
            # If we don't yet have a generator for station beam TRLs for a
            # given station ID, make one.
            # We only do this if we haven't yet made one, such that we increment
            # correctly.
            if station_id not in trl_generator_dict:
                station_name = station_names[station_id - 1]
                trl_generator_dict[station_id] = _next_station_beam_trl(station_name)
            for _ in substation_list:
                station_beams.append(next(trl_generator_dict[station_id]))

    wait_for_lrcs_to_finish()
    assert controller.GetAssignedResources(1) == json.dumps(
        {"subarray_beams": subarray_beams, "station_beams": station_beams}
    )


@then(
    parsers.parse("{subarray_beam_count} MccsSubarrayBeam report correct assignment"),
    converters={"subarray_beam_count": int},
)
# pylint: disable=too-many-locals
def check_subarray_beam_assignment(
    subarray_beams: list[tango.DeviceProxy],
    resources_to_allocate: dict,
    number_of_channels: int,
    wait_for_lrcs_to_finish: Callable,
    station_names: list[str],
) -> None:
    """
    Check each MccsSubarrayBeam received correct assignment from MccsController.

    :param subarray_beams: list of MccsSubarrayBeam device proxies for use in the test.
    :param resources_to_allocate: Non-compliant resources to allocate ditctionary.
    :param number_of_channels: fixture holding number_of_channels
        used in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param station_names: stations used in the test.
    """
    wait_for_lrcs_to_finish()
    used_subarray_beam_count = 0
    station_beam_count = 0
    trl_generator_dict: dict[str, Generator] = {}
    for subarray_beam_no, station_spec in list(resources_to_allocate.items()):
        subarray_beam = subarray_beams[subarray_beam_no - 1]

        assert subarray_beam.firstSubarrayChannel == number_of_channels * (
            used_subarray_beam_count
        )
        assert subarray_beam.numberOfChannels == number_of_channels

        # We are allocating from an empty system, so station beams will be allocated
        # sequentially starting from 1. For each subarray beam we need to keep track of
        # how many station beams have been allocated already, and how many should be
        # allocated to this subarray beam.
        # Each station has a set of 48 available hardware beams to allocate.
        # We can therefore derive each station beam TRL from the station ID
        # and number of beams we expect to be allocated from that station. So we use a
        # generator for each station ID to keep track of how many from each station we
        # should have used.
        for station in resources_to_allocate[subarray_beam_no].values():
            station_beam_count += len(station)

        station_beam_ids = []
        for station_id, substation_list in station_spec.items():
            # If we don't yet have a generator for station beam TRLs for a
            # given station ID, make one.
            # We only do this if we haven't yet made one, such that we increment
            # correctly.
            if station_id not in trl_generator_dict:
                station_name = station_names[station_id - 1]
                trl_generator_dict[station_id] = _next_station_beam_trl(station_name)
            for _ in substation_list:
                # We get the next sequential station beam for the given station id
                # then only take the last part (low-mccs/beam/ci-1-01 -> ci-1-01)
                station_beam_ids.append(
                    next(trl_generator_dict[station_id]).split(r"/")[-1]
                )
        assert list(subarray_beam.stationbeamids) == station_beam_ids

        used_subarray_beam_count += 1


@then(
    parsers.parse("MccsSubarray {subarray_id} report correct allocation"),
    converters={
        "subarray_count": int,
    },
)
# pylint: disable=too-many-locals
def check_subarray_allocation(
    resources_to_allocate: dict,
    wait_for_lrcs_to_finish: Callable,
    number_of_channels: int,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
    station_names: list[str],
) -> None:
    """
    Check each MccsSubarray received correct allocation from MccsController.

    :param resources_to_allocate: Non-compliant resources to allocate dictionary.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param number_of_channels: fixture holding number_of_channels
        used in the test.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    :param station_names: stations used in the test.
    """
    subarray = subarrays[int(subarray_id) - 1]
    wait_for_lrcs_to_finish()
    subarray_beams = [
        f"low-mccs/subarraybeam/{subarray_beam_id:02}"
        for subarray_beam_id in list(resources_to_allocate.keys())
    ]

    # This works only if we are assigning resources to a single subarray, starting
    # with an empty system. Each station has a set of 48 available hardware beams
    # to allocate. We can therefore derive each station beam TRL from the station ID
    # and number of beams we expect to be allocated from that station.
    station_beams = []
    trl_generator_dict: dict[str, Generator] = {}
    for station_spec in resources_to_allocate.values():
        for station_id, substation_list in station_spec.items():
            # If we don't yet have a generator for station beam TRLs for a
            # given station ID, make one.
            # We only do this if we haven't yet made one, such that we increment
            # correctly.
            if station_id not in trl_generator_dict:
                station_name = station_names[station_id - 1]
                trl_generator_dict[station_id] = _next_station_beam_trl(station_name)
            for _ in substation_list:
                station_beams.append(next(trl_generator_dict[station_id]))

    stations = []
    stations += [
        f"low-mccs/station/{station_names[station - 1]}"
        for stations in list(resources_to_allocate.values())
        for station in stations
    ]

    resources = json.loads(subarray.assignedResources)
    number_of_beams = len(subarray_beams)

    assert subarray.stationTrls == tuple(sorted(set(stations)))
    assert subarray.subarraybeamTrls == tuple(sorted(set(subarray_beams)))
    assert subarray.stationbeamTrls == tuple(sorted(set(station_beams)))
    assert resources["channels"] == [number_of_beams * number_of_channels]


@then("MccsController reports no resources allocated")
def check_controller_no_resources(
    controller: tango.DeviceProxy, wait_for_lrcs_to_finish: Callable
) -> None:
    """
    Check MccsController reports no resources allocated to the MccsSubarray.

    :param controller: MccsController device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    """
    wait_for_lrcs_to_finish()
    assert controller.GetAssignedResources(1) == "{}"


@then(
    parsers.parse(
        "The {station_beam_count} MccsStationBeam report correct configuration"
    )
)
# We can call this step for reconfiguration as the resources_configured dictionary
# will have been updated by reconfiguration.
@then(
    parsers.parse(
        "The {station_beam_count} MccsStationBeam report correct reconfiguration"
    )
)
def check_station_beam_configuration(
    station_beams: dict[str, list[tango.DeviceProxy]],
    resources_configured: dict,
    subarray_beams: list[tango.DeviceProxy],
    wait_for_lrcs_to_finish: Callable,
) -> None:
    """
    Check that MccsStationBeam reports correct configuration from MccsSubarray.

    :param station_beams: list of MccsStationBeam device proxies for use in the test.
    :param subarray_beams: list of MccsSubarrayBeam device proxies for use in the test.
    :param resources_configured: list of configured parameters from
        earlier configuration.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    """
    wait_for_lrcs_to_finish()
    for subarray_beam_config in resources_configured:
        subarray_beam_proxy = subarray_beams[
            subarray_beam_config["subarray_beam_id"] - 1
        ]
        for station_beam in _flatten(station_beams.values()):
            if station_beam.beamId in subarray_beam_proxy.stationbeamids:
                assert station_beam.UpdateRate == subarray_beam_config["update_rate"]
                assert list(station_beam.desiredpointing) == [
                    subarray_beam_config["field"]["attrs"]["c1"],
                    subarray_beam_config["field"]["attrs"]["c1_rate"],
                    subarray_beam_config["field"]["attrs"]["c2"],
                    subarray_beam_config["field"]["attrs"]["c2_rate"],
                ]
                assert (
                    station_beam.pointingReferenceFrame
                    == subarray_beam_config["field"]["attrs"]["reference_frame"]
                )
                assert (
                    station_beam.pointingTimeStamp
                    == subarray_beam_config["field"]["attrs"]["timestamp"]
                )


@then(
    "The 4 MccsStationBeam report unconfigured",
    converters={"station_beam_count": int},
)
def check_resources_unallocated(
    station_beams: dict[str, list[tango.DeviceProxy]],
    wait_for_lrcs_to_finish: Callable,
) -> None:
    """
    Check each MccsStationBeam not configurated after deconfiguration by MccsSubarray.

    :param station_beams: list of MccsStationBeam device proxies for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    """
    wait_for_lrcs_to_finish()
    for station_beam in _flatten(station_beams.values()):
        wait_for_lrcs_to_finish()
        assert station_beam.apertureId == "AP000.00"
        assert station_beam.subarrayId == 0


@when(
    parsers.parse("MccsSubarray {subarray_id} performs a Scan"),
    converters={"subarray_id": int},
)
def scan_step(
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> None:
    """
    Perform a Scan using the MccsSubarray.

    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    """
    subarray = subarrays[int(subarray_id) - 1]
    scan(
        subarray=subarray,
        wait_for_lrcs_to_finish=wait_for_lrcs_to_finish,
        wait_for_obsstate=wait_for_obsstate,
    )


@when(
    parsers.parse("MccsSubarray {subarray_id} ends its Scan"),
    converters={"subarray_id": int},
)
def end_scan_step(
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> None:
    """
    End a Scan performed by the MccsSubarray.

    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    """
    subarray = subarrays[int(subarray_id) - 1]
    end_scan(
        subarray=subarray,
        wait_for_lrcs_to_finish=wait_for_lrcs_to_finish,
        wait_for_obsstate=wait_for_obsstate,
    )


@then("The MccsTiles report that the beamformer is running")
def beamformer_running(tiles: dict[str, list[tango.DeviceProxy]]) -> None:
    """
    Verify the beamformer is running on the tile.

    :param tiles: the MccsTiles device proxies for use in the test.
    """
    for tile in _flatten(tiles.values()):
        assert tile.isBeamformerRunning


@then("The MccsTiles report that the beamformer is not running")
def beamformer_not_running(tiles: dict[str, list[tango.DeviceProxy]]) -> None:
    """
    Verify the beamformer is not running on the tile.

    :param tiles: the MccsTiles device proxies for use in the test.
    """
    for tile in _flatten(tiles.values()):
        assert not tile.isBeamformerRunning


@then(
    parsers.parse(
        "MccsStation {station_id_1} and {station_id_2} "
        "report the expected beamformer table"
    ),
    converters={"station_id_1": int, "station_id_2": int},
)
def check_station_beamformer_table(
    mccs_stations: dict[str, tango.DeviceProxy],
    station_id_1: int,
    station_id_2: int,
    station_names: list[str],
) -> None:
    """
    Verify the beamformer tables of the MccsStation devices.

    The first station is the one with sources assigned and with an SpsStation.

    :param mccs_stations: dict of MccsStation device proxies for use in the test.
    :param station_id_1: id of station 1
    :param station_id_2: id of station 2
    :param station_names: stations used in the test.
    """
    entries_per_substation = 8
    bft0 = np.array(
        [
            [
                i,
                2 if i % entries_per_substation <= 1 else 0,
                i // entries_per_substation,
                1 if i % entries_per_substation <= 1 else 0,
                (
                    entries_per_substation * i
                    if i < entries_per_substation
                    else entries_per_substation * (i - entries_per_substation)
                ),
                1 + (i // (2 * entries_per_substation)),
                [2, 3, 1][i // entries_per_substation],
                [102, 103, 101][i // entries_per_substation],
            ]
            for i in range(18)
        ]
    )
    bft1 = np.array([[0, 2, 0, 1, 64, 2, 1, 201], [1, 2, 0, 1, 72, 2, 1, 201]])
    station_name_1 = station_names[station_id_1 - 1]
    station_name_2 = station_names[station_id_2 - 1]
    assert np.all(mccs_stations[station_name_1].beamformerTable == bft0)
    assert np.all(mccs_stations[station_name_2].beamformerTable == bft1)


@then(
    parsers.parse(
        "MccsStation {station_id_1} and {station_id_2} "
        "report an empty beamformer table"
    ),
    converters={"station_id_1": int, "station_id_2": int},
)
def check_empty_station_beamformer_table(
    mccs_stations: dict[str, tango.DeviceProxy],
    station_id_1: int,
    station_id_2: int,
    station_names: list[str],
) -> None:
    """
    Verify that the MccsStations have empty beamformer tables.

    :param mccs_stations: dict of MccsStation device proxies for use in the test.
    :param station_id_1: id of station 1
    :param station_id_2: id of station 2
    :param station_names: stations used in the test.
    """
    station_name_1 = station_names[station_id_1 - 1]
    station_name_2 = station_names[station_id_2 - 1]
    assert mccs_stations[station_name_1].beamformerTable.size == 0
    assert mccs_stations[station_name_2].beamformerTable.size == 0


@then(
    parsers.parse("SpsStation {station_id} reports the expected beamformer table"),
    converters={"station_id": int},
)
def check_sps_station_beamformer_table(
    sps_stations: dict[str, tango.DeviceProxy],
    station_id: int,
    station_names: list[str],
) -> None:
    """
    Verify the beamformer table of the SpsStation.

    :param sps_stations: SpsStation device proxy for use in the test.
    :param station_id: id of the station.
    :param station_names: stations used in the test.
    """
    station_name = station_names[station_id - 1]
    sps_station = sps_stations[station_name]
    entries_per_substation = 8
    bft = np.array(
        list(
            itertools.chain(
                *[
                    [
                        2 if i % entries_per_substation <= 1 else 0,
                        i // entries_per_substation,
                        1 if i % entries_per_substation <= 1 else 0,
                        (
                            entries_per_substation * i
                            if i < entries_per_substation
                            else entries_per_substation * (i - entries_per_substation)
                        ),
                        1 + (i // (2 * entries_per_substation)),
                        [2, 3, 1][i // entries_per_substation],
                        [102, 103, 101][i // entries_per_substation],
                    ]
                    for i in range(24)
                ]
            )
        )
        + [0] * 24 * 7
    )
    assert np.all(sps_station.beamformerTable == bft)


@then(
    parsers.parse("The MccsTile on {station_id} reports the expected beamformer table"),
    converters={"station_id": int},
)
def check_tile_beamformer_table(
    tiles: dict[str, list[tango.DeviceProxy]],
    station_id: int,
    station_names: list[str],
) -> None:
    """
    Verify the beamformer table of the MccsTile.

    :param tiles: the MccsTiles device proxies for use in the test.
    :param station_id: the station that the MccsTile is on.
    :param station_names: names of the stations in the test.
    """
    station_name = station_names[station_id - 1]
    tile = tiles[station_name][0]
    bft = np.array(
        list(
            itertools.chain(
                *[
                    [
                        2,
                        i // 2,
                        1,
                        [0, 8, 0, 8, 64, 72][i],
                        1 + (i // 4),
                        [2, 2, 3, 3, 1, 1][i],
                        [102, 102, 103, 103, 101, 101][i],
                    ]
                    for i in range(6)
                ]
            )
        )
        + [0] * 42 * 7
    )
    assert np.all(tile.beamformerTable == bft)
