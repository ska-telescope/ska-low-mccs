# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains obs functional test bdd steps of the ska-low-mccs project."""
from __future__ import annotations

from typing import Callable

import tango
from pytest_bdd import given, parsers, when
from ska_control_model import ObsState

from tests.functional.helpers.obs_helpers import (
    _flatten,
    abort,
    allocate_resources,
    configure_resources,
    deconfigure_resources,
    end_scan,
    release_resources,
    scan,
)


@given(
    parsers.parse(
        "MccsController has not allocated resources to MccsSubarray {subarray_id}"
    ),
    converters={
        "subarray_id": int,
    },
)
def check_resources_not_allocated(
    controller: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> None:
    """
    Check that MccsController has not allocated resources, if it has, release them.

    :param controller: MccsController device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    """
    subarray = subarrays[int(subarray_id) - 1]
    if subarray.obsState == ObsState.READY:
        deconfigure_resources(subarray, wait_for_lrcs_to_finish, wait_for_obsstate)
    if controller.GetAssignedResources(subarray_id) != "{}":
        print("Controller has resources assigned, releasing resources")
        wait_for_lrcs_to_finish()
        release_resources(
            controller=controller,
            wait_for_lrcs_to_finish=wait_for_lrcs_to_finish,
            wait_for_obsstate=wait_for_obsstate,
            subarray=subarray,
        )
        wait_for_lrcs_to_finish()
    assert controller.GetAssignedResources(subarray_id) == "{}"
    print("Controller has released resources, continuing with test")


@given(
    parsers.parse(
        "MccsController has allocated resources to MccsSubarray {subarray_id}"
    ),
    target_fixture="resources_allocated",
    converters={"subarray_id": int},
)
def check_resources_allocated(
    controller: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    schema_compliant_allocate: dict,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> dict:
    """
    Check that MccsController has allocated resources, if it hasn't, allocate them.

    :param controller: MccsController device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param schema_compliant_allocate: fixture for schema compliant allocate argument.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.

    :returns: resources allocated if it was needed.
    """
    subarray = subarrays[int(subarray_id) - 1]
    if subarray.obsState == ObsState.SCANNING:
        print("Subarray is scanning, ending scan")
        end_scan(subarray, wait_for_lrcs_to_finish, wait_for_obsstate)
    if controller.GetAssignedResources(subarray_id) == "{}":
        print("Controller has no resources assigned, assigning resources")
        wait_for_lrcs_to_finish()
        resources_allocated = allocate_resources(
            controller=controller,
            wait_for_lrcs_to_finish=wait_for_lrcs_to_finish,
            wait_for_obsstate=wait_for_obsstate,
            schema_compliant_allocate=schema_compliant_allocate,
            subarray=subarray,
        )
        wait_for_lrcs_to_finish()
        assert controller.GetAssignedResources(subarray_id) != "{}"
        return resources_allocated
    assert controller.GetAssignedResources(subarray_id) != "{}"
    print("Controller has assigned resources, continuing with test")
    return schema_compliant_allocate


@given(
    parsers.parse(
        "The {station_beam_count} MccsStationBeam confirm MccsSubarray {subarray_id} "
        "has configured the {subarray_beam_count} MccsSubarrayBeam"
    ),
    converters={
        "subarray_beam_count": int,
        "subarray_id": int,
        "station_beam_count": int,
    },
)
def check_configured_resources(  # pylint: disable=too-many-arguments
    subarray_beam_count: int,
    station_beams: dict[str, list[tango.DeviceProxy]],
    station_beam_count: int,
    resources_allocated: dict,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> None:
    """
    Check that the subarray has configured its resources.

    This function checks the subarray has configured its resources, and
    configures them if not.

    :param subarray_beam_count: amonut of MccsSubarrayBeam used in this test.
    :param station_beams: list of MccsStationBeam device proxies for use in the test.
    :param station_beam_count: amount of MccsStationBeams used in this test.
    :param resources_allocated: schema compliant fixture holding resources
        allocated earlier.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    """
    subarray = subarrays[int(subarray_id) - 1]
    if subarray.obsState == ObsState.SCANNING:
        print("Subarray is scanning, ending scan")
        end_scan(subarray, wait_for_lrcs_to_finish, wait_for_obsstate)
    is_configured = False
    for station_beam in _flatten(list(station_beams.values())):
        wait_for_lrcs_to_finish()
        # Check a config parameter has arrived at station beams
        if station_beam.UpdateRate != 0:
            is_configured = True
            break
    if not is_configured:
        print("Subarray is not configured, configuring subarray")
        configure_resources(
            subarray=subarray,
            subarray_beam_count=subarray_beam_count,
            resources_allocated=resources_allocated,
            wait_for_lrcs_to_finish=wait_for_lrcs_to_finish,
            wait_for_obsstate=wait_for_obsstate,
        )
    for station_beam in _flatten(list(station_beams.values())):
        # Check a config parameter has arrived at station beams

        # We expect ci-2-02 to still be empty as we have not allocated/configured it
        if station_beam.dev_name() == "low-mccs/beam/ci-2-02":
            wait_for_obsstate(ObsState.EMPTY, station_beam)
        else:
            wait_for_obsstate(ObsState.READY, station_beam)
            assert station_beam.UpdateRate != 0

    print("Subarray has been configured, continuing with test")


@given(
    parsers.parse("MccsSubarray {subarray_id} is performing a Scan"),
    converters={"subarray_id": int},
)
def given_scan(
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> None:
    """
    Perform a Scan using the MccsSubarray.

    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    """
    subarray = subarrays[int(subarray_id) - 1]
    if subarray.obsState != ObsState.SCANNING:
        scan(subarray, wait_for_lrcs_to_finish, wait_for_obsstate)
    assert subarray.obsState == ObsState.SCANNING


@given(
    parsers.parse("MccsSubarray {subarray_id} is aborted"),
    converters={"subarray_id": int},
)
def check_aborted(
    subarrays: list[tango.DeviceProxy],
    subarray_id: int,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
) -> None:
    """
    Check that the subarray has been aborted.

    This function checks the subarray has aborted, and
    aborts if not.

    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    subarray = subarrays[subarray_id - 1]
    if subarray.obsstate != ObsState.ABORTED:
        print("Subarray is not aborted, aborting...")
        abort(
            subarray=subarrays[subarray_id - 1],
            wait_for_lrcs_to_finish=wait_for_lrcs_to_finish,
            wait_for_obsstate=wait_for_obsstate,
        )
    print("Subarray is aborted, continuing...")


@given(
    parsers.parse("MccsSubarray {subarray_id} is faulted"),
    converters={"subarray_id": int},
)
def check_faulted(
    subarrays: list[tango.DeviceProxy],
    subarray_id: int,
    station_beams: dict[str, list[tango.DeviceProxy]],
    wait_for_obsstate: Callable,
) -> None:
    """
    Check that the subarray has faulted.

    This function checks the subarray has faulted, and
    faults if not.

    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.
    :param station_beams: dictionary of StationBeams present.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    """
    subarray = subarrays[subarray_id - 1]
    if subarray.obsstate != ObsState.FAULT:
        print("Subarray is not faulted, faulting...")
        for station_beam in _flatten(station_beams.values()):
            station_beam.ToFault()
            wait_for_obsstate(ObsState.FAULT, station_beam)
            wait_for_obsstate(ObsState.FAULT, subarray)
            return
    print("Subarray is faulted, continuing...")


@when(
    parsers.parse("MccsController allocates resources to MccsSubarray {subarray_id}"),
    converters={"subarray_id": int},
    target_fixture="resources_allocated_step",
)
def allocate_resources_step(
    controller: tango.DeviceProxy,
    wait_for_lrcs_to_finish: Callable,
    wait_for_obsstate: Callable,
    schema_compliant_allocate: dict,
    subarray_id: int,
    subarrays: list[tango.DeviceProxy],
) -> dict:
    """
    Allocate resources according to values given by resources_to_allocate.

    :param controller: MccsController device proxy for use in the test.
    :param wait_for_lrcs_to_finish: callable which waits
        for lrcs on our devices to finish.
    :param wait_for_obsstate: callable which asserts we will change to given obsState,
        then waits for obsState to change.
    :param schema_compliant_allocate: fixture for schema compliant allocate argument.
    :param subarray_id: The 1-indexed id of the subarray to use in this test.
    :param subarrays: A 0-indexed list of Subarrays present.

    :returns: resources allocated by MccsController.
    """
    subarray = subarrays[int(subarray_id) - 1]
    return allocate_resources(
        controller=controller,
        wait_for_lrcs_to_finish=wait_for_lrcs_to_finish,
        wait_for_obsstate=wait_for_obsstate,
        schema_compliant_allocate=schema_compliant_allocate,
        subarray=subarray,
    )
