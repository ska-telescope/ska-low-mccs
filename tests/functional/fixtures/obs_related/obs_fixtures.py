# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains obs functional test fixtures of the ska-low-mccs project."""
from __future__ import annotations

import pytest


@pytest.fixture(name="schema_compliant_allocate")
def schema_compliant_allocate_fixture(
    resources_to_allocate: dict, number_of_channels: int
) -> dict:
    """
    Generate a schema compliant dictionary to pass to MccsController.Allocate().

    :param resources_to_allocate: non-compliant dictionary of resources to allocate.
    :param number_of_channels: number of channels to allocate.

    :returns: schema-compliant dictionary
    """
    subarray_beams = []
    for subarray_beam_id in list(resources_to_allocate.keys()):
        apertures = []
        for station_id in list(resources_to_allocate[subarray_beam_id].keys()):
            for substation_id in resources_to_allocate[subarray_beam_id][station_id]:
                apertures.append(
                    {
                        "station_id": station_id,
                        "aperture_id": "AP"
                        + f"{station_id:03}"
                        + f".{substation_id:02}",
                    },
                )
        subarray_beams.append(
            {
                "subarray_beam_id": subarray_beam_id,
                "apertures": apertures,
                "number_of_channels": number_of_channels,
            }
        )
    return {
        "interface": r"https://schema.skao.int/ska-low-mccs-controller-allocate/3.0",
        "subarray_id": 1,
        "subarray_beams": subarray_beams,
    }


@pytest.fixture(name="resources_to_allocate")
def resources_to_allocate_fixture() -> dict:
    """
    Generate non-compliant dictionary of resources to allocate.

    This fixture is a relic of previous schemas. It is here primarily
    for future expansion of the tests, if needed this shorthand can be passed
    directly into the tests from feature files.

    Schema-compliant dictionary is producted in schema_compliant_allocate_fixture.

    Of the form:
        {subarray_beam: {station: [substations]}}

    :returns: non-compliant dictionary of resources to allocate.
    """
    return {1: {1: [2, 3]}, 2: {1: [1], 2: [1]}}


@pytest.fixture(name="number_of_channels")
def number_of_channels_fixture() -> int:
    """
    Return a fixture for number_of_channels property of allocate.

    :return: a fixture for number_of_channels property of allocate.
    """
    return 64


@pytest.fixture(name="resources_allocated")
def resources_allocated_fixture() -> dict:
    """
    Return an empty dictionary for use in testing to hold allocated resources.

    :return: an empty dictionary for use in testing to hold allocated resources.
    """
    return {}
