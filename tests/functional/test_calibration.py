# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests of the calibration system."""
# pylint: disable=too-many-lines
from __future__ import annotations

import json
import os
import time
from dataclasses import asdict, dataclass
from typing import Any, Optional

import pytest
import tango
from psycopg import sql
from psycopg.rows import dict_row
from psycopg_pool import ConnectionPool
from pytest_bdd import given, parsers, scenario, then, when
from ska_control_model import ResultCode
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from tests.harness import (
    get_calibration_store_trl,
    get_field_station_trl,
    get_fndh_trl,
    get_pasdbus_trl,
    get_smartbox_trl,
    get_station_calibrator_trl,
    get_station_trl,
)

NUMBER_OF_SMARTBOX = 24


# pylint: disable=too-many-instance-attributes
@dataclass
class OldDatabaseSolution:
    """
    This class hold the old database solution.

    Used for regression testing.

    This class holds a solution and offers some notion
    of checking types and mandatory keys before attempting to
    load into the tab_mccs_calib table.
    """

    frequency_channel: int  # NOT_NULL in database.
    station_id: int  # NOT_NULL in database.
    preferred: bool  # NOT_NULL in database.
    calibration_path: str  # NOT_NULL in database.
    outside_temperature: float  # NOT_NULL in database.

    def generate_loading_instruction(self) -> tuple[sql.Composed, tuple]:
        """
        Generate instruction needed to load into tab_mccs_calib.

        :return: Templated information to load into database.
            templating here will improve security against sql
            injection attacks.
        """
        # Sort the columns into 2 lists.
        # This is mainly done for ease of testsing.
        column_names, values = zip(*sorted(asdict(self).items()))

        # Generate placeholders and column identifiers dynamically
        placeholders = [sql.Placeholder()] * len(column_names)
        column_identifiers = [sql.Identifier(col) for col in column_names]

        query = sql.SQL(
            "INSERT INTO tab_mccs_calib "
            "(creation_time, {}) VALUES (current_timestamp, {})"
        ).format(
            sql.SQL(", ").join(column_identifiers),
            sql.SQL(", ").join(
                placeholders
            ),  # Use placeholders for parameterized queries, sql injection security.
        )
        return query, values


# pylint: disable=too-many-instance-attributes
@dataclass
class DatabaseSolution:
    """
    Class to hold a solution.

    This class holds a soltion and offers some notion
    of checking types and mandatory keys before attempting to
    load into the tab_mccs_calib table.
    """

    acquisition_time: float  # NOT_NULL in database.
    frequency_channel: int  # NOT_NULL in database.
    station_id: int  # NOT_NULL in database.
    preferred: bool  # NOT_NULL in database.
    solution: list[float]  # NOT_NULL in database.
    calibration_path: str  # NOT_NULL in database.
    outside_temperature: float | None = None
    corrcoeff: list[float] | None = None
    residual_max: list[float] | None = None
    residual_std: list[float] | None = None
    xy_phase: list[float] | None = None
    n_masked_initial: int | None = None
    n_masked_final: int | None = None
    lst: float | None = None
    galactic_centre_elevation: float | None = None
    sun_elevation: float | None = None
    sun_adjustment_factor: float | None = None
    masked_antennas: list[int] | None = None

    def generate_loading_instruction(self) -> tuple[sql.Composed, tuple]:
        """
        Generate instruction needed to load into tab_mccs_calib.

        :return: Templated information to load into database.
            templating here will improve security against sql
            injection attacks.
        """
        # Sort the columns into 2 lists.
        # This is mainly done for ease of testsing.
        column_names, values = zip(*sorted(asdict(self).items()))

        # Generate placeholders and column identifiers dynamically
        placeholders = [sql.Placeholder()] * len(column_names)
        column_identifiers = [sql.Identifier(col) for col in column_names]

        query = sql.SQL(
            "INSERT INTO tab_mccs_calib "
            "(creation_time, {}) VALUES (current_timestamp, {})"
        ).format(
            sql.SQL(", ").join(column_identifiers),
            sql.SQL(", ").join(
                placeholders
            ),  # Use placeholders for parameterized queries, sql injection security.
        )
        return query, values


@scenario(
    "features/calibration.feature",
    "Store a calibration solution",
)
def test_storing_solution(
    database_connection_pool: ConnectionPool, solutions_path: str
) -> None:
    """
    Test storing a calibration solution.

    Check that when a solution is sent to the MccsCalibrationStore, the solution
    is stored in the database.

    Bear in mind that data in the database will persist between test runs if not
    cleaned up, so here we remove any data from the table.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    empty_table(database_connection_pool, solutions_path)


@scenario(
    "features/calibration.feature",
    "Add a calibration solution to existing table",
)
def test_add_to_existing_table(
    database_connection_pool: ConnectionPool, solutions_path: str
) -> None:
    """
    Test storing a calibration solution when there's already data.

    Check that when a solution is sent to the MccsCalibrationStore, the solution
    is stored in the database, without overwriting existing data.

    Bear in mind that data in the database will persist between test runs if not
    cleaned up, so here we remove any data from the table.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    empty_table(database_connection_pool, solutions_path)


@scenario(
    "features/calibration.feature",
    "Load a non-existent calibration solution",
)
def test_loading_non_existent_solution(
    database_connection_pool: ConnectionPool,
    solutions_path: str,
) -> None:
    """
    Test retrieving a calibration solution for inputs that don't have a solution.

    This should return an empty array to indicate the solution doesn't exist.

    Bear in mind that data in the database will persist between test runs if not
    cleaned up, so here we remove any data from the table.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    empty_table(database_connection_pool, solutions_path)


@scenario(
    "features/calibration.feature",
    "Load a calibration solution",
)
def test_loading_solution(
    database_connection_pool: ConnectionPool, solutions_path: str
) -> None:
    """
    Test retrieving a calibration solution.

    Check that when the station calibrator tries to retrieve an existing solution,
    the correct solution is returned.

    Bear in mind that data in the database will persist between test runs if not
    cleaned up, so here we remove any data from the table.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    empty_table(database_connection_pool, solutions_path)


@scenario(
    "features/calibration.feature",
    "Test update database.",
)
def test_update_database(
    database_connection_pool: ConnectionPool,
    solutions_path: str,
) -> None:
    """
    Test the regression of new database changes.

    Check that we have a seemless update to the database
    when mccs is deployed to a platform with a persistent
    database.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    empty_table(database_connection_pool, solutions_path)


@scenario(
    "features/calibration.feature",
    "Load a calibration solution with multiple available",
)
def test_loading_solution_multiple_available(
    database_connection_pool: ConnectionPool,
    solutions_path: str,
) -> None:
    """
    Test retrieving a calibration solution, when there are multiple available.

    Check that when the station calibrator tries to retrieve an existing solution,
    the most recently stored solution is returned.

    Bear in mind that data in the database will persist between test runs if not
    cleaned up, so here we remove any data from the table.

    Any code in this scenario method is run at the *end* of the
    scenario.

    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    empty_table(database_connection_pool, solutions_path)


def empty_table(database_connection_pool: ConnectionPool, solutions_path: str) -> None:
    """
    Empty the table from leftover data from previous test runs.

    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    for file in os.listdir(solutions_path):
        if file.endswith("cal_solutions.npy"):
            file_path = os.path.join(solutions_path, file)
            os.unlink(file_path)
    with database_connection_pool.connection() as cx:
        cx.execute("DELETE FROM tab_mccs_calib")


@pytest.fixture(name="solutions_path")
def solution_path_fixture() -> str:
    """
    Return the path for the artefact repository for calibration solutions.

    :return: artefact repository path
    """
    return "/test-data"


@pytest.fixture(name="database_solution")
def database_solution_fixture(station_id: int) -> DatabaseSolution:
    """
    Return an example solution for this station.

    :param station_id: the id of the station under test.

    :return: A dataclass `DatabaseSolution` with a solution
        ready for storage.
    """
    solution_path = "tests/data/calibration_solution_data.json"
    with open(solution_path, "r", encoding="utf-8") as file:
        solution_data = json.load(file)

    # modify the data station_id to match this station.
    solution_data["station_id"] = station_id

    return DatabaseSolution(**solution_data)


# pylint: disable=inconsistent-return-statements
def poll_until_attribute_has_value(
    device: tango.DeviceProxy, attribute: str, no_of_iters: int = 5
) -> None:
    """
    Poll until attribute on device can be read.

    This function recursively calls itself up to `no_of_iters` times.

    :param device: the Tango device
    :param attribute: the attribute we want to read
    :param no_of_iters: number of times to iterate

    :return: none
    """
    try:
        getattr(device, attribute)
    except tango.DevFailed:
        if no_of_iters == 1:
            pytest.fail(f"Attribute {attribute} did not get a value in time.")
        time.sleep(1)
        return poll_until_attribute_has_value(device, attribute, no_of_iters - 1)
    return


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture(
    station_names: list[str],
) -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of callables to be used as Tango change event callbacks.

    :param station_names: the list of names to the stations under test.

    :return: a dictionary of callables to be used as tango change event
        callbacks.
    """
    station_label = station_names[0]
    pasd_bus_name = get_pasdbus_trl(station_label)
    fndh_name = get_fndh_trl(station_label)
    calibration_store_name = get_calibration_store_trl(station_label)
    station_calibrator_name = get_station_calibrator_trl(station_label)
    field_station_name = get_field_station_trl(station_label)
    mccs_station_name = get_station_trl(station_label)

    attribute_list = [
        f"{pasd_bus_name}/state",
        f"{pasd_bus_name}/healthState",
        f"{pasd_bus_name}/adminMode",
        f"{fndh_name}/state",
        f"{fndh_name}/healthState",
        f"{fndh_name}/adminMode",
        f"{calibration_store_name}/state",
        f"{calibration_store_name}/healthState",
        f"{calibration_store_name}/adminMode",
        f"{station_calibrator_name}/state",
        f"{station_calibrator_name}/healthState",
        f"{station_calibrator_name}/adminMode",
        f"{mccs_station_name}/state",
        f"{mccs_station_name}/healthState",
        f"{mccs_station_name}/adminMode",
        f"{mccs_station_name}/outsideTemperature",
        f"{field_station_name}/state",
        f"{field_station_name}/healthState",
        f"{field_station_name}/adminMode",
        f"{field_station_name}/outsideTemperature",
        f"{field_station_name}/antennaPowerStates",
    ]
    # Add smartboxes.
    for smartbox_id in range(1, NUMBER_OF_SMARTBOX + 1):
        attribute_list.append(f"{get_smartbox_trl(station_label, smartbox_id)}/state")
        attribute_list.append(
            f"{get_smartbox_trl(station_label, smartbox_id)}/healthState"
        )
        attribute_list.append(
            f"{get_smartbox_trl(station_label, smartbox_id)}/adminMode"
        )

    return MockTangoEventCallbackGroup(
        *attribute_list,
        timeout=30.0,
        assert_no_error=False,
    )


@given(
    parsers.parse("MccsPasdBus {station_id} is initialised"),
    converters={"station_id": int},
)
def pasdbus_initialised(
    pasdbuses: dict[str, tango.DeviceProxy], station_id: int, station_names: list[str]
) -> None:
    """
    Initialise the pasd.

    :param pasdbuses: dict of MccsPasdBuses under test.
    :param station_id: id of the station.
    :param station_names: names of the stations in the test.
    """
    station_name = station_names[station_id - 1]
    pasd_bus_device = pasdbuses[station_name]
    pasd_bus_device.initializefndh()


@pytest.fixture(name="database_connection_pool")
def database_connection_pool_fixture(
    true_context: bool,
    database_host: str,
    database_port: int,
    database_name: str,
    database_admin_user: str,
    database_admin_password: str,
) -> ConnectionPool:
    """
    Get a connection pool for the database.

    :param true_context: whether to test against an existing Tango deployment
    :param database_host: the database host
    :param database_port: the database port
    :param database_name: the database name
    :param database_admin_user: the database admin user
    :param database_admin_password: the database admin password
    :return: a connection pool for the database
    """
    if not true_context:
        pytest.xfail(
            "Functional testing of the calibration store requires a "
            "full deployment including a database"
        )
    conninfo = (
        f"host={database_host} "
        f"port={database_port} "
        f"dbname={database_name} "
        f"user={database_admin_user} "
        f"password={database_admin_password}"
    )
    connect_kwargs = {"row_factory": dict_row}

    return ConnectionPool(conninfo, kwargs=connect_kwargs)


@pytest.fixture(name="alternative_calibration_solutions")
def alternative_calibration_solutions_fixture(
    frequency_channel: int,
    outside_temperature: float,
    station_id: int,
) -> dict[tuple[int, float, int], list[float]]:
    """
    Fixture that provides alternative sample calibration solutions.

    This is used to test for when there are multiple solutions for the same inputs,
    that the correct one is retrieved.

    :param station_id: a fixture with the station_id.
    :param outside_temperature: a fixture with an outside temperature
    :param frequency_channel: a fixture with a calibration frequency channel

    :return: a sample calibration solution. The keys are tuples of the channel
        and the outside temperature, and the values are lists of calibration values
    """
    return {
        (frequency_channel, outside_temperature, station_id): [float(1)]
        + [0.5 * i for i in range(8)],
        (23, 25.0, station_id): [float(4)] + [0.8 * i for i in range(8)],
        (45, 25.0, station_id): [float(7)] + [1.4 * (i % 2) for i in range(8)],
        (23, 30.0, station_id): [float(1)] + [0.1 * i for i in range(8)],
        (45, 30.0, station_id): [float(2)] + [1.1 * (i % 2) for i in range(8)],
        (23, 35.0, station_id): [float(4)] + [0.9 * i for i in range(8)],
        (45, 35.0, station_id): [float(8)] + [1.4 * (i % 2) for i in range(8)],
    }


@pytest.fixture(name="unused_outside_temperature")
def unused_outside_temperature_fixture() -> float:
    """
    Fixture for a not calibrated-for outside temperature.

    :return: the outside temperature
    """
    return 15.0


@pytest.fixture(name="unused_frequency_channel")
def unused_frequency_channel_fixture() -> int:
    """
    Fixture for a not calibrated-for calibration frequency channel.

    :return: the frequency channel
    """
    return 13


@given("the database table is initially empty")
def given_initial_empty_table(
    database_connection_pool: ConnectionPool, solutions_path: str
) -> None:
    """
    Empty the table from leftover data from previous test runs.

    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    empty_table(database_connection_pool, solutions_path)


@given("the calibration store database contains calibration solutions")
def given_database_has_solutions(
    calibration_solutions: dict[tuple[int, float, int], list[float]],
    database_connection_pool: ConnectionPool,
    solutions_path: str,
) -> None:
    """
    Populate the database with test data.

    :param calibration_solutions: the calibration solutions to store
    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    empty_table(database_connection_pool, solutions_path)

    with database_connection_pool.connection() as cx:
        for channel, temperature, station_id in calibration_solutions:
            solution = calibration_solutions[(channel, temperature, station_id)]
            mocked_solution = DatabaseSolution(
                frequency_channel=channel,
                outside_temperature=temperature,
                station_id=station_id,
                acquisition_time=time.time(),
                preferred=True,
                calibration_path="some/path/to/correlation-matrix.hdf5",
                solution=solution,
            )
            query, params = mocked_solution.generate_loading_instruction()
            cx.execute(
                query,
                params,
            )


@given("the calibration store is set to closest_in_range")
def given_calibration_configured_for_closest_in_range(
    calibration_stores: dict[str, tango.DeviceProxy],
    station_id: int,
    station_names: list[str],
) -> None:
    """
    Ensure selection policy is closest_in_range.

    :param calibration_stores: proxies to the calibration stores
    :param station_id: id of the station.
    :param station_names: names of the station in the test.
    """
    station_name = station_names[station_id - 1]
    calibration_store = calibration_stores[station_name]
    result_code: str
    message: str
    [result_code], [message] = calibration_store.UpdateSelectionPolicy(
        json.dumps(
            {
                "policy_name": "closest_in_range",
                "frequency_tolerance": 0,
                "temperature_tolerance": 0,
            }
        )
    )
    assert result_code == ResultCode.OK, message


@given(
    "the calibration store database contains multiple calibration solutions "
    "for the same inputs"
)
def given_database_has_multiple_solutions(
    calibration_solutions: dict[tuple[int, float, int], list[float]],
    alternative_calibration_solutions: dict[tuple[int, float, int], list[float]],
    database_connection_pool: ConnectionPool,
    solutions_path: str,
) -> None:
    """
    Populate the database with multiple solutions for the same inputs.

    :param calibration_solutions: the calibration solutions to store
    :param alternative_calibration_solutions: the alternative calibration
        solutions to store
    :param database_connection_pool: connection pool for the database
    :param solutions_path: the path for the artefact repository
        for calibration solutions
    """
    empty_table(database_connection_pool, solutions_path)

    with database_connection_pool.connection() as cx:
        for channel, temperature, station_id in calibration_solutions:
            solution = calibration_solutions[(channel, temperature, station_id)]
            mocked_solution = DatabaseSolution(
                frequency_channel=channel,
                outside_temperature=temperature,
                station_id=station_id,
                acquisition_time=time.time(),
                preferred=True,
                calibration_path="some/path/to/correlation-matrix.hdf5",
                solution=solution,
            )
            query, params = mocked_solution.generate_loading_instruction()
            cx.execute(
                query,
                params,
            )
            cx.commit()

        for channel, temperature, station_id in alternative_calibration_solutions:
            solution = alternative_calibration_solutions[
                (channel, temperature, station_id)
            ]
            mocked_solution = DatabaseSolution(
                frequency_channel=channel,
                outside_temperature=temperature,
                station_id=station_id,
                acquisition_time=time.time(),
                preferred=True,
                calibration_path="some/path/to/correlation-matrix.hdf5",
                solution=solution,
            )
            query, params = mocked_solution.generate_loading_instruction()
            cx.execute(
                query,
                params,
            )


@given(
    parsers.parse("Station {station_id} has read the outside temperature"),
    target_fixture="outside_temperature",
    converters={"station_id": int},
)
def given_station_has_read_temperature(
    mccs_stations: dict[str, tango.DeviceProxy],
    station_id: int,
    station_names: list[str],
) -> None:
    """
    Make the mock field station device push a change in the outside temperature.

    :param mccs_stations: list of Stations under test.
    :param station_id: id of the station.
    :param station_names: names of the station in the test.

    :return: the outside temperature as reported by fieldstation.
    """
    station_name = station_names[station_id - 1]
    station_device = mccs_stations[station_name]
    # Poll until the outside temperature has been polled by the PaSD bus.
    max_polls = 300
    poll_until_attribute_has_value(station_device, "OutsideTemperature", max_polls)
    return station_device.outsideTemperature


@when(
    parsers.parse(
        "MccsCalibrationStore {station_id} is given a solution to store",
    ),
    converters={"station_id": int},
)
def when_store_solution(
    calibration_stores: dict[str, tango.DeviceProxy],
    station_id: int,
    station_names: list[str],
    database_solution: Any,
) -> None:
    """
    Use the Calibration Store device to store a solution.

    :param calibration_stores: proxies to the calibration stores
    :param station_id: id of the station.
    :param database_solution: an example solution for this station.
    :param station_names: names of the station in the test.
    """
    station_name = station_names[station_id - 1]
    calibration_store = calibration_stores[station_name]
    database_solution.preferred = True
    [result_code], [message] = calibration_store.StoreSolution(
        json.dumps(asdict(database_solution))
    )
    assert result_code == ResultCode.OK
    assert message == "Solution stored successfully"


@then("the solution is stored in the database")
def then_solution_stored(
    station_id: int,
    outside_temperature: float,
    frequency_channel: int,
    database_connection_pool: ConnectionPool,
    database_solution: Any,
) -> None:
    """
    Verify the database contains the intended data.

    :param station_id: a fixture with the station_id.
    :param outside_temperature: the outside temperature
    :param frequency_channel: the frequency channel
    :param database_solution: an example solution for this station.
    :param database_connection_pool: connection pool for the database
    """
    with database_connection_pool.connection() as cx:
        query = sql.SQL(
            "SELECT frequency_channel, outside_temperature, "
            "calibration_path, creation_time, station_id, preferred, "
            "solution "
            "FROM tab_mccs_calib "
            "WHERE station_id = %s "
            "AND preferred = TRUE "
            "ORDER BY creation_time DESC"
        )

        result = cx.execute(query, (station_id,))
        # fetchone to get the most recent
        row: Optional[dict[str, Any]] = result.fetchone()  # type: ignore[assignment]
        if row is None:
            pytest.fail(
                "Solution not found in database for "
                f"channel {frequency_channel}, temperature {outside_temperature}"
            )
        assert row["solution"] == database_solution.solution


@then("existing data is not overwritten")
def then_existing_data_not_overwritten(
    calibration_solutions: dict[tuple[int, float, int], list[float]],
    outside_temperature: float,
    station_id: int,
    frequency_channel: int,
    database_connection_pool: ConnectionPool,
    database_solution: Any,
) -> None:
    """
    Verify the database still contains existing data.

    :param station_id: a fixture with the station_id.
    :param calibration_solutions: the initially stored solutions
    :param outside_temperature: the outside temperature
    :param frequency_channel: the frequency channel
    :param database_connection_pool: connection pool for the database
    :param database_solution: an example solution for this station.
    """
    with database_connection_pool.connection() as cx:
        query = sql.SQL(
            "SELECT frequency_channel, outside_temperature, "
            "calibration_path, creation_time, station_id, preferred, solution "
            "FROM tab_mccs_calib "
            "WHERE station_id = %s "
            "AND preferred = TRUE "
            "ORDER BY creation_time DESC"
        )
        result = cx.execute(query, (station_id,))
        # Fetch the first 2 rows
        rows: Any = result.fetchmany(2)

        # Retrieve solutions, using .get() to avoid KeyError
        solution_1 = rows[0].get("solution") if len(rows) > 0 else None
        solution_2 = rows[1].get("solution") if len(rows) > 1 else None

        # Fail if either solution is None
        if solution_1 is None or solution_2 is None:
            pytest.fail("One or both solutions are missing.")

        assert solution_1 == pytest.approx(database_solution.solution)
        last_key = list(calibration_solutions.keys())[0]  # Get the last key
        last_value = calibration_solutions[last_key]  # Get the corresponding value
        assert solution_2 == pytest.approx(last_value)


@when(
    parsers.parse(
        "MccsStationCalibrator {station_id} tries to get a calibration solution"
    ),
    target_fixture="retrieved_solution",
    converters={"station_id": int},
)
def when_get_calibration(
    station_calibrators: dict[str, tango.DeviceProxy],
    station_id: int,
    station_names: list[str],
    frequency_channel: int,
) -> list[float]:
    """
    Retrieve a calibration solution using the station calibrator.

    :param station_calibrators: proxies to the station calibrators
    :param station_id: id of the station.
    :param station_names: names of the station in the test.
    :param frequency_channel: the frequency channel to get a solution for
    :return: the retrieved calibration solution
    """
    station_name = station_names[station_id - 1]
    station_calibrator = station_calibrators[station_name]
    retrieved_solution = station_calibrator.GetCalibration(
        json.dumps({"frequency_channel": frequency_channel})
    )
    # TODO: Fix this race condition properly
    if not list(retrieved_solution):
        time.sleep(5.0)
        retrieved_solution = station_calibrator.GetCalibration(
            json.dumps({"frequency_channel": frequency_channel})
        )
    return retrieved_solution


@when(
    parsers.parse(
        "MccsCalibrationStore {station_id} tries to get a calibration solution"
        " not in the database"
    ),
    target_fixture="missing_solution",
    converters={"station_id": int},
)
def when_get_non_existent_calibration(
    calibration_stores: dict[str, tango.DeviceProxy],
    station_id: int,
    station_names: list[str],
    unused_outside_temperature: float,
    unused_frequency_channel: int,
) -> list[float]:
    """
    Retrieve a non_existent calibration solution using the calibration store.

    This should return just an empty list since the solution does not exist

    :param calibration_stores: proxies to the calibration stores
    :param station_id: id of the station.
    :param station_names: names of the station in the test.
    :param unused_outside_temperature: the outside temperature to get a solution for
    :param unused_frequency_channel: the frequency channel to get a solution for
    :return: the retrieved calibration solution, which is an empty list
    """
    station_name = station_names[station_id - 1]
    calibration_store = calibration_stores[station_name]
    unused_station = station_id + 8
    return calibration_store.GetSolution(
        json.dumps(
            {
                "outside_temperature": unused_outside_temperature,
                "frequency_channel": unused_frequency_channel,
                "station_id": unused_station,
            }
        )
    )


@then("the calibration store returns an empty array")
def then_empty_calibration(
    missing_solution: list[float],
) -> None:
    """
    Verify the returned solution is an empty array.

    :param missing_solution: the retrieved solution from the database
    """
    assert len(missing_solution) == 0


@then("the correct calibration solution is retrieved")
def then_correct_calibration(
    retrieved_solution: list[float],
    calibration_solutions: dict[tuple[int, float, int], list[float]],
    outside_temperature: float,
    station_id: int,
    frequency_channel: int,
) -> None:
    """
    Verify the returned solution is the one stored previously.

    :param station_id: a fixture with the station_id.
    :param retrieved_solution: the retrieved solution from the database
    :param calibration_solutions: the initially stored solutions
    :param outside_temperature: the outside temperature
    :param frequency_channel: the frequency channel
    """
    assert retrieved_solution == pytest.approx(
        calibration_solutions[(frequency_channel, outside_temperature, station_id)]
    )


@then("the most recently stored calibration solution is retrieved")
def then_most_recent_calibration(
    retrieved_solution: list[float],
    alternative_calibration_solutions: dict[tuple[int, float, int], list[float]],
    outside_temperature: float,
    station_id: int,
    frequency_channel: int,
) -> None:
    """
    Verify the returned solution is the one stored most recently.

    :param station_id: a fixture with the station_id.
    :param retrieved_solution: the retrieved solution from the database
    :param alternative_calibration_solutions: the most recently stored solutions
    :param outside_temperature: the outside temperature
    :param frequency_channel: the frequency channel
    """
    assert retrieved_solution == pytest.approx(
        alternative_calibration_solutions[
            (frequency_channel, outside_temperature, station_id)
        ]
    )


def __table_removed(database_connection_pool: ConnectionPool) -> None:
    """
    Remove table, for regression testing.

    :param database_connection_pool: connection pool for the database
    """
    with database_connection_pool.connection() as cx:
        cx.execute("DROP table tab_mccs_calib;")


def __create_current_production_table(database_connection_pool: ConnectionPool) -> None:
    """
    Create the current production table.

    :param database_connection_pool: connection pool for the database
    """
    with database_connection_pool.connection() as cx:
        sql_query = """
            CREATE TABLE tab_mccs_calib
            (id serial NOT NULL PRIMARY KEY,
            creation_time timestamp NOT NULL,
            outside_temperature DOUBLE PRECISION NOT NULL,
            frequency_channel smallint NOT NULL,
            station_id smallint NOT NULL,
            calibration_path text NOT NULL,
            preferred boolean
            );
            """
        cx.execute(sql_query)

    expected_columns = [
        "frequency_channel",
        "creation_time",
        "outside_temperature",
        "id",
        "station_id",
        "preferred",
        "calibration_path",
    ]
    matches = 0
    with database_connection_pool.connection() as cx:
        result = cx.execute(
            "SELECT column_name FROM "
            "information_schema.columns WHERE "
            "table_name = 'tab_mccs_calib';"
        )
        columns: list[Any] = result.fetchall()  # type: ignore[assignment]
        print("Columns in 'tab_mccs_calib':")
        for column in columns:
            name = column["column_name"]  # type: ignore[call-overload]
            assert name in expected_columns
            matches += 1
    assert matches == len(expected_columns)


@given("we have a the old production database table")
def old_database_table(database_connection_pool: ConnectionPool) -> None:
    """
    Create a database for the previous version.

    :param database_connection_pool: connection pool for the database
    """
    # Remove any existing tables.
    __table_removed(database_connection_pool)
    # recreate table with old schema.
    __create_current_production_table(database_connection_pool)


@given(
    "the old database schema has solutions.",
    target_fixture="id_path",
)
def old_database_has_solutions(
    database_connection_pool: ConnectionPool,
) -> list[tuple[Any, Any]]:
    """
    Put solutions in the old database.

    Simulate the fact that the old database had
    solutions.

    :param database_connection_pool: connection pool for the database

    :return: a list of tuples containing unique information
        about entries. (id, calibration_path)
    """
    with database_connection_pool.connection() as cx:
        for i in range(10):
            mocked_solution = OldDatabaseSolution(
                frequency_channel=1,
                outside_temperature=32.5,
                station_id=1,
                preferred=True,
                calibration_path=f"some/path/to/correlation-matrix_{i}.hdf5",
            )
            query, params = mocked_solution.generate_loading_instruction()
            cx.execute(
                query,
                params,
            )

    id_path: list[tuple[Any, Any]] = []
    with database_connection_pool.connection() as cx:
        sql_query = """
        SELECT id, calibration_path FROM tab_mccs_calib;
        """

        result = cx.execute(sql_query)

        # Fetch all results from the query
        rows = result.fetchall()  # type: ignore[assignment]

        # Print the results
        for row in rows:
            cal_id = row["id"]  # type: ignore[call-overload]
            calibration_path = row["calibration_path"]  # type: ignore[call-overload]
            id_path.append((cal_id, calibration_path))

    return id_path


@when("we update to the new schema")
def update_database_to_new_schema(
    database_connection_pool: ConnectionPool,
) -> None:
    """
    Update to the new schema.

    :param database_connection_pool: connection pool for the database
    """
    with database_connection_pool.connection() as cx:
        sql_query = (
            "ALTER TABLE tab_mccs_calib DROP id; "
            "ALTER TABLE tab_mccs_calib ADD COLUMN id "
            "BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY; "
            "ALTER TABLE tab_mccs_calib "
            "ADD COLUMN acquisition_time FLOAT8 NOT NULL DEFAULT(0), "
            "ADD COLUMN solution FLOAT8[], "
            "ADD COLUMN corrcoeff FLOAT8[], "
            "ADD COLUMN residual_max FLOAT8[], "
            "ADD COLUMN residual_std FLOAT8[], "
            "ADD COLUMN xy_phase FLOAT8, "
            "ADD COLUMN n_masked_initial INTEGER, "
            "ADD COLUMN n_masked_final INTEGER, "
            "ADD COLUMN lst FLOAT8, "
            "ADD COLUMN galactic_centre_elevation FLOAT8, "
            "ADD COLUMN sun_elevation FLOAT8, "
            "ADD COLUMN sun_adjustment_factor FLOAT8,"
            "ADD COLUMN masked_antennas TEXT[];"
        )

        # Execute the query
        cx.execute(sql_query)


@then("the database ends up in desired state.")
def database_updated_as_expected(
    database_connection_pool: ConnectionPool, id_path: list[tuple]
) -> None:
    """
    Verify the database contains the intended data.

    :param database_connection_pool: connection pool for the database
    :param id_path: a tuple containing unique information about
        solutions stored in old format.
    """
    with database_connection_pool.connection() as cx:
        sql_query = """
            SELECT id, calibration_path FROM tab_mccs_calib;
        """
        # Execute the query
        result = cx.execute(sql_query)

        # Fetch all results from the query
        rows = result.fetchall()  # type: ignore[assignment]
        # Print the results
        for row in rows:
            cal_id = row["id"]  # type: ignore[call-overload]
            calibration_path = row["calibration_path"]  # type: ignore[call-overload]
            assert (cal_id, calibration_path) in id_path
