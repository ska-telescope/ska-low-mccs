# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
Feature: Test PaSD functionality
    As a developer,
    I want to be able to test the PaSD devices from top to bottom
    So that we are confident in our deployment

    @XTP-27893
    @xfail
    Scenario Outline: MccsController can turn on/off PaSD
        Given an MCCS deployment is available and ready
        And a PaSD deployment is available and ready
        And PaSD deployment is not in state <state>
        When MccsController sends <state> command to FieldStation
        Then FieldStation turns <state> PaSD
        And MccsController reports PaSD state is <state>

        Examples:
            | state |
            | on    |
            | off   |

    @XTP-27892
    @xfail
    Scenario: PaSD does not turn on masked antennas when requested by MccsController
        Given an MCCS deployment is available and ready
        And a PaSD deployment is available and ready
        And PaSD deployment is OFF
        And a set of antennas are masked to not be used
        When MccsController sends ON command to FieldStation
        Then FieldStation only turns ON antennas which are not masked



