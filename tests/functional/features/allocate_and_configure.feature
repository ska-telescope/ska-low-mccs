# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

Feature: Test allocate, configure and scan commands
    As a developer,
    I want to test the allocate, configure and scan commands
    So that we can allocate and configure resources to scans and perform scans

    @XTP-27427
    Scenario: Allocate resources successfully
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsController has not allocated resources to MccsSubarray 1
        When MccsController allocates resources to MccsSubarray 1
        Then MccsController reports correct allocation
        And MccsSubarray 1 report correct allocation
        And 2 MccsSubarrayBeam report correct assignment

    #  Scenario: Unsuccessful simultaneous allocation to multiple MccsSubarray
    #    Given an MCCS deployment which is ONLINE and ON
    #    And 2 stations
    #    And 1 MccsController
    #    And 1 MccsSubarray
    #    And 2 MccsSubarrayBeam
    #    And 2 MccsStationBeam in every station
    #    And MccsStation 1
    #    And MccsController has allocated resources to MccsSubarray 1
    #    And MccsController has not allocated resources to MccsSubarray 2
    #    When MccsController allocates resources to MccsSubarray 2
    #    Then Allocation fails

    @XTP-27768
    Scenario: Allocate resources unsuccessfully with invalid schema
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsStation 2
        And MccsController has not allocated resources to MccsSubarray 1
        When MccsController allocates them to MccsSubarray 1 with invalid schema
        Then Allocation fails
        And MccsStation 1 and 2 report an empty beamformer table

    @XTP-27769
    Scenario: Release allocated resources
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsStation 2
        And MccsController has allocated resources to MccsSubarray 1
        When MccsController releases resources from MccsSubarray 1
        Then MccsController reports no resources allocated
        And MccsStation 1 and 2 report an empty beamformer table


    @XTP-27770
    Scenario: Over-allocate resources unsuccessfully
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsStation 2
        When MccsController allocates them to MccsSubarray 1 with too many channels
        Then Allocation fails

    @XTP-27771
    Scenario: Configure MccsSubarray after allocating
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsStation 2
        And SpsStation 1
        And SpsStation 2
        And 1 MccsTile in every station
        And 1 MccsSubrack in every station
        And MccsController has allocated resources to MccsSubarray 1
        When MccsSubarray 1 configures the 2 MccsSubarrayBeams
        Then The 4 MccsStationBeam report correct configuration
        And MccsStation 1 and 2 report the expected beamformer table
        And SpsStation 1 reports the expected beamformer table
        And The MccsTile on 1 reports the expected beamformer table

    @XTP-27773
    Scenario: Reconfigure configured MccsSubarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsStation 2
        And SpsStation 1
        And SpsStation 2
        And 1 MccsTile in every station
        And 1 MccsSubrack in every station
        And MccsController has allocated resources to MccsSubarray 1
        And The 4 MccsStationBeam confirm MccsSubarray 1 has configured the 2 MccsSubarrayBeam
        When MccsSubarray 1 reconfigures the 2 MccsSubarrayBeams
        Then The 4 MccsStationBeam report correct reconfiguration
        And MccsStation 1 and 2 report the expected beamformer table
        And SpsStation 1 reports the expected beamformer table
        And The MccsTile on 1 reports the expected beamformer table

    @XTP-29435
    Scenario: Scan with configured MccsSubarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsStation 2
        And SpsStation 1
        And SpsStation 2
        And 1 MccsTile in every station
        And 1 MccsSubrack in every station
        And MccsController has allocated resources to MccsSubarray 1
        And The 4 MccsStationBeam confirm MccsSubarray 1 has configured the 2 MccsSubarrayBeam
        When MccsSubarray 1 performs a Scan
        Then The MccsTiles report that the beamformer is running

    @XTP-29436
    Scenario: EndScan with running Scan
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsStation 2
        And SpsStation 1
        And SpsStation 2
        And 1 MccsTile in every station
        And 1 MccsSubrack in every station
        And MccsController has allocated resources to MccsSubarray 1
        And The 4 MccsStationBeam confirm MccsSubarray 1 has configured the 2 MccsSubarrayBeam
        And MccsSubarray 1 is performing a Scan
        When MccsSubarray 1 ends its Scan
        Then The MccsTiles report that the beamformer is not running

    @XTP-27772
    Scenario: Deconfigure configured MccsSubarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsStation 2
        And SpsStation 1
        And SpsStation 2
        And 1 MccsTile in every station
        And 1 MccsSubrack in every station
        And MccsController has allocated resources to MccsSubarray 1
        And The 4 MccsStationBeam confirm MccsSubarray 1 has configured the 2 MccsSubarrayBeam
        When MccsSubarray 1 deconfigure its resources
        And MccsController releases resources from MccsSubarray 1
        Then The 4 MccsStationBeam report unconfigured
