# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

Feature: Test abort and restart commands
    As a developer,
    I want to test the abort and restart commands,
    So that we can recover subarrays from faults.

    @XTP-77127
    Scenario: Abort an IDLE subarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsController has allocated resources to MccsSubarray 1
        When MccsSubarray 1 aborts its observation
        Then MccsSubarray 1 report ABORTED
        And The 2 MccsSubarrayBeam report ABORTED
        And The 4 MccsStationBeam report ABORTED

    @XTP-77128
    Scenario: Restart an ABORTED IDLE subarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsSubarray 1 is aborted
        When MccsController restarts MccsSubarray 1
        Then MccsSubarray 1 report EMPTY
        And The 2 MccsSubarrayBeam report EMPTY
        And The 4 MccsStationBeam report EMPTY
        And MccsController reports no resources allocated

    @XTP-77129
    Scenario: Abort a READY subarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsController has allocated resources to MccsSubarray 1
        And The 4 MccsStationBeam confirm MccsSubarray 1 has configured the 2 MccsSubarrayBeam
        When MccsSubarray 1 aborts its observation
        Then MccsSubarray 1 report ABORTED
        And The 2 MccsSubarrayBeam report ABORTED
        And The 4 MccsStationBeam report ABORTED

    @XTP-77130
    Scenario: Restart an ABORTED READY subarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsSubarray 1 is aborted
        When MccsController restarts MccsSubarray 1
        Then MccsSubarray 1 report EMPTY
        And The 2 MccsSubarrayBeam report EMPTY
        And The 4 MccsStationBeam report EMPTY
        And The 4 MccsStationBeam report unconfigured
        And MccsController reports no resources allocated

    @XTP-77131
    Scenario: Abort a SCANNING subarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsController has allocated resources to MccsSubarray 1
        And The 4 MccsStationBeam confirm MccsSubarray 1 has configured the 2 MccsSubarrayBeam
        And MccsSubarray 1 is performing a Scan
        When MccsSubarray 1 aborts its observation
        Then MccsSubarray 1 report ABORTED
        And The 2 MccsSubarrayBeam report ABORTED
        And The 4 MccsStationBeam report ABORTED
        And The MccsTiles report that the beamformer is not running

    @XTP-77132
    Scenario: Restart an ABORTED SCANNING subarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And 1 MccsTile in every station
        And MccsSubarray 1 is aborted
        When MccsController restarts MccsSubarray 1
        Then MccsSubarray 1 report EMPTY
        And The 2 MccsSubarrayBeam report EMPTY
        And The 4 MccsStationBeam report EMPTY
        And The MccsTiles report that the beamformer is not running
        And The 4 MccsStationBeam report unconfigured
        And MccsController reports no resources allocated

    @XTP-77133
    Scenario: FAULT propagation up the chain
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsController has allocated resources to MccsSubarray 1
        And The 4 MccsStationBeam confirm MccsSubarray 1 has configured the 2 MccsSubarrayBeam
        And MccsSubarray 1 is performing a Scan
        When a MccsStationBeam on MccsSubarrayBeam 1 FAULTS
        Then MccsSubarrayBeam 1 reports FAULT
        And MccsSubarrayBeam 2 reports SCANNING
        And MccsSubarray 1 reports FAULT

    @XTP-77234
    Scenario: Restart a FAULTED subarray
        Given an MCCS deployment which is ONLINE and ON
        And 2 stations
        And 1 MccsController
        And 1 MccsSubarray
        And 2 MccsSubarrayBeam
        And 2 MccsStationBeam in every station
        And MccsStation 1
        And MccsSubarray 1 is faulted
        When MccsController restarts MccsSubarray 1
        Then MccsSubarray 1 report EMPTY
        And The 2 MccsSubarrayBeam report EMPTY
        And The 2 MccsStationBeam report EMPTY
        And MccsController reports no resources allocated