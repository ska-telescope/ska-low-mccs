# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains pytest-specific test harness for MCCS functional (BDD) tests."""
from __future__ import annotations

import os
from typing import Iterator

import _pytest
import pytest
import tango
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext


def pytest_configure(
    config: _pytest.config.Config,
) -> None:
    """
    Register custom markers to avoid pytest warnings.

    :param config: the pytest config object
    """
    config.addinivalue_line("markers", "XTP-1170: XRay BDD test marker")
    config.addinivalue_line("markers", "XTP-1257: XRay BDD test marker")
    config.addinivalue_line("markers", "XTP-1260: XRay BDD test marker")
    config.addinivalue_line("markers", "XTP-1261: XRay BDD test marker")
    config.addinivalue_line("markers", "XTP-1473: XRay BDD test marker")
    config.addinivalue_line("markers", "XTP-1762: XRay BDD test marker")
    config.addinivalue_line("markers", "XTP-1763: XRay BDD test marker")


@pytest.fixture(name="true_context")
def true_context_fixture(request: pytest.FixtureRequest) -> bool:
    """
    Return whether to test against an existing Tango deployment.

    If True, then Tango is already deployed, and the tests will be run
    against that deployment.

    If False, then Tango is not deployed, so the test harness will stand
    up a test context and run the tests against that.

    :param request: A pytest object giving access to the requesting test
        context.
    :return: whether to test against an existing Tango deployment
    """
    if request.config.getoption("--true-context"):
        return True
    if os.getenv("TRUE_TANGO_CONTEXT", None):
        return True
    return False


@pytest.fixture(name="test_context")
def test_context_fixture(
    true_context: bool,
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Create a test harness providing a Tango context for devices under test.

    :param true_context: whether to test against an existing Tango
        deployment

    :yield: A tango context with devices to test.
    """
    harness = MccsTangoTestHarness()

    if not true_context:
        harness.set_controller_device(
            subarrays=[1, 2],
            subarray_beams=[1, 2, 3, 4],
            stations=["ci-1", "ci-2"],
            station_beams=[
                ("ci-1", 1),
                ("ci-1", 2),
                ("ci-1", 3),
                ("ci-2", 1),
                ("ci-2", 2),
            ],
        )
        harness.add_subarray_device(1)
        harness.add_subarray_device(2)
        harness.add_subarray_beam_device(1)
        harness.add_subarray_beam_device(2)
        harness.add_subarray_beam_device(3)
        harness.add_subarray_beam_device(4)
        harness.add_station_device(
            "ci-1",
            1,
            [],  # override harness defaults to remove subservient devices
            field_station_trl="",
            sps_station_trl="",
            station_calibrator_trl="",
        )
        harness.add_station_device(
            "ci-2",
            2,
            [],  # override harness defaults to remove subservient devices
            field_station_trl="",
            sps_station_trl="",
            station_calibrator_trl="",
        )
        harness.add_station_beam_device("ci-1", 1)
        harness.add_station_beam_device("ci-1", 2)
        harness.add_station_beam_device("ci-1", 3)
        harness.add_station_beam_device("ci-2", 1)
        harness.add_station_beam_device("ci-2", 2)

    with harness as context:
        yield context


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture(true_context: bool) -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of callables to be used as Tango change event callbacks.

    :param true_context: true if we are running against a real
        deployment.

    :return: a dictionary of callables to be used as tango change event
        callbacks.
    """
    # Against a true context, our function tests will now have 100s of
    # devices to make subscriptions for
    if true_context:
        db = tango.Database()
        mccs_devices = db.get_device_exported("low-mccs/*")
        obs_devices = ["low-mccs/subarray/", "low-mccs/subarraybeam/", "low-mccs/beam/"]
        subscription_list = ["pasd_device_state", "sps_device_state"]
        for device in mccs_devices:
            subscription_list += [
                f"{device}/state",
                f"{device}/healthState",
                f"{device}/adminMode",
            ]
            if any(obs_device in device for obs_device in obs_devices):
                subscription_list += [f"{device}/obsState"]

        return MockTangoEventCallbackGroup(
            *subscription_list,
            timeout=400.0,
            assert_no_error=False,
        )

    return MockTangoEventCallbackGroup(
        "low-mccs/control/control/state",
        "low-mccs/control/control/healthState",
        "low-mccs/control/control/adminMode",
        "low-mccs/subarray/01/state",
        "low-mccs/subarray/01/healthState",
        "low-mccs/subarray/01/adminMode",
        "low-mccs/subarray/01/obsState",
        "low-mccs/subarray/02/state",
        "low-mccs/subarray/02/healthState",
        "low-mccs/subarray/02/adminMode",
        "low-mccs/subarray/02/obsState",
        "low-mccs/subarraybeam/01/state",
        "low-mccs/subarraybeam/01/healthState",
        "low-mccs/subarraybeam/01/adminMode",
        "low-mccs/subarraybeam/01/obsState",
        "low-mccs/subarraybeam/02/state",
        "low-mccs/subarraybeam/02/healthState",
        "low-mccs/subarraybeam/02/adminMode",
        "low-mccs/subarraybeam/02/obsState",
        "low-mccs/station/ci-1/state",
        "low-mccs/station/ci-1/healthState",
        "low-mccs/station/ci-1/adminMode",
        "low-mccs/station/ci-2/state",
        "low-mccs/station/ci-2/healthState",
        "low-mccs/station/ci-2/adminMode",
        "low-mccs/spsstation/ci-1/state",
        "low-mccs/spsstation/ci-1/healthState",
        "low-mccs/spsstation/ci-1/adminMode",
        "low-mccs/subrack/ci-1-sr1/state",
        "low-mccs/subrack/ci-1-sr1/healthState",
        "low-mccs/subrack/ci-1-sr1/adminMode",
        "low-mccs/tile/ci-1-tpm10/state",
        "low-mccs/tile/ci-1-tpm10/healthState",
        "low-mccs/tile/ci-1-tpm10/adminMode",
        "low-mccs/beam/ci-1-01/state",
        "low-mccs/beam/ci-1-01/healthState",
        "low-mccs/beam/ci-1-01/adminMode",
        "low-mccs/beam/ci-1-01/obsState",
        "low-mccs/beam/ci-1-02/state",
        "low-mccs/beam/ci-1-02/healthState",
        "low-mccs/beam/ci-1-02/adminMode",
        "low-mccs/beam/ci-1-02/obsState",
        "low-mccs/beam/ci-1-03/state",
        "low-mccs/beam/ci-1-03/healthState",
        "low-mccs/beam/ci-1-03/adminMode",
        "low-mccs/beam/ci-1-03/obsState",
        "low-mccs/beam/ci-2-01/state",
        "low-mccs/beam/ci-2-01/healthState",
        "low-mccs/beam/ci-2-01/adminMode",
        "low-mccs/beam/ci-2-01/obsState",
        "low-mccs/beam/ci-2-02/state",
        "low-mccs/beam/ci-2-02/healthState",
        "low-mccs/beam/ci-2-02/adminMode",
        "low-mccs/beam/ci-2-02/obsState",
        "pasd_device_state",
        "sps_device_state",
        timeout=400.0,
        assert_no_error=False,
    )


@pytest.fixture(name="mocked_calibration_solutions")
def mocked_calibration_solutions_fixture() -> dict[tuple[int, float], list[float]]:
    """
    Fixture that provides sample calibration solutions.

    :return: a sample calibration solution. The keys are tuples of the channel
        and the outside temperature, and the values are lists of calibration values
    """
    return {
        (23, 25.0): [float(4)] + [0.8 * i for i in range(8)],
        (45, 25.0): [float(7)] + [1.4 * (i % 2) for i in range(8)],
        (23, 30.0): [float(1)] + [0.1 * i for i in range(8)],
        (45, 30.0): [float(2)] + [1.1 * (i % 2) for i in range(8)],
        (23, 35.0): [float(4)] + [0.9 * i for i in range(8)],
        (45, 35.0): [float(8)] + [1.4 * (i % 2) for i in range(8)],
    }
