#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defines a harness for unit testing the Station calibrator module."""
from __future__ import annotations

import json
import unittest.mock

import pytest
import tango
from ska_control_model import ResultCode
from ska_low_mccs_common.testing.mock import MockDeviceBuilder
from tango.server import command

from ska_low_mccs.station_calibrator import MccsStationCalibrator


@pytest.fixture(name="mock_solver_device_proxy")
def mock_solver_device_proxy_fixture() -> unittest.mock.Mock:
    """
    Fixture that provides a mock Solver device proxy.

    :return: a mock Solver device proxy.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_result_command(
        "Solve", result_code=ResultCode.QUEUED, status="mocked_command_id"
    )
    builder.add_command(
        "CheckLongRunningCommandStatus",
        "COMPLETED",
    )
    return builder()


@pytest.fixture(name="mock_calibration_store_device_proxy")
def mock_calibration_store_device_proxy_fixture(
    calibration_solutions: dict[tuple[int, float, int], list[float]],
    station_id: int,
) -> unittest.mock.Mock:
    """
    Fixture that provides a mock MccsCalibrationStore device proxy.

    :param calibration_solutions: sample calibration solutions to return from the
        calibration store, where the channel and temperature are the key
    :param station_id: the id of the station.

    :return: a mock MccsCalibrationStore device proxy.
    """

    def _GetSolution(argin: str) -> list[float]:
        args = json.loads(argin)
        return calibration_solutions[
            (args["frequency_channel"], args["outside_temperature"], station_id)
        ]

    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    calibration_store = builder()
    calibration_store.GetSolution = _GetSolution
    calibration_store.StoreSolution.return_value = (
        [ResultCode.OK],
        ["Solution stored successfully"],
    )
    return calibration_store


@pytest.fixture(name="mock_station_device_proxy")
def mock_station_device_proxy_fixture() -> unittest.mock.Mock:
    """
    Fixture that provides a mock MccsStation device.

    :return: a mock MccsStation device.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_result_command(
        "AcquireDataForCalibration",
        result_code=ResultCode.QUEUED,
        status="mocked_command_id",
    )
    builder.add_result_command(
        "ConfigureStationForCalibration", result_code=ResultCode.OK, status="Completed"
    )
    return builder()


@pytest.fixture(name="patched_station_calibrator_device_class")
def patched_station_calibrator_device_class_fixture() -> type[MccsStationCalibrator]:
    """
    Return a station calibrator device class patched with extra methods for testing.

    :return: a station calibrator device class patched with extra methods for testing.
    """

    class PatchedStationCalibratorDevice(MccsStationCalibrator):
        """MccsStationCalibrator patched with extra commands for testing purposes."""

        @command(dtype_in="DevDouble", dtype_out="DevVoid")
        def SetOutsideTemperature(
            self: PatchedStationCalibratorDevice, argin: float
        ) -> None:
            """
            Mock a change in the outside temperature.

            :param argin: the outside temperature

            This would typically be polled from the station.
            """
            self.component_manager._station_outside_temperature_changed(
                "outsideTemperature", argin, tango.AttrQuality.ATTR_VALID
            )

    return PatchedStationCalibratorDevice


@pytest.fixture(name="station_label")
def station_label_fixture() -> str:
    """
    Return the station label of this station.

    :return: the station label of this station.
    """
    return "ci-1"
