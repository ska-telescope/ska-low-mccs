# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defined a pytest harness for testing the MCCS subarray beam module."""
from __future__ import annotations

import logging
import unittest

import pytest
import tango
from ska_low_mccs_common.testing.mock import MockDeviceBuilder
from ska_tango_base.commands import ResultCode
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.subarray_beam import SubarrayBeamComponentManager
from tests.harness import get_station_beam_trl

STATION_NAME = "ci-1"


@pytest.fixture(name="callbacks")
def callbacks_fixture() -> MockCallableGroup:
    """
    Return a dictionary of callables to be used as callbacks.

    :return: a dictionary of callables to be used as callbacks.
    """
    return MockCallableGroup(
        "communication_status",
        "configured_changed",
        "component_state",
        "task",
        "abort_task",
        timeout=5.0,
    )


@pytest.fixture(name="subarray_beam_component_manager")
def subarray_beam_component_manager_fixture(
    logger: logging.Logger,
    obs_command_timeout: int,
    callbacks: MockCallableGroup,
    mock_station_beam_device_proxys: unittest.mock.Mock,
) -> SubarrayBeamComponentManager:
    """
    Return a subarray beam component manager.

    :param logger: the logger to be used by this object.
    :param obs_command_timeout: the default timeout for obs
        commands in seconds.
    :param callbacks: A dictionary of callbacks with async support.
    :param mock_station_beam_device_proxys: the mock station beam
        proxy to use in testing.

    :return: a subarray beam component manager
    """
    return SubarrayBeamComponentManager(
        logger,
        obs_command_timeout,
        callbacks["communication_status"],
        callbacks["component_state"],
        _station_beams=mock_station_beam_device_proxys,
    )


@pytest.fixture(name="mock_station_beam_device_proxys")
def mock_station_beam_device_proxys_fixture(
    station_beam_id: int,
) -> dict[str, unittest.mock.Mock]:
    """
    Fixture that provides a mock station beam device proxy.

    :param station_beam_id: the id of the station beam device.

    :return: a dictionary containing the device name and the
        corresponding mock StationBeam device.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_result_command("configure", ResultCode.OK)
    builder.add_result_command("Scan", ResultCode.OK)
    builder.add_command("abort", (ResultCode.STARTED, "Abort has started"))
    return {get_station_beam_trl(STATION_NAME, station_beam_id): builder()}


@pytest.fixture(name="station_beam_id")
def station_beam_name_fixture() -> int:
    """
    Return the id of a station beam device.

    :return: the id of the mock station beam device.
    """
    return 1


@pytest.fixture(name="subarray_beam_id")
def subarray_beam_name_fixture() -> int:
    """
    Return a SubarrayBeam id.

    :return: A SubarrayBeam id.
    """
    return 1
