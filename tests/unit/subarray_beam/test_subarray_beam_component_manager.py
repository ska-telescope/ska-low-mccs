# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests of the subarray beam component manager."""
from __future__ import annotations

import time
import unittest
from typing import Any

import pytest
from ska_control_model import CommunicationStatus, ObsState, ResultCode, TaskStatus
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.subarray_beam import SubarrayBeamComponentManager
from tests.harness import get_station_beam_trl

STATION_NAME = "ci-1"


class TestSubarrayBeam:
    """
    Class for testing commands common to the subarray beam package.

    Because the subarray beam component manager passes commands down to
    to the subarray beam component, many commands are common. Here we
    test those common commands.
    """

    @pytest.fixture()
    def communicating_subarray_beam_component_manager(
        self: TestSubarrayBeam,
        subarray_beam_component_manager: SubarrayBeamComponentManager,
        callbacks: MockCallableGroup,
    ) -> SubarrayBeamComponentManager:
        """
        Return the subarray_beam_component_manager.

        :param subarray_beam_component_manager: the subarray beam component
            component manager to return
        :param callbacks: dictionary of callbacks.

        :return: the subarray_beam_component_manager.
        """
        subarray_beam_component_manager.start_communicating()
        callbacks["communication_status"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)
        return subarray_beam_component_manager

    # TODO: Change the attributes to those actually needed
    @pytest.mark.parametrize(
        ("attribute_name", "expected_value", "write_value"),
        [
            ("subarray_id", 0, None),
            ("subarray_beam_id", 0, None),
            ("station_ids", [], [3, 4, 5, 6]),
            ("logical_beam_id", 0, None),
            ("update_rate", 0.0, None),
            ("is_beam_locked", True, None),
            ("channels", [], None),
            ("antenna_weights", [], None),
            ("phase_centre", [], None),
        ],
    )
    def test_attribute(
        self: TestSubarrayBeam,
        communicating_subarray_beam_component_manager: SubarrayBeamComponentManager,
        attribute_name: str,
        expected_value: Any,
        write_value: Any,
    ) -> None:
        """
        Test read-write attributes.

        Test that the attributes take certain known initial values, and
        that we can write new values if the attribute is writable.

        This is a weak test; over time we should find ways to more
        thoroughly test each of these independently.

        :param communicating_subarray_beam_component_manager: the subarray
            beam component class object under test.
        :param attribute_name: the name of the attribute under test
        :param expected_value: the expected value of the attribute. This
            can be any type, but the test of the attribute is a
            single "==" equality test.
        :param write_value: the value to write to the attribute
        """
        assert (
            getattr(communicating_subarray_beam_component_manager, attribute_name)
            == expected_value
        )

        if write_value is not None:
            setattr(
                communicating_subarray_beam_component_manager,
                attribute_name,
                write_value,
            )
            assert (
                getattr(communicating_subarray_beam_component_manager, attribute_name)
                == write_value
            )

    def test_desired_pointing(
        self: TestSubarrayBeam,
        communicating_subarray_beam_component_manager: SubarrayBeamComponentManager,
    ) -> None:
        """
        Test the desired pointing attribute.

        This is a weak test that simply check that the attribute's
        initial value is as expected, and that we can write a new value
        to it.

        :param communicating_subarray_beam_component_manager: the
            subarray beam component class object under test.
        """
        assert communicating_subarray_beam_component_manager.desired_pointing == []

        value_to_write = [1585619550.0, 192.85948, 2.0, 27.12825, 1.0]
        communicating_subarray_beam_component_manager.desired_pointing = value_to_write
        assert (
            communicating_subarray_beam_component_manager.desired_pointing
            == pytest.approx(value_to_write)
        )

    def test_configure(
        self: TestSubarrayBeam,
        communicating_subarray_beam_component_manager: SubarrayBeamComponentManager,
        callbacks: MockCallableGroup,
        station_beam_id: int,
    ) -> None:
        """
        Test the configure method.

        :param communicating_subarray_beam_component_manager: the
            subarray beam component class object under test.
        :param callbacks: dictionary of callbacks.
        :param station_beam_id: the id of the station beam mock
        """
        subarray_id = 1
        subarray_beam_id = 1
        update_rate = 3.14
        logical_bands = [
            {"start_channel": 0, "number_of_channels": 8},
            {"start_channel": 24, "number_of_channels": 32},
        ]
        desired_pointing = [192.0, 2.0, 27.0, 1.0]
        field = {
            "reference_frame": "ICRS",
            "target_name": "some star",
            "attrs": {
                "c1": desired_pointing[0],
                "c1_rate": desired_pointing[1],
                "c2": desired_pointing[2],
                "c2_rate": desired_pointing[3],
            },
        }
        apertures = [
            {
                "aperture_id": "AP1.2",
                "station_beam_trl": get_station_beam_trl(STATION_NAME, station_beam_id),
            },
        ]
        (
            task_status,
            unique_id,
        ) = communicating_subarray_beam_component_manager.configure(
            subarray_id=subarray_id,
            subarray_beam_id=subarray_beam_id,
            logical_bands=logical_bands,
            update_rate=update_rate,
            apertures=apertures,
            field=field,
            sky_coordinates={},
        )
        time.sleep(0.1)
        communicating_subarray_beam_component_manager._device_obs_state_changed(
            get_station_beam_trl(STATION_NAME, station_beam_id), ObsState.READY
        )
        callbacks["component_state"].assert_call(configured_changed=True)

        assert task_status == TaskStatus.QUEUED
        assert unique_id == "Task queued"

    def test_abort(
        self: TestSubarrayBeam,
        communicating_subarray_beam_component_manager: SubarrayBeamComponentManager,
        callbacks: MockCallableGroup,
        station_beam_id: int,
        mock_station_beam_device_proxys: dict[str, unittest.mock.Mock],
    ) -> None:
        """
        Test the abort method.

        :param communicating_subarray_beam_component_manager: the
            subarray beam component class object under test.
        :param callbacks: dictionary of callbacks.
        :param station_beam_id: the id of the station beam mock.
        :param mock_station_beam_device_proxys: dictionary of mock station beams.
        """
        subarray_id = 1
        subarray_beam_id = 1
        update_rate = 3.14
        logical_bands = [
            {"start_channel": 0, "number_of_channels": 8},
            {"start_channel": 24, "number_of_channels": 32},
        ]
        desired_pointing = [192.0, 2.0, 27.0, 1.0]
        field = {
            "reference_frame": "ICRS",
            "target_name": "some star",
            "attrs": {
                "c1": desired_pointing[0],
                "c1_rate": desired_pointing[1],
                "c2": desired_pointing[2],
                "c2_rate": desired_pointing[3],
            },
        }
        apertures = [
            {
                "aperture_id": "AP1.2",
                "station_beam_trl": get_station_beam_trl(STATION_NAME, station_beam_id),
            },
        ]
        (
            task_status,
            unique_id,
        ) = communicating_subarray_beam_component_manager.configure(
            task_callback=callbacks["task"],
            subarray_id=subarray_id,
            subarray_beam_id=subarray_beam_id,
            logical_bands=logical_bands,
            update_rate=update_rate,
            apertures=apertures,
            field=field,
            sky_coordinates={},
        )

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        communicating_subarray_beam_component_manager.abort(callbacks["abort_task"])
        callbacks["abort_task"].assert_call(status=TaskStatus.IN_PROGRESS)
        time.sleep(0.1)
        communicating_subarray_beam_component_manager._device_obs_state_changed(
            get_station_beam_trl(STATION_NAME, station_beam_id), ObsState.ABORTED
        )
        callbacks["abort_task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Abort command completed OK."),
        )
        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED,
            result=(ResultCode.ABORTED, "_configure aborted"),
        )
        mock_station_beam_device_proxys[
            get_station_beam_trl(STATION_NAME, station_beam_id)
        ].abort.assert_next_call()
        assert communicating_subarray_beam_component_manager._is_configured is False

    def test_abort_device(
        self: TestSubarrayBeam,
        communicating_subarray_beam_component_manager: SubarrayBeamComponentManager,
        callbacks: MockCallableGroup,
        station_beam_id: int,
        mock_station_beam_device_proxys: dict[str, unittest.mock.Mock],
    ) -> None:
        """
        Test the abort method.

        :param communicating_subarray_beam_component_manager: the
            subarray beam component class object under test.
        :param callbacks: dictionary of callbacks.
        :param station_beam_id: the id of the station beam mock.
        :param mock_station_beam_device_proxys: dictionary of mock station beams.
        """
        subarray_id = 1
        subarray_beam_id = 1
        update_rate = 3.14
        logical_bands = [
            {"start_channel": 0, "number_of_channels": 8},
            {"start_channel": 24, "number_of_channels": 32},
        ]
        desired_pointing = [192.0, 2.0, 27.0, 1.0]
        field = {
            "reference_frame": "ICRS",
            "target_name": "some star",
            "attrs": {
                "c1": desired_pointing[0],
                "c1_rate": desired_pointing[1],
                "c2": desired_pointing[2],
                "c2_rate": desired_pointing[3],
            },
        }
        apertures = [
            {
                "aperture_id": "AP1.2",
                "station_beam_trl": get_station_beam_trl(STATION_NAME, station_beam_id),
            },
        ]
        (
            task_status,
            unique_id,
        ) = communicating_subarray_beam_component_manager.configure(
            task_callback=callbacks["task"],
            subarray_id=subarray_id,
            subarray_beam_id=subarray_beam_id,
            logical_bands=logical_bands,
            update_rate=update_rate,
            apertures=apertures,
            field=field,
            sky_coordinates={},
        )

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        communicating_subarray_beam_component_manager.abort_device(
            callbacks["abort_task"]
        )
        callbacks["abort_task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["abort_task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Abort command completed OK."),
        )
        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED,
            result=(ResultCode.ABORTED, "_configure aborted"),
        )
        mock_station_beam_device_proxys[
            get_station_beam_trl(STATION_NAME, station_beam_id)
        ].abort.assert_not_called()
        assert communicating_subarray_beam_component_manager._is_configured is False

    def test_abort_restart(
        self: TestSubarrayBeam,
        communicating_subarray_beam_component_manager: SubarrayBeamComponentManager,
        callbacks: MockCallableGroup,
        station_beam_id: int,
        mock_station_beam_device_proxys: dict[str, unittest.mock.Mock],
    ) -> None:
        """
        Test the restart method.

        :param communicating_subarray_beam_component_manager: the
            subarray beam component class object under test.
        :param callbacks: dictionary of callbacks.
        :param station_beam_id: the id of the station beam mock.
        :param mock_station_beam_device_proxys: dictionary of mock station beams.
        """
        subarray_id = 1
        subarray_beam_id = 1
        update_rate = 3.14
        logical_bands = [
            {"start_channel": 0, "number_of_channels": 8},
            {"start_channel": 24, "number_of_channels": 32},
        ]
        desired_pointing = [192.0, 2.0, 27.0, 1.0]
        field = {
            "reference_frame": "ICRS",
            "target_name": "some star",
            "attrs": {
                "c1": desired_pointing[0],
                "c1_rate": desired_pointing[1],
                "c2": desired_pointing[2],
                "c2_rate": desired_pointing[3],
            },
        }
        apertures = [
            {
                "aperture_id": "AP1.2",
                "station_beam_trl": get_station_beam_trl(STATION_NAME, station_beam_id),
            },
        ]
        (
            task_status,
            unique_id,
        ) = communicating_subarray_beam_component_manager.configure(
            task_callback=callbacks["task"],
            subarray_id=subarray_id,
            subarray_beam_id=subarray_beam_id,
            logical_bands=logical_bands,
            update_rate=update_rate,
            apertures=apertures,
            field=field,
            sky_coordinates={},
        )

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        communicating_subarray_beam_component_manager.abort(callbacks["abort_task"])
        callbacks["abort_task"].assert_call(status=TaskStatus.IN_PROGRESS)
        time.sleep(0.1)
        communicating_subarray_beam_component_manager._device_obs_state_changed(
            get_station_beam_trl(STATION_NAME, station_beam_id), ObsState.ABORTED
        )
        callbacks["abort_task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Abort command completed OK."),
        )
        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED,
            result=(ResultCode.ABORTED, "_configure aborted"),
        )
        mock_station_beam_device_proxys[
            get_station_beam_trl(STATION_NAME, station_beam_id)
        ].abort.assert_next_call()
        assert communicating_subarray_beam_component_manager._is_configured is False

        communicating_subarray_beam_component_manager.restart(callbacks["task"])

        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Restart command completed OK."),
        )

        assert communicating_subarray_beam_component_manager._station_beams == {}
        assert (
            communicating_subarray_beam_component_manager._device_communication_states
            == {}
        )
        assert communicating_subarray_beam_component_manager._device_obs_states == {}
        assert communicating_subarray_beam_component_manager._first_channel == 0
        assert communicating_subarray_beam_component_manager._number_of_channels == 0
