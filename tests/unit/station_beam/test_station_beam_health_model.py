# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for StationBeamHealthModel."""

from __future__ import annotations

from typing import Any

import pytest
from ska_control_model import HealthState

from ska_low_mccs.station_beam.station_beam_health_model import StationBeamHealthModel
from tests.harness import get_antenna_trl

NUMBER_OF_ANTENNAS = 256


class TestStationBeamHealthModel:
    """Tests for the MCCS Station beam health model."""

    @pytest.fixture
    def health_model(self: TestStationBeamHealthModel) -> StationBeamHealthModel:
        """
        Fixture to return the station beam health model.

        :return: Health model to be used.
        """

        def callback(*args: Any, **kwargs: Any) -> None:
            pass

        health_model = StationBeamHealthModel(
            callback,
            ignore_power_state=True,
            thresholds={
                "antennas_degraded_threshold": StationBeamHealthModel.DEGRADED_CRITERIA,
                "antennas_failed_threshold": StationBeamHealthModel.FAILED_CRITERIA,
            },
        )
        health_model.update_state(communicating=True)

        return health_model

    @pytest.mark.parametrize(
        ("beam_locked", "expected_health", "expected_report"),
        [
            pytest.param(
                True,
                HealthState.OK,
                "Health is OK.",
                id="Beam is locked, expected healthy",
            ),
            pytest.param(
                False,
                HealthState.DEGRADED,
                "The beam is not locked",
                id="Beam is unlocked, expected degraded",
            ),
        ],
    )
    def test_station_beam_is_beam_locked(
        self: TestStationBeamHealthModel,
        health_model: StationBeamHealthModel,
        beam_locked: bool,
        expected_health: HealthState,
        expected_report: str,
        station_on_name: str,
    ) -> None:
        """
        Test the evaluate health.

        :param health_model: Fixture to return health model.
        :param beam_locked: If the station beam is beam locked or not.
        :param expected_health: Expected outcome health of the station beam.
        :param expected_report: Expected outcome report of the station beam.
        :param station_on_name: the name of a station which is on.
        """
        health_model.antenna_health_changed(
            get_antenna_trl(station_on_name, "sb1-1"), HealthState.OK
        )
        health_model.station_health_changed(HealthState.OK)
        health_model.is_beam_locked_changed(beam_locked)

        assert health_model.evaluate_health() == (expected_health, expected_report)

    @pytest.mark.parametrize(
        ("station_fault", "expected_health", "expected_report"),
        [
            pytest.param(
                True,
                HealthState.FAILED,
                "The MccsStation has FAULTED",
                id="Station fault ,expected FAILED",
            ),
            pytest.param(
                False,
                HealthState.OK,
                "Health is OK.",
                id="No station fault, expected healthy",
            ),
        ],
    )
    def test_station_beam_station_fault(
        self: TestStationBeamHealthModel,
        health_model: StationBeamHealthModel,
        station_fault: bool,
        expected_health: HealthState,
        expected_report: str,
        station_on_name: str,
    ) -> None:
        """
        Test the evaluate health.

        :param health_model: Fixture to return health model.
        :param station_fault: If the station is at fault.
        :param expected_health: Expected outcome health of the station beam.
        :param expected_report: Expected outcome report of the station beam.
        :param station_on_name: the name of a station which is on.
        """
        health_model.antenna_health_changed(
            get_antenna_trl(station_on_name, "sb1-1"), HealthState.OK
        )
        health_model.station_health_changed(HealthState.OK)
        health_model.is_beam_locked_changed(True)
        health_model.station_fault_changed(station_fault)

        assert health_model.evaluate_health() == (expected_health, expected_report)

    @pytest.mark.parametrize(
        ("antennas", "expected_health", "expected_report"),
        [
            pytest.param(
                {
                    get_antenna_trl("ci-1", "sb1-1"): HealthState.OK,
                    get_antenna_trl("ci-1", "sb1-2"): HealthState.OK,
                },
                HealthState.OK,
                "Health is OK.",
                id="All devices healthy, expect healthy",
            ),
            pytest.param(
                {
                    get_antenna_trl("ci-1", "sb1-1"): HealthState.OK,
                    get_antenna_trl("ci-1", "sb1-2"): HealthState.FAILED,
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: MccsStation: OK, "
                    f"MccsAntenna: ['{get_antenna_trl('ci-1', 'sb1-2'):}']"
                ),
                id="Over 20 percent devices failed, expect failed",
            ),
            pytest.param(
                {
                    get_antenna_trl("ci-1", "sb1-1"): HealthState.OK,
                    get_antenna_trl("ci-1", "sb1-2"): HealthState.DEGRADED,
                },
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: MccsStation: OK, "
                    f"MccsAntenna: ['{get_antenna_trl('ci-1', 'sb1-2'):}']"
                ),
                id="Over 20 percent devices degraded, expect failed",
            ),
            pytest.param(
                {
                    get_antenna_trl("ci-1", "sb1-1"): HealthState.FAILED,
                    get_antenna_trl("ci-1", "sb1-2"): HealthState.OK,
                    get_antenna_trl("ci-1", "sb1-3"): HealthState.OK,
                    get_antenna_trl("ci-1", "sb1-4"): HealthState.OK,
                    get_antenna_trl("ci-1", "sb1-5"): HealthState.OK,
                    get_antenna_trl("ci-1", "sb1-6"): HealthState.OK,
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: MccsStation: OK, "
                    f"MccsAntenna: ['{get_antenna_trl('ci-1', 'sb1-1'):}']"
                ),
                id="Over 5 percent devices failed, expect degraded",
            ),
        ],
    )
    def test_station_beam_antenna_health(
        self: TestStationBeamHealthModel,
        health_model: StationBeamHealthModel,
        antennas: dict,
        expected_health: HealthState,
        expected_report: str,
    ) -> None:
        """
        Test the evaluate health.

        :param health_model: Fixture to return health model.
        :param antennas: Child antennas to be tested and their health
        :param expected_health: Expected outcome health of the beam
        :param expected_report: Expected report of health of the beam
        """
        health_model.station_health_changed(HealthState.OK)
        health_model.is_beam_locked_changed(True)
        health_model.station_fault_changed(False)
        health_model.resources_changed(
            set(antennas.keys()),
        )

        for trl, health_state in antennas.items():
            health_model.antenna_health_changed(trl, health_state)

        assert health_model.evaluate_health() == (expected_health, expected_report)

    @pytest.mark.parametrize(
        (
            "antennas",
            "health_change",
            "expected_init_health",
            "expected_init_report",
            "expected_final_health",
            "expected_final_report",
        ),
        [
            pytest.param(
                {
                    get_antenna_trl("ci-1", f"foo{antenna_no}bar"): HealthState.OK
                    for antenna_no in range(1, NUMBER_OF_ANTENNAS + 1)
                },
                {
                    get_antenna_trl("ci-1", f"foo{antenna_no}bar"): HealthState.FAILED
                    for antenna_no in range(1, int((NUMBER_OF_ANTENNAS / 4) + 1))
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.FAILED,
                (
                    "Too many devices in a bad state: MccsStation: OK, MccsAntenna: "
                    + str(
                        sorted(
                            [
                                get_antenna_trl("ci-1", f"foo{antenna_no}bar")
                                for antenna_no in range(
                                    1, int((NUMBER_OF_ANTENNAS / 4) + 1)
                                )
                            ]
                        )
                    )
                ),
                id="All devices healthy, expect OK, then 1/4 antenna FAILED, "
                "expect FAILED",
            ),
            pytest.param(
                {
                    get_antenna_trl("ci-1", f"foo{antenna_no}bar"): HealthState.OK
                    for antenna_no in range(1, NUMBER_OF_ANTENNAS + 1)
                },
                {
                    get_antenna_trl("ci-1", f"foo{antenna_no}bar"): HealthState.OK
                    for antenna_no in range(1, 2 + 1)
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.OK,
                "Health is OK.",
                id="All devices healthy, expect OK, then 2 antenna FAILED, expect OK",
            ),
        ],
    )
    # pylint: disable=too-many-arguments
    def test_station_beam_evaluate_changed_health(
        self: TestStationBeamHealthModel,
        health_model: StationBeamHealthModel,
        antennas: dict[str, HealthState],
        health_change: dict[str, HealthState],
        expected_init_health: HealthState,
        expected_init_report: str,
        expected_final_health: HealthState,
        expected_final_report: str,
    ) -> None:
        """
        Tests of the station beam health model for a changed health.

        The properties of the health model are set and checked, then the health states
        of subservient devices are updated and the health is checked against the new
        expected value.

        :param health_model: The station beam health model to use
        :param antennas: the antennas for which the station beam cares about health,
            and their healths
        :param health_change: a dictionary of the health changes, key device and value
            dictionary of trl:HealthState
        :param expected_init_health: the expected initial health
        :param expected_init_report: the expected initial report
        :param expected_final_health: the expected final health
        :param expected_final_report: the expected final report
        """
        health_model.resources_changed(
            set(antennas.keys()),
        )
        health_model.station_health_changed(HealthState.OK)
        health_model.is_beam_locked_changed(True)
        health_model.station_fault_changed(False)

        for trl, health_state in antennas.items():
            health_model.antenna_health_changed(trl, health_state)

        assert health_model.evaluate_health() == (
            expected_init_health,
            expected_init_report,
        )

        for antenna in health_change:
            health_model.antenna_health_changed(antenna, health_change[antenna])

        assert health_model.evaluate_health() == (
            expected_final_health,
            expected_final_report,
        )

    @pytest.mark.parametrize(
        (
            "init_thresholds",
            "expected_init_health",
            "expected_init_report",
            "final_thresholds",
            "expected_final_health",
            "expected_final_report",
        ),
        [
            pytest.param(
                {
                    "antenna_degraded_threshold": 0.5,
                    "antenna_failed_threshold": 0.6,
                },
                HealthState.OK,
                "Health is OK.",
                {
                    "antenna_degraded_threshold": 0.05,
                    "antenna_failed_threshold": 0.2,
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: MccsStation: "
                    "OK, MccsAntenna: ['low-mccs/antenna/ci-1-sb1-1']"
                ),
                id="Expect degraded",
            ),
            pytest.param(
                {
                    "antenna_degraded_threshold": 0.05,
                    "antenna_failed_threshold": 0.2,
                },
                HealthState.DEGRADED,
                (
                    "Too many devices in a bad state: MccsStation: "
                    "OK, MccsAntenna: ['low-mccs/antenna/ci-1-sb1-1']"
                ),
                {
                    "antenna_degraded_threshold": 0.5,
                    "antenna_failed_threshold": 0.6,
                },
                HealthState.OK,
                "Health is OK.",
                id="Expect healthy",
            ),
        ],
    )
    # pylint: disable=too-many-arguments
    def test_station_beam_evaluate_health_changed_thresholds(
        self: TestStationBeamHealthModel,
        health_model: StationBeamHealthModel,
        init_thresholds: dict[str, float],
        expected_init_health: HealthState,
        expected_init_report: str,
        final_thresholds: dict[str, float],
        expected_final_health: HealthState,
        expected_final_report: str,
    ) -> None:
        """
        Tests of the station beam health model for changed thresholds.

        The properties of the health model are set and checked, then the thresholds for
        the health rules are changed and the new health is checked against the expected
        value

        :param health_model: The station beam health model to use
        :param init_thresholds: the initial thresholds of the health rules
        :param expected_init_health: the expected initial health
        :param expected_init_report: the expected initial report
        :param final_thresholds: the final thresholds of the health rules
        :param expected_final_health: the expected final health
        :param expected_final_report: the expected final report
        """

        # Use 6 antennas for the test
        antennas = {
            get_antenna_trl("ci-1", f"sb1-{antenna_no}"): HealthState.OK
            for antenna_no in range(1, 6 + 1)
        }
        antennas[get_antenna_trl("ci-1", "sb1-1")] = HealthState.FAILED

        health_model.resources_changed(
            set(antennas.keys()),
        )
        health_model.station_health_changed(HealthState.OK)
        health_model.is_beam_locked_changed(True)
        health_model.station_fault_changed(False)

        for trl, health_state in antennas.items():
            health_model.antenna_health_changed(trl, health_state)

        health_model.health_params = init_thresholds
        assert health_model.evaluate_health() == (
            expected_init_health,
            expected_init_report,
        )

        health_model.health_params = final_thresholds
        assert health_model.evaluate_health() == (
            expected_final_health,
            expected_final_report,
        )
