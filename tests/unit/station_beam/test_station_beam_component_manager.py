# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests of the station beam component manager."""
from __future__ import annotations

import unittest
from typing import Any, Iterator

import pytest
from ska_control_model import CommunicationStatus, ResultCode, TaskStatus
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.station_beam import StationBeamComponentManager
from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext


@pytest.fixture(name="test_context")
def test_context_fixture(
    station_on_name: str,
    station_off_name: str,
    mock_station_on: unittest.mock.Mock,
    mock_station_off: unittest.mock.Mock,
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which mock tile and field station devices are running.

    :param station_on_name: the name of a station which is on.
    :param station_off_name: the name of a station which is off.
    :param mock_station_on: a mock tango device for a station which is on.
    :param mock_station_off: a mock tango device for a station which is off.

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()

    harness.add_mock_station_device(station_on_name, mock_station_on)
    harness.add_mock_station_device(station_off_name, mock_station_off)

    with harness as context:
        yield context


class TestStationBeamComponentManager:
    """Class for testing the station beam component manager."""

    def test_communication(
        self: TestStationBeamComponentManager,
        station_beam_component_manager: StationBeamComponentManager,
        callbacks: MockCallableGroup,
        station_on_name: str,
    ) -> None:
        """
        Test the component manager's establishment of communication with its component.

        (i.e. its station)

        :param station_beam_component_manager: the station beam
            component manager under test.
        :param callbacks: A dictionary of calbacks with async support.
        :param station_on_name: the name of a station which is on.
        """
        station_beam_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            station_beam_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )

    @pytest.mark.parametrize(
        ("attribute_name", "expected_value", "write_value"),
        [
            ("subarray_id", 0, None),
            ("station_id", 0, None),
            ("logical_beam_id", 0, None),
            ("update_rate", 0.0, None),
            ("is_beam_locked", True, None),
            ("channels", [], None),
            ("antenna_weights", [], None),
            ("pointing_delay", [], None),
            ("pointing_delay_rate", [], None),
            ("phase_centre", [], None),
        ],
    )
    def test_attribute(
        self: TestStationBeamComponentManager,
        station_beam_component_manager: StationBeamComponentManager,
        attribute_name: str,
        expected_value: Any,
        write_value: Any,
    ) -> None:
        """
        Test read-write attributes.

        Test that the attributes take certain known initial values, and
        that we can write new values if the attribute is writable.

        This is a weak test; over time we should find ways to more
        thoroughly test each of these independently.

        :param station_beam_component_manager: the station beam
            component manager under test.
        :param attribute_name: the name of the attribute under test
        :param expected_value: the expected value of the attribute. This
            can be any type, but the test of the attribute is a
            single "==" equality test.
        :param write_value: the value to write to the attribute
        """
        assert getattr(station_beam_component_manager, attribute_name) == expected_value

        if write_value is not None:
            setattr(station_beam_component_manager, attribute_name, write_value)
            assert (
                getattr(station_beam_component_manager, attribute_name) == write_value
            )

    def test_beam_id(
        self: TestStationBeamComponentManager,
        station_beam_component_manager: StationBeamComponentManager,
        beam_id: int,
    ) -> None:
        """
        Test read-beam id attributes.

        This is a weak test that simply tests that the beam id is what
        it was initialised with.

        :param station_beam_component_manager: the station beam component
            class object under test.
        :param beam_id: the beam id of the station beam
        """
        assert station_beam_component_manager.beam_id == beam_id

    def test_desired_pointing(
        self: TestStationBeamComponentManager,
        station_beam_component_manager: StationBeamComponentManager,
    ) -> None:
        """
        Test the desired pointing attribute.

        This is a weak test that simply check that the attribute's
        initial value is as expected.

        :param station_beam_component_manager: the station beam component
            class object under test.
        """
        assert station_beam_component_manager.desired_pointing == [0, 0, 0, 0]

    def test_configure(
        self: TestStationBeamComponentManager,
        station_beam_component_manager: StationBeamComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the configure method.

        :param station_beam_component_manager: the station beam component class
            object under test.
        :param callbacks: A dictionary of calbacks with async support.
        """
        station_beam_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            station_beam_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )
        update_rate = 3.14
        logical_bands = [
            {"start_channel": 0, "number_of_channels": 8},
            {"start_channel": 24, "number_of_channels": 32},
        ]
        desired_pointing = [192.0, 2.0, 27.0, 1.0]
        field = {
            "reference_frame": "ICRS",
            "target_name": "some star",
            "attrs": {
                "c1": desired_pointing[0],
                "c1_rate": desired_pointing[1],
                "c2": desired_pointing[2],
                "c2_rate": desired_pointing[3],
            },
        }

        task_status, response = station_beam_component_manager.configure(
            callbacks["task"],
            update_rate=update_rate,
            logical_bands=logical_bands,
            field=field,
            sky_coordinates={},
        )

        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"
        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED, result="Configure has completed."
        )

        assert station_beam_component_manager.update_rate == pytest.approx(update_rate)
        assert station_beam_component_manager.desired_pointing == pytest.approx(
            desired_pointing
        )

    def test_abort(
        self: TestStationBeamComponentManager,
        station_beam_component_manager: StationBeamComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the abort method.

        :param station_beam_component_manager: the station beam component class
            object under test.
        :param callbacks: A dictionary of calbacks with async support.
        """

        station_beam_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            station_beam_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )
        update_rate = 3.14
        logical_bands = [
            {"start_channel": 0, "number_of_channels": 8},
            {"start_channel": 24, "number_of_channels": 32},
        ]
        desired_pointing = [192.0, 2.0, 27.0, 1.0]
        field = {
            "reference_frame": "ICRS",
            "target_name": "some star",
            "attrs": {
                "c1": desired_pointing[0],
                "c1_rate": desired_pointing[1],
                "c2": desired_pointing[2],
                "c2_rate": desired_pointing[3],
            },
        }

        task_status, response = station_beam_component_manager.configure(
            callbacks["task"],
            update_rate=update_rate,
            logical_bands=logical_bands,
            field=field,
            sky_coordinates={},
        )

        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"
        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        # Waiting to abort until we know we have started execution.
        station_beam_component_manager.abort(callbacks["abort_task"])
        callbacks["abort_task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["abort_task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Abort command completed OK."),
        )

        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED, result=(ResultCode.ABORTED, "_configure aborted")
        )

        # We could be aborting from any point in the execution, but we should always
        # abort before we hit the end of the method.
        assert station_beam_component_manager._is_configured is False

    def test_abort_restart(
        self: TestStationBeamComponentManager,
        station_beam_component_manager: StationBeamComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the restart method.

        :param station_beam_component_manager: the station beam component class
            object under test.
        :param callbacks: A dictionary of calbacks with async support.
        """

        station_beam_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            station_beam_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )
        update_rate = 3.14
        logical_bands = [
            {"start_channel": 0, "number_of_channels": 8},
            {"start_channel": 24, "number_of_channels": 32},
        ]
        desired_pointing = [192.0, 2.0, 27.0, 1.0]
        field = {
            "reference_frame": "ICRS",
            "target_name": "some star",
            "attrs": {
                "c1": desired_pointing[0],
                "c1_rate": desired_pointing[1],
                "c2": desired_pointing[2],
                "c2_rate": desired_pointing[3],
            },
        }

        task_status, response = station_beam_component_manager.configure(
            callbacks["task"],
            update_rate=update_rate,
            logical_bands=logical_bands,
            field=field,
            sky_coordinates={},
        )

        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"
        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)

        # Waiting to abort until we know we have started execution.
        station_beam_component_manager.abort(callbacks["abort_task"])
        callbacks["abort_task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["abort_task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Abort command completed OK."),
        )

        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED, result=(ResultCode.ABORTED, "_configure aborted")
        )

        # We could be aborting from any point in the execution, but we should always
        # abort before we hit the end of the method.
        assert station_beam_component_manager._is_configured is False

        task_status, response = station_beam_component_manager.restart(
            callbacks["task"]
        )

        assert task_status == TaskStatus.QUEUED
        assert response == "Task queued"
        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Restart command completed OK."),
        )

        assert station_beam_component_manager._subarray_id == 0
        assert station_beam_component_manager._subarray_beam_id == 0
        assert station_beam_component_manager._station_id == 0
        assert station_beam_component_manager._aperture_id == "AP000.00"
        assert station_beam_component_manager._number_of_channels == 0
        assert station_beam_component_manager._channel_table == []

        for block in station_beam_component_manager._channel_table:
            assert block[2] == 0
        assert station_beam_component_manager._update_rate == 0.0
        assert station_beam_component_manager._pointing_reference_frame == "AltAz"
        assert (
            station_beam_component_manager._pointing_timestamp
            == "2000-01-01T00:00:00.000Z"
        )
        assert station_beam_component_manager._desired_pointing == [0.0] * 4
