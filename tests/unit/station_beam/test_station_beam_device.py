# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for MccsStationBeam."""
from __future__ import annotations

import gc
import unittest.mock
from typing import Any, Iterator, Type

import pytest
import tango
from ska_control_model import CommunicationStatus, HealthState
from ska_low_mccs_common import MccsDeviceProxy
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango.server import command

from ska_low_mccs.station_beam.station_beam_component_manager import (
    StationBeamComponentManager,
)
from ska_low_mccs.station_beam.station_beam_device import MccsStationBeam
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_station_trl,
)

# To prevent tests hanging during gc.
gc.disable()


@pytest.fixture(name="patched_station_beam_device_class")
def patched_station_beam_device_class_fixture(
    mock_station_beam_component_manager: StationBeamComponentManager,
) -> Type[MccsStationBeam]:
    """
    Return a station beam device class, patched with extra methods for testing.

    :param mock_station_beam_component_manager: A fixture that provides
        a partially mocked component manager which has access to the
        component_state_callback.

    :return: a patched station beam device class, patched with extra
        methods for testing

    """

    # pylint: disable=too-many-ancestors
    class PatchedStationBeamDevice(MccsStationBeam):
        """
        MccsStationBeam patched with extra commands for testing purposes.

        The extra commands allow us to mock the receipt of obs state
        change events from subservient devices, turn on DeviceProxies,
        set the initial obsState and examine the health model.
        """

        @command(dtype_in=str)
        def set_obs_state(
            self: PatchedStationBeamDevice,
            obs_state_name: str,
        ) -> None:
            """
            Set the obsState of this device.

            A method to set the obsState for testing purposes.

            :param obs_state_name: The name of the obsState to directly transition to.
            """
            self.obs_state_model._straight_to_state(obs_state_name)

        @command(dtype_in=bool, dtype_out=int)
        def examine_health_model(
            self: PatchedStationBeamDevice,
            get_station_health: bool,
        ) -> HealthState | None:
            """
            Return the health state of the station beam or station.

            Returns the health state of the station beam or the station
            for use in tests.

            :param get_station_health: Whether to return the station
                health instead of station beam.

            :return: The HealthState of the device.
            """
            if get_station_health:
                return self._health_model._station_health
            return self._health_state

        @command(dtype_out=bool)
        def get_station_fault(
            self: PatchedStationBeamDevice,
        ) -> bool:
            """
            Return the station fault state.

            Returns the fault state of the station for use in tests.

            :return: The HealthState of the device.
            """
            return self._health_model._station_fault

        @command(dtype_out=int)
        def get_beam_health(
            self: PatchedStationBeamDevice,
        ) -> HealthState:
            """
            Return the station fault state.

            Returns the fault state of the station for use in tests.

            :return: The HealthState of the device.
            """
            return self._health_model._health_state

        def create_component_manager(
            self: PatchedStationBeamDevice,
        ) -> StationBeamComponentManager:
            """
            Return a partially mocked component manager instead of the usual one.

            :return: a mock component manager
            """
            mscbm = mock_station_beam_component_manager
            mscbm._communication_state_callback = self._communication_state_changed
            mock_station_beam_component_manager._component_state_callback = (
                self._component_state_callback
            )

            return mock_station_beam_component_manager

    return PatchedStationBeamDevice


@pytest.fixture(name="test_context")
def test_context_fixture(
    station_on_name: str,
    station_off_name: str,
    mock_station_on: unittest.mock.Mock,
    mock_station_off: unittest.mock.Mock,
    beam_id: int,
    patched_station_beam_device_class: type[MccsStationBeam],
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which mock tile and field station devices are running.

    :param station_on_name: the name of a station which is on.
    :param station_off_name: the name of a station which is off.
    :param mock_station_on: a mock tango device for a station which is on.
    :param mock_station_off: a mock tango device for a station which is off.
    :param beam_id: a beam id for the station beam under test.
    :param patched_station_beam_device_class: MccsStationBeam patched with extra
        commands for testing purposes.

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()

    harness.add_station_beam_device(
        station_on_name, beam_id, device_class=patched_station_beam_device_class
    )

    harness.add_mock_station_device(station_on_name, mock_station_on)
    harness.add_mock_station_device(station_off_name, mock_station_off)

    with harness as context:
        yield context


class TestMccsStationBeam:
    """Test class for MccsStationBeam tests."""

    @pytest.fixture(name="device_under_test")
    def device_under_test_fixture(
        self: TestMccsStationBeam,
        test_context: MccsTangoTestHarnessContext,
        station_on_name: str,
        beam_id: int,
    ) -> MccsDeviceProxy:
        """
        Fixture that returns the device under test.

        :param test_context: the test context in which this test will be run.
        :param station_on_name: the name of a station which is on.
        :param beam_id: a beam id for the station beam under test.

        :return: the device under test.
        """
        return test_context.get_station_beam_device(station_on_name, beam_id)

    def test_healthState(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for healthState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        device_under_test.subscribe_event(
            "healthState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.OK)
        assert device_under_test.healthState == HealthState.OK

    @pytest.mark.parametrize(
        ("attribute", "initial_value", "write_value"),
        [
            ("subarrayId", 0, None),
            ("stationId", 0, None),
            ("logicalBeamId", 0, None),
            ("updateRate", 0.0, None),
            # ("isBeamLocked", False, None),
        ],
    )
    def test_scalar_attributes(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
        attribute: str,
        initial_value: Any,
        write_value: Any,
    ) -> None:
        """
        Test attribute values.

        This is a very weak test that simply checks that attributes take
        certain initial values, and that, when we write to a writable
        attribute, the write sticks.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param attribute: name of the attribute under test.
        :param initial_value: the expected initial value of the attribute.
        :param write_value: a value to write to check that it sticks
        """
        assert getattr(device_under_test, attribute) == initial_value

        if write_value is not None:
            device_under_test.write_attribute(attribute, write_value)
            assert getattr(device_under_test, attribute) == write_value

    def test_beamId(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
        beam_id: int,
    ) -> None:
        """
        Test the beam id attribute.

        This is a very weak test that simply checks that it takes a
        certain initial value.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param beam_id: the beam id of the device.
        """
        assert device_under_test.beamId == beam_id

    def test_stationId(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test stationId attribute.

        This is a very weak test that simply checks that the attribute
        starts as zero.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.stationId == 0

    @pytest.mark.parametrize(
        "attribute",
        [
            "channels",
            "antennaWeights",
            "phaseCentre",
            "pointingDelay",
            "pointingDelayRate",
        ],
    )
    def test_empty_list_attributes(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
        attribute: str,
    ) -> None:
        """
        Test attribute values for attributes that return lists of floats.

        This is a very weak test that simply checks that attributes are
        initialised to the empty list.

        Due to a Tango bug with return of empty lists through image
        attributes or float spectrum attributes, the attribute value may
        come through as None rather than [].

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param attribute: name of the attribute under test.
        """
        value = getattr(device_under_test, attribute)
        assert value is None or not list(value)

    def test_desired_pointing(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test the desired pointing attribute.

        This is a weak test that simply check that the attribute's
        initial value is as expected.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert list(device_under_test.desiredPointing) == [0.0, 0.0, 0.0, 0.0]

    @pytest.mark.parametrize("target_health_state", list(HealthState))
    def test_component_state_callback_health(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
        mock_station_beam_component_manager: StationBeamComponentManager,
        target_health_state: HealthState,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test `component_state_changed` properly handles health updates.

        Here we test that the change event is pushed and that we receive
        it and that the health state is correctly updated.

        :param mock_station_beam_component_manager: A fixture that
            provides a partially mocked component
            manager which has access to the
            component_state_callback.

        :param target_health_state: The HealthState that the device should end up in.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        device_under_test.subscribe_event(
            "healthState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.OK)
        get_station_health = False  # We want station beam health.

        # Initial state is OK so skip that one.
        if target_health_state != HealthState.OK:
            mock_station_beam_component_manager._component_state_callback(
                health=target_health_state
            )
            change_event_callbacks["healthState"].assert_change_event(
                target_health_state
            )
            dev_final_health_state = device_under_test.examine_health_model(
                get_station_health
            )
            assert dev_final_health_state == target_health_state

    @pytest.mark.parametrize("target_health_state", list(HealthState))
    def test_component_state_callback_station_health(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
        mock_station_beam_component_manager: StationBeamComponentManager,
        target_health_state: HealthState,
        station_on_name: str,
    ) -> None:
        """
        Test `component_state_changed` properly handles health updates.

        Test that the station health attribute is correctly updated.

        :param mock_station_beam_component_manager: A fixture that
            provides a partially mocked component
            manager which has access to the
            component_state_callback.

        :param target_health_state: The HealthState that the device should end up in.
        :param station_on_name: the name of a station which is on.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        get_station_health = True  # We want station health.

        mock_station_beam_component_manager._component_state_callback(
            health=target_health_state, trl=get_station_trl(station_on_name)
        )
        # Check that station health has updated.
        station_final_health_state = device_under_test.examine_health_model(
            get_station_health
        )
        assert station_final_health_state == target_health_state

    @pytest.mark.parametrize("station_fault", [True, False])
    def test_component_state_callback_station_fault(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
        mock_station_beam_component_manager: StationBeamComponentManager,
        station_fault: bool,
        station_on_name: str,
    ) -> None:
        """
        Test `component_state_changed` properly handles station fault updates.

        Test that the station_fault attribute updated correctly.

        :param mock_station_beam_component_manager: A fixture that
            provides a partially mocked component
            manager which has access to the
            component_state_callback.

        :param station_fault: Whether the station is faulting or coming out of fault.
        :param station_on_name: the name of a station which is on.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        mock_station_beam_component_manager._component_state_callback(
            fault=station_fault, trl=get_station_trl(station_on_name)
        )
        final_fault = device_under_test.get_station_fault()
        assert final_fault == station_fault

    @pytest.mark.parametrize("beam_locked", [True, False])
    def test_component_state_callback_beam_locked(
        self: TestMccsStationBeam,
        device_under_test: MccsDeviceProxy,
        mock_station_beam_component_manager: StationBeamComponentManager,
        beam_locked: bool,
    ) -> None:
        """
        Test `component_state_changed` properly handles health updates.

        Here we only test that the change event is pushed and that we
        receive it. HealthState.UNKNOWN is omitted due to it being the
        initial state.

        :param mock_station_beam_component_manager: A fixture that
            provides a partially mocked component
            manager which has access to the
            component_state_callback.

        :param beam_locked: Whether the beam is locked or not.
        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert (
            mock_station_beam_component_manager._communication_state_callback
            is not None
        )
        mock_station_beam_component_manager._communication_state_callback(
            CommunicationStatus.ESTABLISHED
        )
        mock_station_beam_component_manager._component_state_callback(
            beam_locked=beam_locked
        )
        mock_station_beam_component_manager._component_state_callback(
            trl=get_station_trl("ci-1"), health=HealthState.OK
        )
        beam_health = device_under_test.get_beam_health()
        if beam_locked:
            assert beam_health == HealthState.OK
        else:
            assert beam_health == HealthState.DEGRADED
