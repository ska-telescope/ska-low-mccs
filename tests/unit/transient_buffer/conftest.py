# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defined a pytest harness for testing the MCCS transient buffer module."""
from __future__ import annotations

import logging

import pytest
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.transient_buffer import (
    TransientBuffer,
    TransientBufferComponentManager,
)


@pytest.fixture(name="transient_buffer_component")
def transient_buffer_component_fixture(
    logger: logging.Logger,
) -> TransientBuffer:
    """
    Fixture that returns a transient buffer.

    :param logger: a logger for the transient buffer to use.
    :return: a transient buffer
    """
    return TransientBuffer(logger)


@pytest.fixture(name="transient_buffer_component_manager")
def transient_buffer_component_manager_fixture(
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> TransientBufferComponentManager:
    """
    Return a transient buffer component manager.

    :param logger: the logger to be used by this object.
    :param callbacks: A dictionary of callbacks with async support.
    :return: a transient buffer component manager
    """
    return TransientBufferComponentManager(
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )
