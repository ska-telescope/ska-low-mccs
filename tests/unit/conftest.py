# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains pytest-specific test harness for MCCS unit tests."""
from typing import Optional

import pytest
from ska_low_mccs_common.testing.tango_harness import (
    DevicesToLoadType,
    DeviceToLoadType,
)


def pytest_itemcollected(item: pytest.Item) -> None:
    """
    Modify a test after it has been collected by pytest.

    This pytest hook implementation adds the "forked" custom mark to all
    tests that use the ``tango_harness`` fixture, causing them to be
    sandboxed in their own process.

    :param item: the collected test for which this hook is called
    """
    if "tango_harness" in item.fixturenames:  # type: ignore[attr-defined]
        item.add_marker("forked")


@pytest.fixture(name="devices_to_load")
def devices_to_load_fixture(
    device_to_load: Optional[DeviceToLoadType],
) -> Optional[DevicesToLoadType]:
    """
    Fixture that provides specifications of devices to load.

    In this case, it maps the simpler single-device spec returned by the
    "device_to_load" fixture used in unit testing, onto the more general
    multi-device spec.

    :param device_to_load: fixture that provides a specification of a
        single device to load; used only in unit testing where tests
        will only ever stand up one device at a time.
    :return: specification of the devices (in this case, just one
        device) to load
    """
    if device_to_load is None:
        return None

    device_spec: DevicesToLoadType = {
        "path": device_to_load["path"],
        "package": device_to_load["package"],
        "devices": [
            {
                "name": device_to_load["device"],
                "proxy": device_to_load["proxy"],
            }
        ],
    }
    if "patch" in device_to_load:
        assert device_spec["devices"] is not None  # for the type checker
        device_spec["devices"][0]["patch"] = device_to_load["patch"]

    return device_spec


# @pytest.fixture(name="callbacks")
# def callbacks_fixture() -> MockCallableGroup:
#     """
#     Return a dictionary of callbacks with asynchrony support.

#     :return: a collections.defaultdict that returns callbacks by name.
#     """
#     return MockCallableGroup(
#         "communication_state",
#         "component_state",
#         "task",
#         timeout=10.0,
#     )

# @pytest.fixture(name="change_event_callbacks")
# def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
#     """
#     Return a dictionary of change event callbacks with asynchrony support.

#     :return: a callback group.
#     """
#     return MockTangoEventCallbackGroup(
#         "healthState",
#         "adminMode",
#         "state",
#         "longRunningCommandResult",
#         "obsState",
#         timeout=15.0,
#     )


@pytest.fixture(name="device_to_load")
def device_to_load_fixture() -> Optional[DeviceToLoadType]:
    """
    Fixture that specifies the device to be loaded for testing.

    This default implementation specified no devices to be loaded,
    allowing the fixture to be left unspecified if no devices are
    needed.

    :return: specification of the device to be loaded
    """
    return None
