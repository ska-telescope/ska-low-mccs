# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests of the controller component manager."""
from __future__ import annotations

import json
import time
import unittest.mock
from copy import copy
from typing import Callable, Iterator

import pytest
import tango
from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    HealthState,
    ObsState,
    PowerState,
    ResultCode,
    TaskStatus,
)
from ska_low_mccs_common import MccsDeviceProxy
from ska_low_mccs_common.testing.mock import MockCallable
from ska_tango_base.faults import ComponentError
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.controller import ControllerComponentManager
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_station_beam_trl,
    get_station_trl,
    get_subarray_beam_trl,
    get_subarray_trl,
)


# pylint: disable=too-many-locals
@pytest.fixture(name="test_context")
def test_context_fixture(  # pylint: disable=too-many-arguments
    subarray_ids: list[int],
    mock_subarray_factory: Callable[[], unittest.mock.Mock],
    subarray_beam_ids: list[int],
    mock_subarray_beam_factory: Callable[[], unittest.mock.Mock],
    station_names: list[str],
    mock_station_1_factory: Callable[[], unittest.mock.Mock],
    mock_station_2_factory: Callable[[], unittest.mock.Mock],
    station_beam_ids: list[tuple[str, int]],
    mock_station_beam_factory: Callable[[], unittest.mock.Mock],
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which mock tile and field station devices are running.

    :param subarray_ids: A list of Subarray IDs.
    :param mock_subarray_factory: a factory that returns a mock subarray
        device each time it is called
    :param subarray_beam_ids: A list of SubarrayBeam IDs.
    :param mock_subarray_beam_factory: a factory that returns a mock
        subarray beam device each time it is called
    :param station_names: A list of Station names.
    :param mock_station_1_factory: a factory that returns a mock
        station device with ID 1 each time it is called
    :param mock_station_2_factory: a factory that returns a mock
        station device with ID 2 each time it is called
    :param station_beam_ids: A list of StationBeam IDs.
    :param mock_station_beam_factory: a factory that returns a mock
        station beam device each time it is called

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()

    for subarray_id in subarray_ids:
        harness.add_mock_subarray_device(subarray_id, mock_subarray_factory())
    for subarray_beam_id in subarray_beam_ids:
        harness.add_mock_subarray_beam_device(
            subarray_beam_id, mock_subarray_beam_factory()
        )
    mock_station_factory = [mock_station_1_factory, mock_station_2_factory]
    for index, station_name in enumerate(station_names):
        harness.add_mock_station_device(station_name, mock_station_factory[index]())
    for station_name, station_beam_id in station_beam_ids:
        harness.add_mock_station_beam_device(
            station_name, station_beam_id, mock_station_beam_factory()
        )

    with harness as context:
        yield context


class TestControllerComponentManager:
    """Tests of the controller component manager."""

    def test_communication(
        self: TestControllerComponentManager,
        test_context: MccsTangoTestHarnessContext,
        controller_component_manager: ControllerComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the controller component manager's management.

        :param test_context: the test context in which this test will be run.
        :param controller_component_manager: the controller component
            manager under test.
        :param callbacks: A dictionary of callbacks with async support.
        """
        assert (
            controller_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        controller_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )

        for trl in controller_component_manager._subarrays.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        for trl in controller_component_manager._stations.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        for trl in controller_component_manager._subarray_beams.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        for trl in controller_component_manager._station_beams.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        assert (
            controller_component_manager._communication_state
            == CommunicationStatus.ESTABLISHED
        )
        controller_component_manager.stop_communicating()
        callbacks["communication_state"].assert_call(CommunicationStatus.DISABLED)

        assert (
            controller_component_manager._communication_state
            == CommunicationStatus.DISABLED
        )

    def test_power_commands(
        self: TestControllerComponentManager,
        test_context: MccsTangoTestHarnessContext,
        controller_component_manager: ControllerComponentManager,
        station_names: list[str],
        station_proxies: list[unittest.mock.Mock],
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test that the power commands work as expected.

        :param test_context: the test context in which this test will be run.
        :param controller_component_manager: the controller component
            manager under test.
        :param station_names: names of station devices
        :param station_proxies: list of proxies to MCCS station devices
        :param callbacks: A dictionary of callbacks with async support.
        """
        assert controller_component_manager._component_state_callback is not None
        controller_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        assert (
            controller_component_manager._communication_state
            == CommunicationStatus.ESTABLISHED
        )
        controller_component_manager.on()

        for proxy in station_proxies:
            result, msg = proxy.On()
            assert result == [ResultCode.QUEUED]

        # pretend to receive events
        for trl in [get_station_trl(label) for label in station_names]:
            controller_component_manager._component_state_callback(
                power=PowerState.ON,
                trl=trl,
            )

        for proxy in station_proxies:
            result, msg = proxy.StartAcquisition("{}")
            assert result == [ResultCode.QUEUED]

        controller_component_manager._component_state_callback(power=PowerState.ON)

        controller_component_manager.off()

        for proxy in station_proxies:
            result, msg = proxy.Off()
            assert result == [ResultCode.QUEUED]

        # pretend to receive events
        for label in station_names:
            controller_component_manager._component_state_callback(
                power=PowerState.OFF, trl=get_station_trl(label)
            )

    # pylint: disable=too-many-locals, too-many-statements
    def test_subarray_allocation(
        self: TestControllerComponentManager,
        test_context: MccsTangoTestHarnessContext,
        controller_component_manager: ControllerComponentManager,
        subarray_proxies: dict[str, MccsDeviceProxy],
        allocate_resource_spec: dict,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test for subarray allocation.

        That:

        * If we try to allocate resources to a subarray before we the
          controller component manager has been turned on, the attempt
          will fail.

        * If we try to allocate resources to an unknown subarray, the
          allocation will be rejected

        * If we try to allocate unknown resources to a known subarray,
          the allocation will be rejected

        * If we try to allocate known resources to a known subarray, the
          allocation will be rejected until the controller component
          manager learns that the subarray is online

        * If we try to allocate resources to an online subarray,
          the allocation will succeed, and the subarray will be assigned
          the resources.

        :param test_context: the test context in which this test will be run.
        :param controller_component_manager: the controller component
            manager under test.
        :param subarray_proxies: proxies to the this controller's
            subarrays.
        :param allocate_resource_spec: a schema_compliant allocate schema for use
            in testing.
        :param callbacks: A dictionary of callbacks with async support.
        """

        allocated_subarray_id = allocate_resource_spec["subarray_id"]
        allocated_subarray_beam_id = allocate_resource_spec["subarray_beams"][0][
            "subarray_beam_id"
        ]
        allocated_number_of_channels = allocate_resource_spec["subarray_beams"][0][
            "number_of_channels"
        ]

        controller_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )

        for trl in controller_component_manager._subarrays.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        for trl in controller_component_manager._stations.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        for trl in controller_component_manager._subarray_beams.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        for trl in controller_component_manager._station_beams.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            controller_component_manager._communication_state
            == CommunicationStatus.ESTABLISHED
        )

        # Subarray is an always-on device, so this should always be received after we
        # establish communication with it.
        for trl in controller_component_manager._subarrays.keys():
            controller_component_manager._subarrays[trl]._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )
        for trl in controller_component_manager._subarray_beams.keys():
            controller_component_manager._subarray_beams[trl]._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )
        for trl in controller_component_manager._station_beams.keys():
            controller_component_manager._station_beams[trl]._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )
        for trl in controller_component_manager._station_beams.keys():
            controller_component_manager._station_beam_health_changed(
                trl,
                HealthState.OK,
            )

        with pytest.raises(ComponentError, match="Component is not powered ON"):
            result, msg = controller_component_manager.allocate(
                subarray_id=allocated_subarray_id,
                subarray_beams=allocate_resource_spec["subarray_beams"],
            )
            assert result == TaskStatus.FAILED

        # Callbacks are handled in the device, so need to tell the
        # component manager explicitly that each device is ON
        for trl in controller_component_manager._stations.keys():
            controller_component_manager._device_power_states[trl] = PowerState.ON

        for subarray in controller_component_manager._subarrays.values():
            subarray._component_state["power"] = PowerState.ON
        controller_component_manager._evaluate_power_state()

        assert controller_component_manager.power_state == PowerState.ON

        unready_subarray = get_subarray_trl(allocated_subarray_id)
        task_callback = MockCallable()
        controller_component_manager.allocate(
            task_callback,
            subarray_id=allocated_subarray_id,
            subarray_beams=allocate_resource_spec["subarray_beams"],
        )
        time.sleep(0.5)
        controller_component_manager._device_obs_state_changed(
            get_subarray_trl(allocated_subarray_id), ObsState.EMPTY
        )

        _, kwargs = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.QUEUED

        _, kwargs = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.IN_PROGRESS

        _, kwargs = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.REJECTED
        assert kwargs["result"] == (
            ResultCode.REJECTED,
            f"Allocatee is unready: {unready_subarray}.",
        )

        for trl in controller_component_manager._subarrays.keys():
            controller_component_manager._subarray_health_changed(
                trl,
                HealthState.OK,
            )

        controller_component_manager.allocate(
            subarray_id=allocated_subarray_id,
            subarray_beams=allocate_resource_spec["subarray_beams"],
        )
        time.sleep(0.5)
        controller_component_manager._device_obs_state_changed(
            get_subarray_trl(allocated_subarray_id), ObsState.EMPTY
        )

        expected_apertures = copy(
            allocate_resource_spec["subarray_beams"][0]["apertures"]
        )
        expected_apertures[0]["station_trl"] = get_station_trl(
            f"ci-{expected_apertures[0]['station_id']}"
        )

        expected_apertures[0]["station_beam_trl"] = "low-mccs/beam/ci-1-01"
        expected_apertures[0]["hardware_beam"] = 0
        expected_apertures[0]["channel_blocks"] = list(
            range(int(allocated_number_of_channels / 8))
        )

        expected_assign_resources_call = {
            "subarray_id": allocated_subarray_id,
            "subarray_beams": [
                {
                    "subarray_beam_id": allocated_subarray_beam_id,
                    "apertures": expected_apertures,
                    "number_of_channels": allocated_number_of_channels,
                    "first_subarray_channel": 0,
                    "subarray_beam_trl": get_subarray_beam_trl(
                        allocated_subarray_beam_id
                    ),
                }
            ],
        }

        subarray_proxies[allocated_subarray_id - 1].AssignResources.assert_next_call(
            json.dumps(expected_assign_resources_call)
        )

        allocate_resource_spec["subarray_id"] = 2
        # This should cause a resourcing error, as we just allocated all available
        # channels to the previous subarray.
        allocate_resource_spec["subarray_beams"][0]["number_of_channels"] = 8

        result, msg = controller_component_manager.allocate(
            subarray_id=allocate_resource_spec["subarray_id"],
            subarray_beams=allocate_resource_spec["subarray_beams"],
        )
        assert result == TaskStatus.QUEUED

        # We tried to allocate with too many resources, so our second subarray
        # shouldn't receive any calls.
        subarray_proxies[
            allocate_resource_spec["subarray_id"] - 1
        ].AssignResources.assert_not_called()

        controller_component_manager.release(1)
        subarray_proxies[
            allocated_subarray_id - 1
        ].ReleaseAllResources.assert_next_call()

    # pylint: disable=too-many-branches, too-many-statements
    def test_allocate(  # noqa: C901
        self: TestControllerComponentManager,
        test_context: MccsTangoTestHarnessContext,
        controller_component_manager: ControllerComponentManager,
        station_beam_ids: list[tuple],
        allocate_resource_spec: dict,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test for ControllerComponentManager _allocate() method.

        This is the method that is submitted as a long running command.
        We test it separately here so as not to rely on a simple
        TaskStatus.FAILED callback.

        :param test_context: the test context in which this test will be run.
        :param controller_component_manager: the controller component
            manager under test.
        :param station_beam_ids: a list of station beam IDs in the test.
        :param allocate_resource_spec: a schema_compliant allocate schema for use
            in testing.
        :param callbacks: A dictionary of callbacks with async support.
        """
        controller_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )

        for trl in controller_component_manager._subarrays.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        for trl in controller_component_manager._stations.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        for trl in controller_component_manager._subarray_beams.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        for trl in controller_component_manager._station_beams.keys():
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        assert (
            controller_component_manager._communication_state
            == CommunicationStatus.ESTABLISHED
        )

        # Subarray is an always-on device, so this should always be received after we
        # establish communication with it.
        for trl in controller_component_manager._subarrays.keys():
            controller_component_manager._subarrays[trl]._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )
            controller_component_manager._subarrays[trl]._update_communication_state(
                CommunicationStatus.ESTABLISHED
            )
        for trl in controller_component_manager._subarray_beams.keys():
            controller_component_manager._subarray_beams[trl]._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )
            controller_component_manager._subarray_beams[
                trl
            ]._update_communication_state(CommunicationStatus.ESTABLISHED)
        for trl in controller_component_manager._station_beams.keys():
            controller_component_manager._station_beams[trl]._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )
            controller_component_manager._station_beams[
                trl
            ]._update_communication_state(CommunicationStatus.ESTABLISHED)

        for trl in controller_component_manager._subarrays.keys():
            controller_component_manager._subarray_health_changed(
                trl,
                HealthState.OK,
            )
        for trl in controller_component_manager._station_beams.keys():
            controller_component_manager._station_beam_health_changed(
                trl,
                HealthState.OK,
            )
        for trl in controller_component_manager._subarray_beams.keys():
            controller_component_manager._subarray_beam_health_changed(
                trl,
                HealthState.OK,
            )

        # Make certain our proxies are up and ready.
        start_time = time.time()
        timeout = 1
        while time.time() < start_time + timeout:
            try:
                for subarray in controller_component_manager._subarrays.values():
                    assert subarray._proxy is not None
                for (
                    subarray_beam
                ) in controller_component_manager._subarray_beams.values():
                    assert subarray_beam._proxy is not None
                for (
                    station_beam
                ) in controller_component_manager._station_beams.values():
                    assert station_beam._proxy is not None
                for station in controller_component_manager._stations.values():
                    assert station._proxy is not None
                break
            except AssertionError:
                time.sleep(0.1)

        for subarray in controller_component_manager._subarrays.values():
            assert subarray._proxy is not None
        for subarray_beam in controller_component_manager._subarray_beams.values():
            assert subarray_beam._proxy is not None
        for station_beam in controller_component_manager._station_beams.values():
            assert station_beam._proxy is not None
        for station in controller_component_manager._stations.values():
            assert station._proxy is not None

        for trl in controller_component_manager._stations.keys():
            controller_component_manager._station_power_state_changed(
                trl, PowerState.ON
            )

        allocated_apertures = allocate_resource_spec["subarray_beams"][0]["apertures"][
            0
        ]
        allocated_apertures["station_beam_trl"] = get_station_beam_trl(
            station_beam_ids[0][0], station_beam_ids[0][1]
        )
        allocated_apertures["channel_blocks"] = allocate_resource_spec[
            "subarray_beams"
        ][0]["number_of_channels"]
        allocated_apertures["hardware_beam"] = 0
        allocated_apertures["station_trl"] = get_station_trl(station_beam_ids[0][0])

        allocated_subarray_id = allocate_resource_spec["subarray_id"]
        allocated_subarray_beam = allocate_resource_spec["subarray_beams"]
        allocated_subarray_beam[0]["first_subarray_channel"] = 0

        controller_component_manager._evaluate_power_state()
        task_callback = MockCallable()
        controller_component_manager.allocate(
            task_callback,
            subarray_id=allocated_subarray_id,
            subarray_beams=allocated_subarray_beam,
        )

        _, kwargs = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.QUEUED

        _, kwargs = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.IN_PROGRESS

        time.sleep(0.5)
        controller_component_manager._device_obs_state_changed(
            get_subarray_trl(allocated_subarray_id), ObsState.EMPTY
        )

        time.sleep(0.5)
        controller_component_manager._device_obs_state_changed(
            get_subarray_trl(allocated_subarray_id), ObsState.IDLE
        )

        (_, kwargs) = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.COMPLETED

    # pylint: disable=too-many-statements
    def test_restart_subarray(  # noqa: C901
        self: TestControllerComponentManager,
        test_context: MccsTangoTestHarnessContext,
        controller_component_manager: ControllerComponentManager,
        station_beam_ids: list[tuple],
        allocate_resource_spec: dict,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test for ControllerComponentManager _restart_subarray() method.

        This is the method that is submitted as a long running command.
        We test it separately here so as not to rely on a simple
        TaskStatus.FAILED callback.

        :param test_context: the test context in which this test will be run.
        :param controller_component_manager: the controller component
            manager under test.
        :param station_beam_ids: a list of station beam IDs in the test.
        :param allocate_resource_spec: a schema_compliant allocate schema for use
            in testing.
        :param callbacks: A dictionary of callbacks with async support.
        """
        controller_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )

        for trl in (
            list(controller_component_manager._subarrays.keys())
            + list(controller_component_manager._stations.keys())
            + list(controller_component_manager._subarray_beams.keys())
            + list(controller_component_manager._station_beams.keys())
        ):
            controller_component_manager._device_communication_state_changed(
                trl,
                CommunicationStatus.ESTABLISHED,
            )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        assert (
            controller_component_manager._communication_state
            == CommunicationStatus.ESTABLISHED
        )

        # Subarray is an always-on device, so this should always be received after we
        # establish communication with it.
        for trl in controller_component_manager._subarrays.keys():
            controller_component_manager._subarrays[trl]._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )
            controller_component_manager._subarrays[trl]._update_communication_state(
                CommunicationStatus.ESTABLISHED
            )
        for trl in controller_component_manager._subarray_beams.keys():
            controller_component_manager._subarray_beams[trl]._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )
            controller_component_manager._subarray_beams[
                trl
            ]._update_communication_state(CommunicationStatus.ESTABLISHED)
        for trl in controller_component_manager._station_beams.keys():
            controller_component_manager._station_beams[trl]._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )
            controller_component_manager._station_beams[
                trl
            ]._update_communication_state(CommunicationStatus.ESTABLISHED)

        for trl in controller_component_manager._subarrays.keys():
            controller_component_manager._subarray_health_changed(
                trl,
                HealthState.OK,
            )
        for trl in controller_component_manager._station_beams.keys():
            controller_component_manager._station_beam_health_changed(
                trl,
                HealthState.OK,
            )
        for trl in controller_component_manager._subarray_beams.keys():
            controller_component_manager._subarray_beam_health_changed(
                trl,
                HealthState.OK,
            )

        # Make certain our proxies are up and ready.
        start_time = time.time()
        timeout = 1
        while time.time() < start_time + timeout:
            try:
                for proxy in (
                    list(controller_component_manager._subarrays.values())
                    + list(controller_component_manager._subarray_beams.values())
                    + list(controller_component_manager._station_beams.values())
                    + list(controller_component_manager._stations.values())
                ):
                    assert proxy._proxy is not None
                break
            except AssertionError:
                time.sleep(0.1)

        for proxy in (
            list(controller_component_manager._subarrays.values())
            + list(controller_component_manager._subarray_beams.values())
            + list(controller_component_manager._station_beams.values())
            + list(controller_component_manager._stations.values())
        ):
            assert proxy._proxy is not None

        for trl in controller_component_manager._stations.keys():
            controller_component_manager._station_power_state_changed(
                trl, PowerState.ON
            )

        allocated_apertures = allocate_resource_spec["subarray_beams"][0]["apertures"][
            0
        ]
        allocated_apertures["station_beam_trl"] = get_station_beam_trl(
            station_beam_ids[0][0], station_beam_ids[0][1]
        )
        allocated_apertures["channel_blocks"] = allocate_resource_spec[
            "subarray_beams"
        ][0]["number_of_channels"]
        allocated_apertures["hardware_beam"] = 0
        allocated_apertures["station_trl"] = get_station_trl(station_beam_ids[0][0])

        allocated_subarray_id = allocate_resource_spec["subarray_id"]
        allocated_subarray_beam = allocate_resource_spec["subarray_beams"]
        allocated_subarray_beam[0]["first_subarray_channel"] = 0
        controller_component_manager._evaluate_power_state()
        task_callback = MockCallable()
        controller_component_manager.allocate(
            task_callback,
            subarray_id=allocated_subarray_id,
            subarray_beams=allocated_subarray_beam,
        )

        _, kwargs = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.QUEUED

        _, kwargs = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.IN_PROGRESS

        time.sleep(0.5)
        controller_component_manager._device_obs_state_changed(
            get_subarray_trl(allocated_subarray_id), ObsState.EMPTY
        )

        time.sleep(0.5)
        controller_component_manager._device_obs_state_changed(
            get_subarray_trl(allocated_subarray_id), ObsState.IDLE
        )

        # RestartSubarray cares about devices other than just Subarray.
        controller_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(allocated_subarray_beam[0]["subarray_beam_id"]),
            ObsState.IDLE,
        )
        controller_component_manager._device_obs_state_changed(
            get_station_beam_trl(station_beam_ids[0][0], station_beam_ids[0][1]),
            ObsState.IDLE,
        )

        (_, kwargs) = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.COMPLETED

        controller_component_manager.restart_subarray(
            allocated_subarray_id, task_callback=task_callback
        )
        _, kwargs = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.QUEUED

        _, kwargs = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.IN_PROGRESS

        time.sleep(0.5)
        controller_component_manager._device_obs_state_changed(
            get_subarray_trl(allocated_subarray_id), ObsState.ABORTED
        )
        controller_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(allocated_subarray_beam[0]["subarray_beam_id"]),
            ObsState.ABORTED,
        )
        controller_component_manager._device_obs_state_changed(
            get_station_beam_trl(station_beam_ids[0][0], station_beam_ids[0][1]),
            ObsState.ABORTED,
        )

        time.sleep(0.5)
        controller_component_manager._device_obs_state_changed(
            get_subarray_trl(allocated_subarray_id), ObsState.EMPTY
        )
        controller_component_manager._device_obs_state_changed(
            get_subarray_beam_trl(allocated_subarray_beam[0]["subarray_beam_id"]),
            ObsState.EMPTY,
        )
        controller_component_manager._device_obs_state_changed(
            get_station_beam_trl(station_beam_ids[0][0], station_beam_ids[0][1]),
            ObsState.EMPTY,
        )

        (_, kwargs) = task_callback.get_next_call()
        assert kwargs["status"] == TaskStatus.COMPLETED
        assert (
            controller_component_manager._resource_manager.get_allocated(
                get_subarray_trl(allocated_subarray_id)
            )
            == {}
        )

    @pytest.mark.parametrize(
        "device_type, expected",
        [
            (
                "subarray",
                {
                    "subarray": {
                        get_subarray_trl(1): "OK",
                        get_subarray_trl(2): "OK",
                    },
                },
            ),
            (
                "all",
                {
                    "beam": {
                        get_station_beam_trl("ci-1", 1): None,
                        get_station_beam_trl("ci-1", 2): "OK",
                        get_station_beam_trl("ci-2", 1): None,
                        get_station_beam_trl("ci-2", 2): None,
                    },
                    "station": {
                        get_station_trl("ci-1"): None,
                        get_station_trl("ci-2"): None,
                    },
                    "subarraybeam": {
                        get_subarray_beam_trl(1): None,
                        get_subarray_beam_trl(2): None,
                        get_subarray_beam_trl(3): None,
                        get_subarray_beam_trl(4): None,
                    },
                    "subarray": {
                        get_subarray_trl(1): "OK",
                        get_subarray_trl(2): "OK",
                    },
                },
            ),
        ],
    )
    def test_get_healths(
        self: TestControllerComponentManager,
        test_context: MccsTangoTestHarnessContext,
        controller_component_manager: ControllerComponentManager,
        device_type: str,
        expected: dict,
        station_beam_ids: list[tuple],
    ) -> None:
        """
        Testing get_healths(device_type) functions correctly.

            * When called without arguments, the function should
                return the health state of all subdevices.

            * When called with a specific subdevice type, it should
                return the health of all devices of that subtype.

        :param test_context: the test context in which this test will be run.
        :param controller_component_manager: the controller component
            manager under test.
        :param device_type: the device type to return the healths of.
        :param expected: a dictionary containing expected health return.
        :param station_beam_ids: a list of station beam IDs in the test.
        """

        for trl in controller_component_manager._subarrays:
            controller_component_manager._subarrays[trl]._device_admin_mode_changed(
                "adminmode", AdminMode.ONLINE, tango.AttrQuality.ATTR_VALID
            )
        for trl in controller_component_manager._subarrays:
            controller_component_manager._subarrays[trl]._device_health_state_changed(
                "healthstate", HealthState.OK, tango.AttrQuality.ATTR_VALID
            )

        controller_component_manager._station_beams[
            get_station_beam_trl(station_beam_ids[1][0], station_beam_ids[1][1])
        ]._device_admin_mode_changed(
            "adminmode", AdminMode.ONLINE, tango.AttrQuality.ATTR_VALID
        )
        controller_component_manager._station_beams[
            get_station_beam_trl(station_beam_ids[1][0], station_beam_ids[1][1])
        ]._device_health_state_changed(
            "healthstate", HealthState.OK, tango.AttrQuality.ATTR_VALID
        )

        assert controller_component_manager.get_healths(device_type) == expected

    def test_get_healths_invalid_device(
        self: TestControllerComponentManager,
        test_context: MccsTangoTestHarnessContext,
        controller_component_manager: ControllerComponentManager,
    ) -> None:
        """
        Testing get_healths(device_type).

            * When called with an invalid device, return a ValueError.

        :param test_context: the test context in which this test will be run.
        :param controller_component_manager: the controller component
            manager under test.
        """
        with pytest.raises(ValueError):
            controller_component_manager.get_healths("invalid_device")

    def test_get_health_trl(
        self: TestControllerComponentManager,
        test_context: MccsTangoTestHarnessContext,
        controller_component_manager: ControllerComponentManager,
    ) -> None:
        """
        Testing get_health_trl(trl) functions correctly.

            * When called with a valid trl, it returns either string
                or none if the device is in adminmode.

            * When called with an invalid trl, a ValueError is raised.

        :param test_context: the test context in which this test will be run.
        :param controller_component_manager: the controller component
            manager under test.
        """
        controller_component_manager._station_beams[
            get_station_beam_trl("ci-1", 2)
        ]._device_admin_mode_changed(
            "adminmode", AdminMode.ONLINE, tango.AttrQuality.ATTR_VALID
        )
        controller_component_manager._station_beams[
            get_station_beam_trl("ci-1", 2)
        ]._device_health_state_changed(
            "healthstate", HealthState.OK, tango.AttrQuality.ATTR_VALID
        )
        assert (
            controller_component_manager.get_health_trl(get_station_beam_trl("ci-1", 2))
            == HealthState.OK
        )
        assert (
            controller_component_manager.get_health_trl(get_station_beam_trl("ci-2", 1))
            is None
        )
        with pytest.raises(ValueError):
            controller_component_manager.get_health_trl("invalid/trl/oo3")
