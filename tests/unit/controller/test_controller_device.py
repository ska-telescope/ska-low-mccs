# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Contains the tests for the MccsController Tango device_under_test prototype."""
from __future__ import annotations

import gc
import json
import sys
import time
import unittest
from typing import Iterator

import pytest
import tango
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    PowerState,
    ResultCode,
    SimulationMode,
    TestMode,
)
from ska_low_mccs_common import MccsDeviceProxy
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_low_mccs.controller import MccsController
from ska_low_mccs.controller.controller_component_manager import (
    ControllerComponentManager,
)
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_station_beam_trl,
    get_station_trl,
    get_subarray_beam_trl,
    get_subarray_trl,
)

# To prevent tests hanging during gc.
gc.disable()


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> MccsDeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the context in which the tests run

    :return: the device under test
    """
    return test_context.get_controller_device()


class TestMccsController:
    """Tests of the MccsController device."""

    @pytest.fixture(name="test_context")
    def test_context_fixture(
        self: TestMccsController,
        subarray_ids: list[int],
        subarray_beam_ids: list[int],
        station_names: list[str],
        station_beam_ids: list[tuple[str, int]],
        patched_controller_device_class: type[MccsController],
    ) -> Iterator[MccsTangoTestHarnessContext]:
        """
        Create a test harness providing a Tango context for devices under test.

        :param subarray_ids: A list of Subarray IDs.
        :param subarray_beam_ids: A list of SubarrayBeam IDs.
        :param station_names: A list of Station names.
        :param station_beam_ids: A list of StationBeam IDs.
        :param patched_controller_device_class: A Controller device
            class patched with additional methods for testing.

        :yield: A tango context with devices to test.
        """
        harness = MccsTangoTestHarness()
        harness.set_controller_device(
            subarray_ids,
            subarray_beam_ids,
            station_names,
            station_beam_ids,
            device_class=patched_controller_device_class,
        )
        with harness as context:
            yield context

    def test_InitDevice(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for Initialisation.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.healthState == HealthState.UNKNOWN
        assert device_under_test.controlMode == ControlMode.REMOTE
        assert device_under_test.simulationMode == SimulationMode.FALSE
        # TODO: Where is the following line meant to be set?
        # assert device_under_test.testMode == TestMode.TEST
        assert device_under_test.State() == tango.DevState.DISABLE
        assert device_under_test.adminMode == AdminMode.OFFLINE
        assert device_under_test.Status() == "The device is in DISABLE state."

    def test_adminMode(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
        mock_component_manager: ControllerComponentManager,
    ) -> None:
        """
        Test adminMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param mock_component_manager: a mock component manager that has
            been patched into the device under test

        """
        assert device_under_test.adminMode == AdminMode.OFFLINE
        assert device_under_test.state() == tango.DevState.DISABLE
        device_under_test.adminMode = AdminMode.ONLINE

        time.sleep(0.2)

        mcm = mock_component_manager
        mcm.start_communicating.assert_called_once_with()  # type: ignore[attr-defined]

        assert device_under_test.adminMode == AdminMode.ONLINE
        assert device_under_test.state() == tango.DevState.OFF

    @pytest.mark.parametrize(
        ("device_command", "component_method"),
        [
            ("On", "on"),
            ("Off", "off"),
            ("Standby", "standby"),
        ],
    )
    def test_command(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
        device_command: str,
        mock_component_manager: unittest.mock.Mock,
        component_method: str,
    ) -> None:
        """
        Test that device commands are passed through to the component manager.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param device_command: name of the device command
        :param mock_component_manager: a mock component manager that has
            been patched into the device under test

        :param component_method: name of the component method that
            implements the device command

        """
        device_under_test.adminMode = AdminMode.ONLINE
        assert device_under_test.adminMode == AdminMode.ONLINE
        [[result_code], [uid]] = getattr(device_under_test, device_command)()
        if result_code == ResultCode.REJECTED:
            assert uid == f"Device is already in {device_command.upper()} state."
        else:
            assert result_code == ResultCode.QUEUED
            assert uid.split("_")[-1] == device_command
            method = getattr(mock_component_manager, component_method)
            method.assert_called_once()
            assert len(method.call_args[0]) == 1

    @pytest.mark.skip(reason="too weak a test to count")
    def test_Reset(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for Reset.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        with pytest.raises(
            tango.DevFailed,
            match="Command Reset not allowed when the device is in DISABLE state",
        ):
            device_under_test.Reset()

    @pytest.mark.xfail(reason="")
    def test_buildState(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for buildState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert (
            device_under_test.buildState == sys.modules["ska_low_mccs"].__version_info__
        )

    def test_versionId(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for versionId.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.versionId == sys.modules["ska_low_mccs"].__version__

    # pylint: disable=too-many-arguments
    def test_healthState(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
        subarray_beam_ids: list[int],
        station_beam_ids: list[tuple[str, int]],
        station_names: list[str],
        mock_component_manager: unittest.mock.Mock,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for healthState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param subarray_beam_ids: list of subarraybeam IDs to check
            the callbacks from
        :param station_beam_ids: list of stationbeam IDs to check
            the callbacks from
        :param station_names: list of station names to check the callbacks from
        :param mock_component_manager: a mock component manager that has
            been patched into the device under test

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        # The device has subscribed to healthState change events on
        # its subsidiary devices, but hasn't heard from them (because in
        # unit testing these devices are mocked out), so its healthState
        # is UNKNOWN
        device_under_test.adminMode = AdminMode.ONLINE
        assert device_under_test.adminMode == AdminMode.ONLINE
        mock_component_manager._component_state_callback(power=PowerState.ON)
        device_under_test.subscribe_event(
            "healthState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.UNKNOWN)
        assert device_under_test.healthState == HealthState.UNKNOWN

        for beam_trl in [get_subarray_beam_trl(i) for i in subarray_beam_ids] + [
            get_station_trl(label) for label in station_names
        ]:
            mock_component_manager._component_state_callback(
                health=HealthState.OK, trl=beam_trl
            )

        for label, i in station_beam_ids:
            beam_trl = get_station_beam_trl(label, i)
            mock_component_manager._component_state_callback(
                health=HealthState.OK, trl=beam_trl, station_id=label
            )

        change_event_callbacks["healthState"].assert_change_event(
            HealthState.OK, lookahead=2
        )
        assert device_under_test.healthState == HealthState.OK

    def test_controlMode(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for controlMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.controlMode == ControlMode.REMOTE

    def test_simulationMode(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for simulationMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.simulationMode == SimulationMode.FALSE

    @pytest.mark.skip(reason="Attribute doesn't appear to exist.")
    def test_testMode(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for testMode.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.testMode == TestMode.TEST

    def test_allSubdevicesHealths(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test attribute holding health of all subdevices.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.subDeviceHealths == json.dumps(
            {
                "beam": {
                    get_station_beam_trl("ci-1", 1): None,
                    get_station_beam_trl("ci-1", 2): "OK",
                    get_station_beam_trl("ci-2", 1): None,
                    get_station_beam_trl("ci-2", 2): None,
                },
                "station": {
                    get_station_trl("ci-1"): None,
                    get_station_trl("ci-2"): None,
                },
                "subarraybeam": {
                    get_subarray_beam_trl(1): None,
                    get_subarray_beam_trl(2): None,
                    get_subarray_beam_trl(3): None,
                    get_subarray_beam_trl(4): None,
                },
                "subarray": {
                    get_subarray_trl(1): "OK",
                    get_subarray_trl(2): "OK",
                },
            }
        )

    @pytest.mark.parametrize(
        "device_type,expected",
        [
            (
                "subarray",
                json.dumps(
                    {
                        get_subarray_trl(1): "OK",
                        get_subarray_trl(2): "OK",
                    }
                ),
            ),
            (
                "station",
                json.dumps(
                    {
                        get_station_trl("ci-1"): None,
                        get_station_trl("ci-2"): None,
                    }
                ),
            ),
            (
                "subarraybeam",
                json.dumps(
                    {
                        get_subarray_beam_trl(1): None,
                        get_subarray_beam_trl(2): None,
                        get_subarray_beam_trl(3): None,
                        get_subarray_beam_trl(4): None,
                    }
                ),
            ),
            (
                "beam",
                json.dumps(
                    {
                        get_station_beam_trl("ci-1", 1): None,
                        get_station_beam_trl("ci-1", 2): "OK",
                        get_station_beam_trl("ci-2", 1): None,
                        get_station_beam_trl("ci-2", 2): None,
                    }
                ),
            ),
        ],
    )
    def test_healthAttributes(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
        expected: dict,
        device_type: str,
    ) -> None:
        """
        Test for individual subdevice type health attributes.

        This test is looped through for each subdevice type.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param expected: JSON string representing expected result for
            given device type.

        :param device_type: device type to test health of.
        """
        attribute_dict = {
            "subarray": device_under_test.subarrayHealths,
            "station": device_under_test.stationHealths,
            "subarraybeam": device_under_test.subarrayBeamHealths,
            "beam": device_under_test.stationBeamHealths,
        }
        attribute = attribute_dict[device_type]
        assert attribute == expected

    @pytest.mark.parametrize(
        "trl, expected",
        [
            (get_station_beam_trl("ci-1", 2), "OK"),
            (get_station_beam_trl("ci-1", 1), "None"),
        ],
    )
    def test_getHealthTrl(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
        trl: str,
        expected: str,
    ) -> None:
        """
        Test for command retrieving health of subdevice identified by TRL.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param expected: string representing expected result for given TRL.
        :param trl: TRL of device to check health of.
        """
        assert device_under_test.getHealthTrl(trl) == expected

    def test_getHealthTrl_invalid_trl(
        self: TestMccsController,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for command retrieving health of subdevice identified by TRL.

            * When called with an invalid TRL, the command should return
                tango.DevFailed.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        with pytest.raises(tango.DevFailed):
            device_under_test.getHealthTrl("invalid/device/01")
