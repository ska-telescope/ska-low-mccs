# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains tests of the station calibration solver Tango device."""
from __future__ import annotations

import gc
import json
import os
from typing import Iterator

import pytest
import tango
from ska_control_model import AdminMode, TaskStatus
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from tests.conftest import ensure_command_result
from tests.harness import MccsTangoTestHarness

# To stop gc causing some tests to hang.
gc.disable()


@pytest.fixture(name="invalid_solve_argument")
def invalid_solve_argument_fixture() -> str:
    """
    Return a invalid argument for the solve command.

    :returns: a argument with a invalid tmdata path.
    """
    cwd = os.path.dirname(os.path.realpath(__file__))
    return json.dumps(
        {
            "data_path": f"{cwd}/testing_data/"
            "correlation_burst_140_20240611_13120_0.hdf5",
            "solution_path": f"{cwd}/testing_data/"
            "correlation_burst_140_20240611_13120_0",
            "eep_filebase": "tmp_",
            "station_config_path": ["invalid", "path"],
            "skymodel": "gsm",
            "min_uv": 3,
            "refant": 2,
            "gain_threshold": 0.5,
        }
    )


@pytest.fixture(name="valid_solve_argument")
def valid_solve_argument_fixture(
    station_config_path: list[str],
) -> str:
    """
    Return a valid argument to the solve command.

    :param station_config_path: a URI to a valid
        tmdata source

    :returns: a argument with a valid argument.
    """
    cwd = os.path.dirname(os.path.realpath(__file__))
    return json.dumps(
        {
            "data_path": f"{cwd}/testing_data/"
            "correlation_burst_140_20240611_13120_0.hdf5",
            "solution_path": f"{cwd}/testing_data/"
            "correlation_burst_140_20240611_13120_0",
            "eep_filebase": "tmp_",
            "station_config_path": station_config_path,
            "skymodel": "gsm",
            "min_uv": 0,
            "refant": 1,
            "gain_threshold": 0.5,
            "ignore_eeps": True,
        }
    )


@pytest.fixture(name="solver_device")
def solver_device_fixture(station_name: str) -> Iterator[tango.DeviceProxy]:
    """
    Return a proxy to the solver device under test.

    :param station_name: the name of the station this solver is
        coupled to.

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()
    cwd = os.path.dirname(os.path.realpath(__file__))
    harness.set_solver_device(
        station_name, f"{cwd}/testing_data/", f"{cwd}/testing_data/"
    )
    with harness as context:
        yield context.get_solver_device(station_name)
    # teardown delete products
    cwd = os.path.dirname(os.path.realpath(__file__))

    if os.path.exists(f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.h5"):
        os.remove(f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.h5")
    if os.path.exists(f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.npy"):
        os.remove(f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.npy")


def test_communication(
    solver_device: tango.DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Test for assignResources.

    :param solver_device: a proxy to the solver device under test.
    :param change_event_callbacks: A dictionary of change event
        callbacks with async support.
    """
    solver_device.subscribe_event(
        "adminMode",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["adminMode"],
    )
    solver_device.subscribe_event(
        "state",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )

    for _ in range(4):
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert solver_device.adminMode == AdminMode.OFFLINE
        change_event_callbacks["state"].assert_change_event(tango.DevState.DISABLE)
        assert solver_device.state() == tango.DevState.DISABLE

        solver_device.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert solver_device.adminMode == AdminMode.ONLINE
        change_event_callbacks["state"].assert_change_event(tango.DevState.ON)
        assert solver_device.state() == tango.DevState.ON

        solver_device.adminMode = AdminMode.OFFLINE


def test_solve_task_status(
    solver_device: tango.DeviceProxy,
    invalid_solve_argument: str,
    valid_solve_argument: str,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Test command result in response to arguments.

    :param solver_device: a fixture yielding a tango.DeviceProxy to
        the solver instance under test.
    :param invalid_solve_argument: An invalid argument to the solve command
    :param valid_solve_argument: An valid argument to the solve command
    :param change_event_callbacks: A dictionary of change event
        callbacks with async support.
    """
    solver_device.subscribe_event(
        "adminMode",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["adminMode"],
    )
    solver_device.subscribe_event(
        "state",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    change_event_callbacks["state"].assert_change_event(tango.DevState.DISABLE)
    change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)

    solver_device.adminMode = AdminMode.ONLINE

    change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
    change_event_callbacks["state"].assert_change_event(tango.DevState.ON)

    # Check incorrect TMdata URI
    [_], [cmd_id] = solver_device.solve(invalid_solve_argument)
    ensure_command_result(solver_device, 40, TaskStatus.FAILED, cmd_id)

    # Check happy command.
    [_], [cmd_id] = solver_device.solve(valid_solve_argument)
    ensure_command_result(solver_device, 40, TaskStatus.COMPLETED, cmd_id)
