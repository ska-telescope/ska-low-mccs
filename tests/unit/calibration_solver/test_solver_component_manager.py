# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""This module contains tests of the station calibration solver component manager."""
from __future__ import annotations

# import json
# import threading
# import time
import logging
import os
import time
from pathlib import Path
from typing import Iterator

import numpy as np
import pytest
from ska_control_model import (  # ObsState,; PowerState,; ResultCode,; TaskStatus,
    CommunicationStatus,
    PowerState,
    ResultCode,
    TaskStatus,
)
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.calibration_solver import (
    CalibrationSolutionProduct,
    StationCalibrationSolverComponentManager,
)

# import unittest
# from typing import Iterator


@pytest.fixture(name="reference_1_0_product")
def reference_1_0_product_fixture() -> CalibrationSolutionProduct:
    """
    Return a reference 1.0 product to test for changes against.

    :return: the reference 1.0 solution product.
    """
    cwd = os.path.dirname(os.path.realpath(__file__))
    known_1_0_product = CalibrationSolutionProduct(structure_version="1.0")
    known_1_0_product.load_from_hdf5(
        f"{cwd}/testing_data/correlation_burst_"
        "140_20240611_13120_0_known_1_0_solution.h5"
    )
    return known_1_0_product


@pytest.fixture(name="solver_component_manager")
def solver_component_manager_fixture(
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> Iterator[StationCalibrationSolverComponentManager]:
    """
    Return a station calibration solver component manager for testing.

    :param logger: the logger to be used by this object.
    :param callbacks: A dictionary of callbacks with async support.

    :yields: a station calibration solver component manager
    """
    yield StationCalibrationSolverComponentManager(
        "/dummy",
        "/dummy",
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )
    # teardown delete products
    cwd = os.path.dirname(os.path.realpath(__file__))

    if os.path.exists(f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.h5"):
        os.remove(f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.h5")
    if os.path.exists(f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.npy"):
        os.remove(f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.npy")


class TestStationCalibrationSolverComponentManager:
    """Class for testing the station calibration solver component manager."""

    def test_communication(
        self: TestStationCalibrationSolverComponentManager,
        solver_component_manager: StationCalibrationSolverComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's communication with its assigned devices.

        :param solver_component_manager: a fixture returning the
            station calibration solver component manager.
        :param callbacks: A dictionary of callbacks with async support.
        """
        for _ in range(4):
            solver_component_manager.start_communicating()
            callbacks["communication_state"].assert_call(
                CommunicationStatus.ESTABLISHED
            )
            callbacks["component_state"].assert_call(power=PowerState.ON)

            solver_component_manager.stop_communicating()
            callbacks["communication_state"].assert_call(CommunicationStatus.DISABLED)
            callbacks["component_state"].assert_call(power=None)

    def test_solve(
        self: TestStationCalibrationSolverComponentManager,
        solver_component_manager: StationCalibrationSolverComponentManager,
        station_config_path: list[str],
        reference_1_0_product: CalibrationSolutionProduct,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the solve command can run to completion.

        This is a very simple test, checking that solve can run to complation and
        dump a solution to file.

        NOTE: the FEKO_AAVS3_vogel_256_elem_50ohm_140MHz_Xpol_phi.mat files are
        excluded due to the size, leaving us with just the generated file from the
        convert_eep2npy process.

        :param solver_component_manager: a fixture returning the
            station calibration solver component manager.
        :param station_config_path: the url and path of the
            telmodel data.
        :param reference_1_0_product: a reference product to check against.
        :param callbacks: A dictionary of callbacks with async support.
        """
        solver_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        callbacks["component_state"].assert_call(power=PowerState.ON)

        cwd = os.path.dirname(os.path.realpath(__file__))
        solver_component_manager.internal_eep_store = Path(f"{cwd}/testing_data/")
        solver_component_manager.eep_root_path = Path(f"{cwd}/testing_data/")

        solver_component_manager.solve(
            data_path=f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.hdf5",
            solution_path=f"{cwd}/testing_data/"
            "correlation_burst_140_20240611_13120_0",
            eep_filebase="tmp_",
            station_config_path=station_config_path,
            task_callback=callbacks["task_status"],
            ignore_eeps=True,
        )

        callbacks["task_status"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task_status"].assert_call(status=TaskStatus.IN_PROGRESS)

        time_limit = 120  # seconds
        tick = 2  # seconds
        t1 = time.time()
        file_creation_timestamp = 0.0

        while time.time() < t1 + time_limit:
            if os.path.isfile(
                f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.npy"
            ):
                file_creation_timestamp = os.path.getctime(
                    f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0.npy"
                )
                if t1 <= file_creation_timestamp:
                    print("created")
                    break
            time.sleep(tick)

        if not t1 <= file_creation_timestamp <= t1 + time_limit:
            pytest.fail("file not created")

        product = CalibrationSolutionProduct(structure_version="1.0")
        product.load_from_hdf5(
            f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0" ".h5"
        )

        callbacks["task_status"].assert_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Solution successfully calculated"),
        )
        product = CalibrationSolutionProduct(structure_version="1.0")
        product.load_from_hdf5(
            f"{cwd}/testing_data/correlation_burst_140_20240611_13120_0" ".h5"
        )
        _check_product_not_changed(product, reference_1_0_product)


def _check_product_not_changed(
    product: CalibrationSolutionProduct,
    reference: CalibrationSolutionProduct,
) -> None:
    """
    Check that the product matches reference.

    Using a reference product created from a known correlation matrix.
    Check that there have been no changes. If there has this signifies a
    bug or a ftucture change requireing a bump to product structure_version.

    :param product: the product under test.
    :param reference: a reference product to chack against.

    """

    product_error_message = (
        "Product does not match reference dataset. "
        f"Product under test has version {product.structure_version}. "
        f"Reference product has version {reference.structure_version}. "
        "Do we need to bump version or report bug?"
    )

    assert (
        product.structure_version == reference.structure_version
    ), product_error_message

    np.testing.assert_array_almost_equal(
        np.array(product.corrcoeff),
        np.array(reference.corrcoeff),
        decimal=4,
        err_msg=product_error_message,
    )

    np.testing.assert_array_almost_equal(
        np.array(product.residual_max),
        np.array(reference.residual_max),
        decimal=5,
        err_msg=product_error_message,
    )

    np.testing.assert_array_almost_equal(
        np.array(product.residual_std),
        np.array(reference.residual_std),
        decimal=5,
        err_msg=product_error_message,
    )
    assert product.xy_phase == pytest.approx(
        reference.xy_phase, abs=1e-6
    ), product_error_message
    assert product.n_masked_initial == reference.n_masked_initial, product_error_message
    assert product.n_masked_final == reference.n_masked_final, product_error_message
    assert product.lst == pytest.approx(reference.lst, abs=1e-6), product_error_message
    assert product.galactic_centre_elevation == pytest.approx(
        reference.galactic_centre_elevation, abs=1e-6
    ), product_error_message
    assert product.sun_elevation == pytest.approx(
        reference.sun_elevation, abs=1e-6
    ), product_error_message
    assert product.sun_adjustment_factor == pytest.approx(
        reference.sun_adjustment_factor, abs=1e-6
    ), product_error_message

    np.testing.assert_array_almost_equal(
        product.masked_antennas,
        reference.masked_antennas,
        decimal=5,
        err_msg=product_error_message,
    )

    np.testing.assert_array_almost_equal(
        product.solution,
        reference.solution,
        decimal=5,
        err_msg=product_error_message,
    )
    assert (
        product.frequency_channel == reference.frequency_channel
    ), product_error_message
    assert product.station_id == reference.station_id, product_error_message
    assert product.acquisition_time == reference.acquisition_time, product_error_message
