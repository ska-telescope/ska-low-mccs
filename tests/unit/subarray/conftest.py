# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defined a pytest harness for testing the MCCS subarray module."""
from __future__ import annotations

import logging
import unittest.mock
from typing import Type

import pytest
import tango
from ska_control_model import ResultCode
from ska_low_mccs_common.testing import TangoHarness
from ska_low_mccs_common.testing.mock import MockDeviceBuilder
from ska_tango_testing.mock import MockCallable, MockCallableGroup

from ska_low_mccs.subarray import MccsSubarray, SubarrayComponentManager
from tests.harness import get_station_beam_trl, get_station_trl, get_subarray_beam_trl


@pytest.fixture(name="patched_subarray_device_class")
def patched_subarray_device_class_fixture(
    mock_subarray_component_manager: SubarrayComponentManager,
) -> Type[MccsSubarray]:
    """
    Return a subarray device class, patched with extra methods for testing.

    :param mock_subarray_component_manager: A fixture that provides a
        partially mocked component manager which has access to the
        component_state_callback.
    :return: a patched subarray device class, patched with extra methods
        for testing
    """

    # pylint: disable=too-many-ancestors
    class PatchedSubarrayDevice(MccsSubarray):
        """
        MccsSubarray patched with extra commands for testing purposes.

        The extra commands allow us to mock the receipt of obs state
        change events from subservient devices, turn on DeviceProxies,
        set the initial obsState and examine the health model.
        """

        def create_component_manager(
            self: PatchedSubarrayDevice,
        ) -> SubarrayComponentManager:
            """
            Return a partially mocked component manager instead of the usual one.

            :return: a mock component manager
            """
            wrapped_state_changed_callback = MockCallable(
                wraps=self._component_state_callback
            )
            wrapped_communication_changed_callback = MockCallable(
                wraps=self._communication_state_callback
            )
            mock_subarray_component_manager._communication_state_callback = (
                wrapped_communication_changed_callback
            )
            mock_subarray_component_manager._component_state_callback = (
                wrapped_state_changed_callback
            )

            return mock_subarray_component_manager

    return PatchedSubarrayDevice


@pytest.fixture(name="subarray_component_manager")
def subarray_component_manager_fixture(
    test_context: TangoHarness,
    logger: logging.Logger,
    obs_command_timeout: int,
    callbacks: MockCallableGroup,
) -> SubarrayComponentManager:
    """
    Return a subarray component manager.

    This fixture is identical to `mock_subarray_component_manager`
    except for the inclusion of `tango_harness`. Without `tango_harness`
    the component manager tests experience errors.This fixture is used
    to test subarray_component_manager.

    :param test_context: a test context with devices to test.
    :param logger: the logger to be used by this object.
    :param obs_command_timeout: the default timeout for obs
        commands in seconds.
    :param callbacks: A dictionary of callbacks with async support.
    :return: a subarray component manager
    """
    subarray_id = 1
    cpt_mgr = SubarrayComponentManager(
        subarray_id,
        "nonexistent_skuid_url",
        logger,
        obs_command_timeout,
        callbacks["communication_state"],
        callbacks["component_state"],
    )
    wrapped_device_communication_changed_callback = MockCallable(
        wraps=cpt_mgr._device_communication_state_changed
    )
    cpt_mgr._device_communication_state_changed = (  # type: ignore[assignment]
        wrapped_device_communication_changed_callback
    )
    return cpt_mgr


@pytest.fixture(name="mock_subarray_component_manager")
def mock_subarray_component_manager_fixture(
    logger: logging.Logger, callbacks: MockCallableGroup, obs_command_timeout: int
) -> SubarrayComponentManager:
    """
    Return a subarray component manager.

    This fixture is identical to the `subarray_component_manager`
    fixture except for the `tango_harness` which is omitted here to
    avoid a circular reference. This fixture is used to test
    subarray_device.

    :param logger: the logger to be used by this object.
    :param obs_command_timeout: the default timeout for obs
        commands in seconds.
    :param callbacks: A dictionary of callbacks with async support.
    :return: a subarray component manager
    """
    subarray_id = 1
    cpt_mgr = SubarrayComponentManager(
        subarray_id,
        "nonexistent_skuid_url",
        logger,
        obs_command_timeout,
        callbacks["communication_state"],
        callbacks["component_state"],
    )
    wrapped_device_communication_changed_callback = MockCallable(
        wraps=cpt_mgr._device_communication_state_changed
    )
    cpt_mgr._device_communication_state_changed = (  # type: ignore[assignment]
        wrapped_device_communication_changed_callback
    )
    return cpt_mgr


@pytest.fixture(name="station_off_name")
def station_off_name_fixture() -> str:
    """
    Return the label of a mock station that is powered off.

    :return: the label of a mock station that is powered off.
    """
    return "ci-1"


@pytest.fixture(name="station_on_name")
def station_on_name_fixture() -> str:
    """
    Return the label of a mock station that is powered on.

    :return: the label of a mock station that is powered on.
    """
    return "ci-2"


@pytest.fixture(name="station_off_id")
def station_off_id_fixture() -> int:
    """
    Return the id of a mock station that is powered off.

    :return: the id of a mock station that is powered off.
    """
    return 1


@pytest.fixture(name="station_on_id")
def station_on_id_fixture() -> int:
    """
    Return the id of a mock station that is powered on.

    :return: the id of a mock station that is powered on.
    """
    return 2


@pytest.fixture(name="subarray_id")
def subarray_id_fixture() -> int:
    """
    Return the id of a subarray.

    :return: the id of a subarray.
    """
    return 1


@pytest.fixture(name="subarray_beam_id")
def subarray_beam_id_fixture() -> int:
    """
    Return the id of a mock subarray beam.

    :return: the id of a mock subarray beam.
    """
    return 3


@pytest.fixture(name="station_beam_id")
def station_beam_id_fixture() -> int:
    """
    Return the id of a mock station beam.

    :return: the id of a mock station beam.
    """
    return 3


@pytest.fixture(name="channel_blocks")
def channel_blocks_fixture() -> list[int]:
    """
    Return a list of channel blocks.

    :return: a list of channel blocks.
    """
    return [1]


@pytest.fixture(name="mock_station_off")
def mock_station_off_fixture() -> unittest.mock.Mock:
    """
    Return a mock station device that is powered off.

    :return: a mock station device that is powered off
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_result_command("Configure", result_code=ResultCode.QUEUED)
    builder.add_result_command("DeallocateSubarray", result_code=ResultCode.QUEUED)
    builder.add_command(
        "ApplyConfiguration", return_value=([ResultCode.REJECTED], ["unique_id"])
    )
    builder.add_result_command("Scan", result_code=ResultCode.QUEUED)
    builder.add_result_command("EndScan", result_code=ResultCode.QUEUED)
    return builder()


@pytest.fixture(name="mock_station_on")
def mock_station_on_fixture() -> unittest.mock.Mock:
    """
    Return a mock station device that is powered on.

    :return: a mock station device that is powered on
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_result_command("Configure", result_code=ResultCode.QUEUED)
    builder.add_result_command("DeallocateSubarray", result_code=ResultCode.QUEUED)
    builder.add_command(
        "ApplyConfiguration",
        return_value=([ResultCode.OK], ["Configure command has completed OK."]),
    )
    builder.add_result_command("Scan", result_code=ResultCode.QUEUED)
    builder.add_result_command("EndScan", result_code=ResultCode.QUEUED)
    return builder()


@pytest.fixture(name="mock_subarray_beam")
def mock_subarray_beam_fixture() -> unittest.mock.Mock:
    """
    Return a mock subarray beam device.

    :return: a mock subarray beam device.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    # builder.add_attribute("ObsState", ObsState.EMPTY)
    builder.add_result_command("Configure", result_code=ResultCode.QUEUED)
    builder.add_result_command("Scan", result_code=ResultCode.QUEUED)
    builder.add_result_command("ReleaseAllResources", result_code=ResultCode.QUEUED)
    builder.add_result_command("EndScan", result_code=ResultCode.QUEUED)
    builder.add_result_command("End", result_code=ResultCode.QUEUED)
    builder.add_command("Abort", (ResultCode.STARTED, "Abort has started"))
    return builder()


@pytest.fixture(name="mock_station_beam")
def mock_station_beam_fixture() -> unittest.mock.Mock:
    """
    Return a mock station beam device.

    :return: a mock station beam device.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    # builder.add_attribute("ObsState", ObsState.EMPTY)
    builder.add_result_command("Configure", result_code=ResultCode.QUEUED)
    return builder()


@pytest.fixture(name="scan_id")
def scan_id_fixture() -> int:
    """
    Return a scan id for use in testing.

    :return: a scan id for use in testing.
    """
    return 1


@pytest.fixture(name="start_time")
def start_time_fixture() -> float:
    """
    Return a scan start time for use in testing.

    :return: a scan start time for use in testing.
    """
    return 1.0


@pytest.fixture(name="apertures")
def apertures_fixture(
    station_on_id: int,
    station_on_name: str,
    station_beam_id: int,
    channel_blocks: list[int],
) -> list[dict]:
    """
    Return an example aperture for use in testing.

    :param station_on_id: the id of a station which is on.
    :param station_on_name: the label of a station which is on.
    :param station_beam_id: the id of a station beam
    :param channel_blocks: list of channel blocks for use in testing.

    :return: an example fixture for use in testing.

    """
    apertures = [
        {
            "station_id": station_on_id,
            "aperture_id": "AP001.01",
            "station_trl": get_station_trl(station_on_name),
            "station_beam_trl": get_station_beam_trl(station_on_name, station_beam_id),
            "channel_blocks": channel_blocks,
            "hardware_beam": 16,
        },
    ]

    return apertures


@pytest.fixture(name="assign_resource_spec")
def assign_resource_spec_fixture(
    subarray_beam_id: int,
    subarray_id: int,
    apertures: list[dict],
) -> dict:
    """
    Return an example schema-complient allocate argument for use in testing.

    :param subarray_beam_id: the id of a subarray beam.
    :param subarray_id: the id of a subarray.
    :param apertures: an example list of apertures for use in testing.

    :return: an example schema-complient allocate argument for use in testing.
    """
    resource_spec = {
        "subarray_id": subarray_id,
        "subarray_beams": [
            {
                "subarray_beam_id": subarray_beam_id,
                "subarray_beam_trl": get_subarray_beam_trl(subarray_beam_id),
                "apertures": apertures,
                "first_subarray_channel": 96,
                "number_of_channels": 8,
            }
        ],
    }
    return resource_spec


@pytest.fixture(name="configure_resource_spec")
def configure_resource_spec_fixture(
    subarray_beam_id: int,
    apertures: list[dict],
) -> dict:
    """
    Return an example schema-complient configure argument for use in testing.

    :param subarray_beam_id: the id of a subarray beam.
    :param apertures: an example list of apertures for use in testing.

    :return: an example schema-complient configure argument for use in testing.
    """
    resource_spec = {
        "transaction_id": "0hush-1uzx6-20230101-95040539",
        "subarray_beams": [
            {
                "subarray_beam_id": subarray_beam_id,
                "update_rate": 1.0,
                "logical_bands": [{"start_channel": 10, "number_of_channels": 8}],
                "apertures": apertures,
                "field": {
                    "target_name": "some star",
                    "timestamp": "2023-01-01T12:12:34+00:00",
                    "reference_frame": "ICRS",
                    "attrs": {
                        "c1": 120.51262735625372,
                        "c1_rate": 0.012002185993764836,
                        "c2": 50.18504722994737,
                        "c2_rate": 0.00918239142320329,
                    },
                },
            }
        ],
    }
    return resource_spec


@pytest.fixture(name="callbacks")
def callbacks_fixture() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "communication_state",
        "component_state",
        "task",
        "abort_task",
        timeout=15.0,
    )
