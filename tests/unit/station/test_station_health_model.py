# -*- coding: utf-8 -*
# pylint: disable=too-many-arguments
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for MccsStation."""
from __future__ import annotations

from typing import Any, Callable

import pytest
from ska_control_model import HealthState, PowerState

from ska_low_mccs.station.station_health_model import StationHealthModel
from tests.harness import get_antenna_trl


class TestMccsStationHealthModel:
    """A class for tests of the station health model."""

    @pytest.fixture
    def health_model(self: TestMccsStationHealthModel) -> StationHealthModel:
        """
        Fixture to return the station health model.

        :return: Health model to be used.
        """

        def callback(*args: Any, **kwargs: Any) -> None:
            pass

        health_model = StationHealthModel(
            "field_station", "sps_station", ["antenna"], callback
        )
        health_model.update_state(communicating=True, power=PowerState.ON)

        return health_model

    @pytest.mark.parametrize(
        ("sub_devices", "thresholds", "expected_health", "expected_report"),
        [
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                None,
                HealthState.OK,
                "Health is OK.",
                id="All devices healthy, expect OK",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): (
                            HealthState.OK if i > 5 else HealthState.FAILED
                        )
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                None,
                HealthState.OK,
                "Health is OK.",
                id="Few antennas unhealthy, expect OK",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): (
                            HealthState.OK if i > 20 else HealthState.FAILED
                        )
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                None,
                HealthState.DEGRADED,
                (
                    "Too many subdevices are in a bad state: "
                    "FieldStation: OK SpsStation: OK, "
                    f"Antennas: {[get_antenna_trl('ci-1', str(i)) for i in range(21)]}"
                ),
                id="Some antennas unhealthy, expect DEGRADED",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): (
                            HealthState.OK if i > 20 else HealthState.FAILED
                        )
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                {"antenna_degraded": 0.00001, "antenna_failed": 0.02},
                HealthState.FAILED,
                (
                    "Too many subdevices are in a bad state: "
                    "FieldStation: OK SpsStation: OK, "
                    f"Antennas: {[get_antenna_trl('ci-1', str(i)) for i in range(21)]}"
                ),
                id="Some antennas unhealthy, lowered thresholds, expect FAILED",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.DEGRADED,
                    "sps_station": HealthState.OK,
                },
                None,
                HealthState.DEGRADED,
                (
                    "Too many subdevices are in a bad state: "
                    "FieldStation: DEGRADED SpsStation: OK, Antennas: []"
                ),
                id="FieldStation DEGRADED, expect DEGRADED",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): (
                            HealthState.OK if i > 60 else HealthState.FAILED
                        )
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                None,
                HealthState.FAILED,
                (
                    "Too many subdevices are in a bad state:"
                    " FieldStation: OK SpsStation: OK, "
                    f"Antennas: {[get_antenna_trl('ci-1', str(i)) for i in range(61)]}"
                ),
                id="Many antennas unhealthy, expect FAILED",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.FAILED,
                    "sps_station": HealthState.OK,
                },
                None,
                HealthState.FAILED,
                (
                    "Too many subdevices are in a bad state: FieldStation:"
                    " FAILED SpsStation: OK, Antennas: []"
                ),
                id="FieldStation FAILED, expect FAILED",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): (
                            HealthState.OK if i > 15 else HealthState.UNKNOWN
                        )
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                None,
                HealthState.UNKNOWN,
                (
                    "Some devices are unknown: FieldStation: OK SpsStation: OK, "
                    f"Antennas: {[get_antenna_trl('ci-1', str(i)) for i in range(16)]}"
                ),
                id="Some antennas UNKNOWN, expect UNKNOWN",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.UNKNOWN,
                    "sps_station": HealthState.OK,
                },
                None,
                HealthState.UNKNOWN,
                (
                    "Some devices are unknown: FieldStation: UNKNOWN"
                    " SpsStation: OK, Antennas: []"
                ),
                id="FieldStation UNKNOWN, expect UNKNOWN",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.FAILED,
                },
                None,
                HealthState.FAILED,
                (
                    "Too many subdevices are in a bad state: FieldStation:"
                    " OK SpsStation: FAILED, Antennas: []"
                ),
                id="SpsStation FAILED, expect FAILED",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.UNKNOWN,
                },
                None,
                HealthState.UNKNOWN,
                (
                    "Some devices are unknown: FieldStation: "
                    "OK SpsStation: UNKNOWN, Antennas: []"
                ),
                id="SpsStation UNKOWN, expect UNKNOWN",
            ),
        ],
    )
    def test_station_evaluate_health(
        self: TestMccsStationHealthModel,
        health_model: StationHealthModel,
        sub_devices: dict,
        thresholds: dict[str, float],
        expected_health: HealthState,
        expected_report: str,
    ) -> None:
        """
        Tests for evaluating station health.

        :param health_model: The station health model to use
        :param sub_devices: the devices for which the station cares
            about health, and their healths
        :param thresholds: A dictionary of thresholds with the param
            name as key and threshold as value
        :param expected_health: the expected health values
        :param expected_report: the expected health report
        """
        health_model._antenna_health = sub_devices["antenna"]
        health_model._field_station_health = sub_devices["field_station"]
        health_model._sps_station_health = sub_devices["sps_station"]

        if thresholds is not None:
            health_model.health_params = thresholds

        assert health_model.evaluate_health() == (expected_health, expected_report)

    @pytest.mark.parametrize(
        (
            "sub_devices",
            "health_change",
            "expected_init_health",
            "expected_init_report",
            "expected_final_health",
            "expected_final_report",
        ),
        [
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.FAILED
                        for i in range(64)
                    },
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.FAILED,
                (
                    "Too many subdevices are in a bad state:"
                    " FieldStation: OK SpsStation: OK, "
                    f"Antennas: {[get_antenna_trl('ci-1', str(i)) for i in range(64)]}"
                ),
                id="All devices healthy, expect OK, then 1/4 antennas FAILED, "
                "expect FAILED",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.FAILED
                        for i in range(2)
                    },
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.OK,
                "Health is OK.",
                id="All devices healthy, expect OK, then 2 antennas FAILED, expect OK",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                {
                    "field_station": {"low-mccs/fieldstation/00": HealthState.UNKNOWN},
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.UNKNOWN,
                (
                    "Some devices are unknown: FieldStation: UNKNOWN"
                    " SpsStation: OK, Antennas: []"
                ),
                id="All devices healthy, expect OK,"
                "then FieldStation UNKNOWN, expect UNKNOWN",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): HealthState.OK
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                {
                    "sps_station": {"low-mccs/spsstation/00": HealthState.UNKNOWN},
                },
                HealthState.OK,
                "Health is OK.",
                HealthState.UNKNOWN,
                (
                    "Some devices are unknown: FieldStation: "
                    "OK SpsStation: UNKNOWN, Antennas: []"
                ),
                id="All devices healthy, expect OK, then SpsStation UNKNOWN, "
                "expect UNKNOWN",
            ),
        ],
    )
    def test_station_evaluate_changed_health(
        self: TestMccsStationHealthModel,
        health_model: StationHealthModel,
        sub_devices: dict,
        health_change: dict[str, dict[str, HealthState]],
        expected_init_health: HealthState,
        expected_init_report: str,
        expected_final_health: HealthState,
        expected_final_report: str,
    ) -> None:
        """
        Tests of the station health model for a changed health.

        The properties of the health model are set and checked, then the
        health states of subservient devices are updated and the health
        is checked against the new expected value.

        :param health_model: The station health model to use
        :param sub_devices: the devices for which the station cares
            about health, and their healths
        :param health_change: a dictionary of the health changes, key
            device and value dictionary of HealthState
        :param expected_init_health: the expected initial health
        :param expected_init_report: the expected initial health report
        :param expected_final_health: the expected final health
        :param expected_final_report: the expected final health report

        """
        health_model._antenna_health = sub_devices["antenna"]
        health_model._field_station_health = sub_devices["field_station"]
        health_model._sps_station_health = sub_devices["sps_station"]
        health_model._field_station_trl = "low-mccs/fieldstation/00"
        health_model._sps_station_trl = "low-mccs/spsstation/00"

        assert health_model.evaluate_health() == (
            expected_init_health,
            expected_init_report,
        )

        health_update: dict[str, Callable] = {
            "antenna": health_model.antenna_health_changed,
            # "field_station": lambda _, health:
            # health_model.field_station_health_changed(health),
            "field_station": health_model.field_station_health_changed,
            "sps_station": health_model.sps_station_health_changed,
        }

        for device in health_change:
            changes = health_change[device]
            for change in changes:
                health_update[device](change, changes[change])

        assert health_model.evaluate_health() == (
            expected_final_health,
            expected_final_report,
        )

    @pytest.mark.parametrize(
        (
            "sub_devices",
            "init_thresholds",
            "expected_init_health",
            "expected_init_report",
            "final_thresholds",
            "expected_final_health",
            "expected_final_report",
        ),
        [
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): (
                            HealthState.OK if i > 8 else HealthState.FAILED
                        )
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                {
                    "antenna_degraded": 0.05,
                    "antenna_failed": 0.2,
                    "ignore_pasd": False,
                    "ignore_sps": False,
                },
                HealthState.OK,
                "Health is OK.",
                {
                    "antenna_degraded": 0.0004,
                    "antenna_failed": 0.008,
                    "ignore_pasd": False,
                    "ignore_sps": False,
                },
                HealthState.FAILED,
                (
                    "Too many subdevices are in a bad state:"
                    " FieldStation: OK SpsStation: OK, "
                    f"Antennas: {[get_antenna_trl('ci-1', str(i)) for i in range(9)]}"
                ),
                id="Few antennas unhealthy, expect OK, then lower DEGRADED and FAILED"
                "threshold, expect FAILED",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): (
                            HealthState.OK if i > 8 else HealthState.FAILED
                        )
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.OK,
                },
                {
                    "antenna_degraded": 0.05,
                    "antenna_failed": 0.2,
                    "ignore_pasd": False,
                    "ignore_sps": False,
                },
                HealthState.OK,
                "Health is OK.",
                {
                    "antenna_degraded": 0.04,
                    "antenna_failed": 0.08,
                    "ignore_pasd": False,
                    "ignore_sps": False,
                },
                HealthState.OK,
                "Health is OK.",
                id="Few antennas unhealthy, expect OK, then lower DEGRADED and FAILED "
                "threshold but not by much, expect OK",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): (HealthState.OK)
                        for i in range(256)
                    },
                    "field_station": HealthState.OK,
                    "sps_station": HealthState.FAILED,
                },
                {
                    "antenna_degraded": 0.05,
                    "antenna_failed": 0.2,
                    "ignore_pasd": False,
                    "ignore_sps": False,
                },
                HealthState.FAILED,
                (
                    "Too many subdevices are in a bad state: FieldStation:"
                    " OK SpsStation: FAILED, Antennas: []"
                ),
                {
                    "antenna_degraded": 0.04,
                    "antenna_failed": 0.08,
                    "ignore_pasd": False,
                    "ignore_sps": True,
                },
                HealthState.OK,
                "Health is OK.",
                id="Failed SpsStation, which we then choose to ignore",
            ),
            pytest.param(
                {
                    "antenna": {
                        get_antenna_trl("ci-1", str(i)): (HealthState.OK)
                        for i in range(256)
                    },
                    "field_station": HealthState.UNKNOWN,
                    "sps_station": HealthState.OK,
                },
                {
                    "antenna_degraded": 0.05,
                    "antenna_failed": 0.2,
                    "ignore_pasd": False,
                    "ignore_sps": False,
                },
                HealthState.UNKNOWN,
                (
                    "Some devices are unknown: FieldStation: "
                    "UNKNOWN SpsStation: OK, Antennas: []"
                ),
                {
                    "antenna_degraded": 0.04,
                    "antenna_failed": 0.08,
                    "ignore_pasd": True,
                    "ignore_sps": False,
                },
                HealthState.OK,
                "Health is OK.",
                id="Unknown FieldStation, which we then choose to ignore",
            ),
        ],
    )
    def test_station_evaluate_health_changed_thresholds(
        self: TestMccsStationHealthModel,
        health_model: StationHealthModel,
        sub_devices: dict,
        init_thresholds: dict[str, float],
        expected_init_health: HealthState,
        expected_init_report: str,
        final_thresholds: dict[str, float],
        expected_final_health: HealthState,
        expected_final_report: str,
    ) -> None:
        """
        Tests of the station health model for changed thresholds.

        The properties of the health model are set and checked, then the
        thresholds for the health rules are changed and the new health
        is checked against the expected value

        :param health_model: The station health model to use
        :param sub_devices: the devices for which the station cares
            about health, and their healths
        :param init_thresholds: the initial thresholds of the health rules
        :param expected_init_health: the expected initial health
        :param expected_init_report: the expected initial health report
        :param final_thresholds: the final thresholds of the health rules
        :param expected_final_health: the expected final health
        :param expected_final_report: the expected final health report
        """
        health_model._antenna_health = sub_devices["antenna"]
        health_model._field_station_health = sub_devices["field_station"]
        health_model._sps_station_health = sub_devices["sps_station"]

        health_model.health_params = init_thresholds
        assert health_model.evaluate_health() == (
            expected_init_health,
            expected_init_report,
        )

        health_model.health_params = final_thresholds
        assert health_model.evaluate_health() == (
            expected_final_health,
            expected_final_report,
        )
