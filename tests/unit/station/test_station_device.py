# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for MccsStation."""
from __future__ import annotations

import gc
import json
import sys
import unittest.mock
from typing import Callable, Iterator

import pytest
import tango
from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    PowerState,
    SimulationMode,
)
from ska_low_mccs_common import MccsDeviceProxy
from ska_tango_testing.mock import MockCallable
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_low_mccs import MccsStation
from ska_low_mccs.station import StationComponentManager
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_field_station_trl,
    get_sps_station_trl,
    get_station_calibrator_trl,
)

# To stop tests hanging during gc.
gc.disable()


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: MccsTangoTestHarnessContext,
    station_name: str,
) -> MccsDeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the context in which the tests run
    :param station_name: name of the station.

    :return: the device under test
    """
    return test_context.get_station_device(station_name)


class TestMccsStation:
    """Test class for MccsStation tests."""

    @pytest.fixture(name="test_context")
    def test_context_fixture(  # pylint: disable=too-many-arguments
        self: TestMccsStation,
        station_name: str,
        station_id: int,
        mock_field_station: unittest.mock.Mock,
        mock_station_calibrator: unittest.mock.Mock,
        mock_sps_station: unittest.mock.Mock,
        antenna_names: list[str],
        mock_antenna_factory: Callable[[], unittest.mock.Mock],
        tile_ids: list[int],
        mock_tile_factory: Callable[[], unittest.mock.Mock],
    ) -> Iterator[MccsTangoTestHarnessContext]:
        """
        Create a test harness providing a Tango context for devices under test.

        :param station_name: name of the station
        :param station_id: ID of the station
        :param mock_field_station: a mock to be returned when a proxy to the
            field station is created
        :param mock_station_calibrator: a mock to be returned when a proxy to the
            station calibrator is created
        :param mock_sps_station: a mock to be returned when a proxy to the
            SPS station is created
        :param antenna_names: A list of antenna IDs.
        :param mock_antenna_factory: a factory that returns a mock antenna
            device each time it is called
        :param tile_ids: A list of tile IDs.
        :param mock_tile_factory: a factory that returns a mock tile
            device each time it is called

        :yield: A tango context with devices to test.
        """
        harness = MccsTangoTestHarness()

        harness.add_mock_field_station_device(station_name, mock_field_station)
        harness.add_mock_station_calibrator_device(
            station_name, mock_station_calibrator
        )
        harness.add_mock_sps_station_device(station_name, mock_sps_station)

        for antenna_name in antenna_names:
            harness.add_mock_antenna_device(
                station_name, antenna_name, mock_antenna_factory()
            )

        for tile_id in tile_ids:
            harness.add_mock_tile_device(station_name, tile_id, mock_tile_factory())

        harness.add_station_device(
            station_name,
            station_id,
            antenna_names,
        )
        with harness as context:
            yield context

    def test_InitDevice(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for Initial state.

        A freshly initialised station device has no assigned resources.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        device_under_test.adminMode = AdminMode.ONLINE

        assert device_under_test.healthState == HealthState.UNKNOWN
        assert device_under_test.controlMode == ControlMode.REMOTE
        assert device_under_test.simulationMode == SimulationMode.FALSE
        # assert device_under_test.testMode == TestMode.TEST

        # The following reads might not be allowed in this state once properly
        # implemented
        assert device_under_test.transientBufferTrl == ""
        assert not device_under_test.isCalibrated
        assert not device_under_test.isConfigured
        assert device_under_test.calibrationJobId == 0
        assert device_under_test.daqJobId == 0
        assert device_under_test.dataDirectory == ""
        assert device_under_test.beamTrls == ()
        assert not list(device_under_test.delayCentre)
        assert not list(device_under_test.calibrationCoefficients)

    def test_healthState(
        self: TestMccsStation,
        device_under_test: tango.DeviceProxy,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for healthState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        device_under_test.subscribe_event(
            "healthState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.UNKNOWN)
        assert device_under_test.healthState == HealthState.UNKNOWN

    # overridden base class attributes
    @pytest.mark.xfail(reason="Upstream change to common on version info.")
    def test_buildState(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for buildState.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert (
            device_under_test.buildState == sys.modules["ska_low_mccs"].__version_info__
        )

    # overridden base class commands
    @pytest.mark.xfail(reason="Upstream change to common on version info.")
    def test_GetVersionInfo(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for GetVersionInfo.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        # vinfo = [release.get_release_info(device_under_test.info().dev_class)]
        vinfo = sys.modules["ska_low_mccs"].__version_info__
        assert device_under_test.GetVersionInfo() == vinfo

    def test_versionId(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for versionId.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.versionId == sys.modules["ska_low_mccs"].__version__

    def test_beamTrls(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for beamTrls attribute.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.beamTrls == ()

    def test_transientBufferTrl(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for transientBufferTrl attribute.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.transientBufferTrl == ""

    def test_delayCentre(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for delayCentre attribute.

        This is a messy test because there is some loss of floating-
        point precision during transfer, so you have to check
        approximate equality when reading back what you've written.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert not list(device_under_test.delayCentre)

        # SETUP
        dummy_location = (-30.72113, 21.411128)
        float_format = "{:3.4f}"
        dummy_location_str = [float_format.format(x) for x in dummy_location]

        # RUN
        device_under_test.delayCentre = dummy_location
        delay_centre = device_under_test.delayCentre

        # CHECK
        delay_centre_str = [float_format.format(x) for x in delay_centre]
        assert delay_centre_str == dummy_location_str

    def test_calibrationCoefficients(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for calibrationCoefficients attribute.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert not list(device_under_test.calibrationCoefficients)

    def test_isCalibrated(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for isCalibrated attribute.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert not device_under_test.isCalibrated

    def test_isConfigured(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for isConfigured attribute.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert not device_under_test.isConfigured

    def test_calibrationJobId(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for calibrationJobId attribute.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.calibrationJobId == 0

    def test_daqJobId(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for daqJobId attributes.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.daqJobId == 0

    def test_dataDirectory(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
    ) -> None:
        """
        Test for dataDirectory attribute.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        """
        assert device_under_test.dataDirectory == ""

    @pytest.mark.parametrize(
        ("expected_init_params", "new_params"),
        [
            pytest.param(
                {
                    "antenna_degraded": 0.05,
                    "antenna_failed": 0.2,
                    "ignore_sps": False,
                    "ignore_pasd": False,
                },
                {
                    "antenna_degraded": 0.1,
                    "antenna_failed": 0.3,
                    "ignore_sps": True,
                    "ignore_pasd": False,
                },
                id="Check correct initial values, write new and "
                "verify new values have been written",
            )
        ],
    )
    def test_healthParams(
        self: TestMccsStation,
        device_under_test: MccsDeviceProxy,
        expected_init_params: dict[str, float],
        new_params: dict[str, float],
    ) -> None:
        """
        Test for healthParams attributes.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.

        :param expected_init_params: the initial values which the health
            model is expected to have initially

        :param new_params: the new health rule params to pass to the health model
        """
        assert device_under_test.healthModelParams == json.dumps(expected_init_params)
        new_params_json = json.dumps(new_params)
        device_under_test.healthModelParams = new_params_json
        assert device_under_test.healthModelParams == new_params_json


class TestPatchedStation:
    """
    Test class for MccsStation tests that patches the component manager.

    These are thin tests that simply test that commands invoked on the
    device are passed through to the component manager
    """

    @pytest.fixture(name="test_context")
    def test_context_fixture(  # pylint: disable=too-many-arguments
        self: TestPatchedStation,
        station_name: str,
        station_id: int,
        mock_field_station: unittest.mock.Mock,
        mock_station_calibrator: unittest.mock.Mock,
        mock_sps_station: unittest.mock.Mock,
        antenna_names: list[str],
        mock_antenna_factory: Callable[[], unittest.mock.Mock],
        tile_ids: list[int],
        mock_tile_factory: Callable[[], unittest.mock.Mock],
        patched_station_class: type[MccsStation],
    ) -> Iterator[MccsTangoTestHarnessContext]:
        """
        Create a test harness providing a Tango context for devices under test.

        :param station_name: name of the station
        :param station_id: ID of the station
        :param mock_field_station: a mock to be returned when a proxy to the
            field station is created
        :param mock_station_calibrator: a mock to be returned when a proxy to the
            station calibrator is created
        :param mock_sps_station: a mock to be returned when a proxy to the
            SPS station is created
        :param antenna_names: A list of antenna IDs.
        :param mock_antenna_factory: a factory that returns a mock antenna
            device each time it is called
        :param tile_ids: A list of tile IDs.
        :param mock_tile_factory: a factory that returns a mock tile
            device each time it is called
        :param patched_station_class: a station device class that has
            been patched with extra commands to support testing

        :yield: A tango context with devices to test.
        """
        harness = MccsTangoTestHarness()

        harness.add_mock_field_station_device(station_name, mock_field_station)
        harness.add_mock_station_calibrator_device(
            station_name, mock_station_calibrator
        )
        harness.add_mock_sps_station_device(station_name, mock_sps_station)

        for antenna_name in antenna_names:
            harness.add_mock_antenna_device(
                station_name, antenna_name, mock_antenna_factory()
            )

        for tile_id in tile_ids:
            harness.add_mock_tile_device(station_name, tile_id, mock_tile_factory())

        harness.add_station_device(
            station_name,
            station_id,
            antenna_names,
            field_station_trl=get_field_station_trl(station_name),
            station_calibrator_trl=get_station_calibrator_trl(station_name),
            sps_station_trl=get_sps_station_trl(station_name),
            device_class=patched_station_class,
        )
        with harness as context:
            yield context

    def test_configure(
        self: TestPatchedStation,
        device_under_test: MccsDeviceProxy,
        mock_station_component_manager: StationComponentManager,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for configure command.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param mock_station_component_manager: the mock component manager to
            patch into this station.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.
        """
        assert isinstance(
            mock_station_component_manager.apply_configuration,
            MockCallable,
        )
        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        device_under_test.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )

        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE
        change_event_callbacks["state"].assert_change_event(tango.DevState.DISABLE)
        assert device_under_test.state() == tango.DevState.DISABLE

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert device_under_test.adminMode == AdminMode.ONLINE

        # Pretend all of our subdevices are online and on.
        mock_station_component_manager._update_component_state(power=PowerState.ON)

        change_event_callbacks["state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        assert device_under_test.state() == tango.DevState.ON
        transaction_id = "0hush-1uzx6-20230101-95040539"
        device_under_test.ApplyConfiguration(transaction_id)
        mock_station_component_manager.apply_configuration.assert_call(
            transaction_id, unittest.mock.ANY
        )

    @pytest.mark.xfail(
        reason="MCCS-1764 Command is deprecated and substituted with "
        "TrackObject, which requires a specific test"
    )
    def test_applyPointing(
        self: TestPatchedStation,
        device_under_test: MccsDeviceProxy,
        mock_station_component_manager: StationComponentManager,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for ApplyPointing command.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param mock_station_component_manager: the mock component manager to
            patch into this station.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.
        """
        assert isinstance(
            mock_station_component_manager.apply_pointing_delays,
            MockCallable,
        )
        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        device_under_test.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )

        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE
        change_event_callbacks["state"].assert_change_event(tango.DevState.DISABLE)
        assert device_under_test.state() == tango.DevState.DISABLE

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert device_under_test.adminMode == AdminMode.ONLINE

        # Pretend all of our subdevices are online and on.
        mock_station_component_manager._update_component_state(power=PowerState.ON)

        change_event_callbacks["state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        assert device_under_test.state() == tango.DevState.ON

        iso_time = "2023-11-28T15:12:34Z"
        device_under_test.ApplyPointingDelays(iso_time)
        mock_station_component_manager.apply_pointing_delays.assert_call(
            iso_time, unittest.mock.ANY
        )

    @pytest.mark.xfail(
        reason="MCCS-1873. GetPointingDelays implemented as Slow in device,"
        " Fast in component manager"
    )
    def test_get_pointing_delays(
        self: TestPatchedStation,
        device_under_test: MccsDeviceProxy,
        mock_station_component_manager: StationComponentManager,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test for GetPointingDelays command.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param mock_station_component_manager: the mock component manager to
            patch into this station.
        :param change_event_callbacks: A dictionary of change event
            callbacks with async support.

        """
        assert isinstance(
            mock_station_component_manager.get_pointing_delays,
            MockCallable,
        )
        device_under_test.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        device_under_test.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )

        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert device_under_test.adminMode == AdminMode.OFFLINE
        change_event_callbacks["state"].assert_change_event(tango.DevState.DISABLE)
        assert device_under_test.state() == tango.DevState.DISABLE

        device_under_test.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert device_under_test.adminMode == AdminMode.ONLINE

        # Pretend all of our subdevices are online and on.
        mock_station_component_manager._update_component_state(power=PowerState.ON)

        change_event_callbacks["state"].assert_change_event(
            tango.DevState.ON, lookahead=2
        )
        assert device_under_test.state() == tango.DevState.ON
        pointing = {
            "pointing_type": "ra_dec",
            "pointing_time": "2023-11-28T12:34:56",
            "values": {
                "right_ascension": 1.0,
                "declination": -1.0,
            },
        }

        device_under_test.GetPointingDelays(json.dumps(pointing))

        pointing_type = pointing["pointing_type"]
        pointing_time = pointing["pointing_time"]
        values = pointing["values"]
        mock_station_component_manager.get_pointing_delays.assert_call(
            pointing_type=pointing_type, pointing_time=pointing_time, values=values
        )

    def test_StartAcquisition(
        self: TestPatchedStation,
        device_under_test: MccsDeviceProxy,
        mock_station_component_manager: unittest.mock.Mock,
    ) -> None:
        """
        Test for StartAcquisition.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param mock_station_component_manager: a mock component manager that has
            been patched into the device under test
        """
        assert isinstance(
            mock_station_component_manager.start_acquisition,
            MockCallable,
        )
        device_under_test.adminMode = AdminMode.ONLINE
        assert device_under_test.adminMode == AdminMode.ONLINE
        device_under_test.StartAcquisition("{}")
        mock_station_component_manager.start_acquisition.assert_call(unittest.mock.ANY)

    def test_AcquireDataForCalibration(
        self: TestPatchedStation,
        device_under_test: MccsDeviceProxy,
        mock_station_component_manager: unittest.mock.Mock,
    ) -> None:
        """
        Test for test_AcquireDataForCalibration.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        :param mock_station_component_manager: a mock component manager that has
            been patched into the device under test
        """
        assert isinstance(
            mock_station_component_manager.acquire_data_for_calibration,
            MockCallable,
        )
        device_under_test.adminMode = AdminMode.ONLINE
        assert device_under_test.adminMode == AdminMode.ONLINE
        device_under_test.AcquireDataForCalibration(10)
        mock_station_component_manager.acquire_data_for_calibration.assert_call(
            10, unittest.mock.ANY
        )
