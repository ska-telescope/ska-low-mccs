# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for the ska_low_mccs.point_station module."""
from __future__ import annotations

import os
from typing import Any

import numpy as np
import pytest
from astropy.coordinates import Angle
from astropy.time.core import Time

from ska_low_mccs.station.point_station import Pointing, StationInformation

locations_file = "tests/data/AAVS2_loc_italia_190429.txt"
ant_pos_eep_order_file = "tests/data/aavs3_pos_eep_order.npy"
ant_pos_smartbox_order_file = "tests/data/aavs3_pos_smartbox_order.npy"
output_file = "tests/results/pointingtest.txt"
stat_lat, stat_lon, stat_height = (-26.82472208, 116.7644482, 346.59)


class TestPointStation:
    """Tests of point_station.py."""

    def test_create_pointing_file(self: TestPointStation) -> None:
        """Test pointing class instance and the supporting StationInformation object."""
        station = StationInformation()
        # Load standard AAVS displacements
        station.load_displacements_file(locations_file)
        # Exercise bounds checks
        with pytest.raises(ValueError):
            station.set_location(-111.11, stat_lon, stat_height)
            station.set_location(111.11, stat_lon, stat_height)
            station.set_location(stat_lat, -999.99, stat_height)
            station.set_location(stat_lat, 999.99, stat_height)
            station.set_location(stat_lat, stat_lon, -1234)
            station.set_location(stat_lat, stat_lon, 99999.99)
        # Set station reference position to array centre
        station.set_location(stat_lat, stat_lon, stat_height)
        # We have 256 elements and therefore expect a 256 x 3 array
        assert station.antennas.xyz is not None  # for the type checker
        assert station.antennas.xyz.shape == (256, 3)
        # Check location data
        assert station.latitude == stat_lat
        assert station.longitude == stat_lon
        assert station.ellipsoidalheight == stat_height
        pointing = Pointing(station)

        point_kwargs: dict[str, Any] = {
            "altitude": Angle(90.0, "deg"),
            "azimuth": Angle(0.0, "deg"),
        }
        pointing.point_array_static(**point_kwargs)
        # Pointing to zenith with flat station => zero delays
        assert np.mean(np.absolute(pointing._delays)) < 1e-15
        point_kwargs = {
            "altitude": Angle(10.0, "deg"),
            "azimuth": Angle(0.0, "deg"),
        }
        pointing.point_array_static(**point_kwargs)
        # Point near horizon => we get significant delays
        assert np.mean(np.absolute(pointing._delays)) > 1e-9

        # Check this delays against expected values of a previous run
        expected_delays_file = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "../../data/expected_delays_alt10_az0.npy",
        )
        expected_delays = np.load(expected_delays_file)
        assert np.allclose(pointing._delays, expected_delays, atol=1e-15)

        # Exercise equatorial pointing near the SCP
        point_kwargs = {
            "pointing_time": Time("2021-05-09T23:00:00", format="isot", scale="utc"),
            "right_ascension": Angle(0.0, "deg"),
            "declination": Angle(-70.0, "deg"),
        }
        pointing.point_array_equatorial(**point_kwargs)
        # Delays should be ns-scale
        assert np.mean(np.absolute(pointing._delays)) > 1e-9
        # Delay rates will be sub ps-scale
        assert np.mean(np.absolute(pointing._delay_rates)) > 1e-13

    def test_create_pointing_array(self: TestPointStation) -> None:
        """Test pointing class instance getting information from arrays."""
        station = StationInformation()
        # Set station reference position to array centre
        station.set_location(stat_lat, stat_lon, stat_height)
        # Check location data
        assert station.latitude == stat_lat
        assert station.longitude == stat_lon
        assert station.ellipsoidalheight == stat_height

        # Load standard AAVS3 positions in smartbox order
        xyz_id = np.load(ant_pos_smartbox_order_file)
        xyz_id[:, 2] = 0.0  # Set all z positions to zero to make zenith delays flat
        station.load_displacements_arrays(
            xyz_id[:, :3],
            xyz_id[:, 3],
        )
        # The array has been rearranged into EEP order
        assert np.allclose(
            station.antennas.xyz[:, :2],
            np.load(ant_pos_eep_order_file)[:, :2],
            atol=1e-2,
        )
        # We have 256 elements and therefore expect a 256 x 3 array
        assert station.antennas.xyz is not None  # for the type checker
        assert station.antennas.xyz.shape == (256, 3)

        pointing = Pointing(station)
        point_kwargs: dict[str, Any] = {
            "altitude": Angle(90.0, "deg"),
            "azimuth": Angle(0.0, "deg"),
        }
        pointing.point_array_static(**point_kwargs)
        # Pointing to zenith with flat station => zero delays
        assert np.mean(np.absolute(pointing._delays)) < 1e-15
        point_kwargs = {
            "altitude": Angle(10.0, "deg"),
            "azimuth": Angle(0.0, "deg"),
        }
        pointing.point_array_static(**point_kwargs)
        # Point near horizon => we get significant delays
        assert np.mean(np.absolute(pointing._delays)) > 1e-9

        # Exercise equatorial pointing near the SCP
        point_kwargs = {
            "pointing_time": Time("2021-05-09T23:00:00", format="isot", scale="utc"),
            "right_ascension": Angle(0.0, "deg"),
            "declination": Angle(-70.0, "deg"),
        }
        pointing.point_array_equatorial(**point_kwargs)
        # Delays should be ns-scale
        assert np.mean(np.absolute(pointing._delays)) > 1e-9
        # Delay rates will be sub ps-scale
        assert np.mean(np.absolute(pointing._delay_rates)) > 1e-13

    def test_create_pointing_individual(self: TestPointStation) -> None:
        """Test pointing class instance getting information from individual antennas."""
        station = StationInformation()

        for i in range(256):
            x_pos = 1.5 * i
            y_pos = -1.5 * i
            z_pos = 0.0
            eep_id = 256 - i  # Element Id in inverse order

            # Load standard displacements
            station.load_displacements_individual(
                x_pos,
                y_pos,
                z_pos,
                eep_id,
            )
        # Assert that the antennas are in EEP order
        assert station.antennas.xyz[0, 0] > station.antennas.xyz[1, 0]
        # We have 256 elements and therefore expect a 256 x 3 array
        assert station.antennas.xyz is not None  # for the type checker
        assert station.antennas.xyz.shape == (256, 3)

        # Exercise bounds checks
        with pytest.raises(ValueError):
            station.set_location(-111.11, stat_lon, stat_height)
            station.set_location(111.11, stat_lon, stat_height)
            station.set_location(stat_lat, -999.99, stat_height)
            station.set_location(stat_lat, 999.99, stat_height)
            station.set_location(stat_lat, stat_lon, -1234)
            station.set_location(stat_lat, stat_lon, 99999.99)
        # Set station reference position to array centre
        station.set_location(stat_lat, stat_lon, stat_height)
        # Check location data
        assert station.latitude == stat_lat
        assert station.longitude == stat_lon
        assert station.ellipsoidalheight == stat_height
        pointing = Pointing(station)

        point_kwargs: dict[str, Any] = {
            "altitude": Angle(90.0, "deg"),
            "azimuth": Angle(0.0, "deg"),
        }
        pointing.point_array_static(**point_kwargs)
        # Pointing to zenith with flat station => zero delays
        assert np.mean(np.absolute(pointing._delays)) < 1e-15
        point_kwargs = {
            "altitude": Angle(10.0, "deg"),
            "azimuth": Angle(0.0, "deg"),
        }
        pointing.point_array_static(**point_kwargs)
        # Point near horizon => we get significant delays
        assert np.mean(np.absolute(pointing._delays)) > 1e-9

        # Exercise equatorial pointing near the SCP
        point_kwargs = {
            "pointing_time": Time("2021-05-09T23:00:00", format="isot", scale="utc"),
            "right_ascension": Angle(0.0, "deg"),
            "declination": Angle(-70.0, "deg"),
        }
        pointing.point_array_equatorial(**point_kwargs)
        # Delays should be ns-scale
        assert np.mean(np.absolute(pointing._delays)) > 1e-9
        # Delay rates will be sub ps-scale
        assert np.mean(np.absolute(pointing._delay_rates)) > 1e-13
