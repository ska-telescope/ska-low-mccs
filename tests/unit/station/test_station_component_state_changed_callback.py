# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests of the station component state changed callback."""
from __future__ import annotations

import functools
import logging
import time
import unittest.mock
from typing import Callable, Iterator, Type

import numpy as np
import pytest
import tango
from ska_control_model import CommunicationStatus, PowerState, TaskStatus
from ska_low_mccs_common import MccsDeviceProxy
from ska_tango_testing.mock import MockCallable, MockCallableGroup

from ska_low_mccs.station import MccsStation, StationComponentManager
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_antenna_trl,
    get_field_station_trl,
    get_sps_station_trl,
    get_station_calibrator_trl,
)


# pylint: disable=too-many-arguments
@pytest.fixture(name="station_component_manager")
def station_component_manager_fixture(
    station_name: str,
    station_id: int,
    antenna_names: list[str],
    antenna_positions_smartbox_order: np.ndarray,
    antenna_element_ids_smartbox_order: list[int],
    ref_latitude: float,
    ref_longitude: float,
    ref_height: float,
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> StationComponentManager:
    """
    Return a station component manager.

    :param station_name: name of the station
    :param station_id: the ID of the station
    :param antenna_names: names of the station's antennas
    :param antenna_positions_smartbox_order: array of the
        x, y, z positions of the antennas
    :param antenna_element_ids_smartbox_order: list of the element IDs of the antennas
    :param ref_latitude: Reference latitude for testing.
    :param ref_longitude: Reference longitude for testing.
    :param ref_height: Reference height for testing.

    :param logger: the logger to be used by this object.
    :param callbacks: A dictionary of callbacks with async support.

    :return: a station component manager
    """
    return StationComponentManager(
        station_id,
        ref_latitude,
        ref_longitude,
        ref_height,
        get_field_station_trl(station_name),
        [get_antenna_trl(station_name, antenna_name) for antenna_name in antenna_names],
        antenna_positions_smartbox_order,
        antenna_element_ids_smartbox_order,
        get_station_calibrator_trl(station_name),
        get_sps_station_trl(station_name),
        1.0,
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )


@pytest.fixture(name="patched_station_device_class")
def patched_station_device_class_fixture(
    station_component_manager: StationComponentManager,
) -> Type[MccsStation]:
    """
    Return a station device class, patched with extra methods for testing.

    :param station_component_manager: the station component manager to be patched
        into instances of this station device class

    :return: a patched station device class, patched with extra methods
        for testing

    """

    # pylint: disable=too-many-ancestors
    class PatchedStationDevice(MccsStation):
        """
        MccsStation patched with extra commands for testing purposes.

        The extra commands allow us to mock the receipt of obs state
        change events from subservient devices.
        """

        def create_component_manager(
            self: PatchedStationDevice,
        ) -> StationComponentManager:
            """
            Return a partially mocked component manager instead of the usual one.

            :return: a mock component manager
            """
            wrapped_state_changed_callback = MockCallable(
                wraps=self._component_state_callback
            )
            station_component_manager._component_state_callback = (
                wrapped_state_changed_callback
            )

            field_station_proxy = station_component_manager._field_station_proxy
            assert field_station_proxy is not None  # for the type checker
            field_station_proxy._component_state_callback = functools.partial(
                station_component_manager._component_state_callback,
                trl=station_component_manager._field_station_trl,
            )
            assert (
                station_component_manager._sps_station_proxy is not None
            )  # for the type checker
            sps_station_proxy = station_component_manager._sps_station_proxy
            sps_station_proxy._component_state_callback = functools.partial(
                station_component_manager._component_state_callback,
                trl=station_component_manager._sps_station_trl,
            )
            for (
                antenna_trl,
                antenna_proxy,
            ) in station_component_manager._antenna_proxies.items():
                antenna_proxy._component_state_callback = functools.partial(
                    station_component_manager._component_state_callback,
                    trl=antenna_trl,
                )

            return station_component_manager

    return PatchedStationDevice


@pytest.fixture(name="test_context")
def test_context_fixture(  # pylint: disable=too-many-arguments
    patched_station_device_class: type[MccsStation],
    station_name: str,
    station_id: int,
    mock_field_station: unittest.mock.Mock,
    mock_station_calibrator: unittest.mock.Mock,
    mock_sps_station: unittest.mock.Mock,
    antenna_names: list[str],
    mock_antenna_factory: Callable[[], unittest.mock.Mock],
    tile_ids: list[int],
    mock_tile_factory: Callable[[], unittest.mock.Mock],
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which mock tile and field station devices are running.

    :param patched_station_device_class: a station device class that has
        been patched with extra commands to support testing
    :param station_name: name of the station
    :param station_id: ID of the station
    :param mock_field_station: a mock to be returned when a proxy to the
        field station is created.
    :param mock_station_calibrator: a mock to be returned when a proxy to the
        station calibrator is created
    :param mock_sps_station: a mock to be returned when a proxy to the
        SPS station is created.
    :param antenna_names: A list of antenna names.
    :param mock_antenna_factory: a factory that returns a mock antenna
        device each time it is called
    :param tile_ids: A list of tile IDs.
    :param mock_tile_factory: a factory that returns a mock tile
        device each time it is called

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()

    harness.add_mock_field_station_device(station_name, mock_field_station)
    harness.add_mock_station_calibrator_device(station_name, mock_station_calibrator)
    harness.add_mock_sps_station_device(station_name, mock_sps_station)

    for antenna_name in antenna_names:
        harness.add_mock_antenna_device(
            station_name, antenna_name, mock_antenna_factory()
        )

    for tile_id in tile_ids:
        harness.add_mock_tile_device(station_name, tile_id, mock_tile_factory())

    harness.add_station_device(
        station_name,
        station_id,
        antenna_names,
        device_class=patched_station_device_class,
    )

    with harness as context:
        yield context


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: MccsTangoTestHarnessContext,
    station_name: str,
) -> MccsDeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the context in which the tests run
    :param station_name: name of the station.

    :return: the device under test
    """
    return test_context.get_station_device(station_name)


class TestStationComponentStateChangedCallback:
    """Tests of the station component manager."""

    def test_power_events(
        self: TestStationComponentStateChangedCallback,
        device_under_test: MccsDeviceProxy,
        station_component_manager: StationComponentManager,
    ) -> None:
        """
        Test the station component manager's management of power mode.

        :param device_under_test: proxy to our (patched) station device.
            This is only included so that our patched
            station device replaces our
            mock_station_component_manager's
            component_state_callback with its own
        :param station_component_manager: the component manager used by
            the station device.
        """
        station_component_manager.start_communicating()
        time.sleep(0.1)  # wait for events to come through

        # TODO: implement a way to check that the tango device's
        # _component_power_state_changed callback is called i.e.
        # mock_station_component_manager._component_state_callback.assert_call(
        #     PowerState.UNKNOWN, lookahead=5
        #     )
        assert station_component_manager.power_state == PowerState.UNKNOWN

        for antenna_proxy in station_component_manager._antenna_proxies.values():
            antenna_proxy._device_state_changed(
                "state", tango.DevState.OFF, tango.AttrQuality.ATTR_VALID
            )
            assert station_component_manager.power_state == PowerState.UNKNOWN
            # TODO: implement a way to check that the tango device's
            # _component_power_state_changed callback is NOT called i.e.
            # component_power_state_changed_callback.assert_not_called()

        assert (
            station_component_manager._field_station_proxy is not None
        )  # for the type checker
        station_component_manager._field_station_proxy._device_state_changed(
            "state", tango.DevState.OFF, tango.AttrQuality.ATTR_VALID
        )

        assert (
            station_component_manager._sps_station_proxy is not None
        )  # for the type checker
        station_component_manager._sps_station_proxy._device_state_changed(
            "state", tango.DevState.OFF, tango.AttrQuality.ATTR_VALID
        )

        # TODO: implement a way to check that the tango device's
        # _component_power_state_changed callback is called i.e.
        # component_power_state_changed_callback.assert_next_call(PowerState.OFF)
        assert station_component_manager.power_state == PowerState.OFF

    # # pylint: disable=too-many-arguments
    @pytest.mark.parametrize(
        "command,device_command,arg",
        [
            ("apply_pointing_delays", "ApplyPointingDelays", "load_time"),
            ("load_pointing_delays", "LoadPointingDelays", "pointing_delays"),
        ],
    )
    def test_sps_commands_pointing(
        self: TestStationComponentStateChangedCallback,
        command: str,
        device_command: str,
        arg: str,
        callbacks: MockCallableGroup,
        device_under_test: MccsDeviceProxy,
        station_component_manager: StationComponentManager,
        request: pytest.FixtureRequest,
    ) -> None:
        """
        Test MccsStation -> SpsStaion commands.

        Test that when we call MccsStation commands, the requisite
        command on SpsStation is called.

        :param command: command to run on MccsStation component manager.
        :param device_command: command we expect to be ran on SpsStation proxy.
        :param arg: argument for command on MccsStation.
        :param callbacks: A dictionary of callbacks with async support.
        :param device_under_test: proxy to our (patched) station device.
            This is only included so that our patched
            station device replaces our
            mock_station_component_manager's
            component_state_callback with its own
        :param station_component_manager: the component manager used by
            the station device
        :param request: pytest fixture requestor so we can pull in fixture
            from arg.
        """
        assert isinstance(
            station_component_manager._component_state_callback,
            MockCallable,
        )
        # Large lookaheads are needed as the callback is being absolutely hammered.
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        # Check initial power events from subservient devices.
        station_component_manager._component_state_callback.assert_call(
            power=PowerState.UNKNOWN,
            trl=station_component_manager._field_station_trl,
            lookahead=20,
        )
        station_component_manager._component_state_callback.assert_call(
            power=PowerState.UNKNOWN,
            trl=station_component_manager._sps_station_trl,
            lookahead=20,
        )
        for trl in station_component_manager._antenna_proxies.keys():
            # pylint: disable=line-too-long
            station_component_manager._component_state_callback.assert_call(  # noqa: E501
                power=PowerState.UNKNOWN, trl=trl, lookahead=20
            )

        # We don't use `callbacks["component_state"]` here as we need the real callback
        # so we've wrapped it with a ska_tango_testing.MockCallable in the cpt_mgr.
        station_component_manager._component_state_callback.assert_call(
            power=PowerState.UNKNOWN, lookahead=20
        )
        assert station_component_manager.power_state == PowerState.UNKNOWN
        assert (
            station_component_manager._field_station_proxy is not None
        )  # for the type checker
        # Tell this station each of its components is on, so that it thinks it is on
        station_component_manager._field_station_proxy._device_state_changed(
            "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
        )
        assert (
            station_component_manager._sps_station_proxy is not None
        )  # for the type checker
        station_component_manager._sps_station_proxy._device_state_changed(
            "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
        )

        for antenna_proxy in station_component_manager._antenna_proxies.values():
            antenna_proxy._device_state_changed(
                "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
            )

        # Check the power events from subservient devices we just "turned on".
        station_component_manager._component_state_callback.assert_call(
            power=PowerState.ON,
            trl=station_component_manager._field_station_trl,
            lookahead=20,
        )
        station_component_manager._component_state_callback.assert_call(
            power=PowerState.ON,
            trl=station_component_manager._sps_station_trl,
            lookahead=20,
        )
        for trl in station_component_manager._antenna_proxies.keys():
            # pylint: disable=line-too-long
            station_component_manager._component_state_callback.assert_call(  # noqa: E501
                power=PowerState.ON, trl=trl, lookahead=20
            )

        station_component_manager._component_state_callback.assert_call(
            power=PowerState.ON, lookahead=25
        )
        assert station_component_manager.power_state == PowerState.ON

        value = request.getfixturevalue(arg)
        getattr(station_component_manager, command)(
            value, task_callback=callbacks["task"]
        )
        sps_proxy = station_component_manager._sps_station_proxy._proxy
        getattr(sps_proxy, device_command).assert_next_call(value)

        # Check task status has gone through cycle of QUEUED, IN_PROGRESS,
        # and then COMPLETED
        # This will likely change once we start monitoring the tile command progress
        # Calls with QUEUED will have no message, other calls will have messages (but
        # they will all be different, so we just assert that we will get a message).
        for status, msg in [
            (TaskStatus.QUEUED, False),
            (TaskStatus.IN_PROGRESS, True),
            (TaskStatus.COMPLETED, True),
        ]:
            if msg:
                assert callbacks["task"].assert_call(
                    status=status, result=unittest.mock.ANY
                )
            else:
                assert callbacks["task"].assert_call(status=status)

    def test_power_commands(
        self: TestStationComponentStateChangedCallback,
        device_under_test: MccsDeviceProxy,
        station_component_manager: StationComponentManager,
        callbacks: MockCallableGroup,
        field_station_proxy: unittest.mock.Mock,
        sps_station_proxy: unittest.mock.Mock,
        station_name: str,
        antenna_names: list[str],
        antenna_proxies: list[unittest.mock.Mock],
    ) -> None:
        """
        Test that the power commands work as expected.

        :param device_under_test: proxy to our (patched) station device.
            This is only included so that our patched
            station device replaces our
            mock_station_component_manager's
            component_state_callback with its own
        :param station_component_manager: the component manager used
            by the station device.
        :param callbacks: A dictionary of callbacks with async support.
        :param field_station_proxy: proxy to this station's FieldStation device.
        :param sps_station_proxy: proxy to this station's SpsStation device.
        :param station_name: name of the station
        :param antenna_names: names of the antennas within the station
        :param antenna_proxies: list of proxies to this station's
            antenna devices

        """
        station_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(
            CommunicationStatus.ESTABLISHED,
        )
        time.sleep(0.1)
        assert (
            station_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )
        station_component_manager.on(task_callback=callbacks["task"])

        field_station_proxy.On.assert_next_call()

        time.sleep(1)
        # pretend to receive FIELDSTATION power mode changed event
        station_component_manager._field_station_power_state_changed(PowerState.ON)

        # pretend to receive SpsStation power mode event
        station_component_manager._sps_station_power_state_changed(PowerState.ON)

        # Check task status has gone through cycle of QUEUED, IN_PROGRESS
        # This will likely change once we start monitoring the subservient device
        # command progress
        for status in [
            TaskStatus.QUEUED,
            TaskStatus.IN_PROGRESS,
            TaskStatus.COMPLETED,
        ]:
            callbacks["task"].assert_call(status=status)

        # Antennas should just be passively reflecting the
        #  power state of their smartbox port?

        # for antenna_proxy in antenna_proxies:
        #     antenna_proxy.On.assert_next_call()
        sps_station_proxy.On.assert_next_call()

        # pretend to receive antenna events
        for antenna_name in antenna_names:
            station_component_manager._antenna_power_state_changed(
                get_antenna_trl(station_name, antenna_name), PowerState.ON
            )

        assert station_component_manager.power_state == PowerState.ON

        station_component_manager.off(task_callback=callbacks["task"])

        time.sleep(1)

        # pretend to receive FieldStation power mode changed event
        station_component_manager._field_station_power_state_changed(PowerState.OFF)

        # pretend to receive tile and antenna events
        for antenna_name in antenna_names:
            station_component_manager._antenna_power_state_changed(
                get_antenna_trl(station_name, antenna_name), PowerState.OFF
            )

        station_component_manager._sps_station_power_state_changed(PowerState.OFF)

        field_station_proxy.Off.assert_next_call()
        # Check task status has gone through cycle of QUEUED, IN_PROGRESS, and
        # then COMPLETED
        # This will likely change once we start monitoring the subservient device
        # command progress
        for status in [
            TaskStatus.QUEUED,
            TaskStatus.IN_PROGRESS,
            TaskStatus.COMPLETED,
        ]:
            callbacks["task"].assert_call(status=status)

        # Antennas should just be passively reflecting the
        # power state of their smartbox port?

        # for antenna_proxy in antenna_proxies:
        #     antenna_proxy.Off.assert_next_call()

        sps_station_proxy.Standby.assert_next_call()

        assert station_component_manager.power_state == PowerState.OFF
