# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defined a pytest harness for testing the MCCS station module."""
from __future__ import annotations

import logging
import os
import unittest.mock
from typing import Iterable

import numpy as np
import pytest
import pytest_mock
import tango
from ska_control_model import ResultCode
from ska_low_mccs_common import MccsDeviceProxy
from ska_low_mccs_common.testing.mock import MockDeviceBuilder
from ska_tango_testing.mock import MockCallable, MockCallableGroup

from ska_low_mccs import MccsStation
from ska_low_mccs.station import StationComponentManager
from tests.harness import (
    get_antenna_trl,
    get_field_station_trl,
    get_sps_station_trl,
    get_station_calibrator_trl,
    get_tile_trl,
)

NUMBER_OF_ANTENNAS = 256


@pytest.fixture(name="ref_latitude")
def ref_latitude_fixture() -> float:
    """
    Return example reference latitide.

    :return: latitude of AAVS3
    """
    return -26.704078296


@pytest.fixture(name="ref_longitude")
def ref_longitude_fixture() -> float:
    """
    Return example reference longitude.

    :return: longitude of AAVS3
    """
    return 116.670233065


@pytest.fixture(name="ref_height")
def ref_height_fixture() -> float:
    """
    Return example reference height.

    :return: ellipsoidal height of AAVS3
    """
    return 359.495


@pytest.fixture(name="station_name")
def station_name_fixture() -> str:
    """
    Return the name of this station.

    :return: the name of this station.
    """
    return "ci-1"


@pytest.fixture(name="nof_antenna")
def nof_antenna_fixture() -> int:
    """
    Return the number of antenna in this station.

    :return: an integer value of the number of antenna.
    """
    return 4


@pytest.fixture(name="antenna_names")
def antenna_names_fixture(nof_antenna: int) -> list[str]:
    """
    Return the names of the antenna for which Tango devices are to be deployed.

    :param nof_antenna: the number of antenna belonging to this station.

    :return: list of antenna names.
    """
    return [f"sb1-{antnna_id}" for antnna_id in range(1, nof_antenna)]


@pytest.fixture(name="antenna_positions_eep_order")
def antenna_positions_eep_order_fixture(nof_antenna: int) -> np.ndarray:
    """
    Return an array of the x,y,z positions of the station antennas.

    :param nof_antenna: the number of antenna belonging to this station.

    :return: numpy array of positions.
    """
    aavs3_antenna_positions_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "../../data/aavs3_pos_eep_order.npy",
    )
    # We just grab the first len(nof_antenna). The test context is
    # not deploying a complete station.
    return np.load(aavs3_antenna_positions_file)[:nof_antenna, :3]


@pytest.fixture(name="antenna_element_ids_eep_order")
def antenna_element_ids_eep_order_fixture(nof_antenna: int) -> list[int]:
    """
    Return the IDs of the station antennas.

    :param nof_antenna: the number of antenna belonging to this station.

    :return: list of tile IDs
    """
    return list(range(1, nof_antenna + 1))


@pytest.fixture(name="antenna_positions_smartbox_order")
def antenna_positions_smartbox_order_fixture(nof_antenna: int) -> np.ndarray:
    """
    Return an array of the x,y,z positions of the station antennas.

    :param nof_antenna: the number of antenna belonging to this station.

    :return: numpy array of positions.
    """
    aavs3_antenna_positions_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "../../data/aavs3_pos_smartbox_order.npy",
    )
    # We just grab the first len(nof_antenna). The test context is
    # not deploying a complete station.
    return np.load(aavs3_antenna_positions_file)[:nof_antenna, :3]


@pytest.fixture(name="antenna_element_ids_smartbox_order")
def antenna_element_ids_smartbox_order_fixture(nof_antenna: int) -> list[int]:
    """
    Return the IDs of the station antennas.

    :param nof_antenna: the number of antenna belonging to this station.

    :return: list of tile IDs
    """
    aavs3_antenna_positions_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "../../data/aavs3_pos_smartbox_order.npy",
    )
    # We just grab the first len(nof_antenna). The test context is
    # not deploying a complete station.
    return np.load(aavs3_antenna_positions_file)[:nof_antenna, 3]


@pytest.fixture(name="tile_ids")
def tile_ids_fixture() -> list[int]:
    """
    Return the IDs of tiles for which Tango devices are to be deployed.

    :return: list of tile IDs
    """
    return [1, 2]


@pytest.fixture(name="nof_tiles")
def nof_tiles_fixture(tile_ids: list[int]) -> int:
    """
    Return the number of tiles deployed.

    :param tile_ids: the IDs of the tiles deployed.

    :return: the number of tiles deployed.
    """
    return len(tile_ids)


@pytest.fixture(name="mock_field_station")
def mock_field_station_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a mock fieldStation.

    The only special behaviour of these mocks is they return a
    (result_code, message) tuple in response to the SetPointingDelay
    call.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.add_result_command("Off", result_code=ResultCode.OK)
    builder.add_result_command("On", result_code=ResultCode.OK)
    builder.add_result_command("Configure", result_code=ResultCode.OK)
    return builder()


@pytest.fixture(name="mock_station_calibrator")
def mock_station_calibrator_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a mock station calibrator.

    The only special behaviour of these mocks is they return a
    (result_code, message) tuple in response to the SetPointingDelay
    call.


    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    # Builder doesnt have the ability to mock a command that only returns a
    # value and not a result code, so have to fudge it a little here
    # Correlation mode always has 256.
    # see https://gitlab.com/ska-telescope/
    # aavs-system/-/blob/master/python/pydaq/daq_receiver.py
    # correlator_callback
    builder.add_command(name="GetCalibration", return_value=np.array([0.5] * 256 * 8))
    return builder()


@pytest.fixture(name="mock_sps_station")
def mock_sps_station_fixture(
    station_name: str, tile_ids: list[int]
) -> MockDeviceBuilder:
    """
    Fixture that provides a mock sps station.

    The only special behaviour of these mocks is they return a
    (result_code, message) tuple in response to the SetPointingDelay
    call.
    :param tile_ids: the IDs of the tiles deployed.
    :param station_name: the name of the station under test.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.add_result_command("LoadCalibrationCoefficients", result_code=ResultCode.OK)
    builder.add_result_command("ApplyCalibration", result_code=ResultCode.OK)
    builder.add_result_command("LoadPointingDelays", result_code=ResultCode.OK)
    builder.add_result_command("ApplyPointingDelays", result_code=ResultCode.OK)
    builder.add_result_command("Off", result_code=ResultCode.OK)
    builder.add_result_command("On", result_code=ResultCode.OK)
    builder.add_result_command("Standby", result_code=ResultCode.OK)
    builder.add_result_command("SetBeamformerTable", result_code=ResultCode.OK)
    builder.add_result_command("StartAcquisition", result_code=ResultCode.QUEUED)
    builder.add_result_command(
        "AcquireDataForCalibration", result_code=ResultCode.QUEUED
    )
    return builder(TileFQDNs=[get_tile_trl(station_name, i) for i in tile_ids])


@pytest.fixture(name="mock_antenna_factory")
def mock_antenna_factory_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a factory for mock antennas.

    The only special behaviour of these mocks is they return a
    (result_code, message) tuple in response to the SetPointingDelay
    call.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.add_result_command("Off", result_code=ResultCode.OK)
    builder.add_result_command("On", result_code=ResultCode.OK)

    builder.add_attribute("xDisplacement", 1)
    builder.add_attribute("yDisplacement", 1)
    builder.add_attribute("zDisplacement", 1)
    builder.add_attribute("TileId", 1)
    builder.add_attribute("antennaId", 1)

    return builder


@pytest.fixture(name="mock_tile_factory")
def mock_tile_factory_fixture() -> MockDeviceBuilder:
    """
    Fixture that provides a factory for mock tiles.

    The only special behaviour of these mocks is they return a
    (result_code, message) tuple in response to the SetPointingDelay
    call.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.ON)
    builder.add_result_command("Off", result_code=ResultCode.OK)
    builder.add_result_command("On", result_code=ResultCode.OK)
    builder.add_result_command("SetPointingDelay", result_code=ResultCode.OK)
    return builder


@pytest.fixture(name="pointing_delays")
def pointing_delays_fixture() -> list:
    """
    Return some pointing_delays.

    :return: some pointing delays
    """
    return list(range(1 + 2 * NUMBER_OF_ANTENNAS))


@pytest.fixture(name="load_time")
def load_time_fixture(
    mocker: pytest_mock.MockerFixture,
) -> unittest.mock.Mock:
    """
    Return a mock load time.

    For now this just returns a mock, but later we might need it to
    behave more like a time.

    :param mocker: a fixture that wraps unittest.mock
    :return: a mock load time
    """
    return mocker.Mock()


# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals
@pytest.fixture(name="mock_station_component_manager")
def mock_station_component_manager_fixture(
    station_name: str,
    station_id: int,
    antenna_names: list[str],
    antenna_positions_smartbox_order: np.ndarray,
    antenna_element_ids_smartbox_order: list[int],
    ref_latitude: float,
    ref_longitude: float,
    ref_height: float,
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> StationComponentManager:
    """
    Return a station component manager.

    :param station_name: name of the station
    :param station_id: the ID of the station
    :param antenna_names: IDs of the station's antennas
    :param antenna_positions_smartbox_order: array of the
        x, y, z positions of the antennas
    :param antenna_element_ids_smartbox_order: list of the element IDs of the antennas
    :param ref_latitude: Reference latitude for testing.
    :param ref_longitude: Reference longitude for testing.
    :param ref_height: Reference height for testing.

    :param logger: the logger to be used by this object.
    :param callbacks: A dictionary of callbacks with async support.

    :return: a station component manager
    """
    cpt_mgr = StationComponentManager(
        station_id,
        ref_latitude,
        ref_longitude,
        ref_height,
        get_field_station_trl(station_name),
        [get_antenna_trl(station_name, antenna_name) for antenna_name in antenna_names],
        antenna_positions_smartbox_order,
        antenna_element_ids_smartbox_order,
        get_station_calibrator_trl(station_name),
        get_sps_station_trl(station_name),
        1.0,
        logger,
        callbacks["communication_state"],
        callbacks["component_state"],
    )

    wrapped_device_communication_changed_callback = MockCallable(
        wraps=cpt_mgr._device_communication_state_changed
    )
    cpt_mgr._device_communication_state_changed = (  # type: ignore[assignment]
        wrapped_device_communication_changed_callback
    )

    wrapped_apply_configuration = MockCallable(wraps=cpt_mgr.apply_configuration)
    cpt_mgr.apply_configuration = (  # type: ignore[assignment]
        wrapped_apply_configuration
    )

    wrapped_apply_pointing_delays = MockCallable(wraps=cpt_mgr.apply_pointing_delays)
    cpt_mgr.apply_pointing_delays = (  # type: ignore[assignment]
        wrapped_apply_pointing_delays
    )

    wrapped_get_pointing_delays = MockCallable(wraps=cpt_mgr.get_pointing_delays)
    cpt_mgr.get_pointing_delays = (  # type: ignore[assignment]
        wrapped_get_pointing_delays
    )
    wrapped_start_acquisition = MockCallable(wraps=cpt_mgr.start_acquisition)
    cpt_mgr.start_acquisition = wrapped_start_acquisition  # type: ignore[assignment]

    wrapped_acquire_data_for_calibration = MockCallable(
        wraps=cpt_mgr.acquire_data_for_calibration
    )
    cpt_mgr.acquire_data_for_calibration = (  # type: ignore[assignment]
        wrapped_acquire_data_for_calibration
    )

    return cpt_mgr


@pytest.fixture(name="patched_station_class")
def patched_station_class_fixture(
    mock_station_component_manager: unittest.mock.Mock,
) -> type[MccsStation]:
    """
    Return a station device class that has been patched for testing.

    :param mock_station_component_manager: the mock component manager to patch
        into this station.
    :return: a station device class that has been patched for testing.
    """

    # pylint: disable=too-many-ancestors
    class PatchedStation(MccsStation):
        """A station class that has had its component manager mocked out for testing."""

        def create_component_manager(
            self: PatchedStation,
        ) -> unittest.mock.Mock:
            """
            Return a mock component manager instead of the usual one.

            :return: a mock component manager
            """
            wrapped_state_changed_callback = MockCallable(
                wraps=self._component_state_callback
            )
            mock_station_component_manager._component_state_callback = (
                wrapped_state_changed_callback
            )

            return mock_station_component_manager

    return PatchedStation


@pytest.fixture(name="field_station_proxy")
def field_station_proxy_fixture(
    station_name: str, logger: logging.Logger
) -> MccsDeviceProxy:
    """
    Return a proxy to the FieldStation.

    :param station_name: name of the station
    :param logger: a logger for the proxy to use.

    :return: a proxy to the FieldStation.
    """
    return MccsDeviceProxy(get_field_station_trl(station_name), logger)


@pytest.fixture(name="station_calibrator_proxy")
def station_calibrator_proxy_fixture(
    station_name: str, logger: logging.Logger
) -> MccsDeviceProxy:
    """
    Return a proxy to the station calibrator.

    :param station_name: name of the station
    :param logger: a logger for the proxy to use.

    :return: a proxy to the station calibrator.
    """
    return MccsDeviceProxy(get_station_calibrator_trl(station_name), logger)


@pytest.fixture(name="sps_station_proxy")
def sps_station_proxy_fixture(
    station_name: str, logger: logging.Logger
) -> MccsDeviceProxy:
    """
    Return a proxy to the sps station.

    :param station_name: name of the station
    :param logger: a logger for the proxy to use.

    :return: a proxy to the station calibrator.
    """
    return MccsDeviceProxy(get_sps_station_trl(station_name), logger)


@pytest.fixture(name="tile_proxies")
def tile_proxies_fixture(
    station_name: str, tile_ids: Iterable[int], logger: logging.Logger
) -> list[MccsDeviceProxy]:
    """
    Return a list of proxies to tile devices.

    :param station_name: name of the station
    :param tile_ids: IDs of tiles in this station
    :param logger: a logger for the proxies to use.
    :return: a list of proxies to tile devices
    """
    return [MccsDeviceProxy(get_tile_trl(station_name, i), logger) for i in tile_ids]


@pytest.fixture(name="antenna_proxies")
def antenna_proxies_fixture(
    station_name: str, antenna_names: Iterable[str], logger: logging.Logger
) -> list[MccsDeviceProxy]:
    """
    Return a list of proxies to antenna devices.

    :param station_name: name of the station
    :param antenna_names: names of antennas in this station
    :param logger: a logger for the proxies to use.
    :return: a list of proxies to antenna devices
    """
    return [
        MccsDeviceProxy(get_antenna_trl(station_name, antenna_name), logger)
        for antenna_name in antenna_names
    ]
