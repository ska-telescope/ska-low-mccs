# -*- coding: utf-8 -*
# pylint: disable=too-many-arguments
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests for AntennaHealthModel."""
from __future__ import annotations

import pytest
from ska_control_model import HealthState
from ska_low_mccs_common.testing.mock import MockCallable

from ska_low_mccs.antenna import AntennaHealthModel


class TestAntennaHealthModel:
    """A class for tests of the station health model."""

    @pytest.fixture
    def health_model(self: TestAntennaHealthModel) -> AntennaHealthModel:
        """
        Fixture to return the station health model.

        :return: Health model to be used.
        """
        health_model = AntennaHealthModel(MockCallable())
        health_model.is_communicating(True)

        return health_model

    @pytest.mark.parametrize(
        (
            "health_params_init",
            "health_change",
            "expected_health_init",
            "expected_health_final",
        ),
        [
            pytest.param(
                {
                    "antenna_rms": (2.5, 4.4),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.OK,
                },
                {"antenna_rms": (3.2, 6.1)},
                HealthState.OK,
                HealthState.OK,
                id="Health OK, changed rms voltage, still OK",
            ),
            pytest.param(
                {
                    "antenna_rms": (5.1, 2.7),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.OK,
                },
                {"fieldstation": HealthState.DEGRADED},
                HealthState.OK,
                HealthState.DEGRADED,
                id="Health OK, FieldStation becomes DEGRADED, antenna DEGRADED",
            ),
            pytest.param(
                {
                    "antenna_rms": (1.1, 9.2),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.UNKNOWN,
                },
                {"tile": HealthState.FAILED},
                HealthState.UNKNOWN,
                HealthState.FAILED,
                id="Health UNKNOWN, Tile becomes FAILED, antenna FAILED",
            ),
            pytest.param(
                {
                    "antenna_rms": (0.0, 0.0),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.OK,
                },
                {"antenna_rms": (3.1, 2.5)},
                HealthState.FAILED,
                HealthState.OK,
                id="Health FAILED, ADC shows power reading, antenna OK",
            ),
            pytest.param(
                {
                    "antenna_rms": (1.7, 4.2),
                    "fieldstation": HealthState.UNKNOWN,
                    "tile": HealthState.DEGRADED,
                },
                {"tile": HealthState.FAILED},
                HealthState.UNKNOWN,
                HealthState.FAILED,
                id="Health UNKNOWN, Tile becomes FAILED, antenna FAILED",
            ),
            pytest.param(
                {
                    "antenna_rms": (2.2, 3.1),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.OK,
                },
                {"antenna_rms": (0.0, 3.2)},
                HealthState.OK,
                HealthState.FAILED,
                id="Health OK, ADC x rms goes to 0, antenna FAILED",
            ),
        ],
    )
    def test_antenna_health_changing_values(
        self: TestAntennaHealthModel,
        health_model: AntennaHealthModel,
        health_params_init: dict,
        health_change: dict,
        expected_health_init: HealthState,
        expected_health_final: HealthState,
    ) -> None:
        """
        Tests for evaluating antenna health with changing parameters.

        :param health_model: The antenna health model to use
        :param health_params_init: the tile/fieldstation and adc readings initially
        :param health_change: the change in these health params
        :param expected_health_init: the initial expected health state
        :param expected_health_final: the final expected health state after the params
            have been changed according to health_change
        """
        health_model._tile_adc_rms = health_params_init["antenna_rms"]
        health_model._fieldstation_health = health_params_init["fieldstation"]
        health_model._tile_health = health_params_init["tile"]

        assert health_model.evaluate_health() == expected_health_init

        if "antenna_rms" in health_change:
            health_model._tile_adc_rms = health_change["antenna_rms"]
        if "fieldstation" in health_change:
            health_model._fieldstation_health = health_change["fieldstation"]
        if "tile" in health_change:
            health_model._tile_health = health_change["tile"]

        assert health_model.evaluate_health() == expected_health_final

    @pytest.mark.parametrize(
        (
            "health_params",
            "thresholds_init",
            "thresholds_final",
            "expected_health_init",
            "expected_health_final",
        ),
        [
            pytest.param(
                {
                    "antenna_rms": (2.5, 4.4),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.OK,
                },
                {},
                {
                    "degraded_adc_rms_x": 2.0,
                    "degraded_adc_rms_y": 2.0,
                    "failed_adc_rms_x": 1.0,
                    "failed_adc_rms_y": 1.0,
                },
                HealthState.OK,
                HealthState.OK,
                id="Health OK, raised rms thresholds, still OK",
            ),
            pytest.param(
                {
                    "antenna_rms": (5.5, 6.2),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.OK,
                },
                {},
                {
                    "degraded_adc_rms_x": 20.0,
                    "degraded_adc_rms_y": 20.0,
                    "failed_adc_rms_x": 10.0,
                    "failed_adc_rms_y": 10.0,
                },
                HealthState.OK,
                HealthState.FAILED,
                id="Health OK, raised rms thresholds, antenna FAILED",
            ),
            pytest.param(
                {
                    "antenna_rms": (2.5, 4.4),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.OK,
                },
                {},
                {
                    "degraded_adc_rms_x": 3.0,
                    "degraded_adc_rms_y": 3.0,
                    "failed_adc_rms_x": 2.0,
                    "failed_adc_rms_y": 2.0,
                },
                HealthState.OK,
                HealthState.DEGRADED,
                id="Health OK, raised rms thresholds, antenna DEGRADED",
            ),
            pytest.param(
                {
                    "antenna_rms": (5.5, 6.2),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.OK,
                },
                {},
                {
                    "degraded_adc_rms_x": 6.5,
                    "degraded_adc_rms_y": 6.5,
                    "failed_adc_rms_x": 6.0,
                    "failed_adc_rms_y": 6.0,
                },
                HealthState.OK,
                HealthState.FAILED,
                id="Health OK, raised rms thresholds, antenna FAILED",
            ),
            pytest.param(
                {
                    "antenna_rms": (5.5, 6.2),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.OK,
                },
                {
                    "degraded_adc_rms_x": 5.0,
                    "degraded_adc_rms_y": 5.0,
                    "failed_adc_rms_x": 1.0,
                    "failed_adc_rms_y": 1.0,
                },
                {"degraded_adc_rms_x": 10.0, "failed_adc_rms_x": 7.0},
                HealthState.OK,
                HealthState.FAILED,
                id="Health OK, raised rms x threshold, antenna FAILED",
            ),
            pytest.param(
                {
                    "antenna_rms": (5.5, 6.2),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.DEGRADED,
                },
                {
                    "degraded_adc_rms_x": 5.0,
                    "degraded_adc_rms_y": 5.0,
                    "failed_adc_rms_x": 4.0,
                    "failed_adc_rms_y": 4.0,
                },
                {"degraded_adc_rms_y": 9.0, "failed_adc_rms_y": 7.0},
                HealthState.DEGRADED,
                HealthState.FAILED,
                id="Health DEGRADED, raised rms y threshold, antenna FAILED",
            ),
            pytest.param(
                {
                    "antenna_rms": (2.5, 4.4),
                    "fieldstation": HealthState.DEGRADED,
                    "tile": HealthState.OK,
                },
                {
                    "degraded_adc_rms_x": 5.0,
                    "degraded_adc_rms_y": 5.0,
                    "failed_adc_rms_x": 3.0,
                    "failed_adc_rms_y": 3.0,
                },
                {
                    "degraded_adc_rms_x": 2.0,
                    "degraded_adc_rms_y": 2.0,
                    "failed_adc_rms_x": 1.0,
                    "failed_adc_rms_y": 1.0,
                },
                HealthState.FAILED,
                HealthState.DEGRADED,
                id="Health FAILED, lowered rms thresholds, FieldStation DEGRADED "
                "so DEGRADED",
            ),
            pytest.param(
                {
                    "antenna_rms": (5.5, 1.4),
                    "fieldstation": HealthState.OK,
                    "tile": HealthState.UNKNOWN,
                },
                {
                    "degraded_adc_rms_x": 5.0,
                    "degraded_adc_rms_y": 5.0,
                    "failed_adc_rms_x": 4.0,
                    "failed_adc_rms_y": 4.0,
                },
                {
                    "degraded_adc_rms_x": 2.0,
                    "degraded_adc_rms_y": 2.0,
                    "failed_adc_rms_x": 1.0,
                    "failed_adc_rms_y": 1.0,
                },
                HealthState.FAILED,
                HealthState.UNKNOWN,
                id="Health FAILED, lowered rms thresholds to DEGRADED, "
                "Tile UNKNOWN so UNKNOWN",
            ),
        ],
    )
    def test_antenna_health_changing_thresholds(
        self: TestAntennaHealthModel,
        health_model: AntennaHealthModel,
        health_params: dict,
        thresholds_init: dict,
        thresholds_final: dict,
        expected_health_init: HealthState,
        expected_health_final: HealthState,
    ) -> None:
        """
        Tests for evaluating antenna health with changing thresholds.

        :param health_model: The antenna health model to use
        :param health_params: the tile/fieldstation and adc readings
        :param thresholds_init: the initial health model thresholds
        :param thresholds_final: the final health model thresholds
        :param expected_health_init: the initial expected health state
        :param expected_health_final: the final expected health state after the
            thresholds have been changed according to thresholds_final
        """
        health_model._tile_adc_rms = health_params["antenna_rms"]
        health_model._fieldstation_health = health_params["fieldstation"]
        health_model._tile_health = health_params["tile"]
        health_model.health_params = thresholds_init

        assert health_model.evaluate_health() == expected_health_init

        health_model.health_params = thresholds_final

        assert health_model.evaluate_health() == expected_health_final
