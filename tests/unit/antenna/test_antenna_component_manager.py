# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains unit tests of the MCCS antenna component manager module."""
from __future__ import annotations

import json
import logging
import time
import unittest.mock
from typing import Iterator

import pytest
import tango
from ska_control_model import CommunicationStatus, PowerState, ResultCode, TaskStatus
from ska_tango_testing.mock import MockCallableGroup
from ska_tango_testing.mock.placeholders import Anything

from ska_low_mccs.antenna import AntennaComponentManager
from ska_low_mccs.antenna.antenna_component_manager import (
    _FieldStationProxy,
    _TileProxy,
)
from tests.harness import (
    MccsTangoTestHarness,
    MccsTangoTestHarnessContext,
    get_field_station_trl,
    get_tile_trl,
)

STATION_NAME = "ci-1"
ANTENNA_ID = 1
TILE_ID = 1


@pytest.fixture(name="test_context")
def test_context_fixture(
    mock_field_station: unittest.mock.Mock,
    mock_tile: unittest.mock.Mock,
) -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which mock tile and field station devices are running.

    :param mock_field_station: the mock FieldStation device to be provided when a device
        proxy to the FieldStation TRL is requested.
    :param mock_tile: the mock Tile device to be provided when a device
        proxy to the Tile TRL is requested.

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()
    harness.add_mock_field_station_device(STATION_NAME, mock_field_station)
    harness.add_mock_tile_device(STATION_NAME, TILE_ID, mock_tile)
    with harness as context:
        yield context


class TestAntennaFieldStationProxy:
    """Tests of the _FieldStationProxy class."""

    @pytest.fixture(name="field_station_proxy")
    def field_station_proxy_fixture(
        self: TestAntennaFieldStationProxy,
        test_context: MccsTangoTestHarnessContext,
        logger: logging.Logger,
        callbacks: MockCallableGroup,
    ) -> _FieldStationProxy:
        """
        Return an antenna FieldStation proxy for testing.

        This is a pytest fixture.

        :param test_context: the test context in which this test will be run.
        :param logger: a loger for the antenna component manager to use
        :param callbacks: A dictionary of callbacks with async support.

        :return: an antenna FieldStation proxy
        """
        return _FieldStationProxy(
            get_field_station_trl(STATION_NAME),
            ANTENNA_ID,
            logger,
            callbacks["communication_state"],
            callbacks["component_state"],
        )

    def test_communication(
        self: TestAntennaFieldStationProxy,
        field_station_proxy: _FieldStationProxy,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test this antenna FieldStation proxy's communication with the antenna.

        :param field_station_proxy: a proxy to the antenna's FieldStation device.
        :param callbacks: A dictionary of callbacks with async support.
        """
        assert field_station_proxy.communication_state == CommunicationStatus.DISABLED
        field_station_proxy.start_communicating()

        # communication status is NOT_ESTABLISHED because establishing
        # a connection to MccsFieldStation has been enqueued but not yet run
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )

        # communication status is ESTABLISHED because MccsFieldStation's state
        # is OFF, from which we can infer that the antenna is powered
        # off.
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        field_station_proxy.stop_communicating()
        callbacks["communication_state"].assert_call(CommunicationStatus.DISABLED)

    def test_power_command(
        self: TestAntennaFieldStationProxy,
        field_station_proxy: _FieldStationProxy,
        mock_field_station: unittest.mock.Mock,
        initial_antenna_power_states: list[bool],
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test that the antenna can control its power via field station.

        :param field_station_proxy: a proxy to the antenna's FieldStation device.
        :param mock_field_station: a mock device proxy to an FieldStation
            device.
        :param initial_antenna_power_states: whether each antenna is
            initially on in the FieldStation
        :param callbacks: A dictionary of callbacks with async support.
        """
        with pytest.raises(
            ConnectionError,
            match="Communication with component is not established",
        ):
            field_station_proxy.on()

        field_station_proxy.start_communicating()
        callbacks["component_state"].assert_call(power=PowerState.OFF)
        field_station_proxy._component_state["power"] = PowerState.OFF

        assert field_station_proxy.power_state == PowerState.OFF

        assert field_station_proxy.on(callbacks["task"]) == (
            TaskStatus.QUEUED,
            "Task queued",
        )

        mock_field_station.On.assert_next_call()

        # Fake an event that tells this proxy that
        # the FieldStation has been turned on.
        field_station_proxy._device_state_changed(
            "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
        )
        callbacks["component_state"].assert_call(power=PowerState.ON)

        field_station_proxy._component_state["power"] = PowerState.ON

        assert field_station_proxy.power_state == PowerState.ON
        assert field_station_proxy.supplied_power_state == PowerState.OFF

        time.sleep(0.1)
        assert field_station_proxy.power_on() == ResultCode.OK
        mock_field_station.PowerOnAntenna.assert_next_call(ANTENNA_ID)

        # The antenna power mode won't update until an event confirms that
        # the antenna is on.
        assert field_station_proxy.supplied_power_state == PowerState.OFF

        # Fake an event that tells this proxy that the antenna is now on as requested
        are_antennas_on = initial_antenna_power_states
        are_antennas_on[str(ANTENNA_ID)] = PowerState.ON

        field_station_proxy._antenna_power_state_changed(
            "antennaPowerStates",
            json.dumps(are_antennas_on),
            tango.AttrQuality.ATTR_VALID,
        )
        assert field_station_proxy.supplied_power_state == PowerState.ON

        assert field_station_proxy.power_on() is None
        mock_field_station.PowerOnAntenna.assert_not_called()
        assert field_station_proxy.supplied_power_state == PowerState.ON

        assert field_station_proxy.power_off() == ResultCode.OK
        mock_field_station.PowerOffAntenna.assert_next_call(ANTENNA_ID)

        # The power mode won't update until an event confirms that the antenna is on.
        assert field_station_proxy.supplied_power_state == PowerState.ON

        # Fake an event that tells this proxy that the antenna is now off as requested
        are_antennas_on[str(ANTENNA_ID)] = PowerState.OFF
        field_station_proxy._antenna_power_state_changed(
            "antennaPowerStates",
            json.dumps(are_antennas_on),
            tango.AttrQuality.ATTR_VALID,
        )
        assert field_station_proxy.supplied_power_state == PowerState.OFF

        assert field_station_proxy.power_off() is None
        mock_field_station.PowerOffAntenna.assert_not_called()
        assert field_station_proxy.supplied_power_state == PowerState.OFF

    def test_reset(
        self: TestAntennaFieldStationProxy, field_station_proxy: _FieldStationProxy
    ) -> None:
        """
        Test that this antenna FieldStation proxy refuses to try to reset the antenna.

        :param field_station_proxy: a proxy to the antenna's FieldStation device.
        """
        with pytest.raises(
            NotImplementedError,
            match="Antenna cannot be reset.",
        ):
            field_station_proxy.reset()


class TestAntennaTileProxy:
    """Tests of the _TileProxy class."""

    @pytest.fixture(name="antenna_tile_proxy")
    def antenna_tile_proxy_fixture(
        self: TestAntennaTileProxy,
        test_context: MccsTangoTestHarnessContext,
        tile_channels: tuple[int, int],
        logger: logging.Logger,
        callbacks: MockCallableGroup,
    ) -> _TileProxy:
        """
        Return an antenna tile proxy for testing.

        This is a pytest fixture.

        :param test_context: the test context in which this test will be run.
        :param tile_channels: the tile ADC channels of
            the Y and X signals from the antenna
        :param logger: a loger for the antenna component manager to use
        :param callbacks: A dictionary of callbacks with async support.
        :return: an antenna tile proxy for testing
        """
        return _TileProxy(
            get_tile_trl(STATION_NAME, TILE_ID),
            tile_channels,
            logger,
            callbacks["communication_state"],
            callbacks["component_state"],
        )

    def test_communication(
        self: TestAntennaTileProxy,
        antenna_tile_proxy: _TileProxy,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test that this proxy refuses to try to invoke power commands on the antenna.

        :param antenna_tile_proxy: a proxy to the antenna's tile device.
        :param callbacks: A dictionary of callbacks with async support.
        """
        assert antenna_tile_proxy.communication_state == CommunicationStatus.DISABLED
        antenna_tile_proxy.start_communicating()

        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        antenna_tile_proxy.stop_communicating()

        callbacks["communication_state"].assert_call(CommunicationStatus.DISABLED)

    @pytest.mark.parametrize("command", ["on", "standby", "off"])
    def test_power_command(
        self: TestAntennaTileProxy,
        antenna_tile_proxy: _TileProxy,
        command: str,
    ) -> None:
        """
        Test that this proxy will refuse to try to run power commands on the antenna.

        :param antenna_tile_proxy: a proxy to the antenna's tile device.
        :param command: name of the power command to run.
        """
        with pytest.raises(
            NotImplementedError,
            match="Antenna power state is not controlled via Tile device.",
        ):
            getattr(antenna_tile_proxy, command)()

    def test_reset(self: TestAntennaTileProxy, antenna_tile_proxy: _TileProxy) -> None:
        """
        Test that this antenna tile proxy refuses to try to reset the antenna.

        :param antenna_tile_proxy: a proxy to the antenna's tile device.
        """
        with pytest.raises(
            NotImplementedError,
            match="Antenna hardware is not resettable.",
        ):
            antenna_tile_proxy.reset()

    def test_adc_rms_changed(
        self: TestAntennaTileProxy,
        antenna_tile_proxy: _TileProxy,
        tile_channels: tuple[int, int],
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test that the proxy responds to changes in the ADC rms power.

        :param antenna_tile_proxy: a proxy to the antenna's tile device.
        :param tile_channels: the tile ADC channels of
            the Y and X signals from the antenna
        :param callbacks: A dictionary of callbacks with async support.
        """
        antenna_tile_proxy.start_communicating()
        time.sleep(0.1)

        # Fake an event that tells this proxy that the Tile has been turned on.
        antenna_tile_proxy._device_state_changed(
            "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
        )
        assert antenna_tile_proxy._adc_rms_change_registered

        # Fake a received change in adcPower
        event_data = unittest.mock.Mock
        attr_value = unittest.mock.Mock
        attr_value.name = "adcPower"
        attr_value.value = list(range(32))
        attr_value.quality = tango.AttrQuality.ATTR_VALID
        event_data.attr_value = attr_value
        event_data.err = False

        assert antenna_tile_proxy._proxy is not None
        antenna_tile_proxy._proxy._change_event_received(event_data)

        callbacks["component_state"].assert_call(
            adc_rms=tile_channels,
            lookahead=3,
        )


class TestAntennaComponentManager:
    """Tests of the AntennaComponentManager."""

    @pytest.fixture(name="antenna_component_manager")
    def antenna_component_manager_fixture(
        self: TestAntennaComponentManager,
        test_context: MccsTangoTestHarnessContext,
        tile_channels: tuple[int, int],
        logger: logging.Logger,
        callbacks: MockCallableGroup,
    ) -> AntennaComponentManager:
        """
        Return an antenna component manager.

        :param test_context: the test context in which this test will be run.
        :param tile_channels: the tile ADC channels of
            the Y and X signals from the antenna
        :param logger: a loger for the antenna component manager to use
        :param callbacks: A dictionary of callbacks with async support.

        :return: an antenna component manager
        """
        return AntennaComponentManager(
            get_field_station_trl(STATION_NAME),
            ANTENNA_ID,
            get_tile_trl(STATION_NAME, TILE_ID),
            tile_channels,
            logger,
            callbacks["communication_state"],
            callbacks["component_state"],
        )

    def test_communication(
        self: TestAntennaComponentManager,
        antenna_component_manager: AntennaComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test communication between the antenna component manager and its antenna.

        :param antenna_component_manager: the antenna component manager
            under test
        :param callbacks: A dictionary of callbacks with async support.
        """
        assert (
            antenna_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )

        antenna_component_manager.start_communicating()

        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            antenna_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        )

        antenna_component_manager.stop_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.DISABLED,
            lookahead=2,
        )
        assert (
            antenna_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )

    def test_power_commands(
        self: TestAntennaComponentManager,
        antenna_component_manager: AntennaComponentManager,
        mock_field_station: unittest.mock.Mock,
        initial_antenna_power_states: dict[str, PowerState],
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the power commands.

        :param antenna_component_manager: the antenna component manager
            under test
        :param mock_field_station: a mock proxy to the antenna's
            FieldStation device
        :param initial_antenna_power_states: whether each antenna is
            initially on in the FieldStation
        :param callbacks: A dictionary of callbacks with sync support.
        """
        assert isinstance(
            antenna_component_manager._communication_state_callback,
            MockCallableGroup._Callable,
        )
        antenna_component_manager.start_communicating()

        antenna_component_manager._component_state["power"] = PowerState.OFF
        assert antenna_component_manager.power_state == PowerState.OFF

        antenna_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        antenna_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.ESTABLISHED
        )
        assert (
            antenna_component_manager._field_station_proxy.communication_state
            == CommunicationStatus.ESTABLISHED
        )
        antenna_component_manager._field_station_proxy._device_state_changed(
            "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
        )

        antenna_component_manager._field_station_power_state_changed(PowerState.ON)
        time.sleep(0.2)
        assert antenna_component_manager.power_state == PowerState.OFF
        # FieldStation is on but antenna is off
        assert antenna_component_manager.on(callbacks["task"]) == (
            TaskStatus.QUEUED,
            "Task queued",
        )

        callbacks["task"].assert_call(status=TaskStatus.QUEUED, lookahead=2)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS, lookahead=2)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result="Antenna on completed",
            lookahead=2,
        )
        mock_field_station.PowerOnAntenna.assert_next_call(ANTENNA_ID)

        # The power state won't update until an event confirms that the antenna is on.
        assert antenna_component_manager.power_state == PowerState.OFF

        # Fake an event that tells the FieldStation proxy that the antenna is now on
        are_antennas_on = initial_antenna_power_states
        are_antennas_on[str(ANTENNA_ID)] = PowerState.ON
        antenna_component_manager._field_station_proxy._antenna_power_state_changed(
            "antennaPowerStates",
            json.dumps(are_antennas_on),
            tango.AttrQuality.ATTR_VALID,
        )
        callbacks["component_state"].assert_call(
            power=PowerState.ON,
            trl=antenna_component_manager._field_station_proxy._name,
            lookahead=10,
        )
        antenna_component_manager._component_state["power"] = PowerState.ON

        assert antenna_component_manager.power_state == PowerState.ON
        assert antenna_component_manager.on() == (
            TaskStatus.QUEUED,
            "Task queued",
        )
        mock_field_station.PowerOnAntenna.assert_not_called()
        assert antenna_component_manager.power_state == PowerState.ON

        assert antenna_component_manager.off(task_callback=callbacks["task"]) == (
            TaskStatus.QUEUED,
            "Task queued",
        )
        callbacks["task"].assert_call(status=TaskStatus.QUEUED, lookahead=2)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS, lookahead=2)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result="Antenna off completed",
            lookahead=2,
        )
        mock_field_station.PowerOffAntenna.assert_next_call(ANTENNA_ID)
        # The power state won't update until an event confirms that the antenna is on.
        assert antenna_component_manager.power_state == PowerState.ON

        # Fake an event that tells this proxy that the antenna is now off as requested
        are_antennas_on[str(ANTENNA_ID)] = PowerState.OFF
        antenna_component_manager._field_station_proxy._antenna_power_state_changed(
            "antennaPowerStates",
            json.dumps(are_antennas_on),
            tango.AttrQuality.ATTR_VALID,
        )
        callbacks["component_state"].assert_call(
            power=PowerState.OFF,
            trl=antenna_component_manager._field_station_proxy._name,
            lookahead=3,
        )
        antenna_component_manager._component_state["power"] = PowerState.OFF
        assert antenna_component_manager.power_state == PowerState.OFF

        assert antenna_component_manager.off() == (
            TaskStatus.QUEUED,
            "Task queued",
        )
        mock_field_station.PowerOffAntenna.assert_not_called()
        assert antenna_component_manager.power_state == PowerState.OFF

        with pytest.raises(
            NotImplementedError,
            match="Antenna has no standby state.",
        ):
            antenna_component_manager.standby()

    def test_eventual_consistency_of_on_command(
        self: TestAntennaComponentManager,
        antenna_component_manager: AntennaComponentManager,
        callbacks: MockCallableGroup,
        mock_field_station: unittest.mock.Mock,
    ) -> None:
        """
        Test that eventual consistency semantics of the on command.

        This test tells the antenna component manager to turn on, in
        circumstances in which it cannot possibly do so (the FieldStation is
        turned off). Instead of failing, it waits to the FieldStation to turn
        on, and then executes the on command.

        :param antenna_component_manager: the antenna component manager
            under test
        :param callbacks: A dictionary of callbacks with async support.
        :param mock_field_station: a mock device proxy to a
            FieldStation device.
        """
        antenna_component_manager.start_communicating()

        # Check for power state off callback, then update component manager accordingly
        # callbacks["component_state"].assert_call(
        #     power=PowerState.OFF, lookahead=3
        # )
        antenna_component_manager._component_state["power"] = PowerState.OFF

        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)

        task_status, message = antenna_component_manager.on()
        assert (task_status, message) == (TaskStatus.QUEUED, "Task queued")

        # fieldstation first turns on then the power turns on.
        antenna_component_manager._field_station_power_state_changed(PowerState.ON)
        mock_field_station.PowerOnAntenna.assert_next_call(ANTENNA_ID)

    def test_reset(
        self: TestAntennaComponentManager,
        antenna_component_manager: AntennaComponentManager,
    ) -> None:
        """
        Test that the antenna component manager refused to try to reset the antenna.

        :param antenna_component_manager: the antenna component manager
            under test
        """
        with pytest.raises(
            NotImplementedError,
            match="Antenna cannot be reset.",
        ):
            antenna_component_manager.reset()

    def test_power_commands1(
        self: TestAntennaComponentManager,
        antenna_component_manager: AntennaComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the power commands.

        :param antenna_component_manager: the antenna component manager
            under test
        :param callbacks: A dictionary of callbacks with async support.
        """
        assert isinstance(
            antenna_component_manager._communication_state_callback,
            MockCallableGroup._Callable,
        )
        antenna_component_manager.start_communicating()
        antenna_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        antenna_component_manager._communication_state_callback.assert_call(
            CommunicationStatus.ESTABLISHED
        )

        antenna_component_manager._antenna_power_state_changed(PowerState.UNKNOWN)
        assert antenna_component_manager.power_state == PowerState.UNKNOWN

        antenna_component_manager._field_station_component_fault_changed(True)
        callbacks["component_state"].assert_call(fault=True, lookahead=10)
        # antenna_component_manager._faulty doesn't get set in this test.
        # Instead we check the _component_state attr for fault state.
        assert antenna_component_manager._component_state["fault"] is True

        antenna_component_manager._field_station_component_fault_changed(False)
        callbacks["component_state"].assert_call(fault=False, lookahead=10)
        assert antenna_component_manager._component_state["fault"] is False

        antenna_component_manager._tile_component_fault_changed(True)
        callbacks["component_state"].assert_call(fault=True, lookahead=10)
        assert antenna_component_manager._component_state["fault"] is True

        # case where we try to turn on with no proxy present.

        antenna_component_manager._field_station_proxy._proxy = None
        antenna_component_manager._field_station_power_state = PowerState.ON
        antenna_component_manager._component_state["power"] = PowerState.OFF
        time.sleep(2)

        antenna_component_manager.on(callbacks["task"])
        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(status=TaskStatus.FAILED, result=Anything)

    def test_configure(
        self: TestAntennaComponentManager,
        antenna_component_manager: AntennaComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test tile attribute assignment.

        Specifically, test that when the antenna component manager
        established communication with its tiles, it write its antenna
        id and a unique logical tile id to each one.

        :param antenna_component_manager: the antenna component manager
            under test.
        :param callbacks: A dictionary of callbacks with async support.
        """
        antenna_component_manager.start_communicating()
        callbacks["communication_state"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_state"].assert_call(CommunicationStatus.ESTABLISHED)
        assert (
            antenna_component_manager._field_station_communication_state
            == CommunicationStatus.ESTABLISHED
        )
        assert (
            antenna_component_manager._tile_communication_state
            == CommunicationStatus.ESTABLISHED
        )
        antenna_component_manager._configure(
            task_callback=callbacks["task"],
            antenna_config={"xDisplacement": 1.0, "yDisplacement": 1.0},
            tile_config={"fixed_delays": (1, 2)},
        )
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED,
            result="Configure command has completed",
        )

        callbacks["component_state"].assert_call(
            configuration_changed={"xDisplacement": 1.0, "yDisplacement": 1.0},
            lookahead=10,
        )
