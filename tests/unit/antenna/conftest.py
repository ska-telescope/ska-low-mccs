# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module defined a pytest test harness for testing the MCCS antenna module."""
from __future__ import annotations

import unittest.mock

import pytest
import pytest_mock
import tango
from ska_control_model import PowerState, ResultCode, TaskStatus
from ska_low_mccs_common.testing.mock import MockDeviceBuilder
from ska_tango_testing.mock import MockCallable

STATION_NAME = "ci-1"


@pytest.fixture(name="mock_component_manager")
def mock_component_manager_fixture(
    mocker: pytest_mock.MockerFixture,
) -> unittest.mock.Mock:
    """
    Return a mock to be used as a component manager for the antenna device.

    :param mocker: fixture that wraps the :py:mod:`unittest.mock` module
    :return: a mock to be used as a component manager for the antenna device.
    """
    mock_component_manager = mocker.Mock()
    mock_component_manager.apply_pointing = MockCallable()
    mock_component_manager.apply_pointing.configure_mock(
        return_value=(TaskStatus.QUEUED, "Task queued")
    )

    mock_component_manager.configure = MockCallable()
    mock_component_manager.configure.configure_mock(
        return_value=(TaskStatus.QUEUED, "Task queued")
    )
    mock_component_manager.max_queued_tasks = 0
    mock_component_manager.max_executing_tasks = 1
    return mock_component_manager


@pytest.fixture(name="tile_channels")
def tile_channels_fixture() -> tuple[int, int]:
    """
    Return the tile ADC channels of the Y and X signals from this antenna.

    :return: the tile ADC channels of the Y and X signals from this antenna.
    """
    return (1, 0)


@pytest.fixture(name="initial_antenna_power_state")
def initial_antenna_power_state_fixture() -> PowerState:
    """
    Return the initial power mode of the antenna.

    :return: the initial power mode of the antenna.
    """
    return PowerState.OFF


@pytest.fixture(name="initial_antenna_power_states")
def initial_antenna_power_states_fixture(
    initial_antenna_power_state: PowerState,
) -> dict[str, PowerState]:
    """
    Return whether each FieldStation port is on.

    :param initial_antenna_power_state: The initial power of the FieldStation Ports

    :return: whether each each FieldStation port is initially on.
    """
    power_states = {}
    for i in range(256):
        power_states[str(i)] = initial_antenna_power_state
    return power_states


@pytest.fixture(name="mock_field_station")
def mock_field_station_fixture(
    initial_antenna_power_states: list[bool],
) -> unittest.mock.Mock:
    """
    Fixture that provides a mock MccsFieldStation device.

    :param initial_antenna_power_states: whether each each
        FieldStation port is initially on.

    :return: a mock MccsFieldStation device.
    """
    builder = MockDeviceBuilder()
    builder.set_state(tango.DevState.OFF)
    builder.add_command("IsPortOn", False)
    builder.add_result_command("On", ResultCode.OK)
    builder.add_result_command("PowerOnAntenna", ResultCode.OK)
    builder.add_result_command("PowerOffAntenna", ResultCode.OK)
    builder.add_attribute("antennaPowerStates", initial_antenna_power_states)
    return builder()


@pytest.fixture(name="mock_tile")
def mock_tile_fixture() -> unittest.mock.Mock:
    """
    Fixture that provides a mock MccsTile device.

    This has no tile-specific functionality at present.

    :return: a mock Mccs device.
    """
    builder = MockDeviceBuilder()
    return builder()
