#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains the tests of the calibration store component manager."""
from __future__ import annotations

import logging
from typing import Any, Iterator

import pytest
from ska_control_model import CommunicationStatus
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs.calibration_store import (
    CalibrationStoreComponentManager,
    DatabaseSolution,
)
from tests.harness import MccsTangoTestHarness


@pytest.fixture(name="test_context")
def test_context_fixture() -> Iterator[None]:
    """
    Yield into a context in which Tango is running, with mock devices.

    :yields: into a context in which Tango is running
    """
    harness = MccsTangoTestHarness()
    with harness:
        yield


@pytest.fixture(name="callbacks")
def callbacks_fixture() -> MockCallableGroup:
    """
    Return a dictionary of callables to be used as callbacks.

    :return: a dictionary of callables to be used as callbacks.
    """
    return MockCallableGroup(
        "communication_status",
        "component_state",
        "task",
        timeout=5.0,
    )


# pylint: disable=too-many-arguments
@pytest.fixture(name="calibration_store_component_manager")
def calibration_store_component_manager_fixture(
    test_context: None,
    patched_calibration_store_component_manager_class: type[
        CalibrationStoreComponentManager
    ],
    logger: logging.Logger,
    callbacks: MockCallableGroup,
    database_host: str,
    database_port: int,
    database_name: str,
    database_admin_user: str,
    database_admin_password: str,
) -> CalibrationStoreComponentManager:
    """
    Return a station calibrator component manager.

    The database connection for this component manager talks to a mock connection.

    :param test_context: a Tango test context running the required
        mock subservient devices
    :param patched_calibration_store_component_manager_class: the patched component
        manager class with a mock of the database connection
    :param logger: a logger to be used by the commonent manager
    :param callbacks: callback group
    :param database_host: the database host
    :param database_port: the database port
    :param database_name: the database name
    :param database_admin_user: the database admin user
    :param database_admin_password: the database admin password

    :return: a station calibrator component manager.
    """
    return patched_calibration_store_component_manager_class(
        logger,
        callbacks["communication_status"],
        callbacks["component_state"],
        database_host,
        database_port,
        database_name,
        database_admin_user,
        database_admin_password,
        policy_name="preferred",
        policy_frequency_tolerance=0,
        policy_temperature_tolerance=200.0,
    )


@pytest.mark.forked
def test_communication(
    calibration_store_component_manager: CalibrationStoreComponentManager,
    callbacks: MockCallableGroup,
) -> None:
    """
    Test communication for the calibration store component manager.

    :param calibration_store_component_manager: the calibration store component
        manager under test
    :param callbacks: dictionary of driver callbacks.
    """
    assert (
        calibration_store_component_manager.communication_state
        == CommunicationStatus.DISABLED
    )

    # takes the component out of DISABLED
    calibration_store_component_manager.start_communicating()
    callbacks["communication_status"].assert_call(CommunicationStatus.NOT_ESTABLISHED)
    callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)

    calibration_store_component_manager.stop_communicating()

    callbacks["communication_status"].assert_call(CommunicationStatus.DISABLED)


@pytest.fixture(name="invalid_solution")
def invalid_solution_fixture() -> dict[str, Any]:
    """
    Return a invalid calibration solution to store.

    :return: invalid calibration solution parameters
    """
    return {
        "solution": [2] * 2048,
        "station_id": 5,
        "frequency_channel": 75,
        "outside_temperature": 23.8,
        "acquisition_time": 1718077120.0000677,
        "calibration_path": "some/path/to/correlation_matrix.hdf5",
        # "preferred": True, # missing the preferred column
    }


@pytest.fixture(name="valid_solution")
def valid_solution_fixture() -> dict[str, Any]:
    """
    Return a valid calibration solution to store.

    :return: valid calibration solution parameters
    """
    return {
        "solution": [2] * 2048,
        "station_id": 5,
        "frequency_channel": 75,
        "outside_temperature": 23.8,
        "acquisition_time": 1718077120.0000677,
        "calibration_path": "some/path/to/correlation_matrix.hdf5",
        "preferred": True,
    }


def test_database_solution(
    invalid_solution: dict[str, Any],
    valid_solution: dict[str, Any],
) -> None:
    """
    Test the database solution dataclass.

    check that `generate_loading_instruction` return
    an sql query

    :param invalid_solution: fixture with a invalid argument
    :param valid_solution: fixture with a valid argument
    """
    with pytest.raises(TypeError):
        DatabaseSolution(**invalid_solution)

    database_item = DatabaseSolution(**valid_solution)

    # This is a very specific test checking the sql query is as expected.
    # At the time of writing this our devcontainer does not spin up a
    # postgresql database, meaning we can only test this interface using
    # functional tests. As a result we have this highly specific test
    # checking for any changes to a query.
    assert database_item.generate_loading_instruction()[0].as_string() == (
        'INSERT INTO tab_mccs_calib (creation_time, "acquisition_time", '
        '"calibration_path", "corrcoeff", "frequency_channel", '
        '"galactic_centre_elevation", '
        '"lst", "masked_antennas", "n_masked_final", "n_masked_initial", '
        '"outside_temperature", "preferred", "residual_max", "residual_std", '
        '"solution", "station_id", "sun_adjustment_factor", '
        '"sun_elevation", '
        '"xy_phase") VALUES (current_timestamp, %s, %s, %s, %s, %s, %s, %s, '
        "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    )
    assert database_item.generate_loading_instruction()[1] == (
        valid_solution[
            "acquisition_time"
        ],  # Indexed directly since this is mandatory in database table
        valid_solution.get("calibration_path"),  # None is not defined
        valid_solution.get("corrcoeff"),  # None is not defined
        valid_solution.get("frequency_channel"),  # None is not defined
        valid_solution.get("galactic_centre_elevation"),  # None is not defined
        valid_solution.get("lst"),  # None is not defined
        valid_solution.get("masked_antennas"),  # None is not defined
        valid_solution.get("n_masked_final"),  # None is not defined
        valid_solution.get("n_masked_initial"),  # None is not defined
        valid_solution.get("outside_temperature"),  # None is not defined
        valid_solution[
            "preferred"
        ],  # Indexed directly since this is mandatory in database table
        valid_solution.get("residual_max"),  # None is not defined
        valid_solution.get("residual_std"),  # None is not defined
        valid_solution[
            "solution"
        ],  # Indexed directly since this is mandatory in database table
        valid_solution[
            "station_id"
        ],  # Indexed directly since this is mandatory in database table
        valid_solution.get("sun_adjustment_factor"),  # None is not defined
        valid_solution.get("sun_elevation"),  # None is not defined
        valid_solution.get("xy_phase"),  # None is not defined
    )
