# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains integration tests of tile-subrack interactions in MCCS."""
from __future__ import annotations

import gc
import json
import time
from typing import Iterator

import pytest
import tango
from ska_control_model import AdminMode, HealthState, ObsState
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext

# TODO: Weird hang-at-garbage-collection bug
gc.disable()


@pytest.fixture(name="test_context")
def test_context_fixture() -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which subarray and station beam Tango devices are running.

    :yields: a test context.
    """
    station_name = "ci-1"

    harness = MccsTangoTestHarness()
    harness.add_subarray_beam_device(1)
    harness.add_station_device(station_name, 1, [])
    harness.add_station_beam_device(station_name, 1)

    with harness as context:
        yield context


@pytest.fixture(name="subarray_beam_device")
def subarray_beam_device_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return subarray beam device.

    :param test_context: test context.

    :return: subarray beam device.
    """
    return test_context.get_subarray_beam_device(1)


@pytest.fixture(name="station_beam_device")
def station_beam_device_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return station beam device.

    :param test_context: test context.

    :return: station beam device.
    """
    return test_context.get_station_beam_device("ci-1", 1)


@pytest.fixture(name="station_device")
def station_device_fixture(
    test_context: MccsTangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return station device.

    :param test_context: test context.

    :return: station device.
    """
    return test_context.get_station_device("ci-1")


@pytest.fixture(name="change_event_callbacks")
def change_event_callbacks_fixture() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of callables to be used as Tango change event callbacks.

    :return: a dictionary of callables to be used as tango change event
        callbacks.
    """
    return MockTangoEventCallbackGroup(
        "subarray_beam_state",
        "station_beam_state",
        "station_state",
        "subarray_beam_healthState",
        "station_beam_healthState",
        "station_healthState",
        "subarray_beam_obsState",
        timeout=2.0,
    )


def wait_expected_health_state(
    device: tango.DeviceProxy,
    expected_health: HealthState,
) -> None:
    """
    Wait for the expected health state.

    :param device: The device we are testing
    :param expected_health: The expected health
    """
    i = 0
    is_expected_health = False
    while i < 5:
        if device.healthState == expected_health:
            is_expected_health = True
            break
        time.sleep(0.1)
        i = i + 1

    assert (
        is_expected_health
    ), f"device {device} not in expected health {expected_health}"


def test_subarray_beam(
    subarray_beam_device: tango.DeviceProxy,
    station_beam_device: tango.DeviceProxy,
    station_device: tango.DeviceProxy,
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> None:
    """
    Test the subarray beam integration with station beam.

    :param subarray_beam_device: subarray beam device proxy.
    :param station_beam_device: station beam device proxy.
    :param station_device: station device proxy.
    :param change_event_callbacks: A dictionary of change event
        callbacks with async support.
    """

    # TODO Complete the test when a whole station harness is available

    assert subarray_beam_device.adminMode == AdminMode.OFFLINE
    assert station_beam_device.adminMode == AdminMode.OFFLINE
    assert station_device.adminMode == AdminMode.OFFLINE

    subarray_beam_device.subscribe_event(
        "state",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["subarray_beam_state"],
    )
    change_event_callbacks["subarray_beam_state"].assert_change_event(
        tango.DevState.DISABLE
    )

    station_beam_device.subscribe_event(
        "state",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["station_beam_state"],
    )
    change_event_callbacks["station_beam_state"].assert_change_event(
        tango.DevState.DISABLE
    )

    station_device.subscribe_event(
        "state",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["station_state"],
    )

    subarray_beam_device.subscribe_event(
        "healthState",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["subarray_beam_healthState"],
    )

    subarray_beam_device.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["subarray_beam_obsState"],
    )

    assert subarray_beam_device.healthState == HealthState.OK
    assert station_beam_device.healthState == HealthState.OK

    subarray_beam_device.adminMode = AdminMode.ONLINE
    change_event_callbacks["subarray_beam_state"].assert_change_event(
        tango.DevState.ON, 2
    )

    station_beam_device.adminMode = AdminMode.ONLINE
    change_event_callbacks["station_beam_state"].assert_change_event(
        tango.DevState.ON, 2
    )

    assert subarray_beam_device.adminMode == AdminMode.ONLINE
    assert station_beam_device.adminMode == AdminMode.ONLINE

    # Add station beam to subarray, it should be in healthy state
    # and so subarray should be healthy
    resources = {
        "subarray_id": 1,
        "subarray_beam_id": 1,
        "apertures": [
            {
                "station_id": 1,
                "aperture_id": "AP001.01",
                "station_beam_trl": "low-mccs/beam/ci-1-01",
                "channel_blocks": [1],
                "hardware_beam": 1,
            }
        ],
        "first_subarray_channel": 0,
        "number_of_channels": 8,
    }
    # Station resources are assigned by controller, so these must be
    # also assigned explicitly
    station_resources = {
        "subarray_id": 1,
        "subarray_beam_id": 1,
        "station_id": 1,
        "aperture_id": "AP001.01",
        "station_trl": "low-mccs/station/ci-1",
        "channel_blocks": [1],
        "hardware_beam": 1,
        "first_subarray_channel": 0,
        "number_of_channels": 8,
    }

    subarray_beam_device.AssignResources(json.dumps(resources))
    station_beam_device.AssignResources(json.dumps(station_resources))
    # Wait for command to compete

    change_event_callbacks["subarray_beam_obsState"].assert_change_event(
        ObsState.RESOURCING, 2
    )
    change_event_callbacks["subarray_beam_obsState"].assert_change_event(
        ObsState.IDLE, 2, True
    )

    wait_expected_health_state(station_beam_device, HealthState.OK)

    # TODO Health state from station beam is not propagated to subarray beam.
    # assert subarray_beam_device.healthState == HealthState.OK

    # Release all resources (incremental assign not supported now).
    # Add station beam that doesn't exist to to subarray, it should be in
    # UNKNOWN state and so subarray should be UNKNOWN

    subarray_beam_device.ReleaseAllResources()

    # Wait for command to compete

    change_event_callbacks["subarray_beam_obsState"].assert_change_event(
        ObsState.RESOURCING, 2
    )

    change_event_callbacks["subarray_beam_obsState"].assert_change_event(
        ObsState.EMPTY, 2
    )
    time.sleep(0.1)

    resources_2 = {
        "subarray_id": 1,
        "subarray_beam_id": 1,
        "apertures": [
            {
                "station_id": 2,
                "aperture_id": "AP001.02",
                "station_beam_trl": "low-mccs/beam/ci-1-01",
                "channel_blocks": [2],
                "hardware_beam": 2,
            }
        ],
        "first_subarray_channel": 0,
        "number_of_channels": 8,
    }

    # Station resources are assigned by controller, so these must be
    # also assigned explicitly
    station_resources_2 = {
        "subarray_id": 1,
        "subarray_beam_id": 1,
        "station_id": 1,
        "aperture_id": "AP001.01",
        "station_trl": "low-mccs/station/ci-1",
        "channel_blocks": [2],
        "hardware_beam": 2,
        "first_subarray_channel": 0,
        "number_of_channels": 8,
    }
    subarray_beam_device.AssignResources(json.dumps(resources_2))
    station_beam_device.AssignResources(json.dumps(station_resources_2))

    # Wait for command to compete

    change_event_callbacks["subarray_beam_obsState"].assert_change_event(
        ObsState.RESOURCING, 2
    )
    change_event_callbacks["subarray_beam_obsState"].assert_change_event(
        ObsState.IDLE, 2
    )
    wait_expected_health_state(subarray_beam_device, HealthState.OK)
    # station beam health state stays unknown until all station hardware
    # is added to harness
    # assert station_beam_device.healthState == HealthState.OK

    # configuration = {
    #     "subarray_id": 1,
    #     "subarray_beam_id": 1,
    #     "update_rate": 60.0,
    #     "logical_bands": [
    #         {"start_channel": 108, "number_of_channels": 8},
    #     ],
    #     "apertures": [
    #         {
    #             "aperture_id": "AP001.01",
    #             "station_beam_trl": "low-mccs/station_beam/ci-1-01",
    #         }
    #     ],
    #     "sky_coordinates": {
    #         "reference_frame": "ICRS",
    #         "c1": 123.0,
    #         "c1_rate": 0.0,
    #         "c2": -45.0,
    #         "c2_rate": 0.0,
    #     },
    # }

    # TODO COnfiguration requires full station harness
    # subarray_beam_device.Configure(json.dumps(configuration))

    # Wait for command to compete

    # change_event_callbacks["subarray_beam_obsState"].assert_change_event(
    #     ObsState.CONFIGURING, 2
    # )
    # change_event_callbacks["subarray_beam_obsState"].assert_change_event(
    #     ObsState.READY, 2
    # )
    # time.sleep(0.01)
