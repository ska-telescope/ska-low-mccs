# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains integration tests of health management in MCCS."""
from __future__ import annotations

from typing import Iterator

import pytest
import tango
from ska_control_model import HealthState
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext


@pytest.fixture(name="test_context")
def test_context_fixture() -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which subarray and station beam Tango devices are running.

    :yields: a test context.
    """
    station_names = [f"ci-{station}" for station in range(1, 3)]

    harness = MccsTangoTestHarness()
    harness.set_controller_device(
        subarrays=[],
        subarray_beams=[],
        stations=station_names,
        station_beams=[],
    )
    for i, station_name in enumerate(station_names, start=1):
        harness.add_station_device(station_name, i, [])

    for station_name in station_names:
        for antenna_id in range(1, 5):
            harness.add_antenna_device(
                station_name,
                f"sb1-{antenna_id}",
                antenna_id,
                tile_id=antenna_id,  # just a way to put these on different tiles
                tile_y_channel=1,
                tile_x_channel=0,
            )

    with harness as context:
        yield context


# pylint: disable=too-few-public-methods
class TestHealthManagement:
    """Test cases for the MCCS health management subsystem."""

    def test_controller_health_rollup(
        self: TestHealthManagement,
        test_context: MccsTangoTestHarnessContext,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test that health rolls up to the controller.

        :param test_context: the test context in which this test will be run.
        :param change_event_callbacks: A dictionary of change event callbacks
            with async support.
        """
        controller = test_context.get_controller_device()
        controller.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )

        station_1 = test_context.get_station_device("ci-1")
        station_2 = test_context.get_station_device("ci-2")
        antenna_1 = test_context.get_antenna_device("ci-1", "sb1-1")
        antenna_2 = test_context.get_antenna_device("ci-1", "sb1-2")
        antenna_3 = test_context.get_antenna_device("ci-1", "sb1-3")
        antenna_4 = test_context.get_antenna_device("ci-1", "sb1-4")
        antenna_5 = test_context.get_antenna_device("ci-2", "sb1-1")
        antenna_6 = test_context.get_antenna_device("ci-2", "sb1-2")
        antenna_7 = test_context.get_antenna_device("ci-2", "sb1-3")
        antenna_8 = test_context.get_antenna_device("ci-2", "sb1-4")

        # register a callback so we can block on state changes
        # instead of sleeping
        change_event_callbacks["state"].assert_change_event(tango.DevState.DISABLE)

        controller.subscribe_event(
            "healthState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["healthState"],
        )
        change_event_callbacks["healthState"].assert_change_event(HealthState.UNKNOWN)

        assert antenna_1.healthState == HealthState.OK
        assert antenna_2.healthState == HealthState.OK
        assert antenna_3.healthState == HealthState.OK
        assert antenna_4.healthState == HealthState.OK
        assert antenna_5.healthState == HealthState.OK
        assert antenna_6.healthState == HealthState.OK
        assert antenna_7.healthState == HealthState.OK
        assert antenna_8.healthState == HealthState.OK
        # assert tile_1.healthState == HealthState.UNKNOWN
        # assert tile_2.healthState == HealthState.UNKNOWN
        # assert tile_3.healthState == HealthState.UNKNOWN
        # assert tile_4.healthState == HealthState.UNKNOWN
        assert station_1.healthState == HealthState.UNKNOWN
        assert station_2.healthState == HealthState.UNKNOWN
        assert controller.healthState == HealthState.UNKNOWN

        # TODO: Fix this test once the below bug is fixed.
        # Due to https://gitlab.com/tango-controls/cppTango/-/issues/843
        # we can only run tests for ten seconds before the Tango
        # keepalive thread dies. This is about as far as this test gets
        # before its ten seconds are up.

        # controller.adminMode = AdminMode.ONLINE
        # station_1.adminMode = AdminMode.ONLINE
        # station_2.adminMode = AdminMode.ONLINE
        # apiu_1.adminMode = AdminMode.ONLINE
        # apiu_2.adminMode = AdminMode.ONLINE
        # antenna_1.adminMode = AdminMode.ONLINE
        # antenna_2.adminMode = AdminMode.ONLINE
        # antenna_3.adminMode = AdminMode.ONLINE
        # antenna_4.adminMode = AdminMode.ONLINE
        # antenna_5.adminMode = AdminMode.ONLINE
        # antenna_6.adminMode = AdminMode.ONLINE
        # antenna_7.adminMode = AdminMode.ONLINE
        # antenna_8.adminMode = AdminMode.ONLINE

        # change_event_callbacks["state"].assert_change_event(tango.DevState.UNKNOWN)
        # change_event_callbacks["state"].assert_change_event(
        # tango.DevState.OFF, lookahead=50
        # )

        # assert antenna_1.state() == tango.DevState.OFF
        # assert antenna_2.state() == tango.DevState.OFF
        # assert antenna_3.state() == tango.DevState.OFF
        # assert antenna_4.state() == tango.DevState.OFF
        # assert antenna_5.state() == tango.DevState.OFF
        # assert antenna_6.state() == tango.DevState.OFF
        # assert antenna_7.state() == tango.DevState.OFF
        # assert antenna_8.state() == tango.DevState.OFF
        # assert apiu_1.state() == tango.DevState.OFF
        # assert apiu_2.state() == tango.DevState.OFF
        # assert station_1.state() == tango.DevState.OFF
        # assert station_2.state() == tango.DevState.OFF
        # assert controller.state() == tango.DevState.OFF

        # change_event_callbacks["healthState"].assert_change_event(HealthState.OK)

        # assert antenna_1.healthState == HealthState.OK
        # assert antenna_2.healthState == HealthState.OK
        # assert antenna_3.healthState == HealthState.OK
        # assert antenna_4.healthState == HealthState.OK
        # assert antenna_5.healthState == HealthState.OK
        # assert antenna_6.healthState == HealthState.OK
        # assert antenna_7.healthState == HealthState.OK
        # assert antenna_8.healthState == HealthState.OK
        # assert apiu_1.healthState == HealthState.OK
        # assert apiu_2.healthState == HealthState.OK
        # assert station_1.healthState == HealthState.OK
        # assert station_2.healthState == HealthState.OK
        # assert controller.healthState == HealthState.OK

        # controller.On()

        # change_event_callbacks["state"].assert_change_event(tango.DevState.ON)

        # assert antenna_1.state() == tango.DevState.ON
        # assert antenna_2.state() == tango.DevState.ON
        # assert antenna_3.state() == tango.DevState.ON
        # assert antenna_4.state() == tango.DevState.ON
        # assert antenna_5.state() == tango.DevState.ON
        # assert antenna_6.state() == tango.DevState.ON
        # assert antenna_7.state() == tango.DevState.ON
        # assert antenna_8.state() == tango.DevState.ON
        # assert apiu_1.state() == tango.DevState.ON
        # assert apiu_2.state() == tango.DevState.ON
        # assert station_1.state() == tango.DevState.ON
        # assert station_2.state() == tango.DevState.ON
        # assert controller.state() == tango.DevState.ON

        # assert antenna_1.healthState == HealthState.OK
        # assert antenna_2.healthState == HealthState.OK
        # assert antenna_3.healthState == HealthState.OK
        # assert antenna_4.healthState == HealthState.OK
        # assert antenna_5.healthState == HealthState.OK
        # assert antenna_6.healthState == HealthState.OK
        # assert antenna_7.healthState == HealthState.OK
        # assert antenna_8.healthState == HealthState.OK
        # assert apiu_1.healthState == HealthState.OK
        # assert apiu_2.healthState == HealthState.OK
        # assert station_1.healthState == HealthState.OK
        # assert station_2.healthState == HealthState.OK
        # assert controller.healthState == HealthState.OK

        # # TODO: This test no longer makes sense with the split to the repos.
        # # Should this should be removed or rewritten?

        # # Now let's make tile 1 fail. We should see that failure
        # # propagate up to station and then to controller
        # tile_1.SimulateFault(True)

        # controller_device_health_state_changed_callback.assert_last_change_event(
        #     HealthState.FAILED
        # )

        # assert tile_1.state() == tango.DevState.FAULT
        # assert tile_1.healthState == HealthState.FAILED

        # assert tile_2.healthState == HealthState.OK
        # assert tile_3.healthState == HealthState.OK
        # assert tile_4.healthState == HealthState.OK

        # assert antenna_1.healthState == HealthState.FAILED  # depends on that tile
        # assert antenna_2.healthState == HealthState.FAILED  # depends on that tile
        # assert antenna_3.healthState == HealthState.OK
        # assert antenna_4.healthState == HealthState.OK
        # assert antenna_5.healthState == HealthState.OK
        # assert antenna_6.healthState == HealthState.OK
        # assert antenna_7.healthState == HealthState.OK
        # assert antenna_8.healthState == HealthState.OK
        # assert apiu_1.healthState == HealthState.OK
        # assert apiu_2.healthState == HealthState.OK
        # assert station_1.healthState == HealthState.FAILED
        # assert station_2.healthState == HealthState.OK
        # assert subrack.healthState == HealthState.OK
        # assert controller.healthState == HealthState.FAILED

        # # It might take some time to replace the failed tile 1, and
        # # meanwhile we don't want it alarming for weeks. Let's disable it,
        # # then take it offline. The tile will still report itself as FAILED,
        # # but station will not take it into account when calculating its own
        # # health.
        # tile_1.adminMode = AdminMode.OFFLINE

        # controller_device_health_state_changed_callback.assert_next_change_event(
        #     HealthState.OK
        # )

        # assert tile_1.state() == tango.DevState.DISABLE
        # assert tile_1.healthState == HealthState.UNKNOWN  # and it won't roll up

        # assert tile_2.healthState == HealthState.OK
        # assert tile_3.healthState == HealthState.OK
        # assert tile_4.healthState == HealthState.OK

        # assert antenna_1.healthState == HealthState.OK  # not rolling up tile health
        # assert antenna_2.healthState == HealthState.OK  # not rolling up tile health
        # assert antenna_3.healthState == HealthState.OK
        # assert antenna_4.healthState == HealthState.OK
        # assert antenna_5.healthState == HealthState.OK
        # assert antenna_6.healthState == HealthState.OK
        # assert antenna_7.healthState == HealthState.OK
        # assert antenna_8.healthState == HealthState.OK
        # assert apiu_1.healthState == HealthState.OK
        # assert apiu_2.healthState == HealthState.OK
        # assert station_1.healthState == HealthState.OK  # not rolling up tile health
        # assert station_2.healthState == HealthState.OK
        # assert subrack.healthState == HealthState.OK
        # assert controller.healthState == HealthState.OK  # not rolling up tile health

        # # Okay, we've finally fixed the tile. Let's make it work again, and
        # # put it back online
        # tile_1.SimulateFault(False)
        # tile_1.adminMode = AdminMode.ONLINE

        # controller_device_health_state_changed_callback.assert_last_change_event(
        #     HealthState.OK
        # )

        # assert tile_1.healthState == HealthState.OK
        # assert tile_2.healthState == HealthState.OK
        # assert tile_3.healthState == HealthState.OK
        # assert tile_4.healthState == HealthState.OK

        # assert antenna_1.healthState == HealthState.OK
        # assert antenna_2.healthState == HealthState.OK
        # assert antenna_3.healthState == HealthState.OK
        # assert antenna_4.healthState == HealthState.OK
        # assert antenna_5.healthState == HealthState.OK
        # assert antenna_6.healthState == HealthState.OK
        # assert antenna_7.healthState == HealthState.OK
        # assert antenna_8.healthState == HealthState.OK
        # assert apiu_1.healthState == HealthState.OK
        # assert apiu_2.healthState == HealthState.OK
        # assert station_1.healthState == HealthState.OK
        # assert station_2.healthState == HealthState.OK
        # assert subrack.healthState == HealthState.OK
        # assert controller.healthState == HealthState.OK
