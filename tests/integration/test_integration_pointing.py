# -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests of pointing interactions between devices."""
from __future__ import annotations

import gc
import time
from typing import Iterator

import pytest
import tango
from ska_control_model import AdminMode, ResultCode

from tests.harness import MccsTangoTestHarness, MccsTangoTestHarnessContext

gc.disable()


# some constants to refer to in the tests
BEAM_1_ID = 1
BEAM_1_DELAY_AZIMUTH = 1.0e-9
BEAM_1_DELAY_ELEVATION = 1.0e-9
BEAM_1_DELAY_RATE_AZIMUTH = 0.1e-11
BEAM_1_DELAY_RATE_ELEVATION = 0.1e-11
BEAM_2_ID = 2
BEAM_2_DELAY_AZIMUTH = 2.0e-9
BEAM_2_DELAY_ELEVATION = 2.0e-9
BEAM_2_DELAY_RATE_AZIMUTH = 0.2e-11
BEAM_2_DELAY_RATE_ELEVATION = 0.2e-11
BEAM_3_ID = 3
BEAM_3_DELAY_AZIMUTH = 3.0e-9
BEAM_3_DELAY_ELEVATION = 3.0e-9
BEAM_3_DELAY_RATE_AZIMUTH = 0.3e-11
BEAM_3_DELAY_RATE_ELEVATION = 0.3e-11
BEAM_4_ID = 4
BEAM_4_DELAY_AZIMUTH = 4.0e-9
BEAM_4_DELAY_ELEVATION = 4.0e-9
BEAM_4_DELAY_RATE_AZIMUTH = 0.4e-11
BEAM_4_DELAY_RATE_ELEVATION = 0.4e-11


@pytest.fixture(name="test_context")
def test_context_fixture() -> Iterator[MccsTangoTestHarnessContext]:
    """
    Return a test context in which Tango devices under test are running.

    :yields: a test context.
    """
    harness = MccsTangoTestHarness()
    harness.add_station_device("ci-1", 1, ["sb1-1", "sb1-2", "sb1-3", "sb1-4"])
    harness.add_station_device("ci-2", 1, ["sb1-5", "sb1-6", "sb1-7", "sb1-8"])
    harness.add_station_beam_device("ci-1", 1)
    harness.add_station_beam_device("ci-1", 2)
    harness.add_station_beam_device("ci-1", 1)
    harness.add_station_beam_device("ci-2", 2)
    with harness as context:
        yield context


# pylint: disable=too-few-public-methods
class TestMccsIntegration:
    """Integration test cases for the Mccs device classes."""

    @pytest.mark.xfail(reason="New subarray interface")
    def test_stationbeam_apply_pointing(
        self: TestMccsIntegration,
        test_context: MccsTangoTestHarnessContext,
    ) -> None:
        """
        Test that a MccsStationBeam can apply delays to associated MccsTiles.

        :param test_context: context in which the devices under test are running.
        """
        station_1 = test_context.get_station_device("ci-1")
        station_2 = test_context.get_station_device("ci-2")
        stationbeam_1 = test_context.get_station_beam_device("ci-1", 1)
        stationbeam_2 = test_context.get_station_beam_device("ci-1", 2)
        stationbeam_3 = test_context.get_station_beam_device("ci-2", 1)
        stationbeam_4 = test_context.get_station_beam_device("ci-2", 2)
        # mock_tile_1 = tango_harness.get_device("low-mccs/tile/0001")
        # mock_tile_2 = tango_harness.get_device("low-mccs/tile/0002")
        # mock_tile_3 = tango_harness.get_device("low-mccs/tile/0003")
        # mock_tile_4 = tango_harness.get_device("low-mccs/tile/0004")

        stationbeam_1.adminMode = AdminMode.ONLINE
        stationbeam_2.adminMode = AdminMode.ONLINE
        stationbeam_3.adminMode = AdminMode.ONLINE
        stationbeam_4.adminMode = AdminMode.ONLINE
        station_1.adminMode = AdminMode.ONLINE
        station_2.adminMode = AdminMode.ONLINE

        time.sleep(0.1)

        stationbeam_1.pointingDelay = [
            BEAM_1_DELAY_AZIMUTH,
            BEAM_1_DELAY_ELEVATION,
        ]
        stationbeam_2.pointingDelay = [
            BEAM_2_DELAY_AZIMUTH,
            BEAM_2_DELAY_ELEVATION,
        ]
        stationbeam_3.pointingDelay = [
            BEAM_3_DELAY_AZIMUTH,
            BEAM_3_DELAY_ELEVATION,
        ]
        stationbeam_4.pointingDelay = [
            BEAM_4_DELAY_AZIMUTH,
            BEAM_4_DELAY_ELEVATION,
        ]
        stationbeam_1.pointingDelayRate = [
            BEAM_1_DELAY_RATE_AZIMUTH,
            BEAM_1_DELAY_RATE_ELEVATION,
        ]
        stationbeam_2.pointingDelayRate = [
            BEAM_2_DELAY_RATE_AZIMUTH,
            BEAM_2_DELAY_RATE_ELEVATION,
        ]
        stationbeam_3.pointingDelayRate = [
            BEAM_3_DELAY_RATE_AZIMUTH,
            BEAM_3_DELAY_RATE_ELEVATION,
        ]
        stationbeam_4.pointingDelayRate = [
            BEAM_4_DELAY_RATE_AZIMUTH,
            BEAM_4_DELAY_RATE_ELEVATION,
        ]

        # states are ON because we haven't assigned a station
        assert stationbeam_1.state() == tango.DevState.ON
        assert stationbeam_2.state() == tango.DevState.ON
        assert stationbeam_3.state() == tango.DevState.ON
        assert stationbeam_4.state() == tango.DevState.ON

        # allocate does not currently include station_beams, so assigning manually
        stationbeam_1.stationTrl = "low-mccs/station/001"
        stationbeam_1.logicalBeamId = BEAM_1_ID
        stationbeam_2.stationTrl = "low-mccs/station/001"
        stationbeam_2.logicalBeamId = BEAM_2_ID
        stationbeam_3.stationTrl = "low-mccs/station/002"
        stationbeam_3.logicalBeamId = BEAM_3_ID
        stationbeam_4.stationTrl = "low-mccs/station/002"
        stationbeam_4.logicalBeamId = BEAM_4_ID

        time.sleep(0.1)

        assert stationbeam_1.state() == tango.DevState.ON
        assert stationbeam_2.state() == tango.DevState.ON
        assert stationbeam_3.state() == tango.DevState.ON
        assert stationbeam_4.state() == tango.DevState.ON

        # stations are on because we have mocked all their components to be on
        assert station_1.state() == tango.DevState.ON
        assert station_2.state() == tango.DevState.ON

        ([result_code], _) = stationbeam_1.ApplyPointing()
        assert result_code == ResultCode.QUEUED

        # we need to do this the long way because if Tango is numpy-enabled, then the
        # component manager will be called with an array not a list.
        # (args, kwargs) = mock_tile_1.SetPointingDelay.get_next_call()
        # assert not kwargs
        # assert len(args) == 1
        # assert list(args[0]) == [
        #     float(BEAM_1_ID),
        #     BEAM_1_DELAY_AZIMUTH,
        #     BEAM_1_DELAY_RATE_AZIMUTH,
        #     BEAM_1_DELAY_ELEVATION,
        #     BEAM_1_DELAY_RATE_ELEVATION,
        # ]

        # (args, kwargs) = mock_tile_2.SetPointingDelay.get_next_call()
        # assert not kwargs
        # assert len(args) == 1
        # assert list(args[0]) == [
        #     float(BEAM_1_ID),
        #     BEAM_1_DELAY_AZIMUTH,
        #     BEAM_1_DELAY_RATE_AZIMUTH,
        #     BEAM_1_DELAY_ELEVATION,
        #     BEAM_1_DELAY_RATE_ELEVATION,
        # ]
        # mock_tile_3.SetPointingDelay.assert_not_called()
        # mock_tile_4.SetPointingDelay.assert_not_called()

        ([result_code], _) = stationbeam_2.ApplyPointing()
        assert result_code == ResultCode.QUEUED

        # (args, kwargs) = mock_tile_1.SetPointingDelay.get_next_call()
        # assert not kwargs
        # assert len(args) == 1
        # assert list(args[0]) == [
        #     float(BEAM_2_ID),
        #     BEAM_2_DELAY_AZIMUTH,
        #     BEAM_2_DELAY_RATE_AZIMUTH,
        #     BEAM_2_DELAY_ELEVATION,
        #     BEAM_2_DELAY_RATE_ELEVATION,
        # ]

        # (args, kwargs) = mock_tile_2.SetPointingDelay.get_next_call()
        # assert not kwargs
        # assert len(args) == 1
        # assert list(args[0]) == [
        #     float(BEAM_2_ID),
        #     BEAM_2_DELAY_AZIMUTH,
        #     BEAM_2_DELAY_RATE_AZIMUTH,
        #     BEAM_2_DELAY_ELEVATION,
        #     BEAM_2_DELAY_RATE_ELEVATION,
        # ]
        # mock_tile_3.SetPointingDelay.assert_not_called()
        # mock_tile_4.SetPointingDelay.assert_not_called()

        ([result_code], _) = stationbeam_3.ApplyPointing()
        assert result_code == ResultCode.QUEUED

        # mock_tile_1.SetPointingDelay.assert_not_called()
        # mock_tile_2.SetPointingDelay.assert_not_called()

        # (args, kwargs) = mock_tile_3.SetPointingDelay.get_next_call()
        # assert not kwargs
        # assert len(args) == 1
        # assert list(args[0]) == [
        #     float(BEAM_3_ID),
        #     BEAM_3_DELAY_AZIMUTH,
        #     BEAM_3_DELAY_RATE_AZIMUTH,
        #     BEAM_3_DELAY_ELEVATION,
        #     BEAM_3_DELAY_RATE_ELEVATION,
        # ]
        # (args, kwargs) = mock_tile_4.SetPointingDelay.get_next_call()
        # assert not kwargs
        # assert len(args) == 1
        # assert list(args[0]) == [
        #     float(BEAM_3_ID),
        #     BEAM_3_DELAY_AZIMUTH,
        #     BEAM_3_DELAY_RATE_AZIMUTH,
        #     BEAM_3_DELAY_ELEVATION,
        #     BEAM_3_DELAY_RATE_ELEVATION,
        # ]

        ([result_code], _) = stationbeam_4.ApplyPointing()
        assert result_code == ResultCode.QUEUED

        # mock_tile_1.SetPointingDelay.assert_not_called()
        # mock_tile_2.SetPointingDelay.assert_not_called()
        # (args, kwargs) = mock_tile_3.SetPointingDelay.get_next_call()
        # assert not kwargs
        # assert len(args) == 1
        # assert list(args[0]) == [
        #     float(BEAM_4_ID),
        #     BEAM_4_DELAY_AZIMUTH,
        #     BEAM_4_DELAY_RATE_AZIMUTH,
        #     BEAM_4_DELAY_ELEVATION,
        #     BEAM_4_DELAY_RATE_ELEVATION,
        # ]
        # (args, kwargs) = mock_tile_4.SetPointingDelay.get_next_call()
        # assert not kwargs
        # assert len(args) == 1
        # assert list(args[0]) == [
        #     float(BEAM_4_ID),
        #     BEAM_4_DELAY_AZIMUTH,
        #     BEAM_4_DELAY_RATE_AZIMUTH,
        #     BEAM_4_DELAY_ELEVATION,
        #     BEAM_4_DELAY_RATE_ELEVATION,
        # ]
