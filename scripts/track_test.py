import json
import time

import tango
from astropy.time.core import Time

station = tango.DeviceProxy("low-mccs/station/ci-1")
station.adminMode = 0
tile = tango.DeviceProxy("low-mccs/tile/ci-1-10")
tile.adminMode = 0

time.sleep(1)

semi_static = {
    "station_config": {
        "refLongitude": 10,
        "refLatitude": 10,
        "refHeight": 10,
        "StationId": 1,
    },
    "antenna_config": {
        "antennaId": 1,
        "xDisplacement": 1,
        "yDisplacement": 2,
        "zDisplacement": 3,
    },
    "field_station_config": {},
}
str_semi_static = json.dumps(semi_static)
station.ConfigureSemiStatic(str_semi_static)


coords = {
    "pointing_type": "alt_az",
    "time_step": 1.0,
    "scan_time": 60,
    "station_beam_number": 1,
    "reference_time": "2025-05-09T23:00:00",
    "values": {"altitude": 45.0, "azimuth": 45.0},
}
str_coords = json.dumps(coords)

print("ABOUT TO START")

print(str_coords)
# pointing = {
#     "pointing_type": "ra_dec",
#     "reference_time": "2023-11-28T12:34:56",
#     "values": {
#         "right_ascension": 1.0,
#         "declination": -1.0,
#     },
# }

# print("pointing ")

# a, b = station.GetPointingDelays(json.dumps(pointing))
# print(a)
# print(b)

print("tracking")

([a], b) = station.TrackObject(str_coords)
print(a)
print(b)

time.sleep(2)

print(station.healthState)
print(station.isCalibrated)

time.sleep(10)

([a], b) = station.TrackObject(str_coords)
print(a)
print(b)

time.sleep(10)

station.StopTracking()


([a], b) = station.TrackObject(str_coords)
print(a)
print(b)
