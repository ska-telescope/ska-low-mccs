# How to use the MCCS k9s plugins #

k9s plugins allow for commands to be executed on the pressing of a hotkey when using k9s to view a cluster. The `plugin.yml` file provided here provides some tools that can be used in k9s to monitor and control MCCS devices. The file should be copied into `$XDG_CONFIG_HOME/k9s`. Typically, `$XDG_CONFIG_HOME` is set to be `$HOME/.config`. If the user wants to use other k9s plugins along with these, they can append those plugins to the bottom of the provided `plugin.yml` file.

The plugins will appear in the container view in k9s, which is accessed by highlighting a pod and pressing `Enter`. For hotkeys 1-6 there are some textual displays of key attributes of the device. **NB: you need to press 1-6 when first viewing a device to update these to the correct values.**. For some other hotkeys there are some other commands that can be issued to change the adminMode, turn on/off the device among others. Using these should update the displayed values for adminMode, state and healthState automatically, if they don't, press 1-6 to refresh a few seconds after execution.

The hotkeys used can be changed in the plugin.yml file, however if the hotkey clashes with another defined key, the plugin will not show up.
