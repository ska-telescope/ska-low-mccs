#  -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""A file to store health transition rules for station."""
from __future__ import annotations

from typing import Optional

from ska_control_model import HealthState
from ska_low_mccs_common.health import HealthRules

DEGRADED_STATES = frozenset({HealthState.DEGRADED, HealthState.FAILED, None})


class StationBeamHealthRules(HealthRules):
    """A class to handle transition rules for station."""

    # pylint: disable=arguments-differ
    def unknown_rule(  # type: ignore[override]
        self: StationBeamHealthRules,
        beam_locked: bool,
        station_fault: bool,
        station_health: HealthState,
        antenna_healths: dict[str, Optional[HealthState]],
    ) -> tuple[bool, str]:
        """
        Test whether UNKNOWN is valid for the station beam.

        :param beam_locked: if the beam is locked.
        :param station_fault: If the station is in fault state.
        :param station_health: Health of the station.
        :param antenna_healths: Health of the antennas.

        :return: If the station beam is in unknown state.
        """
        rule_matched = (
            HealthState.UNKNOWN in antenna_healths.values()
            or station_health == HealthState.UNKNOWN
        )
        if rule_matched:
            antenna_states = [
                trl
                for trl, health in antenna_healths.items()
                if health is None or health == HealthState.UNKNOWN
            ]
            report = (
                "Some devices are unknown: "
                f"MccsStation: {station_health.name}, "
                f"MccsAntenna: {antenna_states}"
            )
        else:
            report = ""
        return rule_matched, report

    # pylint: disable=arguments-differ
    def failed_rule(  # type: ignore[override]
        self: StationBeamHealthRules,
        beam_locked: bool,
        station_fault: bool,
        station_health: HealthState,
        antenna_healths: dict[str, Optional[HealthState]],
    ) -> tuple[bool, str]:
        """
        Test whether FAILED is valid for the station beam.

        :param beam_locked: if the beam is locked.
        :param station_fault: If the station is in fault state.
        :param station_health: Health of the station.
        :param antenna_healths: Health of the antennas.

        :return: If the station beam is failed or not.
        """
        if station_fault:
            return True, "The MccsStation has FAULTED"

        rule_matched = (
            self.get_fraction_in_states(antenna_healths, DEGRADED_STATES)
            >= self._thresholds["antenna_failed_threshold"]
            or station_health == HealthState.FAILED
        )
        if rule_matched:
            antenna_states = [
                trl
                for trl, health in antenna_healths.items()
                if health is None or health in DEGRADED_STATES
            ]
            report = (
                "Too many devices in a bad state: "
                f"MccsStation: {station_health.name}, "
                f"MccsAntenna: {antenna_states}"
            )
        else:
            report = ""
        return rule_matched, report

    # pylint: disable=arguments-differ
    def degraded_rule(  # type: ignore[override]
        self: StationBeamHealthRules,
        beam_locked: bool,
        station_fault: bool,
        station_health: HealthState,
        antenna_healths: dict[str, Optional[HealthState]],
    ) -> tuple[bool, str]:
        """
        Test whether DEGRADED is valid for the station.

        :param beam_locked: if the beam is locked.
        :param station_fault: If the station is in fault state.
        :param station_health: Health of the station.
        :param antenna_healths: Health of the antennas.

        :return: If the station beam is degraded or not.
        """
        if not beam_locked:
            return True, "The beam is not locked"
        rule_matched = (
            self.get_fraction_in_states(antenna_healths, DEGRADED_STATES)
            >= self._thresholds["antenna_degraded_threshold"]
            or station_health == HealthState.DEGRADED
        )
        if rule_matched:
            antenna_states = [
                trl
                for trl, health in antenna_healths.items()
                if health is None or health in DEGRADED_STATES
            ]
            report = (
                "Too many devices in a bad state: "
                f"MccsStation: {station_health.name}, "
                f"MccsAntenna: {antenna_states}"
            )
        else:
            report = ""
        return rule_matched, report

    # pylint: disable=arguments-differ
    def healthy_rule(  # type: ignore[override]
        self: StationBeamHealthRules,
        beam_locked: bool,
        station_fault: bool,
        station_health: HealthState,
        antenna_healths: dict[str, Optional[HealthState]],
    ) -> tuple[bool, str]:
        """
        Test whether OK is valid for the station.

        :param beam_locked: if the beam is locked.
        :param station_fault: If the station is in fault state.
        :param station_health: Health of the station.
        :param antenna_healths: Health of the antennas.

        :return: If the station beam is healthy or not.
        """
        return True, "Health is OK."

    @property
    def default_thresholds(self: HealthRules) -> dict[str, float]:
        """
        Get the default thresholds for this device.

        :return: the default thresholds
        """
        return {
            "antenna_degraded_threshold": 0.05,
            "antenna_failed_threshold": 0.2,
        }
