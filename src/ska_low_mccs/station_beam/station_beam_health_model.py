#  -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""An implementation of a health model for station beams."""
from __future__ import annotations

from typing import Callable, Optional

from ska_control_model import HealthState
from ska_low_mccs_common.health import BaseHealthModel

from .station_beam_health_rules import StationBeamHealthRules

__all__ = ["StationBeamHealthModel"]


class StationBeamHealthModel(BaseHealthModel):
    """A health model for station beams."""

    DEGRADED_CRITERIA = 0.05
    FAILED_CRITERIA = 0.2

    def __init__(
        self: StationBeamHealthModel,
        health_changed_callback: Callable,
        ignore_power_state: bool = False,
        thresholds: Optional[dict[str, float]] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param health_changed_callback: a callback to be called when the
            health of the station beam (as evaluated by this model)
            changes
        :param ignore_power_state: whether the health model should ignore
            the power state when computing health.
        :param thresholds: Thresholds for failed degraded states.
        """
        self._station_health: HealthState = HealthState.UNKNOWN
        self._station_fault: bool = False
        self._beam_locked = True
        self._antenna_healths: dict[str, Optional[HealthState]] = {}

        self._health_rules = StationBeamHealthRules(thresholds)

        super().__init__(health_changed_callback, ignore_power_state=ignore_power_state)

    def evaluate_health(
        self: StationBeamHealthModel,
    ) -> tuple[HealthState, str]:
        """
        Compute overall health of the station beam.

        The overall health is based on the whether the beam is locked or
        not.

        :return: an overall health of the station beam
        """
        beam_health, beam_report = super().evaluate_health()
        for health in [
            HealthState.FAILED,
            HealthState.UNKNOWN,
            HealthState.DEGRADED,
            HealthState.OK,
        ]:
            if health == beam_health:
                return beam_health, beam_report
            result, report = self._health_rules.rules[health](
                self._beam_locked,
                self._station_fault,
                self._station_health,
                self._antenna_healths,
            )
            if result:
                return health, report
        return HealthState.UNKNOWN, "No rules matched"

    def resources_changed(self: StationBeamHealthModel, antenna_trls: set[str]) -> None:
        """
        Change the resources used for the station beam.

        :param antenna_trls: TRLs of the antennas used for the beam
        """
        antenna_trl_list = sorted(list(antenna_trls))
        self._antenna_healths = {
            trl: self._antenna_healths.get(trl, HealthState.UNKNOWN)
            for trl in antenna_trl_list
        }

        self.update_health()

    def is_beam_locked_changed(
        self: StationBeamHealthModel, is_beam_locked: bool
    ) -> None:
        """
        Handle a change in whether the station beam is locked.

        This is a callback hook that is called when whether asubarray
        beam is locked changes.

        :param is_beam_locked: whether the station beam is locked
        """
        self._beam_locked = is_beam_locked
        self.update_health()

    def station_health_changed(
        self: StationBeamHealthModel, station_health: HealthState
    ) -> None:
        """
        Handle a change in the health of the station device that this beam controls.

        This is a callback hook that is called when whether a station
        beam detects that the health of its station has changed. This
        could occur because the station's health changes, or because the
        beam changes station.

        :param station_health: the health of the station that is
                controlled by this beam
        """
        self._station_health = station_health
        self.update_health()

    def antenna_health_changed(
        self: StationBeamHealthModel,
        trl: str,
        health_state: HealthState,
    ) -> None:
        """
        Handle change in station health.

        This is a callback hook, called by the component manager when
        the health of a station changes.
        :param trl: the TRL of the antenna whose health has changed
        :param health_state: the new health state of the antenna.
        """
        self._antenna_healths[trl] = health_state
        self.update_health()

    def station_fault_changed(
        self: StationBeamHealthModel,
        station_fault: bool,
    ) -> None:
        """
        Handle a change in the fault state of the station beam's station device.

        This is a callback hook that is called when whether a station
        beam detects that the fault state of its station has changed.
        This could occur because the station's health changes, or
        because the beam changes station.

        :param station_fault: the fault state of the station that is
            controlled by this beam.
        """
        self._station_fault = station_fault
        self.update_health()
