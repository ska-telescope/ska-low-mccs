#  -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""An implementation of a health model for subarray beams."""
from __future__ import annotations

from typing import Callable, Optional, Sequence

from ska_control_model import HealthState
from ska_low_mccs_common.health import BaseHealthModel

from .subarray_beam_health_rules import SubarrayBeamHealthRules

__all__ = ["SubarrayBeamHealthModel"]


class SubarrayBeamHealthModel(BaseHealthModel):
    """A health model for subarray beams."""

    def __init__(
        self: SubarrayBeamHealthModel,
        station_beam_trls: Sequence[str],
        component_state_callback: Callable[..., None],
        ignore_power_state: bool = False,
        thresholds: Optional[dict[str, float]] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param station_beam_trls: the TRLs of this subarray beam's station beams
        :param component_state_callback: callback to be called whenever
            there is a change to this component's state, including the health
            model's evaluated health state.
        :param ignore_power_state: whether the health model should ignore
            the power state when computing health.
        :param thresholds: the threshold parameters for the health rules
        """
        self._is_beam_locked = True
        self._station_beam_healths: dict[str, Optional[HealthState]] = {
            trl: HealthState.UNKNOWN for trl in station_beam_trls
        }
        self._health_rules = SubarrayBeamHealthRules(thresholds)
        super().__init__(
            component_state_callback, ignore_power_state=ignore_power_state
        )

    def evaluate_health(
        self: SubarrayBeamHealthModel,
    ) -> tuple[HealthState, str]:
        """
        Compute overall health of the subarray beam.

        The overall health is based on the whether the beam is locked or
        not.

        :return: an overall health of the subarray beam
        """
        beam_health, beam_report = super().evaluate_health()

        for health in [
            HealthState.FAILED,
            HealthState.UNKNOWN,
            HealthState.DEGRADED,
            HealthState.OK,
        ]:
            if health == beam_health:
                return beam_health, beam_report
            result, report = self._health_rules.rules[health](
                self._is_beam_locked,
                self._station_beam_healths,
            )
            if result:
                return health, report
        return HealthState.UNKNOWN, "No rules matched"

    def is_beam_locked_changed(
        self: SubarrayBeamHealthModel, is_beam_locked: bool
    ) -> None:
        """
        Handle a change in whether the subarray beam is locked.

        This is a callback hook called when the subarray beam lock
        changes.

        :param is_beam_locked: whether the subarray beam is locked
        """
        self._is_beam_locked = is_beam_locked
        self.update_health()

    def station_beam_health_changed(
        self: SubarrayBeamHealthModel,
        station_beam_trl: str,
        station_beam_health: Optional[HealthState],
    ) -> None:
        """
        Handle a change in station beam health.

        If the station beam TRL is not in the internal dictionary of station beam
        healths, it is added with the provided health

        :param station_beam_trl: the TRL of the station beam whose
            health has changed or whose health is to be added
        :param station_beam_health: the health state of the specified
            station beam, or None if the station beam's admin mode
            indicates that its health should not be rolled up.
        """
        if (
            self._station_beam_healths.get(station_beam_trl) != station_beam_health
            or station_beam_trl not in self._station_beam_healths
        ):
            self._station_beam_healths[station_beam_trl] = station_beam_health
            self.update_health()

    def update_station_beams(
        self: SubarrayBeamHealthModel,
        new_station_beams: set[str],
    ) -> None:
        """
        Add and remove station beams so that they are up to date.

        :param new_station_beams: the new set of station beams
        """
        # Beams that that been deallocated should be removed from
        # the health model
        lost_station_beams = {
            beam_trl
            for beam_trl in self._station_beam_healths.keys()
            if beam_trl not in new_station_beams
        }
        for beam_trl in lost_station_beams:
            self.remove_station_beam(beam_trl)

        # New beams that aren't being tracked need to be added to the
        # health model
        unregistered_station_beams = {
            beam_trl
            for beam_trl in new_station_beams
            if beam_trl not in self._station_beam_healths.keys()
        }
        for beam_trl in unregistered_station_beams:
            self.station_beam_health_changed(beam_trl, HealthState.UNKNOWN)

        # Beams that remain in the health model need to have their
        # health state updated
        kept_station_beams = {
            trl: new_health
            for (trl, new_health) in self._station_beam_healths.items()
            if trl in new_station_beams
        }
        for trl in kept_station_beams:
            self.station_beam_health_changed(trl, kept_station_beams[trl])

    def remove_station_beam(
        self: SubarrayBeamHealthModel,
        station_beam_trl: str,
    ) -> None:
        """
        Remove a station beam from the station beams used to compute health.

        :param station_beam_trl: the TRL of the station beam to remove
        """
        if station_beam_trl in self._station_beam_healths:
            self._station_beam_healths.pop(station_beam_trl)
            self.update_health()
