#  -*- coding: utf-8 -*
# pylint: disable=arguments-differ
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""A file to store health transition rules for subarray."""
from __future__ import annotations

from typing import Optional

from ska_control_model import HealthState
from ska_low_mccs_common.health import HealthRules

DEGRADED_STATES = frozenset({HealthState.DEGRADED, HealthState.FAILED, None})


class SubarrayBeamHealthRules(HealthRules):
    """A class to handle transition rules for subarray beam."""

    def unknown_rule(  # type: ignore[override]
        self: SubarrayBeamHealthRules,
        beam_locked: bool,
        station_beam_healths: dict[str, Optional[HealthState]],
    ) -> tuple[bool, str]:
        """
        Test whether UNKNOWN is valid for the subarray beam.

        :param beam_locked: whether the beam is locked
        :param station_beam_healths: health of the station beams.

        :return: If the subarray beam is in unknown state.
        """
        rule_matched = HealthState.UNKNOWN in station_beam_healths.values()
        if rule_matched:
            station_beam_states = [
                trl
                for trl, health in station_beam_healths.items()
                if health is None or health == HealthState.UNKNOWN
            ]
            report = (
                "Some devices are unknown: " f"MccsStationBeam: {station_beam_states}"
            )
        else:
            report = ""
        return rule_matched, report

    def failed_rule(  # type: ignore[override]
        self: SubarrayBeamHealthRules,
        beam_locked: bool,
        station_beam_healths: dict[str, Optional[HealthState]],
    ) -> tuple[bool, str]:
        """
        Test whether FAILED is valid for the subarray beam.

        :param beam_locked: whether the beam is locked
        :param station_beam_healths: health of the station beams.

        :return: If the subarray beam is failed or not.
        """
        rule_matched = (
            self.get_fraction_in_states(station_beam_healths, DEGRADED_STATES)
            >= self._thresholds["station_beam_failed_threshold"]
        )
        if rule_matched:
            station_beam_states = [
                trl
                for trl, health in station_beam_healths.items()
                if health is None or health in DEGRADED_STATES
            ]
            report = (
                "Too many devices in a bad state: "
                f"MccsStationBeam: {station_beam_states}"
            )
        else:
            report = ""
        return rule_matched, report

    def degraded_rule(  # type: ignore[override]
        self: SubarrayBeamHealthRules,
        beam_locked: bool,
        station_beam_healths: dict[str, Optional[HealthState]],
    ) -> tuple[bool, str]:
        """
        Test whether DEGRADED is valid for the subarray.

        :param beam_locked: whether the beam is locked
        :param station_beam_healths: health of the station beams.

        :return: If the subarray beam is degraded or not.
        """
        if not beam_locked:
            return True, "The beam is not locked"
        rule_matched = (
            self.get_fraction_in_states(station_beam_healths, DEGRADED_STATES)
            >= self._thresholds["station_beam_degraded_threshold"]
        )
        if rule_matched:
            station_beam_states = [
                trl
                for trl, health in station_beam_healths.items()
                if health is None or health in DEGRADED_STATES
            ]
            report = (
                "Too many devices in a bad state: "
                f"MccsStationBeam: {station_beam_states}"
            )
        else:
            report = ""
        return rule_matched, report

    def healthy_rule(  # type: ignore[override]
        self: SubarrayBeamHealthRules,
        beam_locked: bool,
        station_beam_healths: dict[str, Optional[HealthState]],
    ) -> tuple[bool, str]:
        """
        Test whether OK is valid for the subarray beam.

        :param beam_locked: whether the beam is locked
        :param station_beam_healths: health of the station beams.

        :return: If the subarray beam is healthy or not.
        """
        return True, "Health is OK."

    @property
    def default_thresholds(self: HealthRules) -> dict[str, float]:
        """
        Get the default thresholds for this device.

        :return: the default thresholds
        """
        return {
            "station_beam_degraded_threshold": 0.05,
            "station_beam_failed_threshold": 0.2,
        }
