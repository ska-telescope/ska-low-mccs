#  -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
# pylint: disable = too-many-lines
"""This module implements component management for subarray beams."""
from __future__ import annotations

import functools
import json
import logging
import threading
import time
from typing import Any, Callable, Iterable, Optional

from ska_control_model import CommunicationStatus, ObsState, ResultCode, TaskStatus
from ska_low_mccs_common import EventSerialiser
from ska_low_mccs_common.component import ObsDeviceComponentManager
from ska_tango_base.base import TaskCallbackType, check_communicating, check_on
from ska_tango_base.executor import TaskExecutorComponentManager

__all__ = ["SubarrayBeamComponentManager"]


class _StationBeamProxy(ObsDeviceComponentManager):
    """A proxy to a station beam device."""

    @check_communicating
    @check_on
    def configure(
        self: _StationBeamProxy, configuration: dict
    ) -> tuple[TaskStatus, str]:
        """
        Configure the station beam.

        :param configuration: the configuration to be applied to this
            station beam

        :return: A task status and response message.
        """
        assert self._proxy is not None
        configuration_str = json.dumps(configuration)
        (result_code, unique_id) = self._proxy.Configure(configuration_str)
        return (result_code, unique_id)

    @check_communicating
    @check_on
    def scan(
        self: _StationBeamProxy,
        scan_id: int,
        start_time: Optional[str],
        duration: float,
    ) -> tuple[TaskStatus, str]:
        """
        Start the subarray beam scanning.

        :param scan_id: the id of the scan
        :param start_time: the start time of the scan
        :param duration: the scan duration

        :return: A task status and response message.
        """
        assert self._proxy is not None
        scan_arg: dict[str, int | float | str] = {
            "scan_id": scan_id,
            "duration": duration,
        }
        if start_time is not None:
            scan_arg["start_time"] = start_time

        ([result_code], unique_id) = self._proxy.Scan(json.dumps(scan_arg))
        return (result_code, unique_id)

    @check_communicating
    @check_on
    def end(self: _StationBeamProxy) -> tuple[TaskStatus, str]:
        """
        Deconfigure the station beam.

        :return: A task status and response message.
        """
        assert self._proxy is not None
        (result_code, unique_id) = self._proxy.End()
        return (result_code, unique_id)

    @check_communicating
    @check_on
    def release_all_resources(self: _StationBeamProxy) -> tuple[TaskStatus, str]:
        """
        Release station beam resources.

        :return: A task status and response message.
        """
        assert self._proxy is not None
        (result_code, unique_id) = self._proxy.ReleaseAllResources()
        return (result_code, unique_id)

    @check_communicating
    @check_on
    def end_scan(self: _StationBeamProxy) -> tuple[TaskStatus, str]:
        """
        End station beam scan.

        :return: A task status and response message.
        """
        assert self._proxy is not None
        (result_code, unique_id) = self._proxy.EndScan()
        return (result_code, unique_id)

    @check_communicating
    @check_on
    def abort(
        self: _StationBeamProxy, task_callback: TaskCallbackType | None = None
    ) -> tuple[TaskStatus, str]:
        """
        Abort current scan or configuration/allocation command.

        :param task_callback: Update task state, defaults to None

        :return: A task status and response message.
        """
        assert self._proxy is not None
        (result_code, unique_id) = self._proxy.Abort()
        return (result_code, unique_id)

    @check_communicating
    @check_on
    def obsreset(self: _StationBeamProxy) -> tuple[TaskStatus, str]:
        """
        Restart beam to IDLE form FAULT/ABORTED.

        :return: A task status and response message.
        """
        assert self._proxy is not None
        (result_code, unique_id) = self._proxy.ObsReset()
        return (result_code, unique_id)

    @check_communicating
    @check_on
    def restart(self: _StationBeamProxy) -> tuple[TaskStatus, str]:
        """
        Restart beam to EMPTY from ABORTED or FAULT.

        :return: A task status and response message.
        """
        assert self._proxy is not None
        (result_code, unique_id) = self._proxy.Restart()
        return (result_code, unique_id)


# pylint: disable=too-many-instance-attributes,too-many-public-methods
class SubarrayBeamComponentManager(TaskExecutorComponentManager):
    """A component manager for a subarray beam."""

    # messages for long running command task callback
    _task_message = {
        TaskStatus.COMPLETED: "completed OK.",
        TaskStatus.ABORTED: "been aborted.",
        TaskStatus.FAILED: "timed out.",
    }

    _task_to_result = {
        TaskStatus.COMPLETED: ResultCode.OK,
        TaskStatus.ABORTED: ResultCode.ABORTED,
        TaskStatus.FAILED: ResultCode.FAILED,
    }

    def __init__(
        self: SubarrayBeamComponentManager,
        logger: logging.Logger,
        obs_command_timeout: int,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
        event_serialiser: Optional[EventSerialiser] = None,
        _station_beams: Optional[dict[str, Any]] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param logger: the logger to be used by this object.
        :param communication_state_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param obs_command_timeout: the default timeout for obs
            commands in seconds.
        :param component_state_callback: callback to be called
            when the component state changes
        :param event_serialiser: the event serialiser to be used by this object.
        :param _station_beams: a optional injected station beams for testing
            purposes only. defaults to None
        """
        self._event_serialiser = event_serialiser
        self._obs_command_timeout = obs_command_timeout
        self._is_configured_changed_callback: Optional[
            Callable[..., None]
        ] = component_state_callback
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=None,  # PowerState.UNKNOWN,
            fault=None,
            beam_locked=None,
        )
        self._abort_complete = threading.Event()
        self._logger = logger
        self._station_beams: dict[str, _StationBeamProxy] = {}
        # for test only
        if isinstance(_station_beams, dict):
            self._station_beams = _station_beams
        elif isinstance(_station_beams, list):
            for beam_trl in _station_beams:
                self._station_beams[beam_trl] = _StationBeamProxy(
                    beam_trl,
                    self.logger,
                    functools.partial(
                        self._device_communication_state_changed, beam_trl
                    ),
                    functools.partial(self._component_state_callback, trl=beam_trl),
                    event_serialiser=self._event_serialiser,
                )

        self._is_configured = False
        self._configuring_resources: set[str] = set()  # these resources should go
        self._desired_state = ObsState.READY  # to this end state
        self._device_obs_states: dict[str, Optional[ObsState]] = {}
        self._device_communication_states: dict[str, CommunicationStatus] = {}

        self._subarray_id = 0
        self._subarray_beam_id = 0
        self._station_ids: list[int] = []
        self._logical_beam_id = 0
        self._update_rate = 0.0
        self._is_beam_locked = True

        self._channels: list[list[int]] = []
        self._first_channel = 0
        self._number_of_channels = 0
        self._desired_pointing: list[float] = []
        self._antenna_weights: list[float] = []
        self._phase_centre: list[float] = []
        self._scan_id: int = 0
        self._scan_start_time: str | None = None
        self._scan_duration = 0.0

        self._communication_state_callback: Callable[
            ..., None
        ] = communication_state_callback
        self._component_state_callback: Callable[..., None] = component_state_callback

    def start_communicating(self: SubarrayBeamComponentManager) -> None:
        """Establish communication with the subarray beam."""
        if self.communication_state == CommunicationStatus.ESTABLISHED:
            return
        if self.communication_state == CommunicationStatus.DISABLED:
            self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
        else:
            self._update_communication_state(CommunicationStatus.ESTABLISHED)

        # self._is_configured_changed_callback = self._component_state_callback
        self._update_communication_state(CommunicationStatus.ESTABLISHED)
        for trl, station_beam_proxy in self._station_beams.items():
            self.logger.debug(f"Start communicating with {trl}")
            station_beam_proxy.start_communicating()

    def stop_communicating(self: SubarrayBeamComponentManager) -> None:
        """Break off communication with the subarray beam."""
        self.logger.debug("Communication stopped")
        # self._is_configured_changed_callback = None
        for trl, station_beam in self._station_beams.items():
            self.logger.debug(f"Stop communicating with {trl}")
            station_beam.stop_communicating()

        self._update_communication_state(CommunicationStatus.DISABLED)
        self._update_component_state(power=None, obsfault=None)

    @property
    def max_executing_tasks(self) -> int:
        """
        Get the max number of tasks that can be executing at once.

        :return: max number of simultaneously executing tasks.
        """
        return 2

    @property
    @check_communicating
    def subarray_id(self: SubarrayBeamComponentManager) -> int:
        """
        Return the subarray id.

        :return: the subarray id
        """
        return self._subarray_id

    @property
    @check_communicating
    def subarray_beam_id(self: SubarrayBeamComponentManager) -> int:
        """
        Return the subarray beam id.

        :return: the subarray beam id
        """
        return self._subarray_beam_id

    @property
    @check_communicating
    def station_beam_ids(self: SubarrayBeamComponentManager) -> list[str]:
        """
        Return the station beam ids.

        :return: list of beam IDs
        """
        beam_ids = []
        for trl in self._station_beams:
            beam_ids.append(trl.rsplit("/", 1)[-1])
        return beam_ids

    @property
    @check_communicating
    def station_ids(self: SubarrayBeamComponentManager) -> list[int]:
        """
        Return the station ids.

        :return: the station ids
        """
        return list(self._station_ids)

    @station_ids.setter
    def station_ids(self: SubarrayBeamComponentManager, value: list[int]) -> None:
        """
        Set the station ids.

        :param value: the new station ids
        """
        self._station_ids = value

    @property
    @check_communicating
    def logical_beam_id(self: SubarrayBeamComponentManager) -> int:
        """
        Return the logical beam id.

        :return: the logical beam id
        """
        return self._logical_beam_id

    @logical_beam_id.setter
    def logical_beam_id(self: SubarrayBeamComponentManager, value: int) -> None:
        """
        Set the logical beam id.

        :param value: the new logical beam id
        """
        self._logical_beam_id = value

    @property
    @check_communicating
    def update_rate(self: SubarrayBeamComponentManager) -> float:
        """
        Return the update rate.

        :return: the update rate
        """
        return self._update_rate

    @property
    @check_communicating
    def is_beam_locked(self: SubarrayBeamComponentManager) -> bool:
        """
        Return whether the beam is locked.

        :return: whether the beam is locked
        """
        return self._is_beam_locked

    @is_beam_locked.setter
    def is_beam_locked(self: SubarrayBeamComponentManager, value: bool) -> None:
        """
        Set whether the beam is locked.

        :param value: new value for whether the beam is locked
        """
        if self._is_beam_locked != value:
            self._is_beam_locked = value
            if self._is_configured_changed_callback is not None:
                self._is_configured_changed_callback(beam_locked=value)

    @property
    @check_communicating
    def channels(self: SubarrayBeamComponentManager) -> list[list[int]]:
        """
        Return the ids of the channels configured for this subarray beam.

        :return: the ids of the channels configured for this subarray
            beam.
        """
        return [list(i) for i in self._channels]  # deep copy

    @property
    @check_communicating
    def antenna_weights(self: SubarrayBeamComponentManager) -> list[float]:
        """
        Return the antenna weights.

        :return: the antenna weights
        """
        return list(self._antenna_weights)

    @property
    @check_communicating
    def desired_pointing(self: SubarrayBeamComponentManager) -> list[float]:
        """
        Return the desired pointing.

        :return: the desired pointing
        """
        return self._desired_pointing

    @desired_pointing.setter
    def desired_pointing(
        self: SubarrayBeamComponentManager, value: list[float]
    ) -> None:
        """
        Set the desired pointing.

        :param value: the new desired pointing
        """
        self._desired_pointing = value

    @property
    @check_communicating
    def phase_centre(self: SubarrayBeamComponentManager) -> list[float]:
        """
        Return the phase centre.

        :return: the phase centre
        """
        return self._phase_centre

    @property
    @check_communicating
    def first_channel(self: SubarrayBeamComponentManager) -> int:
        """
        Return the first assigned subarray channel.

        :return: the first assigned subarray channel.
        """
        return self._first_channel

    @property
    @check_communicating
    def number_of_channels(self: SubarrayBeamComponentManager) -> int:
        """
        Return the number of allocated subarray channels.

        :return: the number of channels
        """
        return self._number_of_channels

    def _check_aborted(
        self: SubarrayBeamComponentManager,
        task_abort_event: Optional[threading.Event],
        task_callback: Optional[Callable],
        method_name: str,
    ) -> bool:
        if task_abort_event is None or task_callback is None:
            self.logger.warning(f"Cannot check {method_name} for abort status")
            return False

        if task_abort_event.is_set():
            self.logger.info(f"{method_name} has been aborted")
            task_callback(
                status=TaskStatus.ABORTED,
                result=(ResultCode.ABORTED, f"{method_name} aborted"),
            )
            return True
        return False

    def assign_resources(
        self: SubarrayBeamComponentManager,
        task_callback: Optional[Callable] = None,
        *,
        interface: Optional[str] = None,
        subarray_id: int,
        subarray_beam_id: Optional[int] = 0,
        apertures: Iterable,
        first_subarray_channel: Optional[int] = 0,
        number_of_channels: int,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the assign_resources slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None

        :param interface: the schema version this is running against.
        :param subarray_id: ID of the subarray which controls this beam
        :param subarray_beam_id: Id of (this) beam
        :param apertures: Allocated apertures
        :param first_subarray_channel: First channel (default 0)
        :param number_of_channels: Number of allocated channels
        :return: A tuple containing a task status and a unique id string
            to identify the command
        """
        if self.subarray_beam_id not in (subarray_beam_id, 0):
            self.logger.error(
                f"Received an allocation for wrong subarray beam {subarray_beam_id}"
            )

        return self.submit_task(
            self._assign_resources,
            args=[
                subarray_id,
                subarray_beam_id,
                first_subarray_channel,
                number_of_channels,
                apertures,
            ],
            task_callback=task_callback,
        )

    # pylint: disable=too-many-arguments
    def _assign_resources(
        self: SubarrayBeamComponentManager,
        subarray_id: int,
        subarray_beam_id: int,
        first_subarray_channel: int,
        number_of_channels: int,
        apertures: dict,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Assign resources to device.

        :param subarray_id: ID of the subarray to which the beam belongs
        :param subarray_beam_id: Id of (this) beam
        :param first_subarray_channel: First logical channel assigned to subarray
        :param number_of_channels: Number of channels assigned to beam
        :param apertures: list of dictionaries iwith each entry containing
            * station_id: (int) in range 1-512
            * aperture_id: (str) with format APx.y; x must match station_ID
            * station_beam_trl: (str)
            * channel_blocks: Allocated channel blocks for this station
            * hardware_beam: Allocated hardware beam for this station
        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Task abort, defaults to None
        """
        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_assign_resources"):
            return

        self.logger.info(
            f"Configuring subarray beam {subarray_id}:{self.subarray_beam_id}"
        )
        self.logger.debug(
            f"Assigning channels {first_subarray_channel}:{number_of_channels} "
            f"Apertures {apertures}"
        )
        self._first_channel = first_subarray_channel
        self._number_of_channels = number_of_channels

        self._desired_state = ObsState.IDLE
        self._configuring_resources.clear()
        trls_to_add: list[str] = []
        for aperture in apertures:
            aperture_id = aperture.get("aperture_id")
            beam_trl = aperture.get("station_beam_trl")
            self.logger.info(f"Aperture {aperture_id} beam {beam_trl}")
            self._device_obs_states[beam_trl] = ObsState.EMPTY
            self._configuring_resources.add(beam_trl)
            trls_to_add.append(beam_trl)

        if self._check_aborted(task_abort_event, task_callback, "_assign_resources"):
            return

        if trls_to_add:
            # self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
            for beam_trl in trls_to_add:
                self._device_communication_states[
                    beam_trl
                ] = CommunicationStatus.DISABLED
            self._evaluate_communication_state()
            for beam_trl in trls_to_add:
                self._station_beams[beam_trl] = _StationBeamProxy(
                    beam_trl,
                    self.logger,
                    functools.partial(
                        self._device_communication_state_changed, beam_trl
                    ),
                    functools.partial(self._component_state_callback, trl=beam_trl),
                    event_serialiser=self._event_serialiser,
                )
            self.logger.debug("Assigned station beam proxies")
            self.start_communicating()
            for beam_trl in trls_to_add:
                self._station_beams[beam_trl].start_communicating()
                self.logger.debug(
                    f"AssignResources: start communicating with {beam_trl}"
                )
        # wait for all subdevices to reach ObsState.IDLE
        task_result = self._wait_for_obs_state(
            self._obs_command_timeout, task_abort_event
        )
        if self._check_aborted(task_abort_event, task_callback, "_assign_resources"):
            return
        # task_result = TaskStatus.COMPLETED
        self._component_state_callback(
            resources_changed=set(self._station_beams.keys()),
        )
        if task_result == TaskStatus.FAILED:
            self._component_state_callback(obsfault=True)

        self.logger.debug("Subarray Beam Assign " + self._task_message[task_result])
        if task_callback:
            task_callback(
                status=task_result,
                result="Assign resources " + self._task_message[task_result],
            )

    def configure(
        self: SubarrayBeamComponentManager,
        task_callback: Optional[Callable] = None,
        *,
        interface: Optional[str] = None,
        subarray_id: int,
        subarray_beam_id: int,
        update_rate: Optional[float] = 0.0,
        logical_bands: list[dict],
        apertures: list[dict],
        sky_coordinates: Optional[dict[str, Any]] = None,
        field: Optional[dict[str, Any]] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the configure slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None

        :param interface: the schema version this is running against.
        :param subarray_id: ID of the subarray to which this
        :param subarray_beam_id: ID of (this) subarray beam
        :param update_rate: Update rate for pointing in seconds
        :param logical_bands: list of spectral bands to observe
        :param apertures: list of aperture definition
        :param sky_coordinates: Direction to point
        :param field: Direction to point
        :return: A tuple containing a task status and a unique id string
            to identify the command
        """
        if sky_coordinates:
            field = {
                key: val
                for key, val in sky_coordinates.items()
                if key in ["timestamp", "reference_frame", "target_name"]
            }
            field["attrs"] = {
                key: val
                for key, val in sky_coordinates.items()
                if key in ["c1", "c1_rate", "c2", "c2_rate"]
            }
            field.setdefault("target_name", "No name provided")
        return self.submit_task(
            self._configure,
            args=[
                update_rate,
                logical_bands,
                apertures,
                field,
                subarray_beam_id,
                subarray_id,
            ],
            task_callback=task_callback,
        )

    # pylint: disable=too-many-arguments,too-many-locals,too-many-branches
    def _configure(
        self,
        update_rate: float,
        logical_bands: list[dict],
        apertures: list[dict],
        field: dict[str, Any],
        subarray_beam_id: int,
        subarray_id: int,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Implement :py:meth:`.MccsSubarrayBeam.Configure` command.

        :param update_rate: Update rate for pointing in seconds
        :param logical_bands: list of spectral bands to observe
        :param apertures: list of aperture definition
        :param field: Direction to point
        :param subarray_id: ID of the subarray to which this
        :param subarray_beam_id: ID of (this) subarray beam
        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Task abort, defaults to None
        """
        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_configure"):
            return

        has_configuration_changed = False
        self._subarray_beam_id = subarray_beam_id
        self._subarray_id = subarray_id
        self._channels = []
        for band in logical_bands:
            self._channels.append(
                [
                    band["start_channel"],
                    band["number_of_channels"],
                    0,
                ]
            )
        self._update_rate = update_rate
        attributes = field["attrs"]
        self._desired_pointing = [
            attributes["c1"],
            attributes.get("c1_rate", 0.0),
            attributes["c2"],
            attributes.get("c2_rate", 0.0),
        ]

        result_code = TaskStatus.COMPLETED
        configuration: dict[str, dict] = {}
        for aperture in apertures:
            trl = aperture["station_beam_trl"]
            if trl in self._station_beams:
                # TODO MCCS-1571: remove station_beam configuration in
                #   Subarray and perform it here.
                configuration[trl] = {
                    "update_rate": update_rate,
                    "logical_bands": logical_bands,
                    "weighting_key_ref": aperture.get("weighting_key_ref", "uniform"),
                    "field": field,
                }
                has_configuration_changed = True
            else:
                self.logger.error(f"Station beam {trl} not allocated")

        if self._check_aborted(task_abort_event, task_callback, "_configure"):
            return

        # configure station beams
        self.logger.debug(f"Configuration collected: {configuration}")
        self._configuring_resources.clear()
        self._desired_state = ObsState.READY
        for (
            station_beam_trl,
            aperture,
        ) in configuration.items():
            station_beam_proxy = self._station_beams[station_beam_trl]
            proxy_result_code = station_beam_proxy.configure(aperture)
            if proxy_result_code in (TaskStatus.FAILED, TaskStatus.REJECTED):
                result_code = TaskStatus.FAILED
                self.logger.debug(f"Failed configuring {station_beam_trl}")
            else:
                self._configuring_resources.add(station_beam_trl)
                self.logger.debug(f"Configuring {station_beam_trl}")
        if result_code == TaskStatus.FAILED:
            self.logger.debug("Failed configuring station beams")
            if task_callback is not None:
                task_callback(status=TaskStatus.FAILED, result="Configure has failed.")
            return

        # wait for the configuration to complete
        result_code = self._wait_for_obs_state(
            self._obs_command_timeout, task_abort_event
        )
        if self._check_aborted(task_abort_event, task_callback, "_configure"):
            return
        self.logger.debug(f"Waiting for all beams to configure: {result_code.name}")

        # Everything gone fine, change state
        if result_code == TaskStatus.COMPLETED and has_configuration_changed:
            if not self._is_configured:
                self._component_state_callback(configured_changed=True)
            self._is_configured = True
        elif result_code == TaskStatus.FAILED:
            self._component_state_callback(obsfault=True)

        self.logger.debug("Configure command " + self._task_message[result_code])
        if task_callback:
            task_callback(
                status=result_code,
                result="Configure command " + self._task_message[result_code],
            )
        return

    def release_all_resources(
        self, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Submit the ReleaseAllResources slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :return: A tuple containing a task status and a unique id string
            to identify the command
        """
        return self.submit_task(
            self._release_all_resources, args=[], task_callback=task_callback
        )

    def _release_all_resources(
        self,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Implement :py:meth:`.MccsSubarrayBeam.ReleaseAllResources` command.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Task abort, defaults to None
        """
        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if self._check_aborted(
            task_abort_event, task_callback, "_release_all_resoruces"
        ):
            return

        self._desired_state = ObsState.EMPTY
        self._configuring_resources.clear()
        # Release resources in station beams
        # Resources in station are released by the subarray
        self.logger.debug("Deallocating resources")
        for trl, proxy in self._station_beams.items():
            self._configuring_resources.add(trl)
            proxy.release_all_resources()

        if self._check_aborted(
            task_abort_event, task_callback, "_release_all_resoruces"
        ):
            return

        # Wait for beams to go to EMPTY
        task_status = self._wait_for_obs_state(
            self._obs_command_timeout, task_abort_event
        )
        if self._check_aborted(
            task_abort_event, task_callback, "_release_all_resoruces"
        ):
            return
        if self._station_beams:
            self._delete_proxies()
            self._device_obs_states.clear()
            self._first_channel = 0
            self._number_of_channels = 0
            self._component_state_callback(resources_changed=self._station_beams.keys())

        if task_status == TaskStatus.FAILED:
            self._component_state_callback(obsfault=True)
        if task_callback:
            task_callback(
                status=task_status,
                result="ReleaseAllResources has " + self._task_message[task_status],
            )

    def release_resources(
        self, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Submit the ReleaseResources slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :return: A tuple containing a task status and a unique id string
            to identify the command
        """
        return self.submit_task(
            self._release_resources, args=[], task_callback=task_callback
        )

    def _release_resources(
        self,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Implement :py:meth:`.MccsSubarrayBeam.ReleaseResources` command.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Task abort, defaults to None
        """
        self.logger.error("ReleaseResources is not implemented")

        if self._check_aborted(task_abort_event, task_callback, "_release_resoruces"):
            return
        self._component_state_callback(obsfault=True)
        if task_callback:
            task_callback(
                status=TaskStatus.REJECTED,
                result="ReleaseResources command not implemented",
            )

    def end(self, task_callback: Optional[Callable] = None) -> tuple[TaskStatus, str]:
        """
        Submit the End slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :return: A tuple containing a task status and a unique id string
            to identify the command
        """
        return self.submit_task(self._end, args=[], task_callback=task_callback)

    def _end(
        self,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Implement :py:meth:`.MccsSubarrayBeam.End` command.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Task abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_end"):
            return

        # Stuff goes here. This should tell the station beam device to
        # deconfigure.
        self._desired_state = ObsState.IDLE
        task_status = TaskStatus.COMPLETED
        for trl, station_beam_proxy in self._station_beams.items():
            self.logger.debug(f"Deconfiguring station beam {trl}")
            self._configuring_resources.add(trl)
            proxy_result_code = station_beam_proxy.end()
            if proxy_result_code in (TaskStatus.FAILED, TaskStatus.REJECTED):
                task_status = TaskStatus.FAILED

        if task_status == TaskStatus.COMPLETED:
            task_status = self._wait_for_obs_state(
                self._obs_command_timeout, task_abort_event
            )
        if self._check_aborted(task_abort_event, task_callback, "_end"):
            return

        self._component_state_callback(configured_changed=False)
        self._is_configured = False
        if task_callback is not None:
            task_callback(
                status=task_status,
                result="End has " + self._task_message[task_status],
            )

    def _abort_callback(
        self: SubarrayBeamComponentManager,
        *,
        status: Optional[TaskStatus] = None,
        exception: Optional[Exception] = None,
        **kwargs: Any,
    ) -> None:
        if exception is not None:
            self.logger.error(f"abort_commands raised exception: {repr(exception)}")

        if status == TaskStatus.COMPLETED:
            self.logger.debug("abort_commands has finished, setting flag")
            self._abort_complete.set()

    @check_communicating
    def abort(  # type: ignore[override]
        self: SubarrayBeamComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Abort the observation.

        :param task_callback: callback to be called when the status of
            the command changes
        :return: A task status and response message.
        """
        # We have to spin up a separate thread such that we skip the queue which
        # we would append to if we used submit_task
        threading.Thread(
            target=self._abort, kwargs={"task_callback": task_callback}
        ).start()
        return (TaskStatus.IN_PROGRESS, "Abort has started")

    @check_communicating
    def abort_device(
        self: SubarrayBeamComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Abort only this device, for use in RestartSubarray().

        :param task_callback: callback to be called when the status of
            the command changes
        :return: A task status and response message.
        """
        # We have to spin up a separate thread such that we skip the queue which
        # we would append to if we used submit_task
        threading.Thread(
            target=self._abort,
            kwargs={"cascade": False, "task_callback": task_callback},
        ).start()
        return (TaskStatus.IN_PROGRESS, "AbortDevice has started")

    def _abort(
        self,
        cascade: bool = True,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Implement :py:meth:`.MccsSubarrayBeam.Abort` command.

        :param cascade: whether or not to abort sub-devices.
        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Task abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        task_status = TaskStatus.COMPLETED

        # This is sent in a thread, we record progress using the _abort_callback, which
        # will set an Event once the task is complete.
        self.abort_commands(task_callback=self._abort_callback)  # type:ignore

        # Wait until the timeout for the Event to be set by abort_commands
        timeout = 10.0
        if self._abort_complete.wait(timeout=timeout):
            self._abort_complete.clear()
        else:
            self.logger.error(f"Abort timed out in {timeout} seconds.")
            task_status = TaskStatus.FAILED
            # Reinitialize our event. The old one could get set at any point now.
            self._abort_complete = threading.Event()

        if cascade:
            if task_status == TaskStatus.COMPLETED:
                self._desired_state = ObsState.ABORTED
                self._configuring_resources.clear()
                for trl, beam_proxy in self._station_beams.items():
                    self._configuring_resources.add(trl)
                    proxy_result_code, response = beam_proxy.abort()
                    self.logger.debug(
                        f"Aborting {trl} has {ResultCode(proxy_result_code).name}"
                    )
                    if proxy_result_code in (
                        ResultCode.FAILED,
                        ResultCode.REJECTED,
                        ResultCode.NOT_ALLOWED,
                    ):
                        task_status = TaskStatus.FAILED
                        break

            # Wait until subdevices reach ObsState.ABORTED
            if task_status == TaskStatus.COMPLETED:
                task_status = self._wait_for_obs_state(timeout=10.0)

        if task_callback:
            task_callback(
                status=task_status,
                result=(
                    self._task_to_result[task_status],
                    "Abort command " + self._task_message[task_status],
                ),
            )

    def obsreset(
        self, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Submit the ObsReset command.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :return: A tuple containing a task status and a unique id string
            to identify the command
        """
        return self.submit_task(self._obsreset, args=[], task_callback=task_callback)

    def _obsreset(
        self,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Implement :py:meth:`.MccsSubarrayBeam.ObsReset` command.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Task abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_obsreset"):
            return

        result_code = TaskStatus.COMPLETED

        for beam_proxy in self._station_beams.values():
            proxy_result_code, response = beam_proxy.obsreset()
            if proxy_result_code in (TaskStatus.FAILED, TaskStatus.REJECTED):
                result_code = TaskStatus.FAILED
        if task_callback:
            task_callback(
                status=result_code,
                result="ObsReset command " + self._task_message[result_code],
            )

    def restart(
        self, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Submit the Restart command.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :return: A tuple containing a task status and a unique id string
            to identify the command
        """
        return self.submit_task(self._restart, args=[], task_callback=task_callback)

    def _restart(
        self,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Implement :py:meth:`.MccsSubarrayBeam.Restart` command.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Task abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_restart"):
            return

        result_code = TaskStatus.COMPLETED

        if self._station_beams:
            self._delete_proxies()
            self._device_obs_states.clear()
            self._first_channel = 0
            self._number_of_channels = 0

        self._is_configured = False

        # for beam_proxy in self._station_beams.values():
        #     proxy_result_code, response = beam_proxy.restart()
        #     if proxy_result_code in (TaskStatus.FAILED, TaskStatus.REJECTED):
        #         result_code = TaskStatus.FAILED
        if task_callback:
            task_callback(
                status=result_code,
                result=(
                    self._task_to_result[result_code],
                    "Restart command " + self._task_message[result_code],
                ),
            )

    def scan(
        self: SubarrayBeamComponentManager,
        task_callback: Optional[Callable] = None,
        *,
        scan_id: int,
        start_time: Optional[str] = None,
        duration: Optional[float] = 0.0,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the Scan slow task.

        This method returns immediately after it is submitted for
        execution.

        :param scan_id: The ID for this scan
        :param start_time: UTC time for begin of scan, None for immediate start
        :param duration: Scan duration in seconds. 0.0 or omitted means forever
        :param task_callback: Update task state, defaults to None
        :return: Task status and response message
        """
        return self.submit_task(
            self._scan,
            args=[scan_id, start_time, duration],
            task_callback=task_callback,
        )

    def _scan(
        self: SubarrayBeamComponentManager,
        scan_id: int,
        start_time: Optional[str],
        duration: float,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Execute the Scan slow task.

        :param scan_id: The ID for this scan
        :param start_time: UTC time for begin of scan, None for immediate start
        :param duration: Scan duration in seconds. 0.0 or omitted means forever
        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_scan"):
            return

        # TODO Fill this with meaningful actions, like commanding the StationBeam
        #

        self._scan_id = scan_id
        self._scan_start_time = start_time
        self._scan_duration = duration

        self._configuring_resources.clear()
        self._desired_state = ObsState.SCANNING

        task_status = TaskStatus.COMPLETED
        for trl, station_beam_proxy in self._station_beams.items():
            proxy_result_code = station_beam_proxy.scan(scan_id, start_time, duration)
            if proxy_result_code == TaskStatus.FAILED:
                task_status = TaskStatus.FAILED
            else:
                self._configuring_resources.add(trl)

        if self._check_aborted(task_abort_event, task_callback, "_scan"):
            return

        # wait for the configuration to complete
        if task_status == TaskStatus.COMPLETED:
            task_status = self._wait_for_obs_state(
                self._obs_command_timeout, task_abort_event
            )

        if self._check_aborted(task_abort_event, task_callback, "_scan"):
            return

        # all station beams in SCANNING
        if task_status == TaskStatus.COMPLETED:
            self._component_state_callback(scanning_changed=True)

        if task_callback is not None:
            task_callback(
                status=task_status,
                result="Scan has " + self._task_message[task_status],
            )

    def end_scan(
        self: SubarrayBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit the EndScan slow task.

        This method returns immediately after it is submitted for
        execution.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        :return: Task status and response message
        """
        return self.submit_task(
            self._end_scan,
            args=[],
            task_callback=task_callback,
        )

    def _end_scan(
        self: SubarrayBeamComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Execute the EndScan slow task.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)

        if self._check_aborted(task_abort_event, task_callback, "_end_scan"):
            return

        # commanding the StationBeam
        self._desired_state = ObsState.READY
        task_status = TaskStatus.COMPLETED
        for trl, station_beam_proxy in self._station_beams.items():
            self._configuring_resources.add(trl)
            proxy_result_code = station_beam_proxy.end_scan()
            if proxy_result_code == TaskStatus.FAILED:
                task_status = TaskStatus.FAILED

        self._wait_for_obs_state(self._obs_command_timeout, task_abort_event)
        if self._check_aborted(task_abort_event, task_callback, "_end_scan"):
            return

        if task_status == TaskStatus.COMPLETED:
            self._component_state_callback(scanning_changed=False)

        if task_callback is not None:
            task_callback(
                status=task_status,
                result="EndScan has " + self._task_message[task_status],
            )

    def _device_communication_state_changed(
        self: SubarrayBeamComponentManager,
        trl: str,
        communication_state: CommunicationStatus,
    ) -> None:
        if trl not in self._device_communication_states:
            self.logger.warning(
                "Received a communication status changed event for a device not "
                "managed by this subarray beam."
                "The event will be discarded."
            )
            return

        self._device_communication_states[trl] = communication_state
        if self.communication_state == CommunicationStatus.DISABLED:
            return

        self._evaluate_communication_state()

    def _evaluate_communication_state(self: SubarrayBeamComponentManager) -> None:
        """Evaluate the communication state for this Subarray Beam."""
        self.logger.debug("Evaluating communication state")
        if CommunicationStatus.DISABLED in self._device_communication_states:
            self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
        elif CommunicationStatus.NOT_ESTABLISHED in self._device_communication_states:
            self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
        else:
            self._update_communication_state(CommunicationStatus.ESTABLISHED)

    def _wait_for_obs_state(
        self: SubarrayBeamComponentManager,
        timeout: float,
        task_abort_event: Optional[threading.Event] = None,
    ) -> TaskStatus:
        """
        Wait for sub-device ObsState to reach desired state.

        :param timeout: Time to wait, in seconds.
        :param task_abort_event: Check for abort, defaults to None

        :return: completed if status reached, FAILED if timed out, ABORTED if aborted
        """
        ticks = int(timeout / 0.01)  # 10 ms resolution
        while self._configuring_resources:
            if task_abort_event and task_abort_event.is_set():
                return TaskStatus.ABORTED
            time.sleep(0.01)
            ticks -= 1
            if ticks == 0:
                self.logger.warning(
                    f"Timed out waiting for ObsState {self._desired_state.name},"
                    f" resources {self._configuring_resources} "
                    f"failed to transition in {timeout} seconds. Attempting final poll."
                )
                return self._final_poll()
        self.logger.debug(
            f"Waited ObsState {self._desired_state.name} for"
            f" {(timeout-ticks*0.01):.3f} seconds"
        )
        return TaskStatus.COMPLETED

    def _device_obs_state_changed(
        self: SubarrayBeamComponentManager,
        trl: str,
        obs_state: ObsState,
    ) -> None:
        old_state = self._device_obs_states.get(trl, None)
        if old_state:
            old_name = old_state.name
        else:
            old_name = "NONE"
        new_state = ObsState(obs_state)
        self._device_obs_states[trl] = new_state
        if obs_state == self._desired_state and trl in self._configuring_resources:
            self._configuring_resources.remove(trl)
        self.logger.debug(
            f"ObsState for {trl} changed: {old_name}->{new_state.name}, "
            f"waiting for {len(self._configuring_resources)} more devices"
            f": {self._configuring_resources}"
        )

    def _device_obs_state_fault(
        self: SubarrayBeamComponentManager,
        trl: str,
        obs_state: ObsState,
    ) -> None:
        if obs_state != ObsState.FAULT:
            self.logger.warning(
                "_device_obs_state_fault called with"
                f" incorrect ObsState {trl} {obs_state.name}"
            )
            return
        self.logger.error(f"{trl} moved to FAULT.")
        self._device_obs_states[trl] = ObsState(obs_state)
        self._component_state_callback(obsfault=True)

    def _final_poll(self: SubarrayBeamComponentManager) -> TaskStatus:
        """
        Direct check of device obsstates as a last attempt before declaring failure.

        Sometimes it appears we miss change events in production, if for some reason
        we haven't received all the change events we expected, double check what we
        think the obsstate is, vs what it actually is on all subdevices that we need to.

        :returns: a taskstatus dependent on whether or not the device was actually in
            the correct state.
        """
        for trl in list(self._configuring_resources):
            device = self._station_beams.get(trl)
            if (
                device
                and device._proxy
                and device._proxy.obsstate == self._desired_state
            ):
                self.logger.info(
                    f"Change event for {trl} was missed, device was in correct state."
                )
                self._component_state_callback(missed_event=True)
                self._configuring_resources.remove(trl)

        return (
            TaskStatus.COMPLETED
            if not self._configuring_resources
            else TaskStatus.FAILED
        )

    def _delete_proxies(self) -> None:
        self.logger.debug("Deleting proxies")
        self._device_communication_states.clear()

        # Before we clear the dicts, we must make the the underlying object removes
        # it's subcriptions.
        for proxy in self._station_beams.values():
            if proxy._proxy is not None:
                proxy._proxy.unsubscribe_all_change_events()
        self._station_beams.clear()
