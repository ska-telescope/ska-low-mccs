{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://schema.skao.int/ska-low-mccs-subarray-configure/3.0",
  "title": "MccsSubarray Configure schema",
  "description": "Schema for MccsSubarray's Allocate command",
  "type": "object",
  "required": [
    "subarray_beams"
  ],
  "properties": {
    "interface": {
      "description": "The schema version which you expect to run against",
      "type": "string",
      "pattern": "^https?:\/\/.+\/.+\/[0-9]+.[0-9]+$"
    },
    "transaction_id": {
      "type": "string",
      "pattern": "[a-z0-9]+-[a-z0-9]+-[0-9]{8}-[0-9]+"
    },
    "subarray_beams": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "subarray_beam_id": {
            "description": "The ID of the subarray beam to which resources are allocated",
            "type": "integer",
            "minimum": 1,
            "maximum": 768
          },
          "update_rate": {
            "type": "number",
            "minimum": 0.0
          },
          "logical_bands": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "start_channel": {
                  "type": "integer",
                  "minimum": 2,
                  "maximum": 504,
                  "multipleOf": 2
                },
                "number_of_channels": {
                  "type": "integer",
                  "minimum": 8,
                  "maximum": 384,
                  "multipleOf": 8
                }
              },
              "required": [
                "start_channel",
                "number_of_channels"
              ]
            },
            "maxItems": 48
          },
          "apertures": {
            "type": "array",
            "items": {
              "description": "Station and aperture IDs contributing to the beam",
              "type": "object",
              "properties": {
                "aperture_id": {
                  "description": "Aperture ID, of the form APXXX.YY, XXX=station YY=substation",
                  "type": "string",
                  "pattern": "^AP(?!0{3})\\d{3}\\.\\d{2}$"
                },
                "weighting_key_ref": {
                  "description": "descriptive ID for the aperture weights in the aperture database",
                  "type": "string"
                }
              },
              "required": [
                "aperture_id"
              ]
            },
            "maxItems": 512
          },
          "sky_coordinates": {
            "description": "Pointed direction, including drift rate",
            "type": "object",
            "properties": {
              "timestamp": {
                "type": "string",
                "description": "UTC time for begin of drift"
              },
              "reference_frame": {
                "type": "string",
                "enum": [
                  "topocentric",
                  "AltAz",
                  "ICRS",
                  "Galactic"
                ]
              },
              "target_name": {
                "type": "string",
                "description": "The name of the target"
              },
              "c1": {
                "description": "first coordinate, RA or azimuth, in degrees",
                "type": "number",
                "minimum": 0.0,
                "maximum": 360.0
              },
              "c1_rate": {
                "description": "Drift rate for first coordinate, in degrees/second",
                "type": "number",
                "minimum": -0.016,
                "maximum": 0.016
              },
              "c2": {
                "description": "second coordinate, dec or elevation, in degrees",
                "type": "number",
                "minimum": -90.0,
                "maximum": 90.0
              },
              "c2_rate": {
                "description": "Drift rate for second coordinate, in degrees/second",
                "type": "number",
                "minimum": -0.016,
                "maximum": 0.016
              }
            },
            "required": [
              "reference_frame",
              "c1",
              "c2"
            ]
          },
          "field": {
            "description": "Pointed direction, including drift rate",
            "type": "object",
            "properties": {
              "timestamp": {
                "type": "string",
                "description": "UTC time for begin of drift"
              },
              "target_name": {
                "type": "string",
                "description": "The name of the target"
              },
              "reference_frame": {
                "type": "string",
                "enum": [
                  "AltAz",
                  "topocentric",
                  "ICRS",
                  "Galactic"
                ]
              },
              "attrs": {
                "description": "Coordinates and rates of scan",
                "type": "object",
                "properties": {
                  "c1": {
                    "description": "first coordinate, RA or azimuth, in degrees",
                    "type": "number",
                    "minimum": 0.0,
                    "maximum": 360.0
                  },
                  "c1_rate": {
                    "description": "Drift rate for first coordinate, in degrees/second",
                    "type": "number",
                    "minimum": -0.016,
                    "maximum": 0.016
                  },
                  "c2": {
                    "description": "second coordinate, dec or elevation, in degrees",
                    "type": "number",
                    "minimum": -90.0,
                    "maximum": 90.0
                  },
                  "c2_rate": {
                    "description": "Drift rate for second coordinate, in degrees/second",
                    "type": "number",
                    "minimum": -0.016,
                    "maximum": 0.016
                  }
                },
                "required": [
                  "c1",
                  "c2"
                ]
              }
            },
            "required": [
              "reference_frame",
              "attrs",
              "target_name"
            ]
          }
        },
        "required": [
          "logical_bands"
        ]
      },
      "maxItems": 48
    }
  }
}