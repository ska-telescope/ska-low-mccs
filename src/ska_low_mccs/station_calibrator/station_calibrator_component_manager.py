#  -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements component management for station calibrators."""
from __future__ import annotations

import json
import logging
import os
import threading
import time
from pathlib import Path
from typing import Any, Callable, Optional

import numpy as np
import tango
from ska_control_model import CommunicationStatus, PowerState, ResultCode
from ska_low_mccs_calibration.utils import read_yaml_antennas
from ska_low_mccs_common import EventSerialiser
from ska_low_mccs_common.component import DeviceComponentManager, MccsCommandProxy
from ska_tango_base.base import check_communicating
from ska_tango_base.executor import TaskExecutorComponentManager
from ska_telmodel.data import TMData  # type: ignore

from ..calibration_solver import CalibrationSolutionProduct

__all__ = ["StationCalibratorComponentManager", "NumpyEncoder"]


class NumpyEncoder(json.JSONEncoder):
    """Converts numpy types to JSON."""

    # pylint: disable=arguments-renamed
    def default(self: NumpyEncoder, obj: Any) -> Any:
        """
        Dereturn the encoded value.

        :param obj: the object.

        :returns: the encoded value.
        """
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def _convert_iteration_to_channel(iteration: int, offset: int, period: int) -> int:
    """
    Return a channel given an interation.

    :param iteration: the iteration in the loop
    :param offset: An offset to apply to an iteration,
        i.e first channel
    :param period: the modulus. i.e the difference between the
        first and last channel.

    :returns: An integer with the channel.
    """
    return iteration % period + offset


class _StationProxy(DeviceComponentManager):
    """A proxy to a station."""

    # pylint: disable=too-many-arguments
    def __init__(
        self: _StationProxy,
        name: str,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
        outside_temperature_changed_callback: Callable[[str, Any, Any], None],
        data_received_result_changed_callback: Callable[..., None],
        event_serialiser: Optional[EventSerialiser] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param name: the name of the device
        :param logger: the logger to be used by this object.
        :param component_state_callback: callback to be
            called when the component state changes
        :param communication_state_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param component_state_callback: callback to be
            called when the component state changes
        :param outside_temperature_changed_callback: callback to be called when a
            Station outsideTemperature change event is received
        :param data_received_result_changed_callback: callback to be called when a
            Station dataReceivedResult change event is received
        :param event_serialiser: the event serialiser to be used by this object.
        """
        self._outside_temperature_changed_callback = (
            outside_temperature_changed_callback
        )
        self._data_received_result_changed_callback = (
            data_received_result_changed_callback
        )

        super().__init__(
            name,
            logger,
            communication_state_callback,
            component_state_callback,
            event_serialiser=event_serialiser,
        )

    def get_change_event_callbacks(self) -> dict[str, Callable]:
        return {
            **super().get_change_event_callbacks(),
            "dataReceivedResult": self._data_received_result_changed_callback,
            "outsideTemperature": self._outside_temperature_changed_callback,
        }


# pylint: disable=too-many-instance-attributes
class StationCalibratorComponentManager(TaskExecutorComponentManager):
    """A component manager for a station calibrator."""

    def __init__(  # pylint: disable=too-many-arguments
        self: StationCalibratorComponentManager,
        logger: logging.Logger,
        station_name: str,
        station_id: int,
        calibration_store_name: str,
        calibration_solver_trl: str,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
        event_serialiser: Optional[EventSerialiser] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param logger: the logger to be used by this object.
        :param station_name: the name of this calibrator's station
        :param station_id: the id of the station.
        :param calibration_store_name: the name of this calibrator's calibration store
        :param calibration_solver_trl: the trl of the solver device.
        :param communication_state_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes.
        :param component_state_callback: callback to be called when the
            component state changes.
        :param event_serialiser: the event serialiser to be used by this object.
        """
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=None,
            fault=None,
        )
        self._event_serialiser = event_serialiser
        self._device_communication_state_lock = threading.Lock()
        self._communication_states = {
            name: CommunicationStatus.DISABLED
            for name in [station_name, calibration_store_name]
        }

        self._component_state_callback: Callable[..., None] = component_state_callback

        self._station_name = station_name
        self._station_id = station_id
        self._station_proxy = _StationProxy(
            station_name,
            logger,
            self._station_communication_state_changed,
            self._station_state_changed,
            self._station_outside_temperature_changed,
            self._station_data_received_result_changed,
            event_serialiser=self._event_serialiser,
        )

        self._calibration_store_name = calibration_store_name
        self._calibration_store_proxy = DeviceComponentManager(
            calibration_store_name,
            logger,
            self._calibration_store_communication_state_changed,
            self._calibration_store_state_changed,
            event_serialiser=self._event_serialiser,
        )
        self.calibration_solver_state = None

        self._station_calibration_solver_name = calibration_solver_trl
        self._station_calibration_solver_proxy = DeviceComponentManager(
            calibration_solver_trl,
            logger,
            self._calibration_solver_communication_changed,
            self._calibration_solver_state_changed,
            event_serialiser=self._event_serialiser,
        )

        self._outside_temperature: Optional[float] = None
        self._data_received_result: Optional[tuple[str, str]] = None
        self.logger = logger
        self._calibration_loop_thread = threading.Thread(target=self._calibration_loop)
        self._calibration_loop_running = False
        self._data_received_event = threading.Event()
        self._data_timeout = 30

    def start_communicating(self: StationCalibratorComponentManager) -> None:
        """Establish communication with the StationCalibrator components."""
        if self.communication_state == CommunicationStatus.ESTABLISHED:
            return
        self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)

        self._station_proxy.start_communicating()
        self._calibration_store_proxy.start_communicating()
        self._station_calibration_solver_proxy.start_communicating()

    def stop_communicating(self: StationCalibratorComponentManager) -> None:
        """Break off communication with the StationCalibrator components."""
        if self.communication_state == CommunicationStatus.DISABLED:
            return

        self._update_communication_state(CommunicationStatus.DISABLED)
        self._update_component_state(power=None, fault=None)

    def _station_state_changed(
        self: StationCalibratorComponentManager, **kwargs: Any
    ) -> None:
        self._component_state_callback(**kwargs, device_name=self._station_name)

    def _calibration_store_state_changed(
        self: StationCalibratorComponentManager, **kwargs: Any
    ) -> None:
        self._component_state_callback(
            **kwargs, device_name=self._calibration_store_name
        )

    def _calibration_solver_state_changed(
        self: StationCalibratorComponentManager, **kwargs: Any
    ) -> None:
        self.logger.info(f"Calibration solver changed state {kwargs}")
        if kwargs.get("power") is not None:
            self.calibration_solver_state = kwargs["power"]

    def _calibration_solver_communication_changed(
        self: StationCalibratorComponentManager,
        communication_state: CommunicationStatus,
    ) -> None:
        self.logger.info(
            "Communication state of calibration solver "
            f"transitioned to {communication_state}"
        )

    def _station_communication_state_changed(
        self: StationCalibratorComponentManager,
        communication_state: CommunicationStatus,
    ) -> None:
        # Only update state on change.
        if communication_state != self._communication_state:
            self._device_communication_state_changed(
                self._station_name, communication_state
            )

    def _calibration_store_communication_state_changed(
        self: StationCalibratorComponentManager,
        communication_state: CommunicationStatus,
    ) -> None:
        if communication_state != self._communication_state:
            self._device_communication_state_changed(
                self._calibration_store_name, communication_state
            )

    def _device_communication_state_changed(
        self: StationCalibratorComponentManager,
        fqdn: str,
        communication_state: CommunicationStatus,
    ) -> None:
        # Many callback threads could be hitting this method at the same time, so it's
        # possible (likely) that the GIL will suspend a thread between checking if it
        # need to update, and actually updating. This leads to callbacks appearing out
        # of order, which breaks tests. Therefore we need to serialise access.
        with self._device_communication_state_lock:
            self._communication_states[fqdn] = communication_state

            if self.communication_state == CommunicationStatus.DISABLED:
                return

            if CommunicationStatus.DISABLED in self._communication_states.values():
                self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
            elif (
                CommunicationStatus.NOT_ESTABLISHED
                in self._communication_states.values()
            ):
                self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
            else:
                self._update_communication_state(CommunicationStatus.ESTABLISHED)

    def _station_outside_temperature_changed(
        self: StationCalibratorComponentManager,
        attr_name: str,
        attr_value: Any,
        attr_quality: tango.AttrQuality,
    ) -> None:
        self.logger.info(
            "Outside temperature changed from "
            f"{self._outside_temperature} to {attr_value}"
        )
        self._outside_temperature = attr_value

    def _station_data_received_result_changed(
        self: StationCalibratorComponentManager,
        attr_name: str,
        attr_value: Any,
        attr_quality: tango.AttrQuality,
    ) -> None:
        self._data_received_result = attr_value
        if (
            self._calibration_loop_running
            and self._data_received_result is not None
            and self._data_received_result[0] == "correlator"
        ):
            self._data_received_event.set()

    @check_communicating
    def get_calibration(
        self: StationCalibratorComponentManager,
        channel: int,
    ) -> list[float]:
        """
        Get a calibration from the calibration store.

        :param channel: the frequency channel to calibrate for

        :raises ValueError: if the outside temperature has not been read yet
        :return: A list of calibration values
        """
        assert self._calibration_store_proxy._proxy is not None
        if self._outside_temperature is None:
            self.logger.error("GetCalibration failed - outside temperature is None")
            raise ValueError("Outside temperature has not been read yet")
        return self._calibration_store_proxy._proxy.GetSolution(
            json.dumps(
                {
                    "frequency_channel": channel,
                    "outside_temperature": self._outside_temperature,
                    "station_id": self._station_id,
                }
            )
        )

    @check_communicating
    def start_calibration_loop(
        self: StationCalibratorComponentManager, kwargs: dict[str, Any]
    ) -> tuple[list[ResultCode], list[str | None]]:
        """
        Start the calibration loop.

        :param kwargs: kwargs to define calibration.

        :return: A tuple containing a return code and a string message
            indicating status. The message is for information
            purpose only.
        """
        if self._calibration_loop_thread.is_alive():
            if self._calibration_loop_running:
                message = (
                    "Calibration loop already in progress. "
                    "Please StopCalibrationLoop before starting anew."
                )
            else:
                message = "Currently waiting on termination of the last loop."
            return [ResultCode.REJECTED], [message]

        if self.calibration_solver_state != PowerState.ON:
            message = (
                "Rejecting: Cluster service `low-mccs/solver/solver` "
                "not present or `ON`."
            )
            self.logger.error(message)
            return [ResultCode.REJECTED], [message]

        self._calibration_loop_running = True
        self._calibration_loop_thread = threading.Thread(
            target=self._calibration_loop, kwargs=kwargs
        )
        self._calibration_loop_thread.start()
        return [ResultCode.OK], ["StartCalibrationLoop completed."]

    def stop_calibration_loop(
        self: StationCalibratorComponentManager,
    ) -> tuple[list[ResultCode], list[str | None]]:
        """
        Stop the calibration loop from running.

        :return: A tuple containing a return code and a string message
            indicating status. The message is for information
            purpose only.
        """
        if self._calibration_loop_thread.is_alive():
            self._calibration_loop_running = False
            return [ResultCode.OK], ["Calibration loop will be stopped"]
        return [ResultCode.REJECTED], ["Calibration loop not running"]

    # pylint: disable=too-many-locals, too-many-statements
    def _calibration_loop(
        self: StationCalibratorComponentManager, **kwargs: Any
    ) -> None:
        """
        Calibrate over all frequencies indefinitely.

        :param kwargs: kwargs to define calibration.

        This runs when start_calibration_loop is called and stops when
        stop_calibration_loop is called.
        """
        assert self._station_calibration_solver_proxy._proxy is not None

        daq_config: dict[str, Any] = kwargs.pop("daq_config", {})

        configure_station_for_calibration_command = MccsCommandProxy(
            self._station_name, "ConfigureStationForCalibration", self.logger
        )
        result, message = configure_station_for_calibration_command(
            arg=json.dumps(daq_config), run_in_thread=False
        )
        if result != ResultCode.OK:
            self.logger.error(f"Failed to configure station for calibration: {message}")
            return

        # Sampling ADC clock 800Mhz, therefore nyquist 400MHz bandwidth
        # 512 channels after channeliser. Therefore, 50MHz is channel 64.
        first_channel: int = kwargs.pop("first_channel", 64)
        last_channel: int = kwargs.pop("last_channel", 448)
        iteration = 0
        while self._calibration_loop_running:
            channel = _convert_iteration_to_channel(
                iteration=iteration,
                offset=first_channel,
                period=last_channel - first_channel + 1,
            )
            try:
                assert (
                    self._station_proxy._proxy is not None
                ), "Station proxy is not connected."
                self._station_proxy._proxy.AcquireDataForCalibration(channel)
                event_received = self._data_received_event.wait(self._data_timeout)
                if not event_received:
                    timeout_error = (
                        "Failed to received calibration data "
                        f"in {self._data_timeout} seconds."
                    )
                    self.logger.error(timeout_error)
                    raise TimeoutError(timeout_error)

                self._data_received_event.clear()

                assert self._data_received_result is not None

                # Get filename from data received callback and strip root path
                # to make relative to mounted volume.
                data_file_name = json.loads(self._data_received_result[1])["file_name"]
                self.logger.info(f"File captured {data_file_name}")
                relative_file_path = data_file_name.split("/product/")[-1]
                kwargs["data_path"] = relative_file_path
                kwargs["solution_path"] = os.path.splitext(relative_file_path)[0]

                solve_command = MccsCommandProxy(
                    self._station_calibration_solver_name,
                    "Solve",
                    logger=self.logger,
                )
                result, message = solve_command(json.dumps(kwargs), run_in_thread=False)
                if result != ResultCode.OK:
                    self.logger.warning("Calibration solver Solve failed.")
                solution_location = os.path.splitext(data_file_name)[0] + ".h5"

                solution_file = Path(solution_location)
                if not solution_file.is_file():
                    self.logger.warning(
                        f"No solution found for {channel=}, skipping..."
                    )
                    continue

                assert (
                    self._calibration_store_proxy._proxy is not None
                ), "Calibration store is not connected."

                solve_product = CalibrationSolutionProduct(structure_version="1.0")
                solve_product.load_from_hdf5(solution_location)

                # Store in the database as a tpm indexed solution.
                tpm_based_solution = self._sort_by_tpm_base(
                    station_config_path=kwargs["station_config_path"],
                    eep_based_solution=solve_product.solution,
                )

                self._calibration_store_proxy._proxy.StoreSolution(
                    json.dumps(
                        {
                            "outside_temperature": self._outside_temperature,
                            "frequency_channel": solve_product.frequency_channel,
                            "solution": tpm_based_solution,
                            "corrcoeff": solve_product.corrcoeff,
                            "residual_max": solve_product.residual_max,
                            "residual_std": solve_product.residual_std,
                            "xy_phase": solve_product.xy_phase,
                            "n_masked_initial": solve_product.n_masked_initial,
                            "n_masked_final": solve_product.n_masked_final,
                            "lst": solve_product.lst,
                            "galactic_centre_elevation": (
                                solve_product.galactic_centre_elevation
                            ),
                            "sun_elevation": solve_product.sun_elevation,
                            "sun_adjustment_factor": (
                                solve_product.sun_adjustment_factor
                            ),
                            "masked_antennas": solve_product.masked_antennas,
                            "acquisition_time": solve_product.acquisition_time,
                            "station_id": solve_product.station_id,
                            "calibration_path": os.path.splitext(data_file_name)[0],
                        },
                        cls=NumpyEncoder,
                    )
                )

            except tango.DevFailed as df:
                self.logger.error(f"DevFailed raised: {repr(df)}")
                self.logger.error(f"Skipping {channel=}")
                time.sleep(2)
            except TimeoutError as te:
                self.logger.error(f"TimeoutError raised: {repr(te)}")
                self.logger.error(f"Skipping {channel=}")
            finally:
                iteration += 1

    def _sort_by_tpm_base(
        self: StationCalibratorComponentManager,
        station_config_path: tuple[str, str],
        eep_based_solution: np.ndarray,
    ) -> np.ndarray:
        """
        Sort solution by tpm base.

        :param station_config_path: configuration of the station.
        :param eep_based_solution: the solution ordered by eep.

        :return: the tpm indexed solution.
        """
        # Get station configuration from TelModel using the station_config_path
        platform_config = TMData([station_config_path[0]])[
            station_config_path[1]
        ].get_dict()
        station_name = list(find_by_key(platform_config, "stations"))[0]
        station_config = find_by_key(
            platform_config["platform"]["stations"], station_name
        )
        antidx_yaml2data, _, _, _ = read_yaml_antennas(station_config)

        # Inverse of antidx_yaml2data
        data2antidx_yaml = np.array(
            [
                (np.where(antidx_yaml2data == k)[0][0])
                for k in range(len(antidx_yaml2data))
            ],
            dtype=int,
        )

        eep_based_solution = eep_based_solution.reshape(-1, 8)
        tpm_based_solution = eep_based_solution[data2antidx_yaml]
        return tpm_based_solution.flatten()


def wait_for_lrc_completion(
    device_proxy: tango.DeviceProxy,
    command_id: str,
    timeout: int = 300,
) -> bool:
    """
    Wait for the finish of the LongRunningCommand.

    :param device_proxy: A `tango.DeviceProxy`.
    :param command_id: The unique_id returned from
        submission of a `SubmittedSlowCommand`.
    :param timeout: the maximum time to wait for the LRC
        endpoint.

    :return: True is LongRunningCommand reported `COMPLETED`
        in time, else False.
    """
    initial_time = time.time()

    final_tile = initial_time + timeout
    command_status = None
    tick = 5
    while time.time() < final_tile:
        command_status = device_proxy.CheckLongRunningCommandStatus(command_id)
        if command_status in ["COMPLETED", "FAILED", "ABORTED", "REJECTED"]:
            break
        time.sleep(tick)

    if command_status == "COMPLETED":
        return True
    return False


def find_by_key(data: dict, target: str) -> Any:
    """
    Search nested dict breadth-first for the first target key and return its value.

    This method is used to find station and antenna config within Low platform spec
    files, and should eventually be replaced by functions specifically designed to
    parse these files, aware of schema versions, etc, probably within ska-telmodel.

    :param data: generic nested dictionary to traverse through.
    :param target: key to find the first value of.

    :returns: the next value for given key.
    """
    bfs_queue = list(data.items())
    while bfs_queue:
        key, value = bfs_queue.pop(0)
        if key == target:
            return value
        if isinstance(value, dict):
            bfs_queue.extend(value.items())
    return None
