#  -*- coding: utf-8 -*
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements the component management for a calibration store."""
from __future__ import annotations

import logging
from typing import Any, Callable, Optional

from ska_control_model import CommunicationStatus, PowerState, ResultCode
from ska_tango_base.executor import TaskExecutorComponentManager

from .calibration_store_database_connection import CalibrationStoreDatabaseConnection
from .selection_policy.selection_manager import (
    SELECTION_POLICIES,
    SelectionManager,
    SelectionPolicyType,
)

__all__ = ["CalibrationStoreComponentManager"]

DevVarLongStringArrayType = tuple[list[ResultCode], list[str]]


# pylint: disable-next=abstract-method
class CalibrationStoreComponentManager(TaskExecutorComponentManager):
    """A component manager for MccsCalibrationStore."""

    # pylint: disable=too-many-arguments
    def __init__(
        self: CalibrationStoreComponentManager,
        logger: logging.Logger,
        communication_state_changed_callback: Callable[[CommunicationStatus], None],
        component_state_changed_callback: Callable[
            [Optional[bool], Optional[PowerState]], None
        ],
        database_host: str,
        database_port: int,
        database_name: str,
        database_admin_user: str,
        database_admin_password: str,
        policy_name: str,
        policy_frequency_tolerance: int,
        policy_temperature_tolerance: float,
    ) -> None:
        """
        Initialise a new instance.

        :param logger: a logger for this object to use
        :param component_state_changed_callback: callback to be
            called when the component state changes
        :param communication_state_changed_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param database_host: the database host
        :param database_port: the database port
        :param database_name: the database name
        :param database_admin_user: the database admin user
        :param database_admin_password: the database admin password
        :param policy_name: The policy type to use.
            Currently ["preferred", closest_in_range"] supported.
        :param policy_frequency_tolerance: a configuration parameter that
            may be used by policy [channel]
        :param policy_temperature_tolerance: a configuration parameter that
            may be used by policy [Degrees]

        :raises ValueError: When the SelectionPolicy configuration is
            incorrect from deployment.
        """
        self._selection_manager = SelectionManager(logger=logger)
        super().__init__(
            logger,
            communication_state_changed_callback,
            component_state_changed_callback,
            power=None,
            fault=None,
        )
        self.logger = logger
        self._database_connection = self.create_database_connection(
            logger,
            communication_state_changed_callback,
            database_host,
            database_port,
            database_name,
            database_admin_user,
            database_admin_password,
        )
        [result_code], [message] = self.set_selection_policy(
            policy_name=policy_name,
            frequency_tolerance=policy_frequency_tolerance,
            temperature_tolerance=policy_temperature_tolerance,
        )
        if result_code != ResultCode.OK:
            # Fail early on configuration issues.
            raise ValueError(
                "Unable to define selectionPolicy with "
                f"properties supplied {message}. "
                "Values supplied from deployment is:"
                f"{policy_name=}, "
                f"{policy_frequency_tolerance=}, "
                f"{policy_frequency_tolerance=}!"
            )

    # pylint: disable=too-many-arguments
    def create_database_connection(
        self: CalibrationStoreComponentManager,
        logger: logging.Logger,
        communication_state_changed_callback: Callable[[CommunicationStatus], None],
        database_host: str,
        database_port: int,
        database_name: str,
        database_admin_user: str,
        database_admin_password: str,
    ) -> CalibrationStoreDatabaseConnection:
        """
        Create the database connection object.

        :param logger: a logger for the connection to use
        :param communication_state_changed_callback: callback to be
            called when the component state changes
        :param database_host: the database host
        :param database_port: the database port
        :param database_name: the database name
        :param database_admin_user: the database admin user
        :param database_admin_password: the database admin password
        :return: the database connection
        """

        return CalibrationStoreDatabaseConnection(
            logger,
            communication_state_changed_callback,
            database_host,
            database_port,
            database_name,
            database_admin_user,
            database_admin_password,
            self._selection_manager,
        )

    def start_communicating(self: CalibrationStoreComponentManager) -> None:
        """Establish communication."""
        if self.communication_state == CommunicationStatus.ESTABLISHED:
            return
        self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)

        self._database_connection.verify_database_connection()

    def stop_communicating(self: CalibrationStoreComponentManager) -> None:
        """Break off communication."""
        if self.communication_state == CommunicationStatus.DISABLED:
            return

        self._update_communication_state(CommunicationStatus.DISABLED)
        self._update_component_state(power=None, fault=None)

    def get_solution(
        self: CalibrationStoreComponentManager, **kwargs: Any
    ) -> list[float]:
        """
        Get a solution for the provided frequency and outside temperature.

        This at present will return the most recently stored solution for the inputs.

        :param kwargs: kwargs to feed policy.

        :returns: the solution or an empty list

        :raises ValueError: when no SelectionPolicy is loaded.
        """
        try:
            if not self._selection_manager.has_policy:
                raise ValueError(
                    "No selection policy defined. Unable to select solution"
                )
            return self._database_connection.get_solution(**kwargs)
        except Exception as e:  # pylint: disable=broad-except
            self.logger.error(f"Failed to get_solution: {repr(e)}")
            return []

    def store_solution(
        self: CalibrationStoreComponentManager,
        solution: list[float],
        frequency_channel: int,
        outside_temperature: float,
        station_id: int,
        calibration_path: str,
        preferred: bool = False,
        **kwargs: Any,
    ) -> DevVarLongStringArrayType:
        """
        Store the provided solution in the database.

        :param solution: the solution to store.
        :param frequency_channel: the frequency channel that the solution is for
        :param outside_temperature: the outside temperature that the solution is for
        :param station_id: the id of the station to store this solution for.
        :param preferred: mark solution as preferred.
        :param calibration_path: the path to the correlation matrix used to
            get this solution.
        :param kwargs: any optional validated kwargs.

        :return: tuple of result code and message
        """
        message = (
            "storing solution for \n"
            f"{station_id=}, \n"
            f"{frequency_channel=}, \n"
            f"{outside_temperature=}, \n"
            f"{preferred=}, \n"
            f"{solution=}, \n"
            f"{calibration_path=}, \n"
            f"{kwargs}, \n"
        )
        self.logger.info(message)
        return self._database_connection.store_solution(
            solution=solution,
            frequency_channel=frequency_channel,
            outside_temperature=outside_temperature,
            station_id=station_id,
            calibration_path=calibration_path,
            preferred=preferred,
            **kwargs,
        )

    def get_policy(self: CalibrationStoreComponentManager) -> str:
        """
        Return the active SelectionPolicy.

        :returns: a JSON servialised dictionary with information
            in a format validated.
        """
        return self._selection_manager.policy

    def get_policy_description(self: CalibrationStoreComponentManager) -> str:
        """
        Return a description of the SelectionPolicy active.

        :returns: a string with information about the policy.
        """
        return self._selection_manager.policy_description

    def set_selection_policy(
        self: CalibrationStoreComponentManager, policy_name: str, **kwargs: Any
    ) -> tuple[list[ResultCode], list[str]]:
        """
        Set a new policy for selecting solutions.

        :param policy_name: The policy to use.
        :param kwargs: kwargs to pass to policy.

        kwargs are used to configure the SelectionPolicy,
        See details on the configure command for selected policy for
        details:
        https://developer.skao.int/projects/ska-low-mccs/en/latest/api/calibration_store/selection_policy/index.html

        :return: a tuple containing the ResultCode and a message.
        """
        try:
            # Load templated policy and substitute
            new_policy: SelectionPolicyType = SELECTION_POLICIES[policy_name](
                self.logger, **kwargs
            )
            self._selection_manager.update_policy(new_policy)
            return ([ResultCode.OK], ["Updated Selection policy"])
        except Exception as e:  # pylint: disable=broad-except
            return ([ResultCode.FAILED], [f"Failed to update Selection policy {e}"])
