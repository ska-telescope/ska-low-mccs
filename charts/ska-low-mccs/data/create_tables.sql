-- Script to create necessary table(s) for calibration database
CREATE TABLE tab_mccs_calib
(
    id serial NOT NULL PRIMARY KEY,                                -- Primary ID [serial]
    creation_time timestamp NOT NULL,                              -- Time of storage [PostgreSQL timestamptz]
    outside_temperature DOUBLE PRECISION NOT NULL,                 -- Outside Temperature [Degrees]
    frequency_channel smallint NOT NULL,                           -- Frequency channel [channel]
    station_id smallint NOT NULL,                                  -- The id of the station this solution belongs to [int]
    preferred boolean,                                             -- True to mark as preferred [bool]
    calibration_path text NOT NULL                                 -- The path to the correlation matrix file [str]
);

ALTER TABLE tab_mccs_calib 
ADD COLUMN acquisition_time FLOAT8 NOT NULL DEFAULT(0),           -- Time the data was acquired. [float]
ADD COLUMN solution FLOAT8[],                                     -- The solution dataset. This is ordered by tpm_idx (i.e 8 values for each tpm_id * 16 + (adc_channel // 2 )).
ADD COLUMN corrcoeff FLOAT8[],                                    -- List of Pearson product-moment correlation coefficients for each polarisation of the calibrated visibilities and the model.
ADD COLUMN residual_max FLOAT8[],                                 -- List of residual visibility maximum absolute deviation per polarisation (K). Length must be equal to 4.
ADD COLUMN residual_std FLOAT8[],                                 -- List of residual visibility standard deviation per polarisation (K). Length must be equal to 4.
ADD COLUMN xy_phase FLOAT8,                                       -- Estimated xy-phase error.
ADD COLUMN n_masked_initial INTEGER,                              -- Initial number of masked antennas.
ADD COLUMN n_masked_final INTEGER,                                -- Final number of masked antennas.
ADD COLUMN lst FLOAT8,                                            -- Apparent sidereal time at station.
ADD COLUMN galactic_centre_elevation FLOAT8,                      -- Elevation of the galactic centre.
ADD COLUMN sun_elevation FLOAT8,                                  -- Elevation of the sun (degrees).
ADD COLUMN sun_adjustment_factor FLOAT8,                          -- Number
ADD COLUMN masked_antennas TEXT[];                                -- Antenna masked as bad. This is currently eep indexed. Length must be between 0 and 256 (inclusive).
