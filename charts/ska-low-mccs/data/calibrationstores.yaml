{{- /*
Defines the deployment for MccsCalibrationStore Tango devices.

Currently all device properties are hardcoded except the logging level default,
so configure like this:


  daqDataRepository: daq-data-repository
  eepDataRepository: eep-data
  deviceServers:
    calibrationstores:
      s1-2:
        low-mccs/calibrationstore/s1-2:
          logging_level_default: 5
          device_properties: # Arbitrary properties.
            foo: bar


*/}}
{{- $calibration_stores := dict}}
{{- range $server_name, $this_server := (dig "calibrationstores" dict .Values.deviceServers)}}
{{- if $this_server}}
{{- $_ := set $calibration_stores $server_name $this_server}}
{{- end}}
{{- end}}
{{- if $calibration_stores}}
name: calibrationstore-{{.Release.Name}}
function: ska-low-mccs-calibrationstore-tango
domain: ska-low-mccs
instances:
{{- range $server_key := keys $calibration_stores}}
  - {{printf "calibrationstore-%s" $server_key}}
{{- end}}
command: mccs_device_server
server:
  name: mccs_device_server
  instances:
{{- range $server_key, $this_server := $calibration_stores}}
    - name: {{printf "calibrationstore-%s" $server_key}}
      classes:
        - name: "MccsCalibrationStore"
          devices:
{{- range $calibration_store_name, $this_calibration_store := $this_server}}
{{- if $this_calibration_store}}
            - name: {{$calibration_store_name}}
              properties:
                - name: "DatabaseHost"
                  values:
                    - "station-calibration-postgresql"
                - name: "DatabasePort"
                  values:
                    - "5432"
                - name: "DatabaseName"
                  values:
                    - "postgres"
                - name: "DatabaseAdminUser"
                  values:
                    - "postgres"
                - name: "DatabaseAdminPassword"
                  values:
                    - "secretpassword"
{{- with $this_calibration_store.logging_level_default}}
                - name: "LoggingLevelDefault"
                  values:
                    - {{quote .}}
{{- end}}
{{- range $property_name, $property_value := ($this_calibration_store | dig "device_properties" dict)}}
{{- if not (kindIs "slice" $property_value)}}
{{- $property_value = list $property_value}}
{{- end}}
                - name: {{$property_name | quote}}
                  values:
{{- range $value := $property_value}}
                  - {{quote $value}}
{{- end}}
{{- end}}
{{- end}}
{{- end}}
{{- end}}
depends_on:
  - device: sys/database/2
image:
{{- with $.Values.image}}
  registry: {{.registry | quote}}
  image: {{.name | quote}}
  tag: {{.tag | default $.Chart.AppVersion | quote}}
  pullPolicy: {{.pullPolicy | quote}}
{{- end}}
livenessProbe:
{{ toYaml $.Values.livenessProbe | indent 2 }}
readinessProbe:
{{ toYaml $.Values.readinessProbe | indent 2 }}
extraVolumes:
- name: daq-data
  persistentVolumeClaim: 
    claimName:  {{$.Values.daqDataRepository}}
extraVolumeMounts:
  - name: daq-data
    mountPath: /product
{{- end}}
